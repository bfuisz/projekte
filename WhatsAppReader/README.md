# README #

WhatsAppReader ist ein kleines Tool welches in der kurzen Zeit entstand, in der WhatsApp Nachrichten als gelesen 
markierte wurden und ich herausfand, dass es ein WhatsApp Widget gibt.
Dieses Tool liest mithilfe eines Accessibility Services Notifications aus der Actionbar aus und speichert 
sie in einer Datenbank.
Dadurch, dass nicht auf die NachrichtenActivity von Whatsapp fokussiert wird, werden beim Chatpartner keine
blauen "Häkchen" angezeigt. Um auf Nachrichten zu antworten muss allerdings immer noch WhatsApp verwendet werden.


### Aufbau ###

Eine Activity die mittels ListView und ResourceCursorAdapter Nachrichten darstellt. 
Außerdem ein AccessibilityService der die Nachrichten liest, indem er den Text einer
Notification ausliest. 
Die Persistenz Schicht wurde wieder mit dem content-provider-generator tool generiert und besteht aus einer 
Tabelle für die Nachrichten. 

## TODOs ##

+ Ignorierte Nachrichten werden mehrmals in der NotificationBar angezeigt und dadurch auch mehrmals abgespeichert. 



## Lerneffekt ##

AccessibilityService 