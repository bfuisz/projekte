<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateDevicesTable extends Migration {
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'devices', function ($table) {
			$table->bigIncrements ( 'id' );
			$table->bigInteger ( 'user_id' )->foreign ( 'user_id' )->references ( 'id' )->on ( 'users' );
			$table->string ( 'ip_adress' );
			$table->string ( 'mac_adress' )->unique ();
			$table->string ( 'clientgcm_id' );
			$table->softDeletes ();
			$table->timestamps();
				
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'devices' );
	}
}
