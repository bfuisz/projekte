<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>

	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
	    <script src="js/prefixfree.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="js/sorttable.js"></script>
</head>
<body>
	<div class="welcome">
	
	<table class="sortable">
	<thead>
	  <tr>
  		<th>ID</th>
		<th>ip_adress</th>
		<th>mac_adress</th>
	  </tr>
	</thead>
	<tbody>
	<?php
		foreach($devs as $dev){
			echo '<tr><td>' . $dev['id'] . '</td><td>' . $dev['ip_adress'] . '</td><td>' . $dev['mac_adress'] . '</td></tr>'; 
		}
	?>
	</tbody>
	</table>
	
	<table class="sortable">
	<thead>
	  <tr>
  		<th>ID</th>
		<th>message</th>
		<th>source</th>
		<th>desctination</th>
	  </tr>
	</thead>
	<tbody>
	<?php
		foreach($tActions as $tAction){
			echo '<tr><td>' . $tAction['id'] . '</td><td>' . $tAction['message'] . '</td><td>' . $tAction['source'] . '</td></tr><td>' . $tAction['destination'] . '</td></tr>'; 
		}
	?>
	</tbody>
	</table>
	</div>
</body>
</html>
