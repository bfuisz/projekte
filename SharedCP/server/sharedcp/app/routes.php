<?php

//http://laravel.io/forum/02-13-2014-i-can-not-get-inputs-from-a-putpatch-request
//PUT plus input not possible use POST instead

Route::get ( 'api/user/{id}', 'UserController@getUser' ); 
Route::put ( 'api/user/{id}/logout', 'UserController@logout' );  //no input necessary continue using put
Route::delete ( 'api/user/{id}/delete', 'UserController@delete' );
Route::post ( 'api/user/login', 'UserController@login' );
Route::post ( 'api/user/register', 'UserController@register' );

Route::get ( 'api/transaction/{id}', 'TransactionController@getTransaction' );
Route::post ( 'api/transaction/{id}/modify', 'TransactionController@modifyTransaction' );
Route::delete ( 'api/transaction/{id}/delete', 'TransactionController@deleteTransaction' );
Route::post('api/transaction', 'TransactionController@newTransaction' );

Route::get ( 'api/transaction/{id}/source', 'TransactionController@listSourceTransactions' ); //list of transactions with id (device) as source
Route::get ( 'api/transaction/{id}/destination', 'TransactionController@listDestinationTransactions' ); //list of transactions with id (device) as destination
Route::get ( 'api/transaction/{id}/user', 'TransactionController@listUserTransactions' ); //list of transaction by user 

Route::get ( 'api/device/{id}', 'DeviceController@listDevices' ); //list of devices for user id 
Route::get ( 'api/device', 'DeviceController@get' ); //get device 
Route::get ( 'api/device/{id}/user', 'DeviceController@listDevicesUser' ); //list of devices for user id 
Route::post ( 'api/device', 'DeviceController@addDevice' ); 
Route::post ( 'api/device/{id}/modify', 'DeviceController@modifyDevice' ); 
Route::delete ( 'api/device/{id}', 'DeviceController@deleteDevice' ); 

Route::get ( 'test', 'TransactionController@test' ); 



Route::get ( '/', 'SiteController@index' ); 
Route::get ( '/login', 'SiteController@login' ); 
Route::post ( '/login', 'SiteController@login' ); 
Route::get ( '/list', 'SiteController@listTransactions' ); 
