<?php
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class DeviceController extends BaseController {


	public function get(){
		try{
			$ip_adress = Input::get('ip_adress'); 
			$response = Device::where("ip_adress", "=", $ip_adress)->get(); 
		}catch(Exception $ex){
			Log::error($ex); 
			$response = array("status" => "failure"); 
		}
		return JsonResponse::create($response);
	}
	public function listDevices($id){
		try{
			$response = Device::where("id", "=", $id)->get(); 
		}catch(Exception $ex){
			Log::error($ex); 
			$response = array("status" => "failure"); 
		}
		return JsonResponse::create($response);
	}
	
	public function listDevicesUser($id){
		try{
			$response = Device::where("user_id", "=", $id)->get(); 
		}catch(Exception $ex){
			Log::error($ex); 
			$response = array("status" => "failure"); 
		}
		return JsonResponse::create($response);
	}
	
	public function addDevice(){
		$response = array("status" => "failure"); 
			Log::info(Input::all()); 

		if($this->checkInput(false)){
		
			$device = new Device(); 
			$device->user_id = Input::get("user_id"); 
			$device->ip_adress = Input::get("ip_adress");
			$device->clientgcm_id = Input::get("clientgcm_id");
			$device->mac_adress = Input::get("mac_adress");
			try{
				$device->save(); 
				return $device->toJson();
			}catch(Exception $ex){
				Log::error("Integrity constraint violation: because device can log in multiple times"); 
			}
		}
		return JsonResponse::create($response);
	}
	
	public function deleteDevice($id){
		//if(Utils::checkSession()){
			try{
				$rows = Device::destroy($id); 
				return JsonResponse::create(array("status" => "okay", "rows" => $rows));
			}catch(Exception $ex){
				Log::error($ex); 
				$response = array("status" => "failure"); 
			}
		//}
		return JsonResponse::create($response);
	}
	
	public function modifyDevice($id){
		//if(Utils::checkSession()){
			try{
				$dev = Device::find($id); 
				if(Input::has('ip_adress')){//only ip_adress for now
					$dev->ip_adress = Input::get("ip_adress");
					$dev->save(); 
					return $dev->toJson();
				}
				
			}catch(Exception $ex){
				Log::error($ex); 
				$response = array("status" => "failure", 'reason' => $ex); 
			}
		//}
		return JsonResponse::create($response);
	}
	
	public function checkInput($req_session){
		if(Input::has('user_id') && Input::has('ip_adress') && Input::has('mac_adress')){
			if($req_session){
				return Utils::checkSession(); 
			}else{
				return true;
			}				
		}else{
			return false; 
		}
	}
}
