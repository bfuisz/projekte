<?php


use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

// Response::json($array('name'=>'Steve'));
class UserController extends BaseController {
	private $secret = '970814klbjsdv098723kbvsdf723';
	
	public function showWelcome() {
		return View::make ( 'hello' );
	}
	
	public function getUser($id) {
		try{
			$response = User::find($id); 
			return $response->toJson(); 
		}catch(Exception $ex){
			Log::error($ex); 
			$response = array("status" => "failure"); 
		}
		return JsonResponse::create($response);
	}
	
	public function login() {
		$response = Utils::checkCredentials(); 
		if (isset($response['status']) && $response['status'] == 'success') {
			try{
				$user = User::where("email", "=", Input::get("email"))->firstOrFail(); 

				if (strcmp(Crypt::decrypt($user->password) , Input::get('password')) == 0){

					$session_key = 	Hash::make($user->email . $user->password);
					Session::put('session_key', $session_key);
					Session::put('user_id', $user->id);
										
					$user->password = "";
					$user->session_key = $session_key;

					return $user->toJson();
				}
			}catch(Exception $ex){
				Log::error($ex);
				$response['status'] = 'error';
				$response['reason'] = 'wrong credentials';
			}
		}
		return JsonResponse::create($response);
	}
	
	public function logout($id) {
		Session::flush();
		$response['status'] = 'success'; 
		return JsonResponse::create($response);
	}
	
	public function register() {
		$response = Utils::checkCredentials(); 
		
		if (isset($response['status']) && $response['status'] == 'success') {

			try {
				$user = new User(); 
				$user->email = Input::get ( 'email' ); 
				$user->password = Crypt::encrypt(Input::get('password'));
				$user->save(); 	

				$session_key = Hash::make($user->email . $user->password); 
				Session::put('user_id', $user->id);
				Session::put('session_key', $session_key);

				$user->password = ""; 
				$user->session_key = $session_key; 						
				return $user->toJson();
				
			} catch (Exception $ex){
    			Log::error($ex);
				$response['status'] = 'error';
				$response['reason'] = 'duplicate entry'; 
			}
			
		}
		
		return JsonResponse::create($response);
	}
	
	public function delete($id) {
		try{
			$rows = User::destroy($id); 
			return JsonResponse::create(array("status" => "success", "rows"=>$rows));
		}catch(Exception $ex){
			Log::error($ex);
			$response['status'] = 'error';
			$response['reason'] = 'duplicate entry'; 
		}
			return JsonResponse::create($response);
	}
	

}
