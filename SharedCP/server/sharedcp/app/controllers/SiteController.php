<?php

class SiteController extends BaseController {


	public function index()
	{
		if(Session::has('user_id')){
			$devs = Device::where('user_id', '=', Session::get('user_id'))->get()->toArray(); 
			$tActions = Transaction::where('user_id', '=', Session::get('user_id'))->get()->toArray(); 
			return View::make('list')->with(array('tActions'=> $tActions, 'devs' => $devs));
		}else{
			return View::make('login');
		}
	}
	
	public function login()
	{
		if(Input::has('email') && Input::has('password')){
			$controller = new UserController(); 
			$response = $controller-> login(); 
			$devs = Device::where('user_id', '=', Session::get('user_id'))->get()->toArray(); 
			$tActions = Transaction::where('user_id', '=', Session::get('user_id'))->get()->toArray(); 
			return View::make('list')->with(array('tActions'=> $tActions, 'devs' => $devs));
		}else{
			return View::make('login');
		}
	}

	
	public function listTransactions()
	{
		if(Session::has('user_id')){
			return View::make('list');
		}else{
			return View::make('login');
		}
	}
}
