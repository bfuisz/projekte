<?php
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
class TransactionController extends BaseController {
	protected $apiKey = 'AIzaSyCKdCA6VeG2cBXM3ncaGHXSPeFcdKsK--8';
	
	public function getTransaction($id) {
		try {
			$tAction = Transaction::findOrFail($id);
			return $tAction->toJson();

		} catch ( Exception $ex ) {
			Log::error($ex); 
				$response = array("status"=>"error",'reason' => $ex ); 	

		}
		return JsonResponse::create ( $response );
	}
	
	
	public function modifyTransaction($id){
		//if(Utils::checkSession()){
			try{
				$tAction = Transaction::find($id); 
				var_dump(Input::all());
			
				if(Input::has('destination')){ 
					$tAction->destination = Input::get("destination");
					$tAction->save(); 
					return $tAction->toJson();
				}else{
				$response = array("status" => "error",'reason' => "modify only when destination present" ); 	
				}
				}catch(Exception $ex){
				Log::error($ex); 
				$response = array("status"=>"error",'reason' => $ex ); 	
			}
		//}
		return JsonResponse::create($response);
	}
	
	public function deleteTransaction($id){
		//if(Utils::checkSession()){
			try{
				$user = Transaction::where('id', '=', $id)->get(array('user_id'))->toArray(); 
				$rows = Transaction::destroy($id); 
				$response = json_encode(array("status" => "deleted", "message" => $id)); 
				Log::info($response); 
				if($rows > 0){
					$this->propagateMessage($user[0], $response); 
				}
				return JsonResponse::create(array("status" => "deleted", "message" => $rows));
			}catch(Exception $ex){
				Log::error($ex); 
				$response = array("status"=>"error",'message' => $ex ); 	
			}
		//}
		return JsonResponse::create($response);
	}
	
	public function newTransaction() {
		try {
			$tAction = new Transaction ();
			$tAction->id = Input::get('id'); 
			$tAction->user_id = Input::get ( 'user_id' );
			$tAction->message = Input::get ( 'message' );
			$tAction->source = Input::get ( 'source' );
			Log::info($tAction); 
			$response = $tAction->save();
			if($response){
				$this->propagateMessage($tAction->user_id, $tAction->toJson()); 
				return $tAction->toJson(); 
			}else{
				$response = array("status"=>"error",'message' => 'error while inserting' ); 	
			}
		} catch ( Exception $ex ) {
			Log::error ( $ex );
			$response = array("status"=>"error",'message' => 'error while inserting' ); 	
		}
		return JsonResponse::create($response);
	}
	
	public function test(){
	}
	
	public function prepareResults($user_id){
		$devices = Device::where('user_id', '=',$user_id)->get(array('clientgcm_id'))->toArray();
		$new_array = array(); 
		foreach($devices as $value) { 
			foreach($value as $tmp) {
				$new_array[] = $tmp; 
			}
		}
		return $new_array; 
	}
	
	public function propagateMessage($user_id, $message){
		$devices = $this->prepareResults($user_id);
		$gcpm = new GCMPushMessage($this->apiKey);
		$gcpm->setDevices($devices);
		$response = $gcpm->send($message, array('title' => 'Server Message'));
		Log::info($response); 
	}
	
	public function listSourceTransactions($source) {
		try {
			$response = Transaction::where ( 'source', '=', $source )->get ();
			return $response->toJson(); 
		} catch ( Exception $ex ) {
			Log::error ( $ex );
			$response = array("status"=>"error",'message' => $ex ); 	
		}
		return JsonResponse::create ( $response );
	}
	
	public function listDestinationTransactions($destination) {
		try {
			$response = Transaction::where ( 'destination', '=', $destination )->get ();
			return $response->toJson(); 
		} catch ( Exception $ex ) {
			Log::error ( $ex );
			$response = array("status"=>"error",'message' => $ex )	; 

		}
		return JsonResponse::create ( $response );
	}
	
	public function listUserTransactions($user_id) {

		try {
			$response  = Transaction::where ( 'user_id', '=', $user_id )->get ();
			return $response->toJson(); 
		} catch ( Exception $ex ) {
			Log::error($ex); 
			$response ["status"] = "error";
			$response ["message"] = $ex;
		}
		return JsonResponse::create ( $response );
	}

	/*
	public function checkInput($req_session){
		if(Input::has('user_id') && Input::has('source') && Input::has('device_id')){
			if($req_session
		}
		
	
	}
	*/

}
