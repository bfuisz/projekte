<?php


use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

// Response::json($array('name'=>'Steve'));
class Utils extends BaseController {

	public static function checkCredentials(){
		$response = array();
		$response['status'] = 'success';
		
		if(!Input::has ( 'email' )){
			$response['status'] = 'error';
			$reason = 'no email adress';
		}
			
		if(!Input::has ( 'password' ) && strlen(Input::get('password')) >= 6){
			$response['status'] = 'error';
			$reason = 'no password';
		}
		return $response; 
	}
	
	public static function saveDevice($user_id){
		if(Input::has('ip_adress') && Input::has('mac_adress')){
			$device = new Device(); 
			$device->user_id = $user_id; 
			$device->ip_adress = Input::get("ip_adress");
			$device->mac_adress = Input::get("mac_adress");
			$device->save(); 
		}
	}
	
	public static function checkSession(){
		if(Session::has('session_key') && Request::has('session_key')){
			return strcmp(Session::get('session_key'), Input::get('session_key')); 
		}
	}
}
