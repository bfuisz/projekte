# README #

Ein kleines Tool, welches den Inhalt des Clipboards �ber einen Server und GCM zu anderen Ger�ten synchronisiert. 
Neben dem WhatsAppReader ist diese App eine der wenigen die ein "Real World Problem" f�r mich l�st.
Das Problem ist, dass in meinem Freundeskreis der Gro�teil WhatsApp verwendet. 
Da WhatsApp eine Telefonnummer ben�tigt, kann ich es nur auf einem meiner drei Android Ger�te installieren. 
Da ich allerdings zum Internet Surfen haupts�chlich mein Tablet (ohne WhatsApp) verwende, ist es oft umst�ndlich 
zum Beispiel Artikel via WhatsApp zu teilen. 
Mit SharedCP kann ich zum Beispiel den Link kopieren, den Zwischenspeicher synchronisieren und am Handy via WhatsApp 
verschicken.
Das Problem w�re einfacher zu l�sen indem wir Google Hangouts verwenden w�rden...  

# App #

## Aufbau Client ##

Die App besteht im gro�en und ganzen aus einer Activity (MainActivity) und einer Datenbank. 
Die Datenbank umfasst 3 Tabellen Device, Transaction und User. 
Die interessanteste der drei Tabellen ist die Transaction Tabelle. 
In der Tabelle werden die Nachrichten zwischen Client und Server gespeichert. 

Den Gro�teil der App stellen jedoch die Services dar. 
Der ClipboardSynchronizeService f�gt dem ClipboardManager einen neuen OnPrimaryClipChangedListener
hinzu. Sobald etwas kopiert wird, schickt die App den Inhalt des Clipboards zum Server welcher 
die Nachricht zu allen angemeldeten Ger�ten. 
Ein im Server registriertes Ger�t bekommt �ber den GcmIntentService die Nachricht vom Server und speichert diese. 

## Aufbau Server ##

Der Server besteht aus einem HTTP-Server der auf einem Raspberry Pi l�uft und wurde mit dem php Framework 
Laravel umgesetzt. 
Clients melden sich beim Server an, der Server speichert das Ger�t mit GCM RegisterID.
Bei einer eingehenden Nachricht broadcastet der Server die Nachricht an alle Ger�te in seiner Datenbank. 

## TODOs ##

Die App ist nicht wirklich ausgereift, da ich sobald das Ziel erreicht war aufgeh�rt habe diese weiterzuentwickeln. 
Zum Beispiel war urspr�nglich ein Benutzersystem angedacht, da ich die App aber als einziger Nutze(nutzen werde), wurde 
diese Idee nicht weiterverfolgt. 

## Lerneffekt ##

Client Server, Google Cloud Messaging. 