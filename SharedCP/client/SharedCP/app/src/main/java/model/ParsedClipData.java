package model;

/**
 * Created by benedikt.
 */
public class ParsedClipData {
    private String type;
    private String data;

    public ParsedClipData() {
    }

    public ParsedClipData(String type, String data) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static ParsedClipData fromString(String data) {
        ParsedClipData pData = new ParsedClipData();
        int index1 = data.indexOf("{");
        int index2 = data.indexOf("{", index1 + 1);
        if (index1 > 0 && index2 > 0) {
            pData.type = data.substring(index1 + 1, index2).trim();
            index1 = data.indexOf("H:");
            if (index1 > 0) {
                pData.data = data.substring(index1 + 2, data.length()).trim();
            }
        }

        return pData;
    }

    @Override
    public String toString() {
        return "ParsedClipData{" +
                "type='" + type + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
