package model;

import android.content.ContentValues;
import android.database.Cursor;

import persistency.contracts.DeviceContract;

/**
 * Created by benedikt.
 */
public class Device {

    private long id;
    private long user_id;
    private String ip_adress;
    private String clientgcm_id;
    private String mac_adress;
    private String deleted_at;
    private String created_at;
    private String updated_at;

    public Device() {
    }

    public Device(long id, long user_id, String ipAdress, String macAdress, String deleted_at, String created_at, String updated_at) {
        this.id = id;
        this.user_id = user_id;
        this.ip_adress = ipAdress;
        this.mac_adress = macAdress;
        this.deleted_at = deleted_at;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }


    public ContentValues values() {
        ContentValues values = new ContentValues();

        if (this.id > 0) {
            values.put(DeviceContract.DeviceEntry._ID, this.id);
        }
        values.put(DeviceContract.DeviceEntry.COLUMN_USER_ID, this.user_id);

        values.put(DeviceContract.DeviceEntry.COLUMN_IP_ADRESS, this.ip_adress);
        values.put(DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS, this.mac_adress);
        values.put(DeviceContract.DeviceEntry.COLUMN_DELETED_AT, this.deleted_at);
        values.put(DeviceContract.DeviceEntry.COLUMN_CREATED_AT, this.created_at);
        values.put(DeviceContract.DeviceEntry.COLUMN_UPDATED_AT, this.updated_at);
        values.put(DeviceContract.DeviceEntry.COLUMN_GCM_REG_ID, this.clientgcm_id);
        return values;
    }

    public static Device from(Cursor cursor) {
        Device dev = new Device();
        dev.id = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_ID_INDEX);
        dev.user_id = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_USER_ID_INDEX);
        dev.mac_adress = cursor.getString(DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS_INDEX);
        dev.ip_adress = cursor.getString(DeviceContract.DeviceEntry.COLUMN_IP_ADRESS_INDEX);
        dev.clientgcm_id = cursor.getString(DeviceContract.DeviceEntry.COLUMN_GCM_REG_ID_INDEX);
        dev.created_at = cursor.getString(DeviceContract.DeviceEntry.COLUMN_CREATED_AT_INDEX);
        dev.updated_at = cursor.getString(DeviceContract.DeviceEntry.COLUMN_UPDATED_AT_INDEX);
        dev.deleted_at = cursor.getString(DeviceContract.DeviceEntry.COLUMN_DELETED_AT_INDEX);
        return dev;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getClientgcmId() {
        return clientgcm_id;
    }

    public void setClientgcmId(String clientgcm_id) {
        this.clientgcm_id = clientgcm_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getIpAdress() {
        return ip_adress;
    }

    public void setIpAdress(String ipAdress) {
        this.ip_adress = ipAdress;
    }

    public String getMacAdress() {
        return mac_adress;
    }

    public void setMacAdress(String macAdress) {
        this.mac_adress = macAdress;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", ip_adress='" + ip_adress + '\'' +
                ", clientgcm_id='" + clientgcm_id + '\'' +
                ", mac_adress='" + mac_adress + '\'' +
                ", deleted_at='" + deleted_at + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
