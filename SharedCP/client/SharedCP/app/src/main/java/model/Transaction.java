package model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

import persistency.contracts.TransactionContract;

/**
 * Created by benedikt.
 */
public class Transaction {
    private int id;
    private long user_id;
    private String message;
    private String source;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (this.id > 0) {
            values.put(TransactionContract.TransactionEntry._ID, this.id);
        }
        values.put(TransactionContract.TransactionEntry.COLUMN_USER_ID, this.user_id);
        values.put(TransactionContract.TransactionEntry.COLUMN_MESSAGE, this.message);
        values.put(TransactionContract.TransactionEntry.COLUMN_SOURCE, this.source);
        values.put(TransactionContract.TransactionEntry.COLUMN_CREATED_AT, this.created_at);
        values.put(TransactionContract.TransactionEntry.COLUMN_UPDATED_AT, this.updated_at);
        values.put(TransactionContract.TransactionEntry.COLUMN_DELETED_AT, this.deleted_at);
        return values;
    }


    public static Transaction from(Cursor cursor) {
        Transaction tAction = new Transaction();
        tAction.id = cursor.getInt(TransactionContract.TransactionEntry.COLUMN_ID_INDEX);
        tAction.user_id = cursor.getInt(TransactionContract.TransactionEntry.COLUMN_USER_ID_INDEX);
        tAction.message = cursor.getString(TransactionContract.TransactionEntry.COLUMN_MESSAGE_INDEX);
        tAction.source = cursor.getString(TransactionContract.TransactionEntry.COLUMN_SOURCE_INDEX);
        tAction.created_at = cursor.getString(TransactionContract.TransactionEntry.COLUMN_CREATED_AT_INDEX);
        tAction.updated_at = cursor.getString(TransactionContract.TransactionEntry.COLUMN_UPDATED_AT_INDEX);
        tAction.deleted_at = cursor.getString(TransactionContract.TransactionEntry.COLUMN_DELETED_AT_INDEX);
        return tAction;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }


    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", message='" + message + '\'' +
                ", source='" + source + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", deleted_at='" + deleted_at + '\'' +
                '}';
    }
}
