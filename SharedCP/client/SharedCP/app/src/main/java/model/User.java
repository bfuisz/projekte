package model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.util.List;

import persistency.contracts.UserContract;

/**
 * Created by benedikt.
 */
public class User implements Serializable {
    private long id;
    private String email;
    private String password;
    private String session_key;
    private String created_at;
    private String updated_at;
    private List<Device> devices;


    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (id > 0) {
            values.put(UserContract.UserEntry._ID, this.id);
        }
        values.put(UserContract.UserEntry.COLUMN_EMAIL, this.email);
        values.put(UserContract.UserEntry.COLUMN_SESSION_KEY, this.session_key);
        values.put(UserContract.UserEntry.COLUMN_CREATED_AT, this.created_at);
        values.put(UserContract.UserEntry.COLUMN_UPDATED_AT, this.updated_at);
        return values;
    }

    public static User from(Cursor cursor) {
        User user = new User();
        user.id = cursor.getInt(UserContract.UserEntry.COLUMN_ID_INDEX);
        user.email = cursor.getString(UserContract.UserEntry.COLUMN_EMAIL_INDEX);
        user.session_key = cursor.getString(UserContract.UserEntry.COLUMN_SESSION_KEY_INDEX);
        user.created_at = cursor.getString(UserContract.UserEntry.COLUMN_CREATED_AT_INDEX);
        user.updated_at = cursor.getString(UserContract.UserEntry.COLUMN_UPDATED_AT_INDEX);
        return user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", session_key='" + session_key + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
