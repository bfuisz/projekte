package utils;

/**
 * Created by benedikt.
 */
public class CopyThat {
    private static CopyThat instance;
    private boolean copyToggle = true;


    private CopyThat() {

    }

    public static CopyThat getInstance() {
        if (instance == null) {
            instance = new CopyThat();
        }
        return instance;
    }

    public boolean allowCopy() {
        return this.copyToggle;
    }

    public boolean setToggle(boolean copyToggle) {
        this.copyToggle = !copyToggle;
        return this.copyToggle;
    }

    public boolean toggle() {
        this.copyToggle = !copyToggle;
        return this.copyToggle;
    }
}
