package utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import model.User;

/**
 * Created by benedikt.
 */
public class PrefUtils {
    public static final String PREFS_FILENAME = "at.fibuszene.sharedcp_prefs";
    public static final String USER_OBJECT = "at.fibuszene.prefs_user";


    public static void saveUser(Context context, User user) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefs.edit().putString(USER_OBJECT, json).apply();
    }

    public static User getUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        String obj = prefs.getString(USER_OBJECT, null);
        Gson gson = new Gson();
        return (obj == null) ? null : gson.fromJson(obj, User.class);
    }

}
