package utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import model.Device;
import model.User;
import persistency.helper.DeviceHelper;

/**
 * Created by benedikt.
 */
public class Utils {

    public static String extraFromURI(Uri uri, int index) {
        return uri.getPathSegments().get(index);
    }

    public static Device getDeviceInfo(Context context) {
        User user = PrefUtils.getUser(context);

        Device dev = new Device();
        if (user != null) {
            dev.setUser_id(user.getId());
        }
        setMacAndIp(context, dev);
        return dev;
    }

    private static Device setMacAndIp(Context context, Device device) {
        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        device.setIpAdress(Formatter.formatIpAddress(ip));
        device.setMacAdress(wifiInfo.getMacAddress());
        return device;
    }

    private static String getMacAdress(Context context) {
        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return wifiInfo.getMacAddress();
    }

    public static Device thisDevice(Context context) {
        String mac = getMacAdress(context);
        Cursor cursor = DeviceHelper.getDevice(context, mac);
        return Device.from(cursor);
    }


}
