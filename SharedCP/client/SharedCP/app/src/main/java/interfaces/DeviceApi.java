package interfaces;


import java.util.List;

import model.Device;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by benedikt.
 * //http://laravel.io/forum/02-13-2014-i-can-not-get-inputs-from-a-putpatch-request
 * //PUT plus input not possible use POST instead
 */
public interface DeviceApi {

    public static final String ENDPOINT = "http://fibuszene.no-ip.org:9000/api";

//    Route::get ( 'device/{id}', 'DeviceController@listDevices' ); //list of devices for user id
//    Route::get ( 'device/{id}/user', 'DeviceController@listDevicesUser' ); //list of devices for user id
//    Route::post ( 'device', 'DeviceController@addDevice' );
//    Route::post ( 'device/{id}/modify', 'DeviceController@modifyDevice' );
//    Route::delete ( 'device/{id}', 'DeviceController@deleteDevice' );


    @GET("/device")
    public Device getDevice(@Query("ip_adress") String ip_adress);

    @GET("/device/{id}/user")
    public List<Device> getDevicesForUser(@Path("id") long user_id);

    @POST("/device")
    public Device addDevice(@Body Device device);

    @POST("/device/{id}/modify")
    public Device modifyDevice(@Body Device device);

    @DELETE("/device/{id}")
    public Device deleteDevice(@Path("id") long device_id);

}
