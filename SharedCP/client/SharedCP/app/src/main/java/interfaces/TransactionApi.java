package interfaces;

import java.util.List;

import model.ServerMessage;
import model.Transaction;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by benedikt.
 * //http://laravel.io/forum/02-13-2014-i-can-not-get-inputs-from-a-putpatch-request
 * //PUT plus input not possible use POST instead
 */
public interface TransactionApi {

    public static final String ENDPOINT = "http://fibuszene.no-ip.org:9000/api";

//    Route::get ( 'transaction/{id}', 'TransactionController@getTransaction' );
//    Route::post ( 'transaction/{id}/modify', 'TransactionController@modifyTransaction' );
//    Route::delete ( 'transaction/{id}/delete', 'TransactionController@deleteTransaction' );
//    Route::post('transaction', 'TransactionController@newTransaction' );
//    Route::get ( 'transaction/{id}/source', 'TransactionController@listSourceTransactions' ); //list of transactions with id (device) as source
//    Route::get ( 'transaction/{id}/destination', 'TransactionController@listDestinationTransactions' ); //list of transactions with id (device) as destination
//    Route::get ( 'transaction/{id}/user', 'TransactionController@listUserTransactions' ); //list of transaction by user


    @GET("/transaction/{id}")
    public Transaction getTransaction(@Path("id") long t_id);

    @POST("/transaction/{id}/modify")
    public Transaction modifyTransaction(@Path("id") long t_id, @Body Transaction transaction);

    @DELETE("/transaction/{id}/delete")
    public ServerMessage deleteTransaction(@Path("id") long t_id);

    @POST("/transaction")
    public Transaction newTransaction(@Body Transaction transaction);

    @GET("/transaction/{id}/source")
    public List<Transaction> listSourceTransactions(@Path("id") long src_id);

    @GET("/transaction/{id}/destination")
    public List<Transaction> listDestinationTransactions(@Path("id") long dst_id);

    @GET("/transaction/{id}/user")
    public List<Transaction> listUserTransactions(@Path("id") long user_id);


}
