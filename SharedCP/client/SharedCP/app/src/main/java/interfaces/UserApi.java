package interfaces;

import model.User;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by benedikt.
 */
public interface UserApi {
    public static final String ENDPOINT = "http://fibuszene.no-ip.org:9000/api";

    //see server -> app/routes.php
//    Route::get ( 'user/{id}', 'UserController@getUser' );
//    Route::put ( 'user/{id}/logout', 'UserController@logout' );
//    Route::delete ( 'user/{id}/delete', 'UserController@delete' );
//    Route::post ( 'user/login', 'UserController@login' );
//    Route::post ( 'user/register', 'UserController@register' );


    @POST("/user/register")
    public User register(@Body User user);

    @POST("/user/login")
    public User login(@Body User user);

    @PUT("/user/{id}/logout")
    public User logout(@Path("id") long userId);

    @DELETE("/user/{id}/delete")
    public void delete(@Path("id") long userId);

    @GET("/user/{id}")
    public User getUser(@Path("id") long userId);

}
