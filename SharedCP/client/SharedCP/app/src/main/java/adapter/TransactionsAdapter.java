package adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v4.widget.ResourceCursorAdapter;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import at.fibuszene.sharedcp.R;
import interfaces.TransactionApi;
import model.ParsedClipData;
import model.ServerMessage;
import model.Transaction;
import persistency.helper.TransactionHelper;
import retrofit.RestAdapter;
import utils.CopyThat;

/**
 * Created by benedikt.
 */
public class TransactionsAdapter extends ResourceCursorAdapter {

    public TransactionsAdapter(Context context, Cursor c) {
        super(context, R.layout.transaction_list_item, c, false);
    }


    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final Transaction tAction = Transaction.from(cursor);
        if (view == null) {
            view = View.inflate(context, R.layout.transaction_list_item, null);
        }

        ImageButton delete = (ImageButton) view.findViewById(R.id.trash);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(TransactionApi.ENDPOINT).build();

                        TransactionApi api = adapter.create(TransactionApi.class);
                        ServerMessage message = api.deleteTransaction(tAction.getId());
                        if (message != null && !message.getStatus().equals("error")) {
                            TransactionHelper.deleteTransaction(context, tAction.getId());
                            return true;
                        }
                        return false;
                    }
                }.execute();
            }
        });

        ImageButton copy = (ImageButton) view.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //circlejerk
                CopyThat.getInstance().toggle();
                ClipboardManager manager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData cData = ClipData.newPlainText("message", tAction.getMessage());
                manager.setPrimaryClip(cData);
            }
        });

        TextView txtView = (TextView) view.findViewById(R.id.messageTextView);
        txtView.setText(tAction.getMessage());
    }

}
