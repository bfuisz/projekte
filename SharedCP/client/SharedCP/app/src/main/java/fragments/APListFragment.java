package fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.view.View;

import adapter.TransactionsAdapter;
import persistency.contracts.TransactionContract;
import persistency.helper.TransactionHelper;
import utils.NetUtils;

/**
 * Created by benedikt.
 */
public class APListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public enum TransactionType {
        T_OUTGOING, T_INCOMING, T_ALL;
    }

    public static final String ARG1 = "at.fibuszene.sharedcp.apfragment_arg1";
    public static final String ARG2 = "at.fibuszene.sharedcp.apfragment_arg2";


    public static final int TRANSACTION_LOADER_ID = 200;
    public static final int TRANSACTION_CURSOR = 200;
    private long userId;
    private TransactionType tType;
    private TransactionsAdapter transactionsAdapter;

    public static Fragment getInstance(TransactionType arg1, long arg2) {
        APListFragment fragment = new APListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG1, arg1);
        args.putLong(ARG2, arg2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null) {
            this.tType = (TransactionType) bundle.getSerializable(ARG1);
            this.userId = bundle.getLong(ARG2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.transactionsAdapter = new TransactionsAdapter(getActivity(), TransactionHelper.getTransactions(getActivity(), 2));
        setListAdapter(transactionsAdapter);
        getActivity().getSupportLoaderManager().restartLoader(TRANSACTION_LOADER_ID, null, this);
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = new String[]{NetUtils.getIPAddress(true)};
        switch (tType) {
            case T_ALL:
                selection = null;
                selectionArgs = null;
                break;
            case T_INCOMING:
                selection = TransactionContract.TransactionEntry.COLUMN_SOURCE + " != ? ";
                break;
            case T_OUTGOING:
                selection = TransactionContract.TransactionEntry.COLUMN_SOURCE + " = ? ";
                break;
        }
        return new CursorLoader(getActivity(), TransactionContract.TransactionEntry.CONTENT_URI, null, selection, selectionArgs, null);
    }


    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        transactionsAdapter.swapCursor(data);
        transactionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        transactionsAdapter.swapCursor(null);
        transactionsAdapter.notifyDataSetChanged();
    }


}
