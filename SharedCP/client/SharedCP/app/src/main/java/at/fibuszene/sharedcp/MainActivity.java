package at.fibuszene.sharedcp;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import adapter.DevicesAdapter;
import fragments.APListFragment;
import model.Device;
import model.User;
import persistency.contracts.DeviceContract;
import persistency.helper.DeviceHelper;
import persistency.helper.UserHelper;
import service.ActionDescription;
import service.ClipboardSynchronizeService;
import service.SyncService;
import utils.GCMUtils;
import utils.NetUtils;
import utils.PrefUtils;
import utils.Utils;

//TODO:

/**
 * -> some devices bug propably a race condition (recreate on fresh reinstall)
 */

public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final int DEVICES_LOADER_ID = 100;
    private GoogleCloudMessaging gcm;
    private String regid;
    private String SENDER_ID = "189259424904";
    private LinearLayout buttonBar;

    private DevicesAdapter devAdapter;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.user = PrefUtils.getUser(this);

        try {
            //falls multiuser am selben geraet
            //
            this.user = UserHelper.getUsersAsList(this).get(0);
        } catch (IndexOutOfBoundsException e) {
            Log.d("MainActivity", "index out of bounds main");
        }

        if (user == null) {
            loginActivity();
            return;
        }
        buttonBar = (LinearLayout) findViewById(R.id.buttonBar);
        deactivateOthers(R.id.allTransactions);

        if (GCMUtils.checkPlayServices(this)) {
            setUpGcm();
        }

        startService(new Intent(this, ClipboardSynchronizeService.class));
        ListView devicesListView = (ListView) findViewById(R.id.devicesListView);
        devAdapter = new DevicesAdapter(this, DeviceHelper.getUserDevices(this, user.getId()));
        devicesListView.setAdapter(devAdapter);

        getSupportLoaderManager().restartLoader(DEVICES_LOADER_ID, null, this);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_ALL, this.user.getId())).commit();
        startSyncService();
    }

    public void incomingTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_INCOMING, this.user.getId())).commit();
    }

    public void outgoingTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_OUTGOING, this.user.getId())).commit();
    }

    public void allTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_ALL, this.user.getId())).commit();
    }


    public void deactivateOthers(int id) {
        int len = buttonBar.getChildCount();
        View child;
        for (int i = 0; i < len; i++) {
            child = buttonBar.getChildAt(i);
            if (child.getId() == id) {
                child.setActivated(true);
            } else {
                child.setActivated(false);
            }
        }

    }

    public void startSyncService() {
        Intent intent = new Intent(this, SyncService.class);
        intent.setAction(SyncService.SYNC_NOW);
        intent.putExtra(SyncService.ARG1, this.user.getId());
        startService(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry.COLUMN_USER_ID + " = " + this.user.getId(), null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.devAdapter.swapCursor(data);
        this.devAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.devAdapter.swapCursor(null);
        this.devAdapter.notifyDataSetChanged();
    }


    public void setUpGcm() {
        gcm = GoogleCloudMessaging.getInstance(this);
        regid = GCMUtils.getRegistrationId(this);

        if (regid.isEmpty()) {
            registerInBackground();
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    sendRegistrationIdToBackend(regid);
                    GCMUtils.storeRegistrationId(MainActivity.this, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d("GCMRegistration", msg + "\n");
//                mDisplay.append(msg + "\n");
            }
        }.execute();
    }

    public void sendRegistrationIdToBackend(String gcmRegId) {
        Device dev = Utils.getDeviceInfo(this);
        dev.setUser_id(this.user.getId());
        dev.setClientgcmId(gcmRegId);
        dev.setIpAdress(NetUtils.getIPAddress(true));

        if (DeviceHelper.saveDevice(this, dev)) {
            Intent intent = new Intent(this, SyncService.class);
            intent.setAction(SyncService.DEVICE_ACTION);
            intent.putExtra(SyncService.ARG1, new ActionDescription().setActionType(ActionDescription.Action.create).setIpAdress(dev.getIpAdress()));
            startService(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (user == null) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_main_log, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_login) {
            loginActivity();
            return true;
        }
        if (id == R.id.action_logout) {
            user = null;
            recreate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
