package persistency;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import persistency.contracts.DeviceContract;
import persistency.contracts.TransactionContract;
import persistency.contracts.UserContract;

/**
 * Created by benedikt.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String AUTHORITY = "at.fibuszene.sharedcp.provider";
    public static final String CONTENT_URI_BASE = "content://" + AUTHORITY;

    public final static String DATABASE_NAME = "sharedcp.db";
    public static final int DB_VERSION = 1;


    private final String CREATE_USER_TABLE = "CREATE TABLE " + UserContract.UserEntry.TABLE_NAME + "("
            + UserContract.UserEntry._ID + " INTEGER PRIMARY KEY, "
            + UserContract.UserEntry.COLUMN_EMAIL + " TEXT NOT NULL UNIQUE ON CONFLICT IGNORE, "
            + UserContract.UserEntry.COLUMN_SESSION_KEY + " varchar(100), "
            + UserContract.UserEntry.COLUMN_CREATED_AT + " varchar(100), "
            + UserContract.UserEntry.COLUMN_UPDATED_AT + " varchar(100)); ";

    private final String CREATE_DEVICES_TABLE = "CREATE TABLE " + DeviceContract.DeviceEntry.TABLE_NAME + "("
            + DeviceContract.DeviceEntry._ID + " INTEGER , "
            + DeviceContract.DeviceEntry.COLUMN_USER_ID + " INTEGER , "
            + DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS + " varchar(100) UNIQUE ON CONFLICT IGNORE, "
            + DeviceContract.DeviceEntry.COLUMN_IP_ADRESS + " varchar(100) PRIMARY KEY, "
            + DeviceContract.DeviceEntry.COLUMN_GCM_REG_ID + " varchar(100), "
            + DeviceContract.DeviceEntry.COLUMN_CREATED_AT + " varchar(100), "
            + DeviceContract.DeviceEntry.COLUMN_UPDATED_AT + " varchar(100), "
            + DeviceContract.DeviceEntry.COLUMN_DELETED_AT + " varchar(100), "
            + "FOREIGN KEY(" + DeviceContract.DeviceEntry.COLUMN_USER_ID + ") REFERENCES " + UserContract.UserEntry.TABLE_NAME + "(" + UserContract.UserEntry._ID + ") ON DELETE CASCADE); ";

    private final String CREATE_TRANSACTIONS_TABLE = "CREATE TABLE " + TransactionContract.TransactionEntry.TABLE_NAME + "("
            + TransactionContract.TransactionEntry._ID + " INTEGER PRIMARY KEY, "
            + TransactionContract.TransactionEntry.COLUMN_USER_ID + " INTEGER , "
            + TransactionContract.TransactionEntry.COLUMN_MESSAGE + " TEXT, "
            + TransactionContract.TransactionEntry.COLUMN_SOURCE + " INTEGER, "
            + TransactionContract.TransactionEntry.COLUMN_CREATED_AT + " varchar(100), "
            + TransactionContract.TransactionEntry.COLUMN_UPDATED_AT + " varchar(100), "
            + TransactionContract.TransactionEntry.COLUMN_DELETED_AT + " varchar(100), "
            + "FOREIGN KEY(" + TransactionContract.TransactionEntry.COLUMN_USER_ID + ") REFERENCES " + UserContract.UserEntry.TABLE_NAME + "(" + UserContract.UserEntry._ID + ") ON DELETE CASCADE); ";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_DEVICES_TABLE);
        db.execSQL(CREATE_TRANSACTIONS_TABLE);
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            db.execSQL("Drop table " + TransactionContract.TransactionEntry.TABLE_NAME);
            db.execSQL("Drop table " + DeviceContract.DeviceEntry.TABLE_NAME);
            db.execSQL("Drop table " + UserContract.UserEntry.TABLE_NAME);
            onCreate(db);
        }
    }
}
