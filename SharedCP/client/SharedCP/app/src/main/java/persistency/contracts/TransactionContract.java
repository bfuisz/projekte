package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class TransactionContract {

    public static final String CONTENT_AUTHORITY = "at.fibuszene.sharedcp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_TRANSACTION = "transaction";

    public static final class TransactionEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRANSACTION).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_TRANSACTION;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_TRANSACTION;

        public static final String TABLE_NAME = "transactions";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_SOURCE = "source";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATED_AT = "updated_at";
        public static final String COLUMN_DELETED_AT = "deleted_at";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_USER_ID_INDEX = 1;
        public static final int COLUMN_MESSAGE_INDEX = 2;
        public static final int COLUMN_SOURCE_INDEX = 3;
        public static final int COLUMN_CREATED_AT_INDEX = 4;
        public static final int COLUMN_UPDATED_AT_INDEX = 5;
        public static final int COLUMN_DELETED_AT_INDEX = 6;

        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildDateURI(String date) {
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

    }
}
