package persistency;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.sql.SQLException;

import persistency.contracts.DeviceContract;
import persistency.contracts.TransactionContract;
import persistency.contracts.UserContract;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class DataContentProvider extends ContentProvider {
    private DatabaseHelper helper;
    private static final UriMatcher uriMatcher = buildUriMatcher();
    private static final int USER = 100;
    private static final int USER_ID = 101;

    private static final int DEVICE = 200;
    private static final int DEVICE_ID = 201;

    private static final int TRANSACTION = 300;
    private static final int TRANSACTION_ID = 301;

    public static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String userAuthority = UserContract.CONTENT_AUTHORITY;
        matcher.addURI(userAuthority, UserContract.PATH_USER, USER);
        matcher.addURI(userAuthority, UserContract.PATH_USER + "/#", USER_ID);

        final String deviceAuthority = DeviceContract.CONTENT_AUTHORITY;
        matcher.addURI(deviceAuthority, DeviceContract.PATH_DEVICE, DEVICE);
        matcher.addURI(deviceAuthority, DeviceContract.PATH_DEVICE + "/#", DEVICE_ID);


        final String transactionAuthority = TransactionContract.CONTENT_AUTHORITY;
        matcher.addURI(transactionAuthority, TransactionContract.PATH_TRANSACTION, TRANSACTION);
        matcher.addURI(transactionAuthority, TransactionContract.PATH_TRANSACTION + "/#", TRANSACTION_ID);

        return matcher;

    }

    @Override
    public boolean onCreate() {
        this.helper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor = null;
        String extra;
        switch (uriMatcher.match(uri)) {

            case USER:
                retCursor = helper.getReadableDatabase().query(
                        UserContract.UserEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            case USER_ID:
                extra = Utils.extraFromURI(uri, 1);
                retCursor = helper.getReadableDatabase().query(
                        UserContract.UserEntry.TABLE_NAME,
                        projection,
                        UserContract.UserEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;

            case DEVICE:
                retCursor = helper.getReadableDatabase().query(
                        DeviceContract.DeviceEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DEVICE_ID:
                extra = Utils.extraFromURI(uri, 1);
                retCursor = helper.getReadableDatabase().query(
                        DeviceContract.DeviceEntry.TABLE_NAME,
                        projection,
                        DeviceContract.DeviceEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;

            case TRANSACTION:
                retCursor = helper.getReadableDatabase().query(
                        TransactionContract.TransactionEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case TRANSACTION_ID:
                extra = Utils.extraFromURI(uri, 1);
                retCursor = helper.getReadableDatabase().query(
                        TransactionContract.TransactionEntry.TABLE_NAME,
                        projection,
                        TransactionContract.TransactionEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        try {
            retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        } catch (NullPointerException npe) {
            //pretty much impossible
        }
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);
        String type = null;
        switch (match) {
            case USER:
                type = UserContract.UserEntry.CONTENT_TYPE;
            case USER_ID:
                type = UserContract.UserEntry.CONTENT_ITEM_TYPE;
                break;
            case DEVICE:
                type = DeviceContract.DeviceEntry.CONTENT_TYPE;
                break;
            case DEVICE_ID:
                type = DeviceContract.DeviceEntry.CONTENT_ITEM_TYPE;
                break;
            case TRANSACTION:
                type = TransactionContract.TransactionEntry.CONTENT_TYPE;
                break;
            case TRANSACTION_ID:
                type = TransactionContract.TransactionEntry.CONTENT_TYPE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return type;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        Uri returnUri;
        long _id = -1;
        switch (match) {
            case USER:
                _id = db.insert(UserContract.UserEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = UserContract.UserEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            case DEVICE:

                try {
                    _id = db.insertOrThrow(DeviceContract.DeviceEntry.TABLE_NAME, null, values);
                } catch (SQLiteConstraintException sce) {
                    //same device checks in twice(happens a lot)* do nothing since the device is already in the db
                    //* e.g. happens if wifi get's switched of and on again
                }
                //mac address => unique
                //insert will "fail" so don't throw an exception
                if (_id > 0) {
                    returnUri = DeviceContract.DeviceEntry.buildIdURI(_id);
                } else {
                    returnUri = uri;
                }

                break;

            case TRANSACTION:
                try {
                    _id = db.insertOrThrow(TransactionContract.TransactionEntry.TABLE_NAME, null, values);
                } catch (SQLiteConstraintException cst) {
                    //the transactions i receive from the server already have an id to keep
                    // clientes and server synchronized the local id has to be the same as the serverid
                    _id = values.getAsLong(TransactionContract.TransactionEntry._ID);
                }
                if (_id > 0)
                    returnUri = TransactionContract.TransactionEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {
            case USER:
                rowsDeleted = db.delete(UserContract.UserEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case DEVICE:
                rowsDeleted = db.delete(DeviceContract.DeviceEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case TRANSACTION:
                rowsDeleted = db.delete(TransactionContract.TransactionEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case USER:
                rowsUpdated = db.update(UserContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case DEVICE:
                rowsUpdated = db.update(DeviceContract.DeviceEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case TRANSACTION:
                rowsUpdated = db.update(TransactionContract.TransactionEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }


}
