package persistency.helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import model.Device;
import model.Transaction;
import model.User;
import persistency.DatabaseHelper;
import persistency.contracts.TransactionContract;

/**
 * Created by benedikt.
 */
public class TransactionHelper {


    //ziemlich unnoetig weil i sowieso nur die mit userid = loggein on device users vom server hol
    public static Cursor getTransactions(Context context, long user_id) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, TransactionContract.TransactionEntry.COLUMN_USER_ID + " = " + user_id, null, null);
        cursor.moveToFirst();
        return cursor;
    }


    public static Cursor getTransaction(Context context, long id) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, TransactionContract.TransactionEntry._ID + " = " + id, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor getTransactions(Context context) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static List<Transaction> getTransactionsAsList(Context context) {
        List<Transaction> tActions = new ArrayList<Transaction>();

        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tActions.add(Transaction.from(cursor));
                cursor.moveToNext();
            }
        }
        return tActions;
    }


    public static long saveTransactionFromServer(Context context, Transaction tAction) {
        long row = -1;
        if (tAction != null) {
            Uri uri = context.getContentResolver().insert(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values());
            if (uri != null) {
                row = Integer.valueOf(uri.getLastPathSegment());
            }
        }
        return row;
    }

    public static long saveTransaction(Context context, Transaction tAction) {
        long row = -1;
        if (tAction != null) {
            if (tAction.getId() > 0) {
                row = context.getContentResolver().update(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values(), TransactionContract.TransactionEntry._ID + " = " + tAction.getId(), null);
            }
            if (row <= 0) {
                Uri uri = context.getContentResolver().insert(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values());
                if (uri != null) {
                    row = Integer.valueOf(uri.getLastPathSegment());
                }
            }
        }

        return row;
    }


    public static long deleteTransaction(Context context, long id) {
        long row = -1;
        if (id > 0) {
            row = context.getContentResolver().delete(TransactionContract.TransactionEntry.CONTENT_URI, TransactionContract.TransactionEntry._ID + " = " + id, null);
        }

        return row;
    }


}
