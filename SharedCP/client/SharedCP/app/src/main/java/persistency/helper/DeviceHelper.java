package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import model.Device;
import persistency.DatabaseHelper;
import persistency.contracts.DeviceContract;

/**
 * Created by benedikt.
 */
public class DeviceHelper {

    public static Cursor getDevice(Context context, String ip) {
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry.COLUMN_IP_ADRESS + " = '" + ip + "'", null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor getUserDevices(Context context, long userId) {
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry.COLUMN_USER_ID + " = " + userId, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor getDevice(Context context, long id) {
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry._ID + " = " + id, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static boolean saveDevice(Context context, Device device) {
        long row = -1;
        if (device != null) {
            if (device.getIpAdress() != null) {
                row = context.getContentResolver().update(DeviceContract.DeviceEntry.CONTENT_URI, device.values(), DeviceContract.DeviceEntry.COLUMN_IP_ADRESS + " = ? ", new String[]{device.getIpAdress()});
            }
            if (row <= 0) {
                Uri uri = context.getContentResolver().insert(DeviceContract.DeviceEntry.CONTENT_URI, device.values());
                if (uri != null) {
                    row = 1;
                }
            }
        }
        return (row > 0);
    }


    public static long deleteDevice(Context context, long id) {
        long row = -1;
        if (id > 0) {
            SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();
            row = context.getContentResolver().delete(DeviceContract.DeviceEntry.CONTENT_URI, DeviceContract.DeviceEntry._ID + " = " + id, null);
        }
        if (row > 0) {
            context.getContentResolver().notifyChange(DeviceContract.DeviceEntry.CONTENT_URI, null);
        }
        return row;
    }
}
