package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import model.Device;
import model.User;
import persistency.DatabaseHelper;
import persistency.contracts.UserContract;

/**
 * Created by benedikt.
 * rather unnecessary for the moment maybe later to share data directly between to user via 'share'
 */
public class UserHelper {

    public static Cursor getUser(Context context, long userId) {
        Cursor cursor = context.getContentResolver().query(UserContract.UserEntry.CONTENT_URI, null, UserContract.UserEntry._ID + " = " + userId, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor getUsers(Context context) {
        Cursor cursor = context.getContentResolver().query(UserContract.UserEntry.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static List<User> getUsersAsList(Context context) {
        List<User> users = new ArrayList<User>();

        Cursor cursor = context.getContentResolver().query(UserContract.UserEntry.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                users.add(User.from(cursor));
                cursor.moveToNext();
            }
        }
        return users;
    }


    public static long saveUser(Context context, User user) {
        long row = -1;
        if (user != null) {
            if (user.getId() > 0) {
                row = context.getContentResolver().update(UserContract.UserEntry.CONTENT_URI, user.values(), UserContract.UserEntry._ID + " = " + user.getId(), null);
            } else {
                Uri uri = context.getContentResolver().insert(UserContract.UserEntry.CONTENT_URI, user.values());
                if (uri != null) {
                    row = 1;
                }
//                context.getContentResolver().query(uri, null, null, null, null);
            }
        }
        return row;
    }


    public static long deleteUser(Context context, long id) {
        long row = -1;
        if (id > 0) {
            row = context.getContentResolver().delete(UserContract.UserEntry.CONTENT_URI, UserContract.UserEntry._ID + " = " + id, null);
        }
        return row;
    }


}
