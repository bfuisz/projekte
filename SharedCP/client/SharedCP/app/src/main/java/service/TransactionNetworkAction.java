package service;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.List;

import interfaces.TransactionApi;
import model.ServerMessage;
import model.Transaction;
import persistency.contracts.TransactionContract;
import persistency.helper.TransactionHelper;

/**
 * Created by benedikt.
 */
public class TransactionNetworkAction extends AbstractNetworkAction {
    private TransactionApi transactionApi;

    public TransactionNetworkAction(Context context, ActionDescription description) {
        super(context, description);
        this.transactionApi = adapter.create(TransactionApi.class);
    }

    @Override
    public void run() {
        switch (description.getActionType()) {
            case sync:

                List<Transaction> transactions = transactionApi.listUserTransactions(description.getDbId());
                String[] serverEntries = new String[transactions.size()];
                int i = 0;
                for (Transaction tmp : transactions) {
                    serverEntries[i] = TransactionHelper.saveTransactionFromServer(context, tmp) + "";
                    i++;
                }
                context.getContentResolver().delete(TransactionContract.TransactionEntry.CONTENT_URI, TransactionContract.TransactionEntry._ID + " NOT IN (?)", serverEntries);
                break;
            case create:
                Cursor cur = TransactionHelper.getTransaction(context, description.getDbId());
                Transaction transaction = Transaction.from(cur);
                transaction = transactionApi.newTransaction(transaction);
                int rows = context.getContentResolver().update(TransactionContract.TransactionEntry.CONTENT_URI, transaction.values(), TransactionContract.TransactionEntry._ID + " = " + description.getDbId(), null);
                break;
            case update:
                break;
            case delete:
                ServerMessage message = transactionApi.deleteTransaction(description.getDbId());

                if (message != null && !message.getStatus().equals("error")) {
                    TransactionHelper.deleteTransaction(context, description.getDbId());
                }
                break;

            case read:
                break;

        }
    }

}
