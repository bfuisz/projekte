package service;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import java.util.List;

import interfaces.DeviceApi;
import model.Device;
import persistency.helper.DeviceHelper;

/**
 * Created by benedikt.
 */
public class DeviceNetworkAction extends AbstractNetworkAction {
    private DeviceApi deviceApi;

    public DeviceNetworkAction(Context context, ActionDescription description) {
        super(context, description);
        deviceApi = adapter.create(DeviceApi.class);
    }

    @Override
    public void run() {
        Device dev;
        switch (description.getActionType()) {
            case sync:
                List<Device> devs = deviceApi.getDevicesForUser(description.getDbId());
                Log.d("Devs", devs + "");
                for (Device tmp : devs) {
                    Log.d("Devs", tmp + "");
                    boolean saved = DeviceHelper.saveDevice(context, tmp);
                }
                break;
            case create:
                dev = Device.from(DeviceHelper.getDevice(context, description.getIpAdress()));
                Log.d("Devs", dev + "");
                dev = deviceApi.addDevice(dev);
                break;
            case update:
                if (description.isUpdateServer()) {
                    dev = Device.from(DeviceHelper.getDevice(context, description.getDbId()));
                    dev = deviceApi.modifyDevice(dev);
                } else {

                }
                break;

            case read:
                if (description.isList()) {
                    List<Device> devices = deviceApi.getDevicesForUser(description.getDbId());
                    //refactor to bulk insert
                    for (Device tmp : devices) {
                        DeviceHelper.saveDevice(context, tmp);
                    }
                } else {
                    Device device = deviceApi.getDevice(description.getIpAdress());
                    DeviceHelper.saveDevice(context, device);
                }
                break;

            case delete:
                dev = deviceApi.deleteDevice(description.getDbId());
                if (dev != null) {
                    DeviceHelper.deleteDevice(context, description.getDbId());
                }
                break;
        }

    }

}
