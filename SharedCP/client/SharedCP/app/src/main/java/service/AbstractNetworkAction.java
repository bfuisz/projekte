package service;

import android.content.Context;

import interfaces.UserApi;
import retrofit.RestAdapter;

/**
 * Created by benedikt.
 */
public abstract class AbstractNetworkAction implements Runnable {
    protected Context context;
    protected ActionDescription description;
    protected RestAdapter adapter;

    protected AbstractNetworkAction(Context context, ActionDescription description) {
        this.context = context;
        this.description = description;
        this.adapter = new RestAdapter.Builder().setEndpoint(UserApi.ENDPOINT).setLogLevel(RestAdapter.LogLevel.BASIC).build();
    }

    @Override
    public abstract void run();

}
