package service;

import java.io.Serializable;

/**
 * Created by benedikt.
 */
public class ActionDescription implements Serializable {
    public enum Action {
        create, read, update, delete, sync,
    }

    private Action actionType;
    private boolean list;
    private boolean updateServer;
    private long dbId;
    private String ipAdress;


    public Action getActionType() {
        return actionType;
    }

    public ActionDescription setActionType(Action actionType) {
        this.actionType = actionType;
        return this;
    }

    public long getDbId() {
        return dbId;
    }

    public ActionDescription setDbId(long db_id) {
        this.dbId = db_id;
        return this;
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public ActionDescription setIpAdress(String ipAdress) {
        this.ipAdress = ipAdress;
        return this;
    }

    public boolean isList() {
        return list;
    }

    public ActionDescription setList(boolean list) {
        this.list = list;
        return this;
    }

    public boolean isUpdateServer() {
        return updateServer;
    }

    /**
     * Descides whether the lokal db needs an update or the server
     * server update e.g. the local ip changes modifyServer = true; with new ip
     * TODO think about it server does not update device ...
     *
     * @param updateServer
     * @return
     */
    public ActionDescription setUpdateServer(boolean updateServer) {
        this.updateServer = updateServer;
        return this;
    }
}
