package service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import adapter.TransactionsAdapter;
import interfaces.TransactionApi;
import model.Transaction;
import model.User;
import persistency.helper.TransactionHelper;
import retrofit.RestAdapter;
import utils.CopyThat;
import utils.NetUtils;
import utils.PrefUtils;
import utils.Utils;

public class ClipboardSynchronizeService extends Service {
    String prev = "";

    public ClipboardSynchronizeService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        clipboard.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                User user = PrefUtils.getUser(ClipboardSynchronizeService.this);
//                  God awful
//                String data = clipboard.getPrimaryClip().getItemAt(0).getHtmlText();

//                if (data == null) {
                String data = clipboard.getPrimaryClip().getItemAt(0).getText() + "";
//                }

                if (CopyThat.getInstance().allowCopy() && !data.equals(prev)) {
                    prev = data;

                    //necessary data (user, source, message);
                    final Transaction action = new Transaction();
                    action.setUser_id(user.getId());
                    action.setMessage(data);
                    action.setSource(NetUtils.getIPAddress(true));
                    long id = TransactionHelper.saveTransaction(ClipboardSynchronizeService.this, action);
                    Log.d("ClipData", action + "");

                    Intent intent = new Intent(ClipboardSynchronizeService.this, SyncService.class);
                    intent.setAction(SyncService.TRANSACTION_ACTION);
                    intent.putExtra(SyncService.ARG1, new ActionDescription().setActionType(ActionDescription.Action.create).setDbId(id));
                    startService(intent);
                } else if (!CopyThat.getInstance().allowCopy()) {
                    //user copied content so toggle back clicklistener in adapter will toggle to false again if user copies something else
                    //since for some reason this gets called twice toggle is not an option to make sure it does something the next time just set it so allow copy
                    CopyThat.getInstance().setToggle(true);
                }
            }
        });
        return START_STICKY;
    }
}
