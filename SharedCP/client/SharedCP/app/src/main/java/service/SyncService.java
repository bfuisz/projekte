package service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import interfaces.UserApi;
import retrofit.RestAdapter;

public class SyncService extends Service {
    private ExecutorService exeService;
    private RestAdapter adapter;
    public static final String DEVICE_ACTION = "at.fibuszene.sharedcp.device_action";
    public static final String USER_ACTION = "at.fibuszene.sharedcp.user_action";
    public static final String TRANSACTION_ACTION = "at.fibuszene.sharedcp.transaction_action";
    public static final String SYNC_NOW = "at.fibuszene.sharedcp.sync_now";

    public static final String ARG1 = "at.fibuszene.sharedcp.arg1";


    public SyncService() {
        adapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(UserApi.ENDPOINT).build();
        exeService = Executors.newCachedThreadPool();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            long userId = -1;
            ActionDescription description = null;
            try {
                description = (ActionDescription) intent.getSerializableExtra(ARG1);
            } catch (ClassCastException cce) {
                userId = intent.getExtras().getLong(ARG1);
                description = new ActionDescription().setActionType(ActionDescription.Action.sync).setDbId(userId);
            }

            AbstractNetworkAction runnableAction = null;

            if (description == null & userId < 0) {
                stopSelf(startId);
            }

            if (action.equals(DEVICE_ACTION)) {
                runnableAction = new DeviceNetworkAction(this, description);
            }

            if (action.equals(USER_ACTION)) {
                runnableAction = new UserNetworkAction(this, description);
            }

            if (action.equals(TRANSACTION_ACTION)) {
                runnableAction = new TransactionNetworkAction(this, description);
            }
            //sqeuentially syncs db no multi user access
            if (action.equals(SYNC_NOW)) {

                runnableAction = new TransactionNetworkAction(this, description);
                exeService.submit(runnableAction);

                runnableAction = new DeviceNetworkAction(this, description);
                exeService.submit(runnableAction);
            }

            if (runnableAction != null) {
                exeService.submit(runnableAction);

                new Thread() {
                    @Override
                    public void run() {
                        try {
                            //hmm blockiert des dann an neustart vom service mit einer anderen task ... ?
                            exeService.awaitTermination(2, TimeUnit.MINUTES);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();


            }
        } else {
            stopSelf(startId);
        }
        return START_STICKY;
    }

}
