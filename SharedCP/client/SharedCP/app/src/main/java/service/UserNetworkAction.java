package service;

import android.content.Context;

import interfaces.UserApi;

/**
 * Created by benedikt.
 */
public class UserNetworkAction extends AbstractNetworkAction {
    private UserApi userApi;


    public UserNetworkAction(Context context, ActionDescription description) {
        super(context, description);
        userApi = adapter.create(UserApi.class);
    }

    @Override
    public void run() {

    }

}
