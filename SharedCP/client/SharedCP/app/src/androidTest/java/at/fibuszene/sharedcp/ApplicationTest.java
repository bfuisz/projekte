package at.fibuszene.sharedcp;

import android.app.Application;
import android.content.ClipData;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import model.ParsedClipData;
import model.Transaction;
import persistency.helper.TransactionHelper;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    //  ClipData { text/html
    // {H:<span style="color: rgb(37, 37, 37); font-family: 'Helvetica Neue', Helvetica, 'Nimbus Sans L', Arial, 'Liberation Sans', sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 26.3999996185303px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">falling</span>}
    // }
    public void testClipData() {
        List<Transaction> tActions = TransactionHelper.getTransactionsAsList(getContext());
        Gson json = new Gson();
        for (Transaction tmp : tActions) {

            Log.d("TAction ", ParsedClipData.fromString(tmp.getMessage()) + "\n");
        }
    }
}