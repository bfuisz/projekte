package service;

import android.content.Intent;

/**
 * Created by benedikt.
 */
public class TimerThread extends Thread {
    public static final String TIMER_ACTION = "at.fibuszene.timer_thread";
    private CountItService service;
    private boolean isRunning;
    private final Object lock = new Object();
    private Boolean pause;
    private Intent intent;
    private long timeInMilis;

    public TimerThread(CountItService service) {
        this.service = service;
        this.pause = false;
        this.isRunning = true;
        this.intent = new Intent(TIMER_ACTION);
    }

    @Override
    public void run() {
        while (isRunning) {
            synchronized (lock) {
                while (pause) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    timeInMilis += 100; //or System.currentTimeMillis() - start; but pause gets more complicated since it keeps running
                    service.sendBroadcast(intent, CountItService.TIMER_NOTIF_ID, this.isRunning, timeInMilis);
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //probaly only one thread so notify all is not too much of a hassle
    public void stopTimer() {
        this.isRunning = false;
        if (pause) { //to release a waiting thread into the isRunning loop consequently stopping it
            restart();
        }
    }

    public void restart() {
        synchronized (lock) {
            this.pause = false;
            lock.notifyAll();
        }
    }

    public void pause() {
        this.pause = true;
    }

    public boolean isPaused() {
        return this.pause;
    }

    public Intent buildIntent() {
        if (this.intent != null) {
            intent.putExtra(CountItService.TIME_EXTRA, this.timeInMilis);

            //if either one is false the timer is not running
            //!pause if pause == true the timer is not running
            boolean running = !this.pause && this.isRunning;

            intent.putExtra(CountItService.RUNNING_EXTRA, running);
        }
        return intent;
    }

}
