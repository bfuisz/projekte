package service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;

import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import fragment.StuffCountDownFragment;
import fragment.StuffTimerFragment;
import utils.PrefUtils;
import utils.Utils;

public class CountItService extends Service {

    public static final String ACTION_START_COUNTDOWN_TASK = "at.fibuszene.start_countdown_task";

    public static final String ACTION_STOP_COUNTDOWN_TASK = "at.fibuszene.stop_countdown_task";

    public static final String ACTION_START_TIMER_TASK = "at.fibuszene.start_timer_task";
    public static final String ACTION_STOP_TIMER_TASK = "at.fibuszene.stop_timer_task";
    public static final String ACTION_PAUSE_TIMER_TASK = "at.fibuszene.pause_timer_task";

    public static final String COUNTDOWN_TIME_ARG = "at.fibusenen.countdown_time_arg";

    public static final String TIME_EXTRA = "at.fibuszene.service.receiver.time_extra";
    public static final String RUNNING_EXTRA = "at.fibuszene.service.receiver.running_extra";

    public static final String NOTIF_ID_START_HOME = "at.fibuszene.service.receiver.notifid_tostart";

    public static final int TIMER_NOTIF_ID = 100;
    public static final int COUNTDOWN_NOTIF_ID = 200;

    private Notification.Builder builder;
    private NotificationManager notificationManager;

    private CustomCountDown countDown;
    private TimerThread timer;


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //intent can be null if the system recreates the service
        //but there are no pending intents -> START_STICKY Doc
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            this.notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (action.equals(ACTION_START_COUNTDOWN_TASK)) { //includes start and pause
                long countDownTime = intent.getLongExtra(COUNTDOWN_TIME_ARG, 0);
                startCountDown(countDownTime);

            } else if (action.equals(ACTION_STOP_COUNTDOWN_TASK)) {
                stopCountDown();

            } else if (action.equals(ACTION_START_TIMER_TASK)) { //include start and pause
                startTimer();
            } else if (action.equals(ACTION_STOP_TIMER_TASK)) {
                stopTimer();
            }
        } else {
            //no pending intents but service recreated most likely
            //all the notifications are not needed
            stopForeground(true);
        }
        return START_STICKY;
    }

    public void startTimer() {
        boolean startForeground = true;
        if (timer == null) {
            timer = new TimerThread(this);
            timer.start();
        } else if (timer != null && !this.timer.isPaused()) {
            pauseTimer();
            stopForeground(true);
            startForeground = false;
        } else {
            timer.restart();
        }
        if (this.timer != null) {
            sendBroadcast(timer.buildIntent(), TIMER_NOTIF_ID);
        }

        if (PrefUtils.showNotifications(this) && startForeground) {
            buildNotification(TIMER_NOTIF_ID, this.getString(R.string.timer_title));
        }

    }

    public void pauseTimer() {
        if (this.timer != null) {
            this.timer.pause();
        }
    }

    public void stopTimer() {
        if (this.timer != null) {
            this.timer.stopTimer();
            sendBroadcast(timer.buildIntent(), TIMER_NOTIF_ID);
            this.timer = null;
            stopForeground(true);
        }
    }

    public void sendBroadcast(Intent intent, int notifId, boolean state, long time) {
        if (intent != null) {
            intent.putExtra(TIME_EXTRA, time);
            intent.putExtra(RUNNING_EXTRA, state);
            LocalBroadcastManager.getInstance(CountItService.this).sendBroadcast(intent);

            if (this.builder != null && state) {
                builder.setContentText(Utils.formatCountDown(time));
                this.notificationManager.notify(notifId, buildNotifcation());
            }
        }
    }

    public void sendBroadcast(Intent intent, int notifId) {
        if (intent != null) {
            LocalBroadcastManager.getInstance(CountItService.this).sendBroadcast(intent);
            if (this.builder != null) {
                builder.setContentText(Utils.formatCountDown(intent.getLongExtra(TIME_EXTRA, 0)));
                this.notificationManager.notify(notifId, buildNotifcation());
            }
        }
    }

    public void buildNotification(int notifId, String ticker) {
        if (PrefUtils.showNotifications(this)) {
            Intent intent = new Intent(this, HomeActivity.class);

            if (notifId == COUNTDOWN_NOTIF_ID) {
                intent.setAction(StuffCountDownFragment.FRAGMENT_NAME);
            } else if (notifId == TIMER_NOTIF_ID) {
                intent.setAction(StuffTimerFragment.FRAGMENT_NAME);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendIntent = PendingIntent.getActivity(this, 0, intent, 0);

            this.builder = new Notification.Builder(getBaseContext());
            builder.setContentIntent(pendIntent);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setTicker(ticker);
            builder.setWhen(System.currentTimeMillis());
            builder.setAutoCancel(false);
            builder.setContentTitle(this.getString(R.string.app_name));
            startForeground(notifId, buildNotifcation());
        }
    }

    public Notification buildNotifcation() {
        Notification notification;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        } else {
            notification = builder.getNotification();
        }
        return notification;
    }


    /**
     * Three cases:
     * countdown == null && time > 0 => start countdown
     * countdown != null && time == 0 && also this.countDown.isRunning() => pause the countdown
     * countdown != null && time == 0 && !this.countDown.isRunning() => countdown is paused and has to be restarted
     *
     * @param time
     */
    public void startCountDown(long time) {
        boolean startForeground = true;
        if (countDown == null && time > 0) { //initial start
            this.countDown = new CustomCountDown(time, this);
            this.countDown.start();

        } else if (this.countDown != null && this.countDown.isRunning()) { //pause
            pauseCountDown();
            stopForeground(true);
            startForeground = false;
        } else if (this.countDown != null && !this.countDown.isRunning()) { //start after pause
            time = countDown.getMillisUntilFinished();
            countDown = null;
            this.countDown = new CustomCountDown(time, this);
            this.countDown.start();
        }

        if (countDown != null) { //should'nt be at this point but you never know
            sendBroadcast(countDown.buildIntent(), COUNTDOWN_NOTIF_ID);
        }

        if (PrefUtils.showNotifications(this) && startForeground) {
            buildNotification(COUNTDOWN_NOTIF_ID, this.getString(R.string.countdown_title));
        }
    }

    public void stopCountDown() {
        if (this.countDown != null) {
            this.countDown.setRunning(false);
            sendBroadcast(countDown.buildIntent(), COUNTDOWN_NOTIF_ID);
            this.countDown.cancel();
            this.countDown = null;

            if (PrefUtils.notifications(this)) {
                if (PrefUtils.vibrate(this)) {
                    Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                    vibrator.vibrate(500);
                }

                String ringtone = PrefUtils.playSound(this);

                if (ringtone != null) {
                    Uri ringtoneURI = Uri.parse(ringtone);
                    MediaPlayer mp = MediaPlayer.create(this, ringtoneURI);

                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                            mp.release();
                            mp = null;
                        }
                    });
                    mp.start();
                }
            }
        }
        stopForeground(true);
    }


    public void pauseCountDown() {
        if (this.countDown != null) {
            this.countDown.pause();
        }
    }

}
