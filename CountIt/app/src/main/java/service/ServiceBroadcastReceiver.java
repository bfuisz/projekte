package service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import listener.ServiceListener;

/**
 * Created by benedikt.
 */
public class ServiceBroadcastReceiver extends BroadcastReceiver {
    private ServiceListener listener;

    public ServiceListener getListener() {
        return listener;
    }

    public void setListener(ServiceListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        long timeL = intent.getLongExtra(CountItService.TIME_EXTRA, 0);
        boolean isRunning = intent.getBooleanExtra(CountItService.RUNNING_EXTRA, false);
        listener.setTime(isRunning, timeL);
    }
}

