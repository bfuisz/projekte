package service;

import android.content.Intent;
import android.os.CountDownTimer;

/**
 * Created by benedikt.
 */
public class CustomCountDown extends CountDownTimer {
    public static final String COUNTDOWN_ACTION = "at.fibuszene.countdown_action";
    private CountItService service;
    private boolean isRunning;
    private long millisUntilFinished;
    private Intent intent;

    public CustomCountDown(long millisInFuture, CountItService service) {
        super(millisInFuture, 1000);//always seconds
        this.service = service;
        this.isRunning = true;
        this.intent = new Intent(COUNTDOWN_ACTION);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        if (isRunning && service != null) {
            this.millisUntilFinished = millisUntilFinished;
            service.sendBroadcast(intent, CountItService.COUNTDOWN_NOTIF_ID, this.isRunning, millisUntilFinished);
        }
    }

    @Override
    public void onFinish() {
        this.isRunning = false;
        service.sendBroadcast(intent, CountItService.COUNTDOWN_NOTIF_ID, this.isRunning, 0);
        service.stopCountDown();
    }

    public long pause() {
        this.cancel();
        this.isRunning = false; //gets recreated anyways
        return this.millisUntilFinished;
    }

    public long getMillisUntilFinished() {
        return millisUntilFinished;
    }

    public void setMillisUntilFinished(long millisUntilFinished) {
        this.millisUntilFinished = millisUntilFinished;
    }

    public Intent buildIntent() {
        if (this.intent != null) {
            intent.putExtra(CountItService.TIME_EXTRA, this.millisUntilFinished);
            intent.putExtra(CountItService.RUNNING_EXTRA, this.isRunning);
        }
        return intent;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        this.isRunning = running;
    }
}