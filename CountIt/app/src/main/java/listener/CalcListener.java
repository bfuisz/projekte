package listener;

/**
 * Created by benedikt.
 */
public interface CalcListener {

    public enum ErrorType {
        empty_text, empty_stack, mismatched_pars
    }

    public void done(String result);

    public void setCalcField(String text);

    public void append(String op);

    public void remove();

    public void clear();

    public void error(ErrorType type);

    public String getExpression();

}
