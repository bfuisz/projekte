package listener;

/**
 * Created by benedikt.
 */
public interface ServiceListener {

    public void setTime(boolean isRunning, long time);

}
