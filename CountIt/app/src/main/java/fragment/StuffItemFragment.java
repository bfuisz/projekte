package fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import model.Stuff;
import persistency.helper.StuffHelper;
import utils.PrefUtils;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class StuffItemFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String FRAGMENT_NAME = "at.fibuszene.countit.StuffItemFragment";

    public static final String STUFF_KEY = "at.fibuszene.countit.stuff_key";
    public static final String STUFF_CONFIG_CHANGE = "at.fibuszene.countit.stuff_config_key";
    private TextView stuffNameTextView;
    private Stuff stuff;
    private TextView counterTextView;
    private ImageView imgView;
    private ViewSwitcher nameSwitcher, counterSwitcher, imageSwitcher;
    private Menu menu;
    private final static int REQUEST_CAMERA = 100;
    private final static int SELECT_FILE = 200;
    private EditText editName, counterText;
    private Handler handler = new Handler();
    private Uri outputFileUri;


    public static StuffItemFragment getInstance(Stuff stuff) {
        StuffItemFragment fragment = new StuffItemFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(STUFF_KEY, stuff);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stuff_item, null);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            this.stuff = (Stuff) savedInstanceState.getSerializable(STUFF_CONFIG_CHANGE);
        }
        if (stuff == null) {
            this.stuff = (Stuff) getArguments().getSerializable(STUFF_KEY);
        }

        try {
            getActivity().getActionBar().setTitle(getString(R.string.tab_counter));
        } catch (NullPointerException npe) {
            //no point in handling because there are no alternatives :D
            //just leave the default which is the app name
        }

        this.stuffNameTextView = (TextView) view.findViewById(R.id.stuffNameTextView);
        this.imgView = (ImageView) view.findViewById(R.id.counterImageView);

        imgView.setClickable(true);
        imgView.setOnClickListener(this);

        stuffNameTextView.setText(stuff.getName());

        if (stuff.getPicture() != null) {
            setImage(Uri.parse(this.stuff.getPicture()));
        }

        nameSwitcher = (ViewSwitcher) view.findViewById(R.id.editStuffNameSwitcher);
        editName = (EditText) nameSwitcher.findViewById(R.id.editStuffName);

        counterSwitcher = (ViewSwitcher) view.findViewById(R.id.editStuffCounterSwitcher);
        counterText = (EditText) counterSwitcher.findViewById(R.id.counterEditTextView);

        imageSwitcher = (ViewSwitcher) view.findViewById(R.id.imageViewSwitcher);
        ListView imageChooserList = (ListView) view.findViewById(R.id.imageChooserList);
        imageChooserList.setOnItemClickListener(this);

        this.counterTextView = (TextView) view.findViewById(R.id.counterTextView);

        ImageButton counterMinusButton = (ImageButton) view.findViewById(R.id.counterMinusButton);
        counterMinusButton.setOnClickListener(this);
        ImageButton counterMinusButtonRight = (ImageButton) view.findViewById(R.id.counterMinusButtonRight);
        counterMinusButtonRight.setOnClickListener(this);

        if (!PrefUtils.showLeftMinus(getActivity())) {
            counterMinusButton.setVisibility(View.INVISIBLE);
        }
        if (!PrefUtils.showRightMinus(getActivity())) {
            counterMinusButtonRight.setVisibility(View.INVISIBLE);
        }

        this.counterTextView.setText(stuff.getCount() + "");
        activateButton();
    }

    public void activateButton() {
        try {
            ((HomeActivity) getActivity()).activateButton(R.id.stuffItemButton);
        } catch (Exception e) {
            //Class cast: unlikely
            //NullPointer:
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (stuff != null && stuff.getPicture() != null) {
            setImage(Uri.parse(this.stuff.getPicture()));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "IMG_" + timeStamp + "_" + this.stuff.getName() + "_" + this.stuff.getId() + ".jpg");

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            outputFileUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values); // store content values
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            // start the image capture Intent
            startActivityForResult(intent, REQUEST_CAMERA);
        } else if (position == 1) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, SELECT_FILE);
        }
    }


    @Override
    public void onClick(View v) {
        HomeActivity.hapticFeedBack();

        if (v.getId() == R.id.counterMinusButtonRight || v.getId() == R.id.counterMinusButton) {
            decreaseCounter();
        }
        if (v.getId() == R.id.counterImageView) {
            increaseCounter();
        }
    }


    public void increaseCounter() {
        stuff.increase();
        updateCounter(true);
        updateStuff();
    }

    public void decreaseCounter() {
        stuff.decrease();
        updateCounter(false);
        updateStuff();
    }

    public void updateCounter(boolean increase) {
        if (PrefUtils.animateCounter(getActivity())) {
            if (increase) {
                YoYo.with(Techniques.RotateOutDownLeft).delay(0).duration(250).playOn(counterTextView);
                YoYo.with(Techniques.RotateInDownLeft).delay(250).duration(250).playOn(counterTextView);
            } else {
                YoYo.with(Techniques.RotateOutUpLeft).delay(0).duration(250).playOn(counterTextView);
                YoYo.with(Techniques.RotateInUpLeft).delay(250).duration(250).playOn(counterTextView);
            }

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    counterTextView.setText(stuff.getCount() + "");
                }
            }, 250);
        } else {
            counterTextView.setText(stuff.getCount() + "");
        }
    }


    public void recycleBitMap() {
        try {
            BitmapDrawable bd = (BitmapDrawable) this.imgView.getDrawable();
            if (bd != null) {
                bd.getBitmap().recycle();
                this.imgView.setImageBitmap(null);
            }
        } catch (NullPointerException npe) {
//            no picture
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            Uri selectedImage;
            try {
                selectedImage = data.getData();
            } catch (NullPointerException npe) {//some devices
                selectedImage = outputFileUri;
            }
            setImage(selectedImage);
            this.stuff.setPicture(selectedImage.toString());
            updateStuff();
        }
    }

    public void setImage(Uri imageUri) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getMetrics(displayMetrics);

        int use;
        int width = displayMetrics.widthPixels - 50;
        int height = displayMetrics.heightPixels - 50;

        if (display.getRotation() == Surface.ROTATION_0 || display.getRotation() == Surface.ROTATION_180) {
            use = height;
        } else {
            use = width;
        }

        try {
            imgView.setImageBitmap(Utils.decodeURI(getActivity(), use, imageUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateStuff() {
        StuffHelper.update(getActivity(), this.stuff);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STUFF_CONFIG_CHANGE, this.stuff);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.stuff, menu);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit_counter) {
            editNameMode(true);
        }

        if (id == R.id.action_submit) {
            if (updateStuffValues()) {
                updateStuff();
                editNameMode(false);
            }
        }

        if (id == R.id.action_cancel) {
            editNameMode(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean updateStuffValues() {
        String newName = editName.getText() + "";
        String newCounter = counterText.getText() + "";

        if (!TextUtils.isEmpty(newName)) {
            this.stuff.setName(newName);
            stuffNameTextView.setText(newName);
        } else {
            showError(editName);
        }

        if (!TextUtils.isEmpty(newCounter)) {
            this.stuff.setCount(Long.valueOf(newCounter));
            counterTextView.setText(newCounter);
        } else {
            showError(counterText);
        }

        return !TextUtils.isEmpty(newName) && !TextUtils.isEmpty(newCounter);
    }

    private void showError(final EditText counterText) {
        final Drawable neutralColor = counterText.getBackground();
        Utils.setBackground(counterTextView, getResources().getDrawable(R.drawable.wrong_border));
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Utils.setBackground(counterTextView, neutralColor);
            }
        }, 1500);

    }

    public void editNameMode(boolean editMode) {
        if (editMode) {
            counterText.requestFocus();
            counterText.setText(counterTextView.getText());
            editName.setText(stuffNameTextView.getText());
        }
        //submit and cancel visible if edit mode true
//        else if editMode == false;
        menu.findItem(R.id.action_cancel).setVisible(editMode);
        menu.findItem(R.id.action_submit).setVisible(editMode);

        menu.findItem(R.id.action_settings).setVisible(!editMode);
        menu.findItem(R.id.action_edit_counter).setVisible(!editMode);
        menu.findItem(R.id.action_about).setVisible(!editMode);

        nameSwitcher.showNext();
        imageSwitcher.showNext();
        counterSwitcher.showNext();
    }

}
