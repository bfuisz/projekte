package fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;


import adapter.TablineAdapter;
import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import at.fibuszene.countit.SettingsActivity;
import calculator.Calculator;
import model.TmpTabLine;
import utils.TmpTabLineList;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class StuffTabFragment extends Fragment implements View.OnClickListener {
    public static final String FRAGMENT_NAME = "at.fibuszene.countit.StuffTabFragment";
    public static final String COUNTER_VALUE = "at.fibuszene.countit.counter_value";
    private TextView totalTextfield;
    private TablineAdapter adapter;
    private EditText costTextField;
    private NumberPicker amountPicker;
    private Handler handler = new Handler();
    private Drawable neutral;
    private long counterValue;

    public static StuffTabFragment getInstance(long counterValue) {
        StuffTabFragment frag = new StuffTabFragment();
        Bundle arge = new Bundle();
        arge.putLong(COUNTER_VALUE, counterValue);
        frag.setArguments(arge);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        this.counterValue = getArguments().getLong(COUNTER_VALUE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stuff_tabs, null);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView tabLineListView = (ListView) view.findViewById(R.id.tabListView);
        totalTextfield = (TextView) view.findViewById(R.id.totalTextfield);
        this.adapter = new TablineAdapter(this, ((HomeActivity) getActivity()).getStuffId());
        tabLineListView.setAdapter(this.adapter);
        getActivity().getLoaderManager().restartLoader(HomeActivity.TAB_LOADER, null, adapter);
        try {
            getActivity().getActionBar().setTitle(getString(R.string.tab_tab));
        } catch (NullPointerException npe) {
            //no point in handling because there are no alternatives :D
            //just leave the dafault which is the app name
        }
        ImageButton button = (ImageButton) view.findViewById(R.id.addTabLineItem);
        button.setOnClickListener(this);
        amountPicker = (NumberPicker) view.findViewById(R.id.amountPicker);
        amountPicker.setMinValue(0);
        amountPicker.setMaxValue(255);
        costTextField = (EditText) view.findViewById(R.id.itemCost);
        neutral = costTextField.getBackground();
        activateButton();
    }

    public void activateButton() {
        try {
            ((HomeActivity) getActivity()).activateButton(R.id.tabButton);
        } catch (Exception e) {
            //Class cast: unlikely
            //NullPointer:
        }
    }

    @Override
    public void onPause() {
        this.adapter.release();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void changeTotal(String total) {
        this.totalTextfield.setText(total);
    }

    @Override
    public void onClick(View v) {
        HomeActivity.hapticFeedBack();

        if (v.getId() == R.id.addTabLineItem) {
            try {
                String cost = costTextField.getText().toString();
                if (!TextUtils.isEmpty(cost)) {
                    TmpTabLine line = new TmpTabLine(Float.valueOf(cost), amountPicker.getValue());
                    this.adapter.add(line);
                    costTextField.setText("");
                } else {
                    showError();
                }
            } catch (NumberFormatException ne) {
                showError();
            }
        }
    }

    public void showError() {
        Utils.setBackground(this.costTextField, getResources().getDrawable(R.drawable.wrong_border));
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.setBackground(costTextField, neutral);
            }
        }, 1500);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.tabs, menu);
        menu.findItem(R.id.action_counter_value).setTitle(getString(R.string.counter_value_actionbar_text) + this.counterValue + "");
    }

}
