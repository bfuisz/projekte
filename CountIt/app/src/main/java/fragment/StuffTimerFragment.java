package fragment;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import adapter.LapAdapter;
import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import listener.ServiceListener;
import model.Stuff;
import persistency.helper.StuffHelper;
import service.ServiceBroadcastReceiver;
import service.TimerThread;
import utils.PrefUtils;

/**
 * Created by benedikt.
 */
public class StuffTimerFragment extends Fragment implements ServiceListener {
    public static final String FRAGMENT_NAME = "at.fibuszene.countit.StuffTimerFragment";
    public static final String STUFF_ID_EXTRA = "at.fibuszene.countit.sutff_id";

    private ServiceBroadcastReceiver receiver;
    private LapAdapter adapter;
    private ImageButton playButton;
    private TextView hourTextView, minuteTextView, secondTextView, milisecondsTextView;
    private long hour = 0, minute = 0, second = 0, miliSecond = 0;
    private boolean isRunning;
    private Stuff stuff;
    private MenuItem stuffCountItem;


    public static StuffTimerFragment getInstance(long stuffId) {
        StuffTimerFragment frag = new StuffTimerFragment();
        Bundle args = new Bundle();
        args.putLong(STUFF_ID_EXTRA, stuffId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        receiver = new ServiceBroadcastReceiver();
        receiver.setListener(this);
        long stuffid = getArguments().getLong(STUFF_ID_EXTRA, 0);

        if (stuffid > 0) {
            this.stuff = StuffHelper.get(getActivity(), stuffid);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stuff_timer, null);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView lapListView = (ListView) view.findViewById(R.id.lapsListView);
        this.adapter = new LapAdapter(getActivity(), ((HomeActivity) getActivity()).getStuffId());
        lapListView.setAdapter(adapter);

        getActivity().getLoaderManager().restartLoader(HomeActivity.LAP_LOADER, null, adapter);
        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getString(R.string.tab_timer));
        }
        this.playButton = (ImageButton) view.findViewById(R.id.timerPlayButton);
        this.hourTextView = (TextView) view.findViewById(R.id.hourTextView);
        this.minuteTextView = (TextView) view.findViewById(R.id.minuteTextView);
        this.secondTextView = (TextView) view.findViewById(R.id.secondTextView);
        this.milisecondsTextView = (TextView) view.findViewById(R.id.milisecondsTextView);

        this.isRunning = PrefUtils.timerRunning(getActivity());
        long time = PrefUtils.timeService(getActivity(), PrefUtils.TYPE_TIMER);
        setTime(isRunning, time);

        activateButton();

        if (stuff != null && stuffCountItem != null) { //Nexus 7 2012
            this.stuffCountItem.setTitle(this.stuff.getCount() + "");
        }
    }

    public void activateButton() {
        try {
            ((HomeActivity) getActivity()).activateButton(R.id.timerButton);
        } catch (Exception e) {
            //Class cast: unlikely
            //NullPointer: if call before isAttached
        }
    }

    @Override
    public void setTime(boolean isRunning, long time) {
        this.isRunning = isRunning;
        if (isAdded()) {
            PrefUtils.timeService(getActivity(), PrefUtils.TYPE_TIMER, time);
            PrefUtils.timerRunning(getActivity(), isRunning);
        }
        changeView(time);
    }


    public void changeView(long time) {
        //TODO Refactor  build custom ViewElement
        long centiseconds = (((time % 1000L) + 5L) / 10L);
        long newSecond = (time / 1000L);
        long newMinute = newSecond / 60;
        newSecond -= newMinute * 60;
        long newHour = newMinute / 60;
        newMinute -= newHour * 60;

        milisecondsTextView.setText(String.format("%01d", centiseconds / 10));

        if (newSecond != this.second) {
            this.second = newSecond;
            flip(secondTextView, String.format("%02d", this.second));
        }

        if (newMinute != this.minute) {
            this.minute = newMinute;
            flip(minuteTextView, String.format("%02d", this.minute));
        }

        if (newHour != this.hour) {
            this.hour = newHour;
            flip(hourTextView, String.format("%02d", this.hour));
        }
        setButton(isRunning);
    }

    public void flip(TextView view, String value) {
        YoYo.with(Techniques.FlipInX).delay(0).playOn(view);
        view.setText(value + "");
    }


    public void setButton(boolean isRunning) {
        if (playButton != null && isAdded()) {//can be null due to setButton() call in setTime which is called by service
            //isAdded bc getActivity fails in some cases of forced behauviour :D
            if (isRunning) {
                this.playButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.pause));
            } else {
                this.playButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.play));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(this.receiver, new IntentFilter(TimerThread.TIMER_ACTION));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(this.receiver);
        this.adapter.release();
        super.onPause();
    }

    public void lap() {
        this.adapter.add(this.hourTextView.getText().toString() + ":" + this.minuteTextView.getText() + ":" + this.secondTextView.getText() + ":" + this.milisecondsTextView.getText());
    }

    public void stop() {
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.miliSecond = 0;
        PrefUtils.timeService(getActivity(), PrefUtils.TYPE_TIMER, 0);
        PrefUtils.timerRunning(getActivity(), false);
    }

    public void setView() {
        this.adapter.clear();
        this.hourTextView.setText(String.format("%02d", this.hour));
        this.minuteTextView.setText(String.format("%02d", this.minute));
        this.secondTextView.setText(String.format("%02d", this.second));
        this.milisecondsTextView.setText(String.format("%01d", this.miliSecond));
        setButton(isRunning);
    }

    public void reset() {
        stop();  //resets attributes
        setView(); //setView according to attributes
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.timer, menu);
        menu.findItem(R.id.action_about).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);

        stuffCountItem = menu.findItem(R.id.counter_value);
        if (stuffCountItem == null) {
            stuffCountItem = menu.getItem(1);
        }
        if (stuffCountItem != null && this.stuff != null) {
            stuffCountItem.setTitle(this.stuff.getCount() + "");
        }
    }

    public void changeStuffItemValue(int value) { //either +1 or -1
        if (this.stuff != null) {
            long newCount = this.stuff.getCount() + value;
            this.stuff.setCount(newCount);
            StuffHelper.update(getActivity(), stuff);
            this.stuffCountItem.setTitle(this.stuff.getCount() + "");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_counter_minus) {
            changeStuffItemValue(-1);
            return true;
        }
        if (id == R.id.action_counter_plus) {
            changeStuffItemValue(1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
