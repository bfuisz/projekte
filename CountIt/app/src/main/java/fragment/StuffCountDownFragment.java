package fragment;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.concurrent.TimeUnit;

import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import listener.ServiceListener;
import service.CountItService;
import service.CustomCountDown;
import service.ServiceBroadcastReceiver;
import utils.PrefUtils;

/**
 * Created by benedikt.
 */
public class StuffCountDownFragment extends Fragment implements ServiceListener {
    public static final String FRAGMENT_NAME = "at.fibuszene.countit.StuffCountDownFragment";
    public static final String TIME_KEY = "at.fibuszene.time_extra";

    private ServiceBroadcastReceiver receiver;
    private LinearLayout countDownLayout;
    private boolean isRunning;
    private TextView hourTextView, minuteTextView, secondTextView;
    private ImageButton countdownPlayButton;
    private long hour = 0, minute = 0, second = 0;
    private int[] setFieldsArray;

    public static StuffCountDownFragment getInstance() {
        return new StuffCountDownFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        receiver = new ServiceBroadcastReceiver();
        receiver.setListener(this);
        this.setFieldsArray = new int[6];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stuff_countdown, null);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getString(R.string.tab_countdown));
        }
        this.hourTextView = (TextView) view.findViewById(R.id.hourTextView);
        this.minuteTextView = (TextView) view.findViewById(R.id.minuteTextView);
        this.secondTextView = (TextView) view.findViewById(R.id.secondTextView);
        this.countdownPlayButton = (ImageButton) view.findViewById(R.id.countdownPlayButton);
        this.countDownLayout = (LinearLayout) view.findViewById(R.id.countDownLayout);
        this.isRunning = PrefUtils.countDownRunning(getActivity());

        long time = PrefUtils.timeService(getActivity(), PrefUtils.TYPE_COUNTDOWN);
        if (time > 0) {
            setTime(isRunning, time);
        } else {
            setButton(isRunning);
        }
        activateButton();
    }


    public void activateButton() {
        try {
            ((HomeActivity) getActivity()).activateButton(R.id.countDownButton);
        } catch (Exception e) {
            //Class cast: unlikely
            //NullPointer:
        }
    }

    private void setCountdown(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String[] time = savedInstanceState.getString(TIME_KEY).split(":");
            this.hour = Long.valueOf(time[0]);
            this.minute = Long.valueOf(time[1]);
            this.second = Long.valueOf(time[2]);
        }
        this.hourTextView.setText(String.format("%02d", this.hour));
        this.minuteTextView.setText(String.format("%02d", this.minute));
        this.secondTextView.setText(String.format("%02d", this.second));
    }

    @Override
    public void setTime(boolean isRunning, long time) {
        this.isRunning = isRunning;
        if (isAdded()) {
            PrefUtils.timeService(getActivity(), PrefUtils.TYPE_COUNTDOWN, time);
            PrefUtils.countDownRunning(getActivity(), isRunning);
        }
        setTimeView(time);
    }

    //TODO code smell duplicat Refactor build custom TextView
    public void setTimeView(long time) {
        long newSecond = TimeUnit.MILLISECONDS.toSeconds(time) % 60;
        long newMinute = TimeUnit.MILLISECONDS.toMinutes(time) % 60;
        long newHour = TimeUnit.MILLISECONDS.toHours(time);

        if (newHour != this.hour) {
            this.hour = newHour;
            pulse(hourTextView, String.format("%02d", this.hour));
        }

        if (newMinute != this.minute) {
            this.minute = newMinute;
            pulse(minuteTextView, String.format("%02d", this.minute));
        }
        if (newSecond != this.second) {
            this.second = newSecond;

            if (this.minute == 0 && this.hour == 0 && this.second <= 10) {
                pulse(secondTextView, String.format("%02d", this.second));
            } else {
                flip(secondTextView, String.format("%02d", this.second));
            }
        }
        setButton(isRunning);
        if (time == 0 || !isRunning) {
            stop();
        }
    }

    public void flip(TextView view, String value) {
        YoYo.with(Techniques.FlipInX).delay(0).playOn(view);
        view.setText(value + "");
    }

    public void pulse(TextView view, String value) {
        YoYo.with(Techniques.Pulse).delay(0).playOn(view);
        view.setText(value + "");
    }

    public long getTime() {
        this.hour = Integer.valueOf(this.hourTextView.getText().toString());
        this.minute = Integer.valueOf(this.minuteTextView.getText().toString());
        this.second = Integer.valueOf(this.secondTextView.getText().toString());
        return TimeUnit.HOURS.toMillis(this.hour) + TimeUnit.MINUTES.toMillis(this.minute) + TimeUnit.SECONDS.toMillis(this.second);
    }

    public void setButton(boolean isRunning) {
        if (countdownPlayButton != null && isAdded()) {//can be null due to setButton() call in setTime which is called by service
            //isAdded bc getActivity fails in some cases of forced behaviour :D
            if (isRunning) {
                this.countdownPlayButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.pause));
            } else {
                this.countdownPlayButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.play));
            }
        }
    }

    public void stop() {
        YoYo.with(Techniques.Shake).duration(1000).playOn(countDownLayout);
        for (int i = 0; i < this.setFieldsArray.length; i++) {
            this.setFieldsArray[i] = 0;
        }
        this.countdownPlayButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.play));
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
        setCountdown(null);
        PrefUtils.timeService(getActivity(), PrefUtils.TYPE_COUNTDOWN, 0);
        PrefUtils.countDownRunning(getActivity(), false);
    }

    @Override
    public void onDestroy() {
        this.setFieldsArray = null;
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(this.receiver, new IntentFilter(CustomCountDown.COUNTDOWN_ACTION));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(this.receiver);
        super.onPause();
    }

    //eigentlich prepend
    public void append(Integer number) {
        if (!isRunning) {
            shiftLeft(this.setFieldsArray);
            setFieldsArray[0] = number;
            this.secondTextView.setText(String.format("%d%d", this.setFieldsArray[1], this.setFieldsArray[0]));
            this.minuteTextView.setText(String.format("%d%d", this.setFieldsArray[3], this.setFieldsArray[2]));
            this.hourTextView.setText(String.format("%d%d", this.setFieldsArray[5], this.setFieldsArray[4]));
        }
    }

    public void removeLast() {
        if (!isRunning) {
            shiftRigth(this.setFieldsArray);
            this.secondTextView.setText(String.format("%d%d", this.setFieldsArray[1], this.setFieldsArray[0]));
            this.minuteTextView.setText(String.format("%d%d", this.setFieldsArray[3], this.setFieldsArray[2]));
            this.hourTextView.setText(String.format("%d%d", this.setFieldsArray[5], this.setFieldsArray[4]));
        }
    }


    public int lastEntry(int[] array) {
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                index = i;
            }
        }
        return index + 1; //at least shift one
    }


    public void shiftLeft(int[] array) {
        int lastNumberIndex = lastEntry(array);
        for (int i = lastNumberIndex; i >= 0; i--) {
            if ((i + 1) < array.length) {
                array[i + 1] = array[i];
            }
        }
        array[0] = 0;
    }

    public void shiftRigth(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if ((i + 1) < array.length) {
                array[i] = array[i + 1];
            }
        }
        array[array.length - 1] = 0;
    }
}
