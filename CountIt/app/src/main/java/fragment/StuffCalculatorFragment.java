package fragment;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import calculator.Calculator;
import listener.CalcListener;
import utils.PrefUtils;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class StuffCalculatorFragment extends Fragment implements CalcListener {
    public static final String FRAGMENT_NAME = "at.fibuszene.countit.StuffCalculatorFragment";
    public static final String CALC_FIELD_EXTRA = "at.fibuszene.calculation_result_extra";

    private EditText calcField;
    private Handler handler = new Handler();
    private Drawable neutralBackground;
    private int neutralTextColor;

    public static StuffCalculatorFragment getInstance() {
        return new StuffCalculatorFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Calculator.getInstance().addListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stuff_calculator, null);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.calcField = (EditText) view.findViewById(R.id.calculatorTextField);
        this.neutralTextColor = calcField.getCurrentTextColor();
        this.neutralBackground = calcField.getBackground();
        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getString(R.string.tab_calculator));
        }
        updateDecPlaces(PrefUtils.decimalPlaces(getActivity()), view.findViewById(R.id.decPlacesButton));

        if (savedInstanceState != null && savedInstanceState.containsKey(CALC_FIELD_EXTRA)) {
            String extra = savedInstanceState.getString(CALC_FIELD_EXTRA);
            this.calcField.setText(extra);
        }

        activateButton();
    }

    public void activateButton() {
        try {
            ((HomeActivity) getActivity()).activateButton(R.id.calculatorButton);
        } catch (Exception e) {
            //Class cast: unlikely
            //NullPointer:
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.calculator, menu);
    }

    @Override
    public void setCalcField(String text) {
        if (this.calcField != null) {
            try {
                prettyPrint(text); //for results
            } catch (NumberFormatException nfe) {
                this.calcField.setText(text);//for expressions e.g. 7+2 bc the user pressed up
            }
        }
    }

    public void prettyPrint(String result) {
        result = result.replace(",", "."); //if decimal points are changed on the fly at least for my crappy htc
        String res = result;
        if (this.calcField != null) {
            try {
                double d = Double.valueOf(res);
                String format = "%.0" + PrefUtils.decimalPlaces(getActivity()) + "f";
                res = String.format(format, d);
            } catch (NumberFormatException nfe) {
                //e.g. divides by zero
                //res already equals result so do nothing
            }
            res = res.replace(".", ",");
            this.calcField.setText(res);
        }
    }

    @Override
    public void done(String result) {
        prettyPrint(result);
    }

    @Override
    public void append(String op) {
        if (this.calcField != null) {
            this.calcField.append(op);
        }
    }

    @Override
    public void remove() {
        if (this.calcField != null) {
            String text = calcField.getText().toString();
            try {
                text = text.substring(0, text.length() - 1);
                this.calcField.setText(text);

            } catch (StringIndexOutOfBoundsException iob) {
                this.calcField.setText(""); //empty
            }
        }
    }

    @Override
    public void clear() {
        if (this.calcField != null) {
            this.calcField.setText("");
        }
    }

    @Override
    public String getExpression() {
        return this.calcField.getText().toString().trim();
    }

    @Override
    public void error(final ErrorType type) {
        switch (type) {
            case empty_text:
                Utils.setBackground(calcField, getActivity().getResources().getDrawable(R.drawable.wrong_border));
                break;
            case empty_stack:
                calcField.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
                break;
            case mismatched_pars:
                Utils.setBackground(calcField, getActivity().getResources().getDrawable(R.drawable.wrong_border));
                calcField.setText("Syntax Error");
                break;
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (type) {
                    case empty_text:
                        Utils.setBackground(calcField, neutralBackground);
                        break;
                    case empty_stack:
                        calcField.setTextColor(neutralTextColor);
                        break;
                    case mismatched_pars:
                        Utils.setBackground(calcField, neutralBackground);
                        calcField.setText("Syntax Error");
                        break;
                }
            }
        }, 1000);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CALC_FIELD_EXTRA, this.calcField.getText().toString());
    }

    public void updateDecPlaces(int currentNumber, View view) {
        ImageButton button = (ImageButton) view;
        TypedArray array = getResources().obtainTypedArray(R.array.number_drawables);
        button.setImageDrawable(array.getDrawable(currentNumber));
        array = getResources().obtainTypedArray(R.array.cds);
        button.setContentDescription(array.getString(currentNumber));
        prettyPrint(calcField.getText().toString());
    }


}
