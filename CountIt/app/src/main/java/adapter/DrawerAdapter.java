package adapter;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import at.fibuszene.countit.HomeActivity;
import at.fibuszene.countit.R;
import model.Stuff;
import persistency.contracts.StuffContract;
import persistency.helper.StuffHelper;

/**
 * Created by benedikt.
 */
public class DrawerAdapter extends ResourceCursorAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    private Context context;

    public DrawerAdapter(Context context) {
        super(context, R.layout.drawer_nav_item, StuffHelper.list(context), false);
        this.context = context;
    }

    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).getId();
        } catch (Exception npe) {
            return 0;
        }
    }

    @Override
    public Stuff getItem(int position) {
        if (getCursor().moveToPosition(position)) {
            return Stuff.from(getCursor());
        } else {
            return null;
        }
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final Stuff item = Stuff.from(cursor);
        TextView txtView = (TextView) view.findViewById(R.id.navItemName);
        txtView.setText(item.getName());

        txtView = (TextView) view.findViewById(R.id.countTextView);
        txtView.setText(item.getCount() + "");

        ImageView button = (ImageView) view.findViewById(R.id.removeNavItem);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StuffHelper.delete(context, item.getId());
                ((HomeActivity) context).counterDeleted(item);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader loader = null;
        if (id == HomeActivity.DRAWER_LOADER) {
            loader = new CursorLoader(context, StuffContract.StuffEntry.CONTENT_URI, null, null, null, null);
        }
        return loader;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        swapCursor(null);
    }

    public void release() {
        this.getCursor().close();
    }
}
