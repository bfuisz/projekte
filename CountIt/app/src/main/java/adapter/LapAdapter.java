package adapter;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.countit.R;
import fragment.StuffTabFragment;
import model.LoaderData;
import model.Stuff;
import model.TmpTabLine;
import persistency.contracts.DatadumpContract;
import persistency.helper.StuffHelper;

/**
 * Created by benedikt.
 */
public class LapAdapter extends DatadumpAdatper {// implements LoaderManager.LoaderCallbacks<Cursor> {
    private Context context;
    private static StuffHelper.DataStringTypes type = StuffHelper.DataStringTypes.laps;
    private long stuffId;


    public LapAdapter(Context context, long stuffId) {
        super(context, R.layout.lap_list_item, StuffHelper.getStringData(context, type, stuffId),
                new LoaderData(stuffId, DatadumpContract.DatadumpEntry.buildIdURI(stuffId),
                        type, DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID + " = ? AND " + DatadumpContract.DatadumpEntry.COLUMN_TYPE + " = ?",
                        new String[]{stuffId + "", type.toString()}));

        this.stuffId = stuffId;
        this.context = context;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String item = cursor.getString(DatadumpContract.DatadumpEntry.COLUMN_DATA_INDEX);
        final long lapId = cursor.getLong(DatadumpContract.DatadumpEntry.COLUMN_ID_INDEX);
        TextView lineText = (TextView) view.findViewById(R.id.tabLineTextView);
        lineText.setText(item);
        ImageButton remove = (ImageButton) view.findViewById(R.id.removeTabLineButton);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(lapId);
            }
        });
    }


    public void remove(long lapId) {
        StuffHelper.deleteStringDataByID(context, lapId);
    }

    public void add(String line) {
        StuffHelper.saveStringData(context, type, line, this.stuffId);
    }

    public void clear() {
        StuffHelper.deleteStringDataForCounter(context, this.stuffId);
    }

}
