package adapter;

import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.countit.R;
import fragment.StuffTabFragment;
import model.LoaderData;
import model.Stuff;
import model.TmpTabLine;
import persistency.contracts.DatadumpContract;
import persistency.helper.StuffHelper;
import utils.TmpTabLineList;

/**
 * Created by benedikt.
 */
public class TablineAdapter extends DatadumpAdatper {
    private static StuffHelper.DataStringTypes type = StuffHelper.DataStringTypes.tabs;
    private Fragment fragment;

    public TablineAdapter(Fragment fragment, long stuffId) {
        super(fragment.getActivity(), R.layout.tab_list_item, StuffHelper.getStringData(fragment.getActivity(), type, stuffId),
                new LoaderData(stuffId, DatadumpContract.DatadumpEntry.buildIdURI(stuffId),
                        type, DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID + " = ? AND " + DatadumpContract.DatadumpEntry.COLUMN_TYPE + " = ?",
                        new String[]{stuffId + "", type.toString()}));
        this.fragment = fragment;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final TmpTabLine item = TmpTabLine.fromCursor(cursor);

        TextView lineText = (TextView) view.findViewById(R.id.tabLineTextView);
        lineText.setText(item.toString());
        ImageButton remove = (ImageButton) view.findViewById(R.id.removeTabLineButton);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(item.getId());
            }
        });
    }

    public void remove(long tmpLineID) {
        StuffHelper.deleteStringDataByID(context, tmpLineID);
        recalcuateTotal();
    }

    //suboptimal
    //da waer eine eigene tabelle natuerlich schon schoener
    public void recalcuateTotal() {
        float total = 0.0f;
        Cursor tmp = getCursor();
        if (tmp != null && tmp.moveToFirst()) {
            while (!tmp.isAfterLast()) {
                total += TmpTabLine.fromCursor(tmp).calculateCost();
                tmp.moveToNext();
            }
        }

        if (fragment instanceof StuffTabFragment) {
            String totalS = String.format("%.2f", total);
            ((StuffTabFragment) fragment).changeTotal(totalS);
        }
    }

    public void add(TmpTabLine line) {
        StuffHelper.saveStringData(context, type, line.toString(), loader.getStuffId());
        recalcuateTotal();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        super.onLoadFinished(loader, data);
        recalcuateTotal();
    }

    @Override
    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        recalcuateTotal();
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        Cursor tmp = super.swapCursor(newCursor);
        recalcuateTotal();
        return tmp;
    }
}
