package adapter;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import model.LoaderData;
import persistency.contracts.DatadumpContract;
import persistency.helper.StuffHelper;

/**
 * Created by benedikt.
 */
public abstract class DatadumpAdatper extends ResourceCursorAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    protected Context context;
    protected LoaderData loader;


    protected DatadumpAdatper(Context context, int layout, Cursor c, LoaderData loader) {
        super(context, layout, c, false);
        this.context = context;
        this.loader = loader;
    }

    @Override
    public int getCount() {
        return getCursor().getCount();
    }

    @Override
    public String getItem(int position) {
        getCursor().moveToPosition(position);
        return getCursor().getString(DatadumpContract.DatadumpEntry.COLUMN_DATA_INDEX);
    }


    @Override
    public long getItemId(int position) {
        getCursor().moveToPosition(position);
        return getCursor().getLong(DatadumpContract.DatadumpEntry.COLUMN_ID_INDEX);
    }

    @Override
    public abstract void bindView(View view, Context context, Cursor cursor);


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(context, this.loader.getUri(), null, this.loader.getSelection(), this.loader.getSelectionArgs(), null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        swapCursor(null);
    }

    public void release() {
        //TODO try to provoke exception "this should only be called when the cursor is valid"
//        getCursor().close();
    }
}
