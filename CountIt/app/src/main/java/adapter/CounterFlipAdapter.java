package adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.countit.R;
import fragment.StuffTabFragment;
import model.TmpTabLine;

/**
 * Created by benedikt.
 */
public class CounterFlipAdapter extends BaseAdapter {
    private Context context;
    private long size;

    public CounterFlipAdapter(Context context, long size) {
        this.context = context;
        this.size = size;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Long getItem(int position) {
        return size;
    }

    @Override
    public long getItemId(int position) {
        return size;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.counter_text_view, null);
        }
        TextView txtView = (TextView) convertView.findViewById(R.id.counterTextView);
        txtView.setText(size + "");
        return convertView;
    }


    public void setSize(long size) {
        this.size = size;
        notifyDataSetChanged();
    }

    public void add() {
        this.size++;
        notifyDataSetChanged();
    }

    public void remove() {
        this.size--;
        notifyDataSetChanged();
    }
}
