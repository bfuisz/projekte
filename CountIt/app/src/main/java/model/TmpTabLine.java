package model;

import android.database.Cursor;

import java.io.Serializable;

import persistency.contracts.DatadumpContract;

/**
 * Created by benedikt.
 */
public class TmpTabLine implements Serializable {
    private static final long serialVersionUID = -8440108698894692623L;
    private long id;
    private long stuffId;
    private float cost;
    private int amount;

    public TmpTabLine() {
    }

    public TmpTabLine(float cost, int amount) {
        this.cost = cost;
        this.amount = amount;
    }

    public static TmpTabLine fromCursor(Cursor cursor) {
//        There's a lot that could horribly go wrong :D
        TmpTabLine line = new TmpTabLine();
        line.id = cursor.getLong(DatadumpContract.DatadumpEntry.COLUMN_ID_INDEX);
        line.stuffId = cursor.getLong(DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID_INDEX);
        String strLine = cursor.getString(DatadumpContract.DatadumpEntry.COLUMN_DATA_INDEX); //format 3 x 4.0 = 12.0
        String[] split = strLine.split("x");
        String times = split[0].trim();
        line.amount = Integer.valueOf(times);
        times = split[1].split("=")[0].trim(); //4.0 = 12.0
        times = times.replace(",", ".");
        line.cost = Float.valueOf(times);
        return line;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStuffId() {
        return stuffId;
    }

    public void setStuffId(long stuffId) {
        this.stuffId = stuffId;
    }

    public float calculateCost() {
        return cost * amount;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TmpTabLine)) return false;

        TmpTabLine that = (TmpTabLine) o;

        if (amount != that.amount) return false;
        return Float.compare(that.cost, cost) == 0;

    }

    @Override
    public int hashCode() {
        int result = (cost != +0.0f ? Float.floatToIntBits(cost) : 0);
        result = 31 * result + amount;
        return result;
    }

    @Override
    public String toString() {
        return String.format("%d x %.02f = %.02f", amount, cost, (amount * cost));
    }
}

