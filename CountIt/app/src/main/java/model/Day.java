package model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.Date;
import java.util.List;

import persistency.contracts.DayContract;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class Day {

    private long id;
    private Date date;
    private List<Stuff> stuff;

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        if (this.id > 0) {
            values.put(DayContract.DayEntry._ID, this.id);
        }
        values.put(DayContract.DayEntry.COLUMN_DATE, Utils.toSQLDate(this.date));

        return values;
    }

    public static Day from(Cursor cursor) {
        Day day = new Day();
        day.id = cursor.getLong(DayContract.DayEntry.COLUMN_ID_INDEX);
        day.date = Utils.fromSQLDate(cursor.getString(DayContract.DayEntry.COLUMN_DATE_INDEX));
        return day;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Stuff> getStuff() {
        return stuff;
    }

    public void setStuff(List<Stuff> stuff) {
        this.stuff = stuff;
    }
}
