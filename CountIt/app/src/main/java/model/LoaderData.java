package model;

import android.net.Uri;

import persistency.helper.StuffHelper;

/**
 * Created by benedikt.
 */
public class LoaderData {
    private long stuffId;
    private Uri uri;
    private StuffHelper.DataStringTypes type;
    private String selection;
    private String[] selectionArgs;

    public LoaderData(long stuffId, Uri uri, StuffHelper.DataStringTypes type, String selection, String[] selectionArgs) {
        this.stuffId = stuffId;
        this.uri = uri;
        this.type = type;
        this.selection = selection;
        this.selectionArgs = selectionArgs;
    }

    public long getStuffId() {
        return stuffId;
    }

    public void setStuffId(long stuffId) {
        this.stuffId = stuffId;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    public void setSelectionArgs(String[] selectionArgs) {
        this.selectionArgs = selectionArgs;
    }
}
