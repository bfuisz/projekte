package model;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import persistency.contracts.DayContract;
import persistency.contracts.StuffContract;

/**
 * Created by benedikt.
 */
public class Stuff implements Serializable {
    private static final long serialVersionUID = 7513571302338522587L;
    private long id;
    private long dayStuffId;
    private String name;
    private long count;
    private String picture;


    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        if (this.id > 0) {
            values.put(StuffContract.StuffEntry._ID, this.id);
        }
        values.put(StuffContract.StuffEntry.COLUMN_NAME, this.name);
        values.put(StuffContract.StuffEntry.COLUMN_COUNT, this.count);
        if (this.picture != null) {
            values.put(StuffContract.StuffEntry.COLUMN_PICTURE, this.picture);
        }
        return values;
    }

    public static Stuff from(Cursor cursor) {
        Stuff stuff = new Stuff();
        stuff.id = cursor.getLong(DayContract.DayEntry.COLUMN_ID_INDEX);
        stuff.name = cursor.getString(StuffContract.StuffEntry.COLUMN_NAME_INDEX);
        stuff.count = cursor.getLong(StuffContract.StuffEntry.COLUMN_COUNT_INDEX);
        try {
            stuff.picture = cursor.getString(StuffContract.StuffEntry.COLUMN_PICTURE_INDEX);
        } catch (NullPointerException pe) {
            stuff.picture = null;
        }
        return stuff;
    }

    //FIXME die richtigen woerter fallen ma grad nit ein increment und ... (decrement ?)
    public void increase() {
        this.count++;
    }

    public void decrease() {
        this.count--;
    }

    public long getDayStuffId() {
        return dayStuffId;
    }

    public void setDayStuffId(long dayStuffId) {
        this.dayStuffId = dayStuffId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Stuff{" +
                "id=" + id +
                ", dayStuffId=" + dayStuffId +
                ", name='" + name + '\'' +
                ", count=" + count +
                ", picture='" + picture + '\'' +
                '}';
    }
}
