package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import model.Day;
import model.Stuff;
import persistency.contracts.DayContract;
import persistency.contracts.DayStuffMappingContract;
import persistency.contracts.StuffContract;

/**
 * Created by benedikt.
 */
public class DayHelper {

    public static Cursor list(Context context) {
        return context.getContentResolver().query(DayContract.DayEntry.CONTENT_URI, null, null, null, null);
    }

    public static List<Day> asList(Context context) {
        Cursor cursor = context.getContentResolver().query(DayContract.DayEntry.CONTENT_URI, null, null, null, null);
        List<Day> days = null;
        if (cursor.moveToFirst()) {
            days = new ArrayList<Day>();
            while (!cursor.isAfterLast()) {
                days.add(Day.from(cursor));
                cursor.moveToNext();
            }
        }
        return days;
    }


    public static Day get(Context context, long id) {
        Cursor cursor = context.getContentResolver().query(DayContract.DayEntry.CONTENT_URI, null, DayContract.DayEntry._ID + " = ?", new String[]{id + ""}, null);
        if (cursor.moveToFirst()) {
            return Day.from(cursor);
        }
        return null;
    }

    public static Day get(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()) {
            return Day.from(cursor);
        }
        return null;
    }

    public static Day insert(Context context, Day day) {
        Uri newDay = null;
        if (day != null) {
            newDay = context.getContentResolver().insert(DayContract.DayEntry.CONTENT_URI, day.toContentValues());
        }
        if (newDay != null) {
            return get(context, newDay);
        }
        return null;
    }

    public static int update(Context context, Day day) {
        if (day != null && day.getId() > 0) {
            return context.getContentResolver().update(DayContract.DayEntry.CONTENT_URI, day.toContentValues(), DayContract.DayEntry._ID + " = ?", new String[]{day.getId() + ""});
        }
        return -1;
    }


    public static int delete(Context context, Day day) {
        int deleted = -1;
        if (day != null) {
            deleted = delete(context, day.getId());
        }
        return deleted;
    }


    public static int delete(Context context, long id) {
        if (id > 0) {
            return context.getContentResolver().delete(DayContract.DayEntry.CONTENT_URI, DayContract.DayEntry._ID + " = ?", new String[]{id + ""});
        }
        return -1;
    }

    public static Cursor getCursordayWithStuff(Context context, long dayId) {
        if (dayId > 0) {
            return context.getContentResolver().query(DayStuffMappingContract.DayStuffMappingEntry.buildIdURI(dayId), null, null, null, null);
        }
        return null;
    }

    public static Cursor getCursordayWithStuff(Context context, Day day) {
        if (day != null) {
            return getCursordayWithStuff(context, day.getId());
        }
        return null;
    }

    public static Day getDayWithStuff(Context context, Day day) {
        if (day != null) {
            return getDayWithStuff(context, day.getId());
        }
        return null;
    }

    public static Day getDayWithStuff(Context context, long dayID) {
        Cursor cursor = context.getContentResolver().query(DayStuffMappingContract.DayStuffMappingEntry.buildIdURI(dayID), null, null, null, null);
        Day day = null;
        //FIXME faster would be to get it out of the joined table
        if (cursor.moveToFirst()) {
            day = get(context, cursor.getLong(1));
            List<Stuff> stuffs = new ArrayList<Stuff>();

            while (!cursor.isAfterLast()) {
                stuffs.add(StuffHelper.get(context, cursor.getLong(2)));
                cursor.moveToNext();
            }
            day.setStuff(stuffs);
        }
        return day;
    }


}
