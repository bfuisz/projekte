package persistency.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import at.fibuszene.countit.R;
import model.Stuff;
import persistency.contracts.DatadumpContract;
import persistency.contracts.StuffContract;
import service.CountItService;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class StuffHelper {

    public enum DataStringTypes {
        laps, tabs;
    }


    public static Cursor list(Context context) {
        return context.getContentResolver().query(StuffContract.StuffEntry.CONTENT_URI, null, null, null, null);
    }


    public static List<Stuff> asList(Context context) {
        Cursor cursor = context.getContentResolver().query(StuffContract.StuffEntry.CONTENT_URI, null, null, null, null);
        List<Stuff> stuffs = null;
        if (cursor.moveToFirst()) {
            stuffs = new ArrayList<Stuff>();
            while (!cursor.isAfterLast()) {
                stuffs.add(Stuff.from(cursor));
                cursor.moveToNext();
            }
        }
        return stuffs;
    }

    public static Stuff get(Context context, long id) {
        Cursor cursor = context.getContentResolver().query(StuffContract.StuffEntry.CONTENT_URI, null, StuffContract.StuffEntry._ID + " = ?", new String[]{id + ""}, null);
        if (cursor.moveToFirst()) {
            return Stuff.from(cursor);
        }
        return null;
    }

    public static Stuff get(Context context, String id) {
        Cursor cursor = context.getContentResolver().query(StuffContract.StuffEntry.CONTENT_URI, null, StuffContract.StuffEntry._ID + " = ?", new String[]{id}, null);
        if (cursor.moveToFirst()) {
            return Stuff.from(cursor);
        }
        return null;
    }


    public static Stuff insert(Context context, Stuff stuff) {
        Uri newStuff = null;
        if (stuff != null) {
            newStuff = context.getContentResolver().insert(StuffContract.StuffEntry.CONTENT_URI, stuff.toContentValues());
        }
        if (newStuff != null) {
            return get(context, Utils.extraFromURI(newStuff, 1));
        }
        return null;
    }

    public static int update(Context context, Stuff stuff) {
        if (stuff != null && stuff.getId() > 0) {
            return context.getContentResolver().update(StuffContract.StuffEntry.CONTENT_URI, stuff.toContentValues(), StuffContract.StuffEntry._ID + " = ?", new String[]{stuff.getId() + ""});
        }
        return -1;
    }


    public static int delete(Context context, Stuff stuff) {
        int deleted = -1;
        if (stuff != null) {
            deleted = delete(context, stuff.getId());
        }
        return deleted;
    }


    public static int delete(Context context, long id) {
        if (id > 0) {
            return context.getContentResolver().delete(StuffContract.StuffEntry.CONTENT_URI, StuffContract.StuffEntry._ID + " = ?", new String[]{id + ""});
        }
        return -1;
    }

    public static int getCount(Context context) {
        return context.getContentResolver().query(StuffContract.StuffEntry.CONTENT_URI, null, null, null, null).getCount();
    }


    public static Cursor getStringData(Context context, DataStringTypes type, long counterID) {
        return context.getContentResolver().query(DatadumpContract.DatadumpEntry.buildIdURI(counterID), null,
                DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID + " = ? AND " + DatadumpContract.DatadumpEntry.COLUMN_TYPE + " = ? AND " + DatadumpContract.DatadumpEntry.COLUMN_DATA + " is not null",
                new String[]{counterID + "", type.toString()}, null);
    }

    public static void saveStringData(Context context, DataStringTypes type, String lap, long counterId) {
        ContentValues values = new ContentValues();
        values.put(DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID, counterId);
        values.put(DatadumpContract.DatadumpEntry.COLUMN_DATA, lap);
        values.put(DatadumpContract.DatadumpEntry.COLUMN_TYPE, type.toString());
        context.getContentResolver().insert(DatadumpContract.DatadumpEntry.CONTENT_URI, values);
    }

    public static void deleteStringDataByID(Context context, long id) {
        context.getContentResolver().delete(DatadumpContract.DatadumpEntry.CONTENT_URI, DatadumpContract.DatadumpEntry._ID + " = ? ", new String[]{id + ""});
    }

    public static void deleteStringDataForCounter(Context context, long counterId) {
        context.getContentResolver().delete(DatadumpContract.DatadumpEntry.CONTENT_URI, DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID + " = ? ", new String[]{counterId + ""});
    }


    public static String extractPictures(Context context) throws IOException {
        File target = new File("images");
        target.mkdir();
        String name = "images";
        AssetManager assetManager = context.getAssets();

        String[] files = null;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            try {
                files = assetManager.list(name);
            } catch (IOException e) {
                Log.e("ERROR", "Failed to get asset file list.", e);
            }
            for (String filename : files) {
                InputStream in = null;
                OutputStream out = null;
                File folder = new File(Environment.getExternalStorageDirectory() + "/" + target.getName() + "/" + name);
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                if (success || folder.isDirectory()) {
                    try {
                        in = assetManager.open(name + "/" + filename);
                        out = new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + target.getName() + "/" + name + "/" + filename);
                        copyFile(in, out);
                        in.close();
                        in = null;
                        out.flush();
                        out.close();
                        out = null;
                    } catch (IOException e) {
                        Log.e("ERROR", "Failed to copy asset file: " + filename, e);
                    }
                } else {
                    // Do something else on failure
                }
            }
        }
        return Environment.getExternalStorageDirectory() + "/" + target.getName() + "/" + name + "/";
    }

    // Method used by copyAssets() on purpose to copy a file.
    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    public static String findByName(File[] files, String name) {
        for (File tmp : files) {
            if (tmp.getName().equals(name)) {
                return Uri.fromFile(tmp).toString();
            }
        }
        return "";
    }

    public static List<Stuff> createDefaultStuff(Context context) throws IOException {
        File f = new File(extractPictures(context));
        File[] images = f.listFiles();


        Stuff stuff = new Stuff();
        stuff.setCount(21);
        stuff.setName(context.getString(R.string.def_drink));
        stuff.setPicture(findByName(images, "drink.png"));

        insert(context, stuff);
        stuff = new Stuff();
        stuff.setCount(2);
        stuff.setName(context.getString(R.string.def_laps));
        stuff.setPicture(findByName(images, "lap.png"));
        insert(context, stuff);

        stuff = new Stuff();
        stuff.setCount(6);
        stuff.setName(context.getString(R.string.def_meals));
        stuff.setPicture(findByName(images, "meal.png"));
        insert(context, stuff);

        stuff = new Stuff();
        stuff.setCount(10);
        stuff.setName(context.getString(R.string.def_swearing));
        stuff.setPicture(findByName(images, "swearing.png"));
        insert(context, stuff);
        return asList(context);
    }
}
