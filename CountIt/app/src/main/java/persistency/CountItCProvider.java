package persistency;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import persistency.contracts.DatadumpContract;
import persistency.contracts.DayContract;
import persistency.contracts.DayStuffMappingContract;
import persistency.contracts.StuffContract;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class CountItCProvider extends ContentProvider {
    private DatabaseHelper helper;
    private static final UriMatcher uriMatcher = buildUriMatcher();


    private static final int DAY = 100;
    private static final int DAY_ID = 101;
    private static final int DAY_DATE = 102;

    private static final int STUFF = 200;
    private static final int STUFF_ID = 201;

    private static final int DAY_STUFF = 300;
    private static final int DAY_STUFF_ID = 301;

    private static final int DATA_DUMP = 400;
    private static final int DATA_DUMP_BY_STUFF_ID = 401;

    private static final SQLiteQueryBuilder sTasksByTaskListQuery;

    static {
        sTasksByTaskListQuery = new SQLiteQueryBuilder();
        //TaskListContract.TaskListEntry.TABLE_NAME inner join ttlpmapping as t2
        sTasksByTaskListQuery.setTables("(" + DayContract.DayEntry.TABLE_NAME + "  INNER JOIN " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME
                + " ON " + DayContract.DayEntry.TABLE_NAME + "." + DayContract.DayEntry._ID + " = " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME + "." + DayStuffMappingContract.DayStuffMappingEntry.COLUMN_DAY_ID + ")" +
                "INNER JOIN " + StuffContract.StuffEntry.TABLE_NAME + " on " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME + "." + StuffContract.StuffEntry._ID + " = " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME + "." + DayStuffMappingContract.DayStuffMappingEntry.COLUMN_STUFF_ID);
    }


    @Override
    public boolean onCreate() {
        this.helper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor = null;
        String extra;
        switch (uriMatcher.match(uri)) {

            case DAY:
                retCursor = helper.getReadableDatabase().query(
                        DayContract.DayEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            case DAY_ID:
                extra = Utils.extraFromURI(uri, 1);

                retCursor = helper.getReadableDatabase().query(
                        DayContract.DayEntry.TABLE_NAME,
                        projection,
                        DayContract.DayEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;

            case STUFF:
                retCursor = helper.getReadableDatabase().query(
                        StuffContract.StuffEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case STUFF_ID:
                extra = Utils.extraFromURI(uri, 1);
                retCursor = helper.getReadableDatabase().query(
                        DayContract.DayEntry.TABLE_NAME,
                        projection,
                        DayContract.DayEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;
            case DAY_STUFF:

                retCursor = sTasksByTaskListQuery.query(helper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DAY_STUFF_ID:
//                day id
                extra = Utils.extraFromURI(uri, 1);
                //where
                selection = DayStuffMappingContract.DayStuffMappingEntry.COLUMN_DAY_ID + " = ?";

                retCursor = sTasksByTaskListQuery.query(helper.getReadableDatabase(),
                        projection,
                        selection,
                        new String[]{extra},
                        null,
                        null,
                        sortOrder
                );
                break;


            case DATA_DUMP_BY_STUFF_ID:

                retCursor = helper.getReadableDatabase().query(
                        DatadumpContract.DatadumpEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );

                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        try {
            retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        } catch (NullPointerException npe) {
            //pretty much impossible
        }
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);
        String type = null;
        switch (match) {
            case DAY:
                type = DayContract.DayEntry.CONTENT_TYPE;
            case DAY_ID:
                type = DayContract.DayEntry.CONTENT_ITEM_TYPE;
                break;
            case STUFF:
                type = StuffContract.StuffEntry.CONTENT_TYPE;
                break;
            case STUFF_ID:
                type = StuffContract.StuffEntry.CONTENT_ITEM_TYPE;
                break;
            //lists
            case DAY_STUFF:
                type = DayStuffMappingContract.DayStuffMappingEntry.CONTENT_TYPE;
                break;
            case DATA_DUMP:
            case DATA_DUMP_BY_STUFF_ID:
                type = DatadumpContract.DatadumpEntry.CONTENT_TYPE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return type;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        Uri returnUri;
        long _id;
        switch (match) {
            case DAY:
                _id = db.insert(DayContract.DayEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = DayContract.DayEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            case STUFF:
                _id = db.insertOrThrow(StuffContract.StuffEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = StuffContract.StuffEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            case DAY_STUFF:
                _id = db.insert(DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = DayStuffMappingContract.DayStuffMappingEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            case DATA_DUMP:
                _id = db.insert(DatadumpContract.DatadumpEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = DatadumpContract.DatadumpEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {
            case DAY:
                rowsDeleted = db.delete(DayContract.DayEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case STUFF:
                rowsDeleted = db.delete(StuffContract.StuffEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case DAY_STUFF:
                rowsDeleted = db.delete(DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case DATA_DUMP:
                rowsDeleted = db.delete(DatadumpContract.DatadumpEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case DAY:
                rowsUpdated = db.update(DayContract.DayEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case STUFF:
                rowsUpdated = db.update(StuffContract.StuffEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case DAY_STUFF:
                rowsUpdated = db.update(DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    public static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String dayAuthority = DayContract.CONTENT_AUTHORITY;
        matcher.addURI(dayAuthority, DayContract.PATH_DAY, DAY);
        matcher.addURI(dayAuthority, DayContract.PATH_DAY + "/#", DAY_ID);
        matcher.addURI(dayAuthority, DayContract.PATH_DAY + "/*", DAY_DATE);

        final String stuffAuthority = StuffContract.CONTENT_AUTHORITY;
        matcher.addURI(stuffAuthority, StuffContract.PATH_STUFF, STUFF);
        matcher.addURI(stuffAuthority, StuffContract.PATH_STUFF + "/#", STUFF_ID);


        final String dayStuffAuthority = DayStuffMappingContract.CONTENT_AUTHORITY;
        matcher.addURI(dayStuffAuthority, DayStuffMappingContract.PATH_DAY_STUFF, DAY_STUFF); //all the stuff from one day with id #
        matcher.addURI(dayStuffAuthority, DayStuffMappingContract.PATH_DAY_STUFF + "/#", DAY_STUFF_ID); //all the stuff from one day with id #

        final String dataAuthority = DatadumpContract.CONTENT_AUTHORITY;
        matcher.addURI(dataAuthority, DatadumpContract.PATH_DATA + "/#", DATA_DUMP_BY_STUFF_ID); //all the stuff from one day with id #
        matcher.addURI(dataAuthority, DatadumpContract.PATH_DATA, DATA_DUMP); //all the stuff from one day with id #

        return matcher;
    }
}
