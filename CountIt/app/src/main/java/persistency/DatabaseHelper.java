package persistency;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import persistency.contracts.DatadumpContract;
import persistency.contracts.DayContract;
import persistency.contracts.DayStuffMappingContract;
import persistency.contracts.StuffContract;

/**
 * Created by benedikt.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "countit.db";
    public static final int DB_VERSION = 1;


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static final String CREATE_STUFF_TABLE = "create table " + StuffContract.StuffEntry.TABLE_NAME + "("
            + StuffContract.StuffEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + StuffContract.StuffEntry.COLUMN_NAME + " text not null, "
            + StuffContract.StuffEntry.COLUMN_COUNT + " int, "
            + StuffContract.StuffEntry.COLUMN_PICTURE + " text)";

    public static final String CREATE_DATA_TABLE = "create table " + DatadumpContract.DatadumpEntry.TABLE_NAME + "("
            + DatadumpContract.DatadumpEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DatadumpContract.DatadumpEntry.COLUMN_COUNTER_ID + " INTEGER REFERENCES " + StuffContract.StuffEntry.TABLE_NAME + "(" + StuffContract.StuffEntry._ID + ") ON DELETE CASCADE, "
            + DatadumpContract.DatadumpEntry.COLUMN_DATA + " text, "
            + DatadumpContract.DatadumpEntry.COLUMN_TYPE + " text)";

    public static final String CREATE_DAY_TABLE = "create table " + DayContract.DayEntry.TABLE_NAME + "("
            + DayContract.DayEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DayContract.DayEntry.COLUMN_DATE + " text not null)";

    public static final String CREATE_DAY_STUFF_TABLE = "CREATE TABLE " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME + "(" +
            DayStuffMappingContract.DayStuffMappingEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DayStuffMappingContract.DayStuffMappingEntry.COLUMN_DAY_ID + " INTEGER REFERENCES " + DayContract.DayEntry.TABLE_NAME + "(" + DayContract.DayEntry._ID + ") ON DELETE CASCADE, " +
            DayStuffMappingContract.DayStuffMappingEntry.COLUMN_STUFF_ID + " INTEGER REFERENCES " + StuffContract.StuffEntry.TABLE_NAME + "(" + StuffContract.StuffEntry._ID + ") ON DELETE CASCADE)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STUFF_TABLE);
        db.execSQL(CREATE_DAY_TABLE);
        db.execSQL(CREATE_DAY_STUFF_TABLE);
        db.execSQL(CREATE_DATA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("Drop Table " + StuffContract.StuffEntry.TABLE_NAME);
        db.execSQL("Drop Table " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME);
        db.execSQL("Drop Table " + DayStuffMappingContract.DayStuffMappingEntry.TABLE_NAME);
        db.execSQL("Drop Table " + DatadumpContract.DatadumpEntry.TABLE_NAME);
    }
}
