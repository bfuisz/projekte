package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class DayStuffMappingContract {
    public static final String CONTENT_AUTHORITY = "at.fibuszene.countit";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_DAY_STUFF = "day_stuff";

    public static final class DayStuffMappingEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DAY_STUFF).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_DAY_STUFF;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_DAY_STUFF;

        public static final String TABLE_NAME = "day_stuff";

        public static final String COLUMN_DAY_ID = "name";
        public static final String COLUMN_STUFF_ID = "stuff_count";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_DAY_ID_INDEX = 1;
        public static final int COLUMN_STUFF_INDEX = 2;


        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }
}
