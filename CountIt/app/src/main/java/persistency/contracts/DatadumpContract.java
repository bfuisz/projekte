package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 * Tmp table  to save strings like tab and laps
 * since I don't want anything fancy and the strings are pretty straightforward
 * this table is going to hold
 * id, counterid, string, type
 * where string is the data and either 4 * 45 = 180 (this format is dictated by the app so it stays the same)
 * or 00:00:00:00
 * and type either tabs or laps
 */
public class DatadumpContract {
    public static final String CONTENT_AUTHORITY = "at.fibuszene.countit";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_DATA = "stringdata";

    public static final class DatadumpEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DATA).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_DATA;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_DATA;

        public static final String TABLE_NAME = "stringdata";

        public static final String COLUMN_COUNTER_ID = "counterid";
        public static final String COLUMN_DATA = "string_data";
        public static final String COLUMN_TYPE = "type";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_COUNTER_ID_INDEX = 1;
        public static final int COLUMN_DATA_INDEX = 2;
        public static final int COLUMN_TYPE_INDEX = 3;


        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }
}
