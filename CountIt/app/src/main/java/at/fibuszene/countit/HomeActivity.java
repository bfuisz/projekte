package at.fibuszene.countit;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;

import adapter.DrawerAdapter;
import calculator.Calculator;
import fragment.StuffCalculatorFragment;
import fragment.StuffCountDownFragment;
import fragment.StuffItemFragment;
import fragment.StuffTabFragment;
import fragment.StuffTimerFragment;
import model.Stuff;
import persistency.helper.StuffHelper;
import service.CountItService;
import utils.PrefUtils;
import utils.Utils;


public class HomeActivity extends FragmentActivity implements AdapterView.OnItemClickListener {
    public static final int DRAWER_LOADER = 100;
    public static final int TAB_LOADER = 101;
    public static final int LAP_LOADER = 102;
    private static Context context;
    private static Vibrator v;
    public static final String SAVE_STUFF = "at.fibuszene.saved_stuff";
    public static final String CURRENT_TAB_FRAGMENT = "at.fibuszene.current_tab";
    public static String CURRENT_TAB_FRAGMENT_NAME;

    private Stuff stuff;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private DrawerAdapter adapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private Handler mHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;

        v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (PrefUtils.keepAwake(this)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        if (PrefUtils.firstStart(this)) {
            try {
                StuffHelper.createDefaultStuff(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.app_name) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(getResources().getString(R.string.app_name));
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(getResources().getString(R.string.drawer_title));
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerList = (ListView) findViewById(R.id.drawerList);
        mDrawerList.addHeaderView(createHeader());

        this.adapter = new DrawerAdapter(this);
        stuff = this.adapter.getItem(0);
        mDrawerList.setAdapter(adapter);
        getLoaderManager().initLoader(DRAWER_LOADER, null, adapter);

        mDrawerList.setOnItemClickListener(this);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        CURRENT_TAB_FRAGMENT_NAME = StuffItemFragment.FRAGMENT_NAME;
        //option 1 config, change different stuff item, ...
        Fragment frag = setFromSavedInstance(savedInstanceState);
        //option 2 user arrives from notification
        setFromAction();

        if (frag == null) {
            setFragment(CURRENT_TAB_FRAGMENT_NAME, stuff);
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, frag, CURRENT_TAB_FRAGMENT_NAME).commit();
        }
    }

    public Fragment setFromSavedInstance(Bundle savedInstanceState) {
        Fragment frag = null;

        if (savedInstanceState != null) {
            CURRENT_TAB_FRAGMENT_NAME = savedInstanceState.getString(CURRENT_TAB_FRAGMENT);
            frag = getSupportFragmentManager().getFragment(savedInstanceState, CURRENT_TAB_FRAGMENT_NAME);
            stuff = (Stuff) savedInstanceState.getSerializable(SAVE_STUFF);
        }
        return frag;
    }


    @Override
    protected void onResume() {
        super.onResume();
        setFromAction();
        getLoaderManager().restartLoader(DRAWER_LOADER, null, adapter);
        setFragment(CURRENT_TAB_FRAGMENT, stuff);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setFromAction();
        setFragment(CURRENT_TAB_FRAGMENT, stuff);
    }


    public void setFromAction() {
        if (getIntent() != null && getIntent().getAction() != null) {
            String action = getIntent().getAction();
            if (action.equals(StuffTimerFragment.FRAGMENT_NAME) || action.equals(StuffCountDownFragment.FRAGMENT_NAME)) {
                CURRENT_TAB_FRAGMENT_NAME = action;
            }
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        setFragment(StuffItemFragment.FRAGMENT_NAME, (adapter.getItem(position - 1)));
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        }, 150);
    }


    @Override
    protected void onDestroy() {
        try {
            ((StuffItemFragment) getSupportFragmentManager().findFragmentByTag(StuffItemFragment.FRAGMENT_NAME)).recycleBitMap();
        } catch (Exception e) {
            //Nullpointer can't really happen but still
            //or bitmap == null
        }
        super.onDestroy();
    }

    public View createHeader() {
        View header = View.inflate(this, R.layout.drawer_header, null);
        final EditText text = (EditText) header.findViewById(R.id.stuffCounterName);
        ImageButton submit = (ImageButton) header.findViewById(R.id.addStuffCounter);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = text.getText().toString();
                if (!TextUtils.isEmpty(name)) {
                    Stuff stuff = new Stuff();
                    stuff.setName(name);
                    StuffHelper.insert(HomeActivity.this, stuff);
                    text.setText("");
                } else {
                    Toast.makeText(HomeActivity.this, R.string.empty_text_exception, Toast.LENGTH_SHORT).show();
                    final Drawable neutral = text.getBackground();
                    Utils.setBackground(text, getResources().getDrawable(R.drawable.wrong_border));

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.setBackground(text, neutral);
                        }
                    }, 1500);
                }
            }
        });
        return header;
    }


    public long getStuffId() {
        return this.stuff.getId();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CURRENT_TAB_FRAGMENT, CURRENT_TAB_FRAGMENT_NAME);
        outState.putSerializable(SAVE_STUFF, stuff);

        try {
            Fragment frag = getSupportFragmentManager().findFragmentByTag(CURRENT_TAB_FRAGMENT_NAME);
            getSupportFragmentManager().putFragment(outState, CURRENT_TAB_FRAGMENT_NAME, frag);
        } catch (NullPointerException npe) {
//            it's possible
        }
    }

    public void activateButton(int id) {
        View view = findViewById(id);
        view.setEnabled(true);
        view.setActivated(true);
    }

    public void changeTabFragment(View view) {
        hapticFeedBack();
        activateButton(view.getId());
        switch (view.getId()) {
            case R.id.stuffItemButton:
                CURRENT_TAB_FRAGMENT_NAME = StuffItemFragment.FRAGMENT_NAME;
                break;
            case R.id.tabButton:
                CURRENT_TAB_FRAGMENT_NAME = StuffTabFragment.FRAGMENT_NAME;
                break;
            case R.id.timerButton:
                CURRENT_TAB_FRAGMENT_NAME = StuffTimerFragment.FRAGMENT_NAME;
                break;
            case R.id.countDownButton:
                CURRENT_TAB_FRAGMENT_NAME = StuffCountDownFragment.FRAGMENT_NAME;
                break;
            case R.id.calculatorButton:
                CURRENT_TAB_FRAGMENT_NAME = StuffCalculatorFragment.FRAGMENT_NAME;
                break;
            default:
                CURRENT_TAB_FRAGMENT_NAME = StuffItemFragment.FRAGMENT_NAME;
                break;
        }
        stuff = StuffHelper.get(this, this.stuff.getId());
        setFragment(CURRENT_TAB_FRAGMENT_NAME, stuff);
    }


    public void setFragment(String fragment, Stuff stuff) {
        Fragment tmp = getSupportFragmentManager().findFragmentByTag(fragment);
        if (fragment.equals(StuffItemFragment.FRAGMENT_NAME)) {
            if (tmp == null || this.stuff.getId() != stuff.getId()) {
                tmp = StuffItemFragment.getInstance(stuff);
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, tmp, StuffItemFragment.FRAGMENT_NAME).commit();
        }
        if (fragment.equals(StuffTabFragment.FRAGMENT_NAME)) {
            if (tmp == null || this.stuff.getId() != stuff.getId()) {
                tmp = StuffTabFragment.getInstance(stuff.getCount());
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, tmp, StuffTabFragment.FRAGMENT_NAME).commit();
        }
        if (fragment.equals(StuffTimerFragment.FRAGMENT_NAME)) {
            if (tmp == null || this.stuff.getId() != stuff.getId()) {
                tmp = StuffTimerFragment.getInstance(stuff.getId());
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, tmp, StuffTimerFragment.FRAGMENT_NAME).commit();
        }
        if (fragment.equals(StuffCountDownFragment.FRAGMENT_NAME)) {
            if (tmp == null || this.stuff.getId() != stuff.getId()) {
                tmp = StuffCountDownFragment.getInstance();
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, tmp, StuffCountDownFragment.FRAGMENT_NAME).commit();
        }
        if (fragment.equals(StuffCalculatorFragment.FRAGMENT_NAME)) {
            if (tmp == null || this.stuff.getId() != stuff.getId()) {
                tmp = StuffCalculatorFragment.getInstance();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.homeContainter, tmp, StuffCalculatorFragment.FRAGMENT_NAME).commit();
        }
        this.stuff = stuff;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (PrefUtils.useVolumeKeys(this) && (CURRENT_TAB_FRAGMENT_NAME.equals(StuffItemFragment.FRAGMENT_NAME))) {
            if ((event.getAction() == KeyEvent.ACTION_UP)) {

                StuffItemFragment tmp = (StuffItemFragment) getSupportFragmentManager().findFragmentByTag(CURRENT_TAB_FRAGMENT_NAME);
                hapticFeedBack();
                switch (event.getKeyCode()) {
                    case KeyEvent.KEYCODE_VOLUME_UP:
                        tmp.increaseCounter();
                        return true;
                    case KeyEvent.KEYCODE_VOLUME_DOWN:
                        tmp.decreaseCounter();
                        return true;
                }
            } else {
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_about) {
            Utils.showAbout(this);
            return true;
        }
        if (id == R.id.action_clearhistory) {
            Calculator.getInstance().clearHistory();
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    public void exprUp(View view) {
        hapticFeedBack();
        Calculator.getInstance().up();
    }

    public void exprDown(View view) {
        hapticFeedBack();

        Calculator.getInstance().down();
    }


    public void decPlaces(View view) {
        hapticFeedBack();

        String string = view.getContentDescription().toString();
        int places;
        try {
            places = Integer.valueOf(string);
        } catch (NumberFormatException nfe) {
            places = 2;
        }
        ++places;
        places = places % 10;
        PrefUtils.decimalPlaces(this, places);
        ((StuffCalculatorFragment) getSupportFragmentManager().findFragmentByTag(StuffCalculatorFragment.FRAGMENT_NAME)).updateDecPlaces(places, view);
    }

    public void operator(View view) {
        hapticFeedBack();
        Calculator calc = Calculator.getInstance();
        if (view.getId() == R.id.removeLast) {
            calc.remove();
        } else {
            String text = ((ImageButton) view).getContentDescription().toString();
            if (text.equals(getString(R.string.equals))) {
                calc.calculateResult();
            } else if (text.equals(getString(R.string.cd_clear))) {
                calc.clear();
            } else if (text.equals(getString(R.string.cd_ans))) {
                calc.ans();
            } else {
                calc.append(text);
            }
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        this.adapter.release();
        try {
            ((StuffItemFragment) getSupportFragmentManager().findFragmentByTag(StuffItemFragment.FRAGMENT_NAME)).recycleBitMap();
        } catch (Exception e) {
            //Nullpointer can't really happen but still
            //or bitmap == null
        }
    }

    public void timerStart(View view) {
        hapticFeedBack();
        Intent intent = new Intent(this, CountItService.class);
        intent.setAction(CountItService.ACTION_START_TIMER_TASK);//includes both start and stop the service takes care of the rest
        startService(intent);
    }


    public void timerStop(View view) {
        hapticFeedBack();
        Intent intent = new Intent(this, CountItService.class);
        intent.setAction(CountItService.ACTION_STOP_TIMER_TASK);
        startService(intent);
    }

    public void timerReset(View view) {
        hapticFeedBack();
        StuffTimerFragment frag = (StuffTimerFragment) getSupportFragmentManager().findFragmentByTag(StuffTimerFragment.FRAGMENT_NAME);
        if (frag != null) {
            timerStop(view);
            frag.reset();
        }
    }

    public void timerLap(View view) {
        hapticFeedBack();

        StuffTimerFragment frag = (StuffTimerFragment) getSupportFragmentManager().findFragmentByTag(StuffTimerFragment.FRAGMENT_NAME);
        if (frag != null) {
            frag.lap();
        }
    }

    public void startCountDown(View view) {
        hapticFeedBack();

        StuffCountDownFragment frag = (StuffCountDownFragment) getSupportFragmentManager().findFragmentByTag(StuffCountDownFragment.FRAGMENT_NAME);
        if (frag != null) {
            long time = frag.getTime();
            if (time > 0) {
                Intent intent = new Intent(this, CountItService.class);
                intent.setAction(CountItService.ACTION_START_COUNTDOWN_TASK);
                intent.putExtra(CountItService.COUNTDOWN_TIME_ARG, time);
                startService(intent);
            }
        }

    }

    public void stopCountDown(View view) {
        hapticFeedBack();
        Intent intent = new Intent(this, CountItService.class);
        intent.setAction(CountItService.ACTION_STOP_COUNTDOWN_TASK);
        startService(intent);
        ((StuffCountDownFragment) getSupportFragmentManager().findFragmentByTag(StuffCountDownFragment.FRAGMENT_NAME)).stop();
    }

    public void countDownNumber(View view) {
        hapticFeedBack();
        String text = ((ImageButton) view).getContentDescription().toString();
        Integer number = 0;
        try {
            number = Integer.valueOf(text);
            ((StuffCountDownFragment) getSupportFragmentManager().findFragmentByTag(StuffCountDownFragment.FRAGMENT_NAME)).append(number);
        } catch (NumberFormatException nfe) {
            number = 0;
            String error = String.format(getString(R.string.number_format_exception), text);
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        }
    }

    public void removeLast(View view) {
        hapticFeedBack();
        ((StuffCountDownFragment) getSupportFragmentManager().findFragmentByTag(StuffCountDownFragment.FRAGMENT_NAME)).removeLast();
    }

    @Override
    public void onBackPressed() {
        mDrawerLayout.closeDrawer(mDrawerList);
        hapticFeedBack();
//        super.onBackPressed();
    }

    public static void hapticFeedBack() {
        if (PrefUtils.hapticFeedBack(context)) {
            v.vibrate(20);
        }
    }

    public void counterDeleted(Stuff item) {
        if (this.stuff != null && this.stuff.getId() == item.getId()) {
            Stuff newItem = this.adapter.getItem(0);
            if (newItem != null) {
                setFragment(CURRENT_TAB_FRAGMENT_NAME, newItem);
                this.stuff = newItem;

            }
        }
    }
}
