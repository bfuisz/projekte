package calculator;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

import utils.PrefUtils;

public class StackCalculator {
    private Stack<String> opStack;
    private Queue<String> outValues;

    private static HashMap<String, Integer> operators;
    private String expr;

    public StackCalculator() {
        this.expr = new String();
        opStack = new Stack<String>();
    }

    static {
        operators = new HashMap<String, Integer>();
        operators.put("+", 4);
        operators.put("-", 4);
        operators.put("*", 5);
        operators.put("/", 5);
        operators.put("%", 5);
        // associativity hab nur Left
    }

    public void setExpr(String expr) {
        this.expr = expr;
    }


    private Double calculate(Double arg1, Double arg2, char operator) {
        switch (operator) {
            case '+':
                return arg1 + arg2;
            case '*':
                return arg1 * arg2;
            case '-':
                return arg1 - arg2;
            case '/':
                return arg1 / arg2;
            case '%':
                return arg1 % arg2;
            default:
                if (operator == '*' || operator == '/') {
                    return 1.0;
                } else {
                    return 0.0;
                }
        }
    }

    public static boolean isOperator(String op) {
        return operators.containsKey(op);
    }

    private int cmpPrecedence(String token1, String token2) {
        if (!isOperator(token1) || !isOperator(token2)) {
            throw new IllegalArgumentException("Invalied tokens: " + token1 + " " + token2);
        }
        return operators.get(token1) - operators.get(token2);
    }

    public void parse() throws EmptyStackException, NumberFormatException {
        this.outValues = new LinkedList<String>();
        String stackToken;

        if (this.expr != null) {

            StringTokenizer tokenize = new StringTokenizer(this.expr);

            while (tokenize.hasMoreTokens()) {
                String next = tokenize.nextToken();
                if (isOperator(next)) {

                    while (!opStack.empty() && isOperator(opStack.peek())) {
                        if ((cmpPrecedence(next, opStack.peek()) <= 0) || cmpPrecedence(next, opStack.peek()) < 0) {
                            outValues.add(opStack.pop());
                            continue;
                        }
                        break;
                    }
                    this.opStack.push(next);

                } else if (next.equals("(")) {
                    opStack.push(next);
                } else if (next.equals(")")) {
                    while (!(stackToken = opStack.pop()).equals("(")) {
                        outValues.add(stackToken);
                    }
                } else {
                    outValues.add(next);
                }
            }
        }

        while (!opStack.isEmpty()) {
            outValues.add(opStack.pop());
        }
    }


    private String evaluateStack() throws NumberFormatException {
        Stack<String> tmpStack = new Stack<String>();
        if (this.outValues != null && this.outValues.size() > 0) {
            double arg1, arg2;
            char op;
            String next = "";
            while (!this.outValues.isEmpty()) {
                next = this.outValues.poll();
                if (!next.equals("")) {
                    if (isOperator(next)) {
                        arg1 = Double.valueOf(tmpStack.pop());
                        arg2 = Double.valueOf(tmpStack.pop());
                        // due to pushing onto the tmpstack
                        tmpStack.push(calculate(arg2, arg1, next.toCharArray()[0]) + "");
                    } else {
                        tmpStack.push(next);
                    }
                }
            }
        }
        return tmpStack.pop();
    }

    public String calculate() throws EmptyStackException, NumberFormatException {
        if (this.expr == null) {
            return null;
        }
        parse();
        return evaluateStack();
    }

    public static void main(String[] args) {
        StackCalculator calc = new StackCalculator();
        String[] expressions = new String[]{
                " ( 4,93 + 17,4 ) • ( 16,5 – 3,21 )",
                " ( 36,94 – 21,06 ) • ( 3,05 + 21,7 : 7 ) ",
                " ( 39,03 : 13,01 + 9,34 ) • ( 23,5 • 2,3 – 46 )",
                " 4,93 + 17,4 • ( 16,5 – 3,21 )",
                " 36,94 + 21,06 • ( 3,05 + 21,7 : 7 ) ",
                " 39,03 : 13,01 + 9,34 • 23,5 • 2,3 – 46 ",
                " 4,93 + 17,4 • 16,5 – 3,21 ",
                " 36,94 + 21,06 • 3,05 + 21,7 : 7 ",
                " 4,93 + ( 17,4 • 16,5 – 3,21 ) "};

        String[] results = new String[]{
                "296,7657",
                "97,662",
                "99,337",
                "236,176",
                "166,459",
                "461,827",
                "288,82",
                "104,273",
                "288,82"
        };
        int i = 0;
        for (String expression : expressions) {
            expression = expression.replace(",", ".");
            expression = expression.replace(":", "/");
            expression = expression.replace("+", "+");
            expression = expression.replace("–", "-");
            expression = expression.replace("•", "*");
            System.out.print(expression);
            calc.setExpr(expression);// 4
            // falsch

            String result = calc.calculate();
            System.out.println(" = " + result);

            assert (results[i].equals(result));
            i++;
        }
    }
}
