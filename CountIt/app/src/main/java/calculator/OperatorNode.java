package calculator;

/**
 * Created by benedikt.
 */
public class OperatorNode {

    public static enum Type {
        Operation, Number
    }

    private char op;
    private float number;
    private Type type;
    private OperatorNode left;
    private OperatorNode right;

    public char getOp() {
        return op;
    }

    public void setOp(char op) {
        this.op = op;
    }

    public float getNumber() {
        return number;
    }

    public void setNumber(float number) {
        this.number = number;
    }

    public OperatorNode getLeft() {
        return left;
    }

    public void setLeft(OperatorNode left) {
        this.left = left;
    }

    public OperatorNode getRight() {
        return right;
    }

    public void setRight(OperatorNode right) {
        this.right = right;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

    public OperatorNode(char op) {
        super();
        this.type = Type.Operation;
        this.op = op;
    }

    public OperatorNode(float number) {
        super();
        this.type = Type.Number;
        this.number = number;
    }

}
