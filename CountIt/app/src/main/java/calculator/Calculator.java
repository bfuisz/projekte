package calculator;

import android.text.TextUtils;
import android.util.Log;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import listener.CalcListener;

/**
 * Created by benedikt.
 */
public class Calculator implements Serializable {
    private static Calculator instance;
    private CalcListener listener;
    private Tree tree;
    private StackCalculator stackCalculator;
    private List<String> expressions;
    private int history = 0;
    private String ans;
    private boolean wasDone;

    private Calculator() {
        ans = "";
        this.expressions = new ArrayList<String>();
        this.stackCalculator = new StackCalculator();
    }

    public static Calculator getInstance() {
        if (instance == null) {
            instance = new Calculator();
        }
        return instance;
    }

    public void clearHistory() {
        this.expressions.clear();
        this.history = 0;
        this.ans = "";

        if (listener != null) {
            listener.clear();
        }
    }

    public void addListener(CalcListener listener) {
        this.listener = listener;
    }


    public String prepareString(String expr) {
        expr = expr.replace("ANS ", "" + ans);
        expr = expr.replace(",", ".");

        return expr.replace("/", " / ")
                .replace("*", " * ")
                .replace("-", " + -")
                .replace("+", " + ")
                .replace("%", " % ")
                .replace("(", " ( ")
                .replace(")", " ) ");
    }


    public void calculateResult() {
        String expr = listener.getExpression();

        if (!TextUtils.isEmpty(expr)) {
            try {
                String explodedExpression = prepareString(expr).trim();

                if (explodedExpression.charAt(0) == '+') { //neutral element to balance equation like
                    // so -6 / 9 becomes + -6 / 9 => 0 + -6 / 9
                    //due to the if clause in append no other operator can be the first operator so don't worry about multiplikation neutral element
                    explodedExpression = 0 + " " + explodedExpression;
                }
                Log.d("expr", explodedExpression);
                stackCalculator.setExpr(explodedExpression);
                stackCalculator.parse();
                String result = stackCalculator.calculate();
                this.ans = result;
                this.expressions.add(expr);
                history = this.expressions.size();
                wasDone = true;
                if (listener != null) {
                    listener.done(result);
                }
            } catch (EmptyStackException ese) {
                listener.error(CalcListener.ErrorType.empty_stack);
            } catch (NumberFormatException nfe) {
                listener.error(CalcListener.ErrorType.mismatched_pars);
            }

        } else {
            listener.error(CalcListener.ErrorType.empty_text);
        }
    }


    public void ans() {
        if (listener != null) {
            if (ans == null) {
                ans = "";
            }
            listener.setCalcField(ans);
        }
    }


    public void append(String op) {
        if (wasDone && !StackCalculator.isOperator(op)) { //e.g. result and then 3
            listener.setCalcField("");
            wasDone = false;
        } else { //e.g. result then + 3 don't reset calc field
            wasDone = false;
        }

        if (listener != null) {
            String expr = listener.getExpression(); //e.g. expr = ""; op =  +; ans = 4;
            //op becomes ANS +
            //later to be replaced by 4 + ...
            if (TextUtils.isEmpty(expr) && StackCalculator.isOperator(op) && !ans.equals("") && !op.equals("-")) { //first char "-" is okay
                op = "ANS " + op;
            }
            listener.append(op);
        }
    }

    public void remove() {
        if (listener != null) {
            String expr = listener.getExpression();
            if (!TextUtils.isEmpty(expr) && (expr.length() - 1) > 0) {
                expr = expr.substring(0, expr.length() - 1);
                listener.setCalcField(expr);
            }
        }
    }

    public void clear() {
        this.wasDone = false;
        if (listener != null) {
            listener.clear();
        }
    }

    public void up() {
        wasDone = false;

        try {
            if (listener != null) {
                listener.setCalcField(this.expressions.get(--history));
            }
        } catch (IndexOutOfBoundsException iob) {
            this.history = 0;
        }
    }

    public void down() {
        wasDone = false;

        try {
            if (listener != null) {
                listener.setCalcField(this.expressions.get(++history));
            }
        } catch (IndexOutOfBoundsException iob) {
            this.history = expressions.size();
            listener.clear();
        }
    }
}
    /*
     *May not be pretty but it works

    public void calculateResult() {
        String result = "";

        this.tree = new Tree();

        // 2 + 3
        // 2 + 3 * 4
        // 3 * (2 + (5+4))
        // 3 * (2 + 5)
        Character[] operatores = new Character[]{'+', '*', '-', '/', '(', ')'};
        List<Character> charList = new ArrayList<Character>();
        for (Character tmp : operatores) {
            charList.add(tmp);
        }

        if (expr != null) {
            this.expr = expr.trim().replace(" ", "");

            char[] arr = this.expr.toCharArray();
            String number = "";

            for (int i = 0; i < arr.length; i++) {
                number = "";

                while (i < arr.length && !charList.contains(arr[i])) {
                    number += arr[i];
                    i++;
                }
                if (number != "") {
                    tree.insert(number); // actual number
                }

                if (i < arr.length) {
                    number = arr[i] + "";
                    tree.insert(number);// operator
                }
            }
        }

        result += tree.evaluate();
        this.ans = result;
        this.expressions.add(expr);
        history = this.expressions.size();
        this.expr = result;
        if (listener != null) {
            listener.done(result);
        }
    }
         */