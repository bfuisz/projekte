package calculator;

/**
 * Created by benedikt.
 */
public class Tree {
    private OperatorNode root;

    public void insert(String insert) {
        float fNmuber = -1;
        char op = ' ';
        try {
            fNmuber = Float.valueOf(insert);
        } catch (NumberFormatException ne) {
            op = insert.toCharArray()[0];
        }
        if (root != null) {
            if (op != ' ') { //
                OperatorNode newRoot = new OperatorNode(op);
                newRoot.setLeft(this.root);
                this.root = newRoot;
            } else if (root.getLeft() == null) {
                root.setLeft(new OperatorNode(fNmuber));
            } else if (root.getRight() == null) {
                root.setRight(new OperatorNode(fNmuber));

                if (root.getOp() == '*' || root.getOp() == '/') {
                    OperatorNode tmpLeft = this.root.getLeft();

                    if (tmpLeft != null && tmpLeft.getRight() != null) {
                        root.setLeft(tmpLeft.getRight());
                        tmpLeft.setRight(root);
                        this.root = tmpLeft;
                    }
                }
            }
        } else {
            if (op != ' ') {
                throw new RuntimeException("+3 ??? macht wenig sinn");
            } else {
                root = new OperatorNode(fNmuber);
            }
        }
    }

    public float result(OperatorNode operation) {
        float result = 0.0f;
        switch (operation.getOp()) {
            case '+':
                result = operation.getLeft().getNumber() + operation.getRight().getNumber();
                break;
            case '-':
                result = operation.getLeft().getNumber() - operation.getRight().getNumber();
                break;
            case '*':
                result = operation.getLeft().getNumber() * operation.getRight().getNumber();
                break;
            case '/':
                result = operation.getLeft().getNumber() / operation.getRight().getNumber();
                break;
        }
        return result;
    }

    public float evaluateResult(OperatorNode node) {
        OperatorNode tmp = node;
        float result = 0.0f;
        while (node.getLeft() != null) {
            if (node.getLeft().getType() == OperatorNode.Type.Operation) {
                node = node.getLeft();
            } else if (node.getLeft().getType() == OperatorNode.Type.Number && node.getRight().getType() == OperatorNode.Type.Number) {
                result = result(node); // -> next node is a number
                node.setLeft(null);
                node.setRight(null);
                node.setNumber(result);
                node.setType(OperatorNode.Type.Number);
                node = tmp;
            } else {
                node = node.getRight();
            }

        }
        return node.getNumber();
    }

    public float evaluate() {
        OperatorNode tmp = this.root;
        return evaluateResult(tmp);
    }

}
