package utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

/**
 * Created by benedikt.
 */
public class PrefUtils {

    public static final String TYPE_TIMER = "at.fibuszene.type_timer";
    public static final String TYPE_COUNTDOWN = "at.fibuszene.type_countdown";

    public static int decimalPlaces(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("decimal_places", 2);
    }

    public static void decimalPlaces(Context context, int decPlaces) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt("decimal_places", decPlaces).commit();
    }


    public static boolean firstStart(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean firstStart = prefs.getBoolean("first_start", true);
        if (firstStart == true) {
            prefs.edit().putBoolean("first_start", false).commit();
        }

        return firstStart;
    }

    public static boolean showNotifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("action_bar_notifications", true);
    }


    public static boolean notifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications", true);
    }

    public static String playSound(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_sound", null);
    }

    public static boolean vibrate(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notification_vibrate", true);
    }

    public static boolean animateCounter(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("counter_animation", true);
    }

    public static boolean showLeftMinus(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("left_minus_enabled", true);
    }

    public static boolean showRightMinus(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("rigth_minus_enabled", true);
    }

    public static boolean keepAwake(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("keep_awake", false);

    }

    public static boolean hapticFeedBack(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("haptic_feedback", true);

    }

    //FIXME shared prefs not the best place to save this move to db or something if every counter should receive it's own time
    public static long timeService(Context context, String type) {//type being countdown or timer
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(type, 0);
    }

    public static void timeService(Context context, String type, long time) {//type being countdown or timer
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(type, time).commit();
    }

    public static boolean timerRunning(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("timer_running", false);
    }

    public static void timerRunning(Context context, boolean toggle) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("timer_running", toggle).commit();
    }

    public static boolean countDownRunning(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("count_down_running", false);
    }

    public static void countDownRunning(Context context, boolean toggle) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("count_down_running", toggle).commit();
    }

    public static boolean useVolumeKeys(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("use_volumen_buttons", false);
    }
}
