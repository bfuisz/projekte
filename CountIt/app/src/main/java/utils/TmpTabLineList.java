package utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.TmpTabLine;

/**
 * Created by benedikt.
 */
public class TmpTabLineList extends ArrayList<TmpTabLine> implements Serializable {
    private static final long serialVersionUID = -130716075692437072L;
}
