# README #

CountIt entstand in einer "Projektpause". 
Eigentliches Ziel, war es einen simplen Zähler zu schreiben. 
Der Zähler sollte nur ein Bild und den Namen des zu zählenden Objekts darstellen.
Allerdings wurde aufgrund schlecht definierte Abgrenzungskriterien (nämlich gar keiner) 
eher eine Ansammlung von verschiedenen Tools daraus. 

1. Counter 
	+ Der Zähler mit Namen und Bild.
		
1. Tab 
	+ Ein Rechner der einzelne Zeilen aufaddiert. 
		
1. Rechner 
	+ Ein einfacher Taschenrechner 
		
1. Stoppuhr
	+ Eine einfache Stoppuhr
		
1. Countdown 
	+ Ein einfacher Countdown 
		
		
### Aufbau ###

Die App besteht im groben aus zwei Activities (HomeActivity, SettingsActivity). 
Die einzelnen Komponenten werden durch Fragmente dargestellt.
Über das DrawerLayout besteht die Möglichkeit neue Zähler hinzuzufügen.  
Die Datenbank besteht im großen und ganzen aus einer richtigen Tabelle nämlich "stuff". 
Die "stringdata" Tabelle dient nur zum Speichern temporäre Daten wie zum Beispiel Rundenzeiten der Stoppuhr 
und Tablines im Tabfeature. 

### Counter ###

Der Kern der App. Der User kann wahlweise durch klicken auf die ImageView oder durch den Vol+ Button 
hin aufzählen und durch klicken auf den Minus oder Vol- Button hinunter zählen. 
Der Stand des Counters, der Pfad des verwendeten Bilds und der Name des Counters werden in der Tabelle
"stuff" gespeichert.

### Tab ###

Mit diesem Feature können mehrere Gegenstände mit der gewünschten Anzahl 
multipliziert und aufaddiert werden.
Im Prinzip die gleiche Funktion wie eine Scannerkasse im Supermarkt nur simpler.  

### Rechner ###

Ein Taschenrechner mit Grundrechnungsarten, Klammern und Ergebnisspeicher. 
Die Implementierung war ursprünglich ein Tree wurde allerdings gegen eine 
wartbarere Stack Implementierung ausgetauscht. 

### Stoppuhr ###

Eine Stoppuhr mit Rundenzeiten und schnell zugriff auf den Zähler. 
Angenommen der User will "Runden" zählen und gleichzeitig die Rundenzeit messen. 

### Countdown ###

Weils geht. 

## TODOs ##

 + Stoppuhr und Countdown uique für jeden Zähler. 
	Im Moment gibt es für alle Zähler nur eine Stoppuhr und einen Countdown. 
	Das bedeutet, wird ein Countdown für den Zähler "Runden" gestartet und der User wechselt zum Zähler
	"Getränke" und dort auf den Tab Countdown, läuft dort der gleiche Countdown. 
 
 + Cursorloader richtig verwenden.  

## Lerneffekt ##

Umgang mit Services und ContentProvidern. 

		