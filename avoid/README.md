# README #

Bei diesem mit Unity entwickelte Spiel geht es darum, 
so lange wie m�glich den fliegenden Objekten auszuweichen.
Ziel der Entwicklung dieser App war es nicht der n�chste 
Google Play Store Hit zu werden, sondern zu sehen was 
ben�tigt wird um in Unity ein simples 2D Spiel f�r Android zu bauen. 

### Aufbau ###

Der interessanteste Folder ist "Assets". Dort befindet sich alles was mit dem Spiel zu tun hat. 
Im Folder "Scenes" werden die Szenen aus dem das Spiel besteht gespeichert. 
Eine Szene wiederum, dient dazu GameObjects darzustellen.
Ein GameObject ist, nach dem erstellen, ein leeres Objekt in einer Szene und kann durch Komponenten erweitert werden. 
Zum Beispiel kann ein Ball als GameObject dargestellt werden.
Folgende Tabelle listet die Komponenten des GameObjects Ball: 

Komponente  | Funktion
------------- | -------------
Transform  	  	| Position, Rotation und Skalierung in der Szene. 
Sprite Renderer | Renderd die Textur des Balles. Au�erdem k�nnen hier Materialeigenschaften angegeben werden. 
Rigidbody 2D	| Gibt dem Ball physikalische Gr��en wie zum Beispiel Masse. Au�erdem kann bestimme werden, ob und wie gro� der Einfluss der Gravitation sein soll.  
Box Collider 2D | Eine zweite "Schicht" rund um dem Ball der zur Collision Detection dient.
Script(Ball Behaviour*)| Steuert den Ball. 

* im Folder Assets\BallBehaviour.cs
	Ohne dieses Script w�rde der Ball nichts tun.(je nach Gravitation und Masse im Raum herumliegen oder aus der Szene fallen) 
	In diesem Script gibt es ein Attribut speed, welches vom GameBehaviour ver�ndert werden kann um die Schwierigkeit zu �ndern. 
	Die Funktion Update(), die periodisch Aufgerufen wird, verwandelt den Speed in eine Transformation um und so kann sich der Ball
	entlang der x und y Achsen bewegen. 
	Da dieses Script genaugenommen keinen Ball, sondern eine Kanonenkugel steuert, verliert der Spieler ein Leben bei 
	einer Kollision zwischen Finger und Kugel. Das passiert in der Callbackmethode OnMouseEnter welche vom Box Collider 2D 
	aufgerufen wird.

Die in der Tabelle genannten Komponenten stellen nur einen Bruchteil der Komponenten dar, die zur Auswahl stehen. 
Weitere Komponenten w�ren zum Beispiel Sound, Animationen, Effekte wie Spur Renderer und Partikelsysteme und viele mehr.  

## Lerneffekt ##

Umgang mit Unity, etwas C#, dass ich kein Designer bin, 
