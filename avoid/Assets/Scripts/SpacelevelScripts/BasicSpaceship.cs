﻿using UnityEngine;
using System.Collections;

public class BasicSpaceship : MonoBehaviour {
	public float speedX; 
	public float speedY; 
	protected float time = 0;
	protected GameBehaviour gameBehav; 

	public void Start(){
		this.gameBehav = (GameBehaviour)GameObject.Find("Background").GetComponent ("GameBehaviour");
	}
	
	public void OnTriggerEnter2D(Collider2D collider) {
		if(collider.name.Equals("LeftWall")){
			Destroy(this.gameObject, 1); 
		}
	}

	public void IncreaseSize(){

	}
		
	public void IncreaseSpeed(){
		transform.rigidbody2D.velocity *= 1.3f; 
	}
	
	void OnMouseEnter(){
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2)){
			gameBehav.RemoveHeart(); 
		}
	}
	

}
