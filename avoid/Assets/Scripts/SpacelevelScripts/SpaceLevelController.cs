﻿using UnityEngine;
using System.Collections;

public class SpaceLevelController : MonoBehaviour {
	public float timer = 10;

	
	void Start () {
		Instantiate(Resources.Load("Prefabs/Spaceship")); 
		Instantiate(Resources.Load("Prefabs/Rocketship")); 
	
	}
	public void SpawnDelayed(string name){
		int random = ((int)(Random.value * 100) % 4);
		name = name.Replace("(Clone)", ""); 
		StartCoroutine(SpawnDelay(random, name)); 			
	}
	
	IEnumerator SpawnDelay(int random, string name){
		yield return new WaitForSeconds(random);
		Instantiate(Resources.Load("Prefabs/"+name)); 
	}
	
	public void SpeedUp(){
		
	}
	
	public void Enlarge(){
		
	}

	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("menu"); }

		timer -= Time.deltaTime;
		if (timer <= 0){
			int random = ((int)(Random.value * 10)) % 2; 
			if(random == 0){
				//SpeedUp();
			}else if(random == 1){
				//Enlarge(); 
			}
			timer = 10; 
		}	
	}
}
