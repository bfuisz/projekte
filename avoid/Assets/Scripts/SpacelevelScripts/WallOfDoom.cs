﻿using UnityEngine;
using System.Collections;

public class WallOfDoom : MonoBehaviour {
	private SpaceLevelController spawn; 
	// Use this for initialization
	void Start () {
		spawn = (SpaceLevelController)GameObject.Find("LevelController").GetComponent ("SpaceLevelController");
	}


	void OnTriggerEnter2D(Collider2D collider) {
		if(!collider.name.Equals("Bullet(Clone)")){
			spawn.SpawnDelayed(collider.gameObject.name); 
		}
	}

}
