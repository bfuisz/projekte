﻿using UnityEngine;
using System.Collections;

public class StraightRocketShip : BasicSpaceship {

	void Start () {
		base.Start(); 
		int randomY = ((int)(Random.value * 100) % 4);
		int plusOrMin = ((int)(Random.value * 100) % 2);
		if(plusOrMin == 0){
			randomY = (randomY * -1); 
		}
		Vector3 tmp = transform.position;
        tmp.y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y; //randomY; 
		transform.position = tmp; 
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 tmp = transform.position; 
		tmp.x += speedX * Time.deltaTime; 
		transform.position = tmp; 
	}
}
