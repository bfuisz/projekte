﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {
	public float speedX = -6; 
	protected GameBehaviour gameBehav; 
	
	public void Start(){
		this.gameBehav = (GameBehaviour)GameObject.Find("Background").GetComponent ("GameBehaviour");
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.name.Equals("LeftWall")){
			Destroy(this.gameObject,1); 
		}
	}
		
	void Update () {
		Vector3 tmp = transform.position; 
		tmp.x += speedX * Time.deltaTime; 
		transform.position = tmp; 
	}

	void OnMouseEnter(){
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2)){
			gameBehav.RemoveHeart(); 
		}
	}
}
