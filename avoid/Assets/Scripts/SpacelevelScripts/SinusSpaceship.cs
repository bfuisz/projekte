﻿using UnityEngine;
using System.Collections;

public class SinusSpaceship : BasicSpaceship {
	protected bool falling;

	public void OnTriggerEnter2D(Collider2D collider) {
		base.OnTriggerEnter2D(collider); 
		if(collider.name.Equals("Ceiling")){
			falling = true; 
		}else if(collider.name.Equals("Floor")){
			falling = false; 
		}
		this.speedY = 0; 
	}

	// Use this for initialization
	void Start () {
		base.Start(); 
		this.speedY = 0; 
		this.falling = true; 

		int randomY = ((int)(Random.value * 100) % 4);
		int plusOrMin = ((int)(Random.value * 100) % 2);
		if(plusOrMin == 0){
			randomY = (randomY * -1); 
		}
		Vector3 tmp = transform.position; 
		tmp.y = randomY; 
		transform.position = tmp; 
	}

	public void Fire(){
		Vector3 tmp = this.transform.position; 
		tmp.x -= 0.4f; 
		 Instantiate(Resources.Load("Prefabs/Bullet"), tmp, Quaternion.identity);
		tmp.x -= 0.4f; 
		 Instantiate(Resources.Load("Prefabs/Bullet"), tmp, Quaternion.identity);
		tmp.x -= 0.4f; 
		Instantiate(Resources.Load("Prefabs/Bullet"), tmp, Quaternion.identity);
	}
	
	void Update(){
		Vector3 tmp = transform.position; 
		tmp.x += speedX * Time.deltaTime; 
		tmp.y += speedY * Time.deltaTime;
		transform.position = tmp; 

		if(falling){
			this.speedY -= 0.1f;
		}else{
			this.speedY += 0.1f;
		}

		time -= Time.deltaTime; 
		if(time <= 0){
			Fire(); 
			time = 3; 
		}
	}
}
