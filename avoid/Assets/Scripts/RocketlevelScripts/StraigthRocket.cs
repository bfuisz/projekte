﻿using UnityEngine;
using System.Collections;

public class StraigthRocket : BasicRocketBehaviour {
	protected Vector3 target; 

	public void Start () {
		base.Start(); 
	}

	public void SetMousePosition(Vector3 mousepos){
		this.target = mousepos; 
	}

	public void Update () {
		if(target != null ){
			if(!this.transform.position.Equals(this.target)){
				transform.position = Vector3.MoveTowards(transform.position, target, speedX * Time.deltaTime);
			}else{
				base.DestroyRocket(); 
			}
		}
	}
}
