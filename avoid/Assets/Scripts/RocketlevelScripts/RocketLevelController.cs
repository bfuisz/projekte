﻿using UnityEngine;
using System.Collections;

public class RocketLevelController : MonoBehaviour {
	public float timer = 10; 
	private LauncherBehaviour launcher;

	void Start () {
		this.launcher = (LauncherBehaviour) GameObject.Find("Launcher").GetComponent ("LauncherBehaviour");
	}

    public void SpeedUp()
    {
        BasicRocketBehaviour.speedX += 1; 
    }

    public void Faster()
    {
        if(LauncherBehaviour.fireTimer > 0){
            LauncherBehaviour.fireTimer -= 0.1f; 
        }
    }

    public void FireShrapnel()
    {
        launcher.FireShrapnel(); 
    }

	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("menu"); }

		timer -= Time.deltaTime;
		if (timer <= 0){
            int random = ((int)(Random.value * 10)) % 3; 
			if(random == 0){
				SpeedUp();
			}else if(random == 1){
				Faster();
            }else if (random == 2){
                FireShrapnel();
            }
			
			timer = 10; 
		}	
	}
}
