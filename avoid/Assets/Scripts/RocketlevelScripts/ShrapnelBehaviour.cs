﻿using UnityEngine;
using System.Collections;

public class ShrapnelBehaviour : BasicRocketBehaviour {
    public float xSpeed = 5, ySpeed;  
	
	// Update is called once per frame
	void Update () {
        Vector3 tmp = this.transform.position;
        tmp.x += xSpeed * Time.deltaTime;
        tmp.y += ySpeed * Time.deltaTime;
        this.transform.position = tmp; 
	}
}
