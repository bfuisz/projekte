﻿using UnityEngine;
using System.Collections;

public class ShrapnelRocket : BasicRocketBehaviour
{
    protected Vector3 target;
    private bool createShrapnel = false;
    float shrapnelTimer;
    private GameObject spawnPoint; 
        
    public void Start()
    {
        base.Start();
        spawnPoint = GameObject.Find("SprapnelSpawn");
        this.shrapnelTimer = Random.value;

    }

    public void SetMousePosition(Vector3 mousepos)
    {
        this.target = mousepos;
    }

    void FixedUpdate()
    {
        if (createShrapnel == true)
        {
            GameObject tmp = (GameObject)Instantiate(Resources.Load("Prefabs/Shrapnel"), spawnPoint.transform.position, spawnPoint.transform.rotation);
            tmp.GetComponent<ShrapnelBehaviour>().ySpeed = 5;
            tmp = (GameObject)Instantiate(tmp, spawnPoint.transform.position, spawnPoint.transform.rotation);
            tmp.GetComponent<ShrapnelBehaviour>().ySpeed = 0;
            tmp = (GameObject)Instantiate(tmp, spawnPoint.transform.position, spawnPoint.transform.rotation);
            tmp.GetComponent<ShrapnelBehaviour>().ySpeed = -5;
            createShrapnel = false;

            base.DestroyRocket(); 
        }
    }

    public void Update()
    {
        if (target != null)
        {
            if (!this.transform.position.Equals(this.target))
            {
                transform.position = Vector3.MoveTowards(transform.position, target, speedX * Time.deltaTime);
                shrapnelTimer -= Time.deltaTime;
                if (shrapnelTimer <= 0)
                {
                    createShrapnel = true;
                }	
            }
            else
            {
                base.DestroyRocket(0);
            }
        }
       
    }
}
