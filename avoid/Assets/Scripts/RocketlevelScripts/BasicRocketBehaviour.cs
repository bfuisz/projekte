﻿using UnityEngine;
using System.Collections;

public class BasicRocketBehaviour : MonoBehaviour {
	public static float speedX = 7, speedY; 
	protected GameBehaviour gameBehav; 
	private Animator animation; 
    private Vector3 tmp; 

	public void Start () {
        animation = this.GetComponent<Animator>();
        this.gameBehav = (GameBehaviour)GameObject.Find("Background").GetComponent ("GameBehaviour");
	}

	public void OnTriggerEnter2D(Collider2D collider) {
        if (collider.name.Equals("RightWall") || collider.name.Equals("Ceiling") || collider.name.Equals("Floor") || collider.name.Equals("LeftWall"))
        {
			DestroyRocket(); 
		}
	}

    private IEnumerator DestroyDelayed(float time = 0)
    {
        yield return new WaitForSeconds(time); 
        animation.SetTrigger("detonate");
        tmp = animation.transform.position;
        tmp.z = 10;
        animation.transform.position = tmp;
        Destroy(this.gameObject, 1);
    }

	void OnMouseEnter(){
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2)){
			gameBehav.RemoveHeart();
            DestroyRocket(0.1f); 
		}
	}

   public void DestroyRocket(float time = 0)
    {
        if (this.name.Contains("Rocket"))
        {
            StartCoroutine(DestroyDelayed(time));
        }
        else
        {
            Destroy(this.gameObject,1); 
        }
    }


}
