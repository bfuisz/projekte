﻿using UnityEngine;
using System.Collections;

public class LauncherBehaviour : MonoBehaviour {
	public float accel;
    public static float fireTimer; 
	private Animator anim;
	private Vector3 mousepos; 
	private GameObject spawnPoint;
    private bool shrapnel = false; 

	// Use this for initialization
	void Start () {
		anim = this.GetComponent<Animator>(); 
		spawnPoint = GameObject.Find("SpawnPoint"); 
	}

	public void Fire(){
		anim.SetTrigger("fireAnimation"); 
		GameObject bullet1 = (GameObject)Instantiate(Resources.Load("Prefabs/Rocket"), spawnPoint.transform.position, spawnPoint.transform.rotation); 
		bullet1.GetComponent<StraigthRocket>().SetMousePosition(mousepos); 
	}

    public void FireShrapnel()
    {
        shrapnel = true; 
        anim.SetTrigger("fireAnimation");
        GameObject bullet1 = (GameObject)Instantiate(Resources.Load("Prefabs/ShrapnelRocket"), spawnPoint.transform.position, spawnPoint.transform.rotation);
        bullet1.GetComponent<ShrapnelRocket>().SetMousePosition(mousepos);
        shrapnel = false; 
    }


	void Update () {
		//Rocketlauncher follow mouse
		mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousepos.y = -mousepos.y;
		mousepos.x = -mousepos.x;
		mousepos.z = -mousepos.z;

		Quaternion tmpRotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.FromToRotation(this.transform.position, mousepos), accel); 
		tmpRotation.x = 0; 
		tmpRotation.y = 0; 
		this.transform.rotation = tmpRotation; 

		//"real" coordinates of mouse for rocket movetowards 
		mousepos.y = -mousepos.y;
		mousepos.x = -mousepos.x;
		mousepos.z = -mousepos.z;
        fireTimer -= Time.deltaTime;

        if (fireTimer <= 0 && !shrapnel)
        {
            Fire();
            fireTimer = 3;
        }	
	}
}
