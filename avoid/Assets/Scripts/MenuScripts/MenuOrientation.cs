﻿using UnityEngine;
using System.Collections;

public class MenuOrientation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Screen.orientation = ScreenOrientation.Portrait;
        DontDestroyOnLoad(this);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("menu");
        }
	}
		
}
