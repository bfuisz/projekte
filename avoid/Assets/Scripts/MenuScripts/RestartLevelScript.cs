﻿using UnityEngine;
using System.Collections;

public class RestartLevelScript : MonoBehaviour {

    void OnMouseDown()
    {
        string levelName = PlayerPrefs.GetString("Level");
        if(levelName != null)
            Application.LoadLevel(levelName);

    }
}
