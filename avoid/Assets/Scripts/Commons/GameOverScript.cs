﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {
    private GUIText scoreText;

    public Sprite ballSprite, spaceSprite, rocketSprite; 

	// Use this for initialization
	void Start () {
        scoreText = GameObject.Find("FinalScore").guiText;
        scoreText.text = PlayerPrefs.GetString("Score");
        string lvlName = PlayerPrefs.GetString("Level");
        DecorateGameOver(lvlName); 
	}

    public void DecorateGameOver(string name)
    {

        if (name.Equals("balllevel"))
        {
            Instantiate(Resources.Load("Prefabs/GameOver/CannonDekoPrefab")); 
            Instantiate(Resources.Load("Prefabs/GameOver/BallDekoPrefab"));
            GameObject.Find("GameOverBackground").GetComponent<SpriteRenderer>().sprite = ballSprite; 

        }else if (name.Equals("rocketlevel"))
        {
 
           Instantiate(Resources.Load("Prefabs/GameOver/LauncherDekoPrefab")); 
           Instantiate(Resources.Load("Prefabs/GameOver/RocketDekoPrefab"));
           GameObject.Find("GameOverBackground").GetComponent<SpriteRenderer>().sprite = rocketSprite; 
            
        }
        else if (name.Equals("spaceshiplevel"))
        {
            Instantiate(Resources.Load("Prefabs/GameOver/SpaceshipDekoPrefab"));
            Instantiate(Resources.Load("Prefabs/GameOver/RocketshipDekoPrefab"));
            GameObject.Find("GameOverBackground").GetComponent<SpriteRenderer>().sprite = spaceSprite; 
        }
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
