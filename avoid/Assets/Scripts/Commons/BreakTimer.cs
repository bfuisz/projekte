﻿using UnityEngine;
using System.Collections;

public class BreakTimer : MonoBehaviour {

	public float timer = 3; 
	private GameBehaviour gameBehav; 

	// Use this for initialization
	void Start () {
		this.gameBehav = (GameBehaviour)GameObject.Find("Background").GetComponent ("GameBehaviour");
	}

	void  FixedUpdate(){
		if(gameBehav.IsUp){
			timer -= Time.deltaTime;
			if (timer > 0){
				this.guiText.text = timer.ToString("F0");
			} else {
				gameBehav.RemoveHeart();
				timer = 3; 
			}
		}else{
			timer = 3; 
			this.guiText.text = timer +"";
		}
	}



	

}
