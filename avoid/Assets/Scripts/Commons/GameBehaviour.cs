﻿using UnityEngine;
using System.Collections;

public class GameBehaviour : MonoBehaviour {
	private GUIText breakText; 
	private GUIText scoreText; 
	private bool isUp; 
	private Vector3 actualMouse; 
	private int heartsLeft; 

	// Use this for initialization
	void Start () {
		breakText = GameObject.Find("BreakSeconds").guiText; 
		scoreText = GameObject.Find("ScoreText").guiText; 
		heartsLeft = 3; 
	}

	void IncreaseScore(){
		int score = System.Int32.Parse(scoreText.text);
		score++; 
		scoreText.text = score + ""; 
	}

	void SetBreakTime(int time){
		breakText.text = time + "";
	}

	int ParseBreakTime(){
		return System.Int32.Parse(breakText.text);
	}

    public void Die()
    {
        PlayerPrefs.SetString("Score", scoreText.text);
        PlayerPrefs.SetString("Level", Application.loadedLevelName); 

        Application.LoadLevel("gameoverscene");
    }

	public bool IsUp {
		get {
			return this.isUp;
		}
	}

	void FixedUpdate () {
		actualMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if((Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))){
			if(this.collider2D.OverlapPoint(actualMouse)){
				IncreaseScore();
				isUp = false; 
			}else{
				isUp = true; 
			}
		}

		if((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))){
			isUp = false; 
		}
		
		if((Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2))){
			isUp = true; 
		}
	}

	public void RemoveHeart(){
		GameObject[] hearts = GameObject.FindGameObjectsWithTag("Heart"); 
		foreach(GameObject tmp in hearts){ 
			if(tmp.renderer.enabled){
				tmp.renderer.enabled = false;
				heartsLeft--; 
				if(heartsLeft == 0){
					Die (); 
				}
				break; 
			}
		}
	}
}
