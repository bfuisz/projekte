﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {
	private int level = 0; 
	float timer = 10;

	private CannonBehaviour cannon;


	// Use this for initialization
	void Start () {
		this.cannon = (CannonBehaviour)GameObject.Find("Cannon").GetComponent ("CannonBehaviour");
	}

	public void AddBall(){
		this.cannon.Fire(); 
	}

	public void SpeedUp(){
		foreach(GameObject obj in this.cannon.Balls){
			((BallBehaviour)obj.GetComponent ("BallBehaviour")).IncreaseSpeed();
		}
	}

	public void Enlarge(){
		Vector3 tmp; 
		foreach(GameObject obj in this.cannon.Balls){
			tmp = obj.transform.localScale;
			tmp.x += 0.5f;
			tmp.y += 0.5f; 
			obj.transform.localScale = tmp; 
		}
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("menu"); }

		timer -= Time.deltaTime;
		if (timer <= 0){
			int random = ((int)(Random.value * 10)) % 3; 
			if(random == 0){
				SpeedUp();
			}else if(random == 1){
				Enlarge(); 
			}else if(random == 2){
				AddBall(); 
			}
			timer = 10; 
		}
	}
}
