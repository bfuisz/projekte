﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CannonBehaviour : MonoBehaviour {
	public float accel = 360f; 
	public float BarrelOffset; 
	private List<GameObject> balls; 
	private GameObject muzzleFlash; 
	private GameObject spawnPoint; 

	// Use this for initialization
	void Start () {
		this.spawnPoint = GameObject.Find("SpawnPoint"); 
		this.balls = new List<GameObject>(); 
		this.muzzleFlash = (GameObject)Instantiate(Resources.Load("Prefabs/Muzzelflash"), spawnPoint.transform.position, this.transform.rotation );
		this.muzzleFlash.renderer.enabled = false;
		this.muzzleFlash.GetComponent<Animator>().enabled= false;
		Fire (); 
	}

	public void PlayMuzzelFlash(){
		StartCoroutine(MuzzelFlash ()); 
	}

	IEnumerator MuzzelFlash(){
		muzzleFlash.GetComponent<Animator>().enabled = true;  
		muzzleFlash.renderer.enabled = true; 
		muzzleFlash.transform.position = this.spawnPoint.transform.position; 
		muzzleFlash.transform.rotation = transform.rotation; 

		yield return new WaitForSeconds(0.4f); 
		muzzleFlash.renderer.enabled = false; 
		muzzleFlash.GetComponent<Animator>().enabled= false;  
	}
	
	public void Fire(){
		Vector3 pos = transform.position; 
		pos.x = this.transform.position.x + BarrelOffset; 
		pos.y = this.transform.position.y + BarrelOffset; 
		GameObject tmpBall = (GameObject)Instantiate(Resources.Load("Prefabs/Ball"), spawnPoint.transform.position, Quaternion.identity);//s.transform.rotation); 
		Vector3 direction = pos - this.transform.position; 

		direction = Vector3.Normalize(direction); 
		direction.x *= 5; 
		direction.y *= 5; 
		tmpBall.rigidbody2D.velocity = direction; 

		PlayMuzzelFlash(); 
		balls.Add(tmpBall); 
	}

	public List<GameObject> Balls {
		get {
			return this.balls;
		}
	}

	void Update () {
		Vector3 mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousepos.y = -mousepos.y;
		mousepos.x = -mousepos.x;
		mousepos.z = -mousepos.z;
		
		Quaternion tmpRotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.FromToRotation(this.transform.position, mousepos), accel); 
		tmpRotation.x = 0; 
		tmpRotation.y = 0; 
		this.transform.rotation = tmpRotation; 


	}
}
