﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {
	public float speed; 
	private GameBehaviour gameBehav; 

	void Start(){
		this.gameBehav = (GameBehaviour)GameObject.Find("Background").GetComponent ("GameBehaviour");

	}

	public void IncreaseSpeed(){
		transform.rigidbody2D.velocity *= 1.3f; 
	}

	void OnMouseEnter(){
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2)){
			gameBehav.RemoveHeart(); 
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
	}

	void Update(){
		Vector3 tmp = transform.position; 
		tmp.x += speed * Time.deltaTime; 
		tmp.y += speed * Time.deltaTime;
		transform.position = tmp; 
	}
}
