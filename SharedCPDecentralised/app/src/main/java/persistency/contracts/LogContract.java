package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class LogContract {

    public static final String CONTENT_AUTHORITY = "at.fibuszene.sharedcpdecentralised";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_LOG = "log";

    public static final class LogEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOG).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_LOG;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_LOG;

        public static final String TABLE_NAME = "log";

        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_LOG_TYPE = "log_type"; //see Utils.LOG_TYPES
        public static final String COLUMN_ORIGIN = "log_origin";//see Utils... to determine wheather the server or the client logged the satetment not really necessary but cool
        public static final String COLUMN_DATE_TIME = "date";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_MESSAGE_INDEX = 1;
        public static final int COLUMN_LOG_TYPE_INDEX = 2;
        public static final int COLUMN_ORIGIN_INDEX = 3;
        public static final int COLUMN_DATE_TIME_INDEX = 4;


        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildDateURI(String date) {
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

    }
}
