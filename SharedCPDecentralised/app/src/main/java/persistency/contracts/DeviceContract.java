package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class DeviceContract {

    public static final String CONTENT_AUTHORITY = "at.fibuszene.sharedcpdecentralised";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_DEVICE = "device";

    public static final class DeviceEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DEVICE).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_DEVICE;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_DEVICE;

        public static final String TABLE_NAME = "devices";

        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_PORT_NUMBER = "portnumber";
        public static final String COLUMN_HOST_NAME = "hostname";
        public static final String COLUMN_MAC_ADRESS = "mac_adress";
        public static final String COLUMN_IPV4_ADRESS = "ip_v4adress";
        public static final String COLUMN_IPV6_ADRESS = "ip_v6adress";
        public static final String COLOUMN_LOCAL_DEVICE = "local_device";
        public static final String COLUMN_SYNCHRONIZED = "synchronized";


        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_HOST_NAME_INDEX = 1;
        public static final int COLUMN_USER_ID_INDEX = 2;
        public static final int COLUMN_PORT_NUMBER_INDEX = 3;
        public static final int COLUMN_MAC_ADRESS_INDEX = 4;
        public static final int COLUMN_IPV4_ADRESS_INDEX = 5;
        public static final int COLUMN_IPV6_ADRESS_INDEX = 6;
        public static final int COLOUMN_LOCAL_DEVICE_INDEX = 7;
        public static final int COLUMN_SYNCHRONIZED_INDEX = 8;


        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildDateURI(String date) {
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

    }
}
