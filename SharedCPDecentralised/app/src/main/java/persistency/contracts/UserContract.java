package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class UserContract {

    public static final String CONTENT_AUTHORITY = "at.fibuszene.sharedcpdecentralised";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_USER = "user";

    public static final class UserEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_USER;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_USER;

        public static final String TABLE_NAME = "user";

        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_SYNCHRONIZED = "synchronized";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_EMAIL_INDEX = 1;
        public static final int COLUMN_SYNCHRONIZED_INDEX = 2;


        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildDateURI(String date) {
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

    }
}
