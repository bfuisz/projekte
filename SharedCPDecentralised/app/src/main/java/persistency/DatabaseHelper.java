package persistency;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import persistency.contracts.DeviceContract;
import persistency.contracts.LogContract;
import persistency.contracts.TransactionContract;
import persistency.contracts.UserContract;

/**
 * Created by benedikt.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String AUTHORITY = "at.fibuszene.sharedcpdecentralised.provider";
    public static final String CONTENT_URI_BASE = "content://" + AUTHORITY;

    public final static String DATABASE_NAME = "sharedcpdecentralised.db";
    public static final int DB_VERSION = 1;

    private final String CREATE_USER_TABLE = "CREATE TABLE " + UserContract.UserEntry.TABLE_NAME + "("
            + UserContract.UserEntry._ID + " INTEGER PRIMARY KEY, "
            + UserContract.UserEntry.COLUMN_EMAIL + " TEXT NOT NULL UNIQUE ON CONFLICT IGNORE, "
            + UserContract.UserEntry.COLUMN_SYNCHRONIZED + " INTEGER); ";


    private final String CREATE_DEVICES_TABLE = "CREATE TABLE " + DeviceContract.DeviceEntry.TABLE_NAME + "("
            + DeviceContract.DeviceEntry._ID + " INTEGER , "
            + DeviceContract.DeviceEntry.COLUMN_HOST_NAME + " VARCHAR(50) , "
            + DeviceContract.DeviceEntry.COLUMN_USER_ID + " INTEGER , "
            + DeviceContract.DeviceEntry.COLUMN_PORT_NUMBER + " INTEGER UNIQUE ON CONFLICT REPLACE, "
            + DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS + " varchar(100), "
            + DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " varchar(100) PRIMARY KEY , "
            + DeviceContract.DeviceEntry.COLUMN_IPV6_ADRESS + " varchar(100) UNIQUE ON CONFLICT REPLACE, "
            + DeviceContract.DeviceEntry.COLOUMN_LOCAL_DEVICE + " INTEGER , "
            + DeviceContract.DeviceEntry.COLUMN_SYNCHRONIZED + " INTEGER, "
            + "FOREIGN KEY(" + DeviceContract.DeviceEntry.COLUMN_USER_ID + ") REFERENCES " + UserContract.UserEntry.TABLE_NAME + "(" + UserContract.UserEntry._ID + ") ON DELETE CASCADE); ";

    private final String CREATE_TRANSACTIONS_TABLE = "CREATE TABLE " + TransactionContract.TransactionEntry.TABLE_NAME + "("
            + TransactionContract.TransactionEntry._ID + " INTEGER PRIMARY KEY, "
            + TransactionContract.TransactionEntry.COLUMN_USER_ID + " INTEGER , "
            + TransactionContract.TransactionEntry.COLUMN_MESSAGE + " TEXT, "
            + TransactionContract.TransactionEntry.COLUMN_SOURCE + " VARCHAR(100), "//device ip is primary key
            + TransactionContract.TransactionEntry.COLUMN_SYNCHRONIZED + " INTEGER, "
            + "FOREIGN KEY(" + TransactionContract.TransactionEntry.COLUMN_USER_ID + ") REFERENCES " + UserContract.UserEntry.TABLE_NAME + "(" + UserContract.UserEntry._ID + ") ON DELETE CASCADE); ";


    private final String CREATE_LOG_TABLE = "CREATE TABLE " + LogContract.LogEntry.TABLE_NAME + "("
            + LogContract.LogEntry._ID + " INTEGER PRIMARY KEY, "
            + LogContract.LogEntry.COLUMN_MESSAGE + " TEXT, "
            + LogContract.LogEntry.COLUMN_LOG_TYPE + " TEXT, "
            + LogContract.LogEntry.COLUMN_ORIGIN + " INTEGER, "
            + LogContract.LogEntry.COLUMN_DATE_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP); ";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_DEVICES_TABLE);
        db.execSQL(CREATE_TRANSACTIONS_TABLE);
        db.execSQL(CREATE_LOG_TABLE);
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            db.execSQL("Drop table " + TransactionContract.TransactionEntry.TABLE_NAME);
            db.execSQL("Drop table " + DeviceContract.DeviceEntry.TABLE_NAME);
            db.execSQL("Drop table " + UserContract.UserEntry.TABLE_NAME);
            db.execSQL("Drop table " + LogContract.LogEntry.TABLE_NAME);
            onCreate(db);
        }
    }
}
