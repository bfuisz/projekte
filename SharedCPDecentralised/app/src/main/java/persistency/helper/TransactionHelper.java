package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import model.Transaction;
import persistency.contracts.TransactionContract;

/**
 * Created by benedikt.
 */
public class TransactionHelper {


    //ziemlich unnoetig weil i sowieso nur die mit userid = loggein on device users vom server hol
    public static Cursor getTransactions(Context context, long user_id) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, TransactionContract.TransactionEntry.COLUMN_USER_ID + " = " + user_id, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static List<Transaction> getTransactions(Context context, String ipAdress) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, TransactionContract.TransactionEntry.COLUMN_SOURCE + " = ?", new String[]{ipAdress}, null, null);
        cursor.moveToFirst();
        List<Transaction> transactions = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            transactions.add(Transaction.from(cursor));
            cursor.moveToNext();
        }
        return transactions;
    }


    public static Cursor getTransaction(Context context, long id) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, TransactionContract.TransactionEntry._ID + " = " + id, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor getTransactions(Context context) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static List<Transaction> getTransactionsAsList(Context context) {
        List<Transaction> tActions = new ArrayList<Transaction>();
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tActions.add(Transaction.from(cursor));
                cursor.moveToNext();
            }
        }
        return tActions;
    }


    public static long saveTransactionFromServer(Context context, Transaction tAction) {
        long row = -1;
        if (tAction != null) {
            Uri uri = context.getContentResolver().insert(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values());
            if (uri != null) {
                row = Integer.valueOf(uri.getLastPathSegment());
            }
        }
        return row;
    }

    public static long saveTransaction(Context context, Transaction tAction) {
        long row = -1;
        if (tAction != null) {
            if (tAction.getId() > 0) {
                row = context.getContentResolver().update(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values(), TransactionContract.TransactionEntry._ID + " = " + tAction.getId(), null);
            } else {
                Uri uri = context.getContentResolver().insert(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values());
                if (uri != null) {
                    row = Integer.valueOf(uri.getLastPathSegment());
                }
            }
        }
        return row;
    }


    public static long deleteTransaction(Context context, long id) {
        long row = -1;
        if (id > 0) {
            row = context.getContentResolver().delete(TransactionContract.TransactionEntry.CONTENT_URI, TransactionContract.TransactionEntry._ID + " = " + id, null);
        }
        return row;
    }

    public static long deleteTransaction(Context context, Transaction ta) {
        long row = -1;
        if (ta != null && ta.getId() > 0) {
            row = deleteTransaction(context, ta.getId());
        }
        return row;
    }

    public static long updateTransaction(Context context, Transaction tAction) {
        long row = -1;
        if (tAction != null && tAction.getId() > 0) {
            row = context.getContentResolver().update(TransactionContract.TransactionEntry.CONTENT_URI, tAction.values(), TransactionContract.TransactionEntry._ID + " = " + tAction.getId(), null);
        }
        return row;
    }

    public static long nextId(Context context) {
        Cursor cursor = context.getContentResolver().query(TransactionContract.TransactionEntry.buildNextId(), null, null, null, null);
        if (cursor.moveToFirst()) {
            return cursor.getLong(0) + 1;
        }
        return -1;
    }


}
