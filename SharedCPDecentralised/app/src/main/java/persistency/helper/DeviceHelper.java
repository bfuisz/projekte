package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import model.Device;
import persistency.DatabaseHelper;
import persistency.contracts.DeviceContract;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class DeviceHelper {

    public static List<Device> listDevices(Context context) {
//        Device thisIp = Utils.getDeviceInfo(context);
        List<Device> devices = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                devices.add(Device.from(cursor));
                cursor.moveToNext();
            }
        }
        return devices;
    }

    public static Device getDevice(Context context, String ip) {
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " = ? ", new String[]{ip}, null);
        cursor.moveToFirst();
        return Device.from(cursor);
    }

    public static Cursor getUserDevices(Context context, long userId) {
        Cursor cursor = context.getContentResolver().query(DeviceContract.DeviceEntry.CONTENT_URI, null, DeviceContract.DeviceEntry.COLUMN_USER_ID + " = " + userId, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    /**
     * Save or update
     *
     * @param context
     * @param device
     * @return
     */
    public static long saveDevice(Context context, Device device) {
        long row = -1;
        if (device != null && device.getIpv4Adress() != null) {
            row = context.getContentResolver().update(DeviceContract.DeviceEntry.CONTENT_URI, device.values(), DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " = ? ", new String[]{device.getIpv4Adress()});
            if (row <= 0) {
                Uri uri = context.getContentResolver().insert(DeviceContract.DeviceEntry.CONTENT_URI, device.values());
                if (uri != null) {
                    try {
                        row = Integer.valueOf(uri.getLastPathSegment());
                    } catch (NumberFormatException nfe) {
                        row = -1; //already is but to be safe
                    }
                }
            }
        }
        return row;
    }


    public static long deleteDevice(Context context, String ip) {
        long row = -1;
        if (ip != null) {
            row = context.getContentResolver().delete(DeviceContract.DeviceEntry.CONTENT_URI, DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " = ?", new String[]{ip});
        }
        return row;
    }


    public static long deleteDevice(Context context, Device device) {
        long row = -1;
        if (device != null) {
            if (device.getId() > 0) {
                row = deleteDevice(context, device);
            } else if (device.getIpv4Adress() != null & row < 0) {
                row = context.getContentResolver().delete(DeviceContract.DeviceEntry.CONTENT_URI, DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " = ?", new String[]{device.getIpv4Adress()});
            } else if (device.getIpv6Adress() != null & row < 0) {
                row = context.getContentResolver().delete(DeviceContract.DeviceEntry.CONTENT_URI, DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS + " = ?", new String[]{device.getIpv4Adress()});
            }
        }
        return row;
    }
}
