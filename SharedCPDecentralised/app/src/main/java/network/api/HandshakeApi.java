package network.api;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import model.Device;
import model.Request;
import model.Response;
import model.Transaction;
import network.MessageException;
import network.NetUtils;
import persistency.helper.DeviceHelper;
import persistency.helper.TransactionHelper;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class HandshakeApi {


    public static Response handleRequest(Context context, Request request) throws MessageException {
        Response response = null;
        if (request == null) {
            throw new MessageException(Response.ResponseCode.BAD_REQUEST);
        }
        switch (request.getOperation()) {
            case stop:
                Device dev = DeviceHelper.getDevice(context, request.getPayload());
                DeviceHelper.deleteDevice(context, dev);
                break;
            case join:
                Device device = new Gson().fromJson(request.getPayload(), Device.class);
                response = DeviceApi.update(context, device);
                if (response.getResponseCode() == Response.ResponseCode.OKAY) {
                    Device thisDev = DeviceHelper.getDevice(context, NetUtils.getIPAddress(true));
                    List<Transaction> tActions = TransactionHelper.getTransactions(context, thisDev.getIpv4Adress());
                    response.setInfo(new Gson().toJson(tActions));
                    response.setDevice(thisDev.getIpv4Adress());
                    response.setResponseCode(Response.ResponseCode.HANDSHAKE);
                    response.setRequest(request);
                }
                break;
            default:
                throw new MessageException(Response.ResponseCode.UNKNOWN_METHOD, request.getOperation() + "");
        }
        return response;
    }

    public void removeDevice() {

    }


}
