package network.api;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

import model.Device;
import model.Request;
import model.Response;
import network.MessageException;
import persistency.helper.DeviceHelper;
import persistency.helper.TransactionHelper;

/**
 * Created by benedikt.
 */
public class DeviceApi {

    public static Response handleRequest(Context context, Request request) throws MessageException {
        Response response;
        Device device = null;
        if (request == null) {
            throw new MessageException(Response.ResponseCode.BAD_REQUEST);
        }
        switch (request.getOperation()) {
            case create:
                device = new Gson().fromJson(request.getPayload(), Device.class);
                response = create(context, device);
                break;
            case update:
                device = new Gson().fromJson(request.getPayload(), Device.class);
                response = update(context, device);
                break;
            case delete:
                device = new Gson().fromJson(request.getPayload(), Device.class);
                response = delete(context, device);
                break;
            case synchronize:
                response = synchronize(context);
                break;
            default:
                throw new MessageException(Response.ResponseCode.UNKNOWN_METHOD, request.getOperation() + "");
        }

        return response;
    }

    public static Response create(Context context, Device device) {
        long id = DeviceHelper.saveDevice(context, device); //no id check just save the thing;
        Response response = new Response();
        if (id > 0) {
            return response.setResponseCode(Response.ResponseCode.OKAY).setInfo(id + "");
        }
        return response.setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to insert Device");
    }

    public static Response update(Context context, Device device) {
        long rows = DeviceHelper.saveDevice(context, device);
        Response response = new Response();
        if (rows > 0) {
            return response.setResponseCode(Response.ResponseCode.OKAY);
        }
        return response.setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to update Device");
    }

    public static Response delete(Context context, Device device) {
        if (device != null) {
            return delete(context, device.getIpv4Adress());
        }
        return new Response().setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to delete Device");
    }

    public static Response delete(Context context, String ipAdress) {
        if (ipAdress == null) {
            return new Response().setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to delete Device");

        }
        long rows = DeviceHelper.deleteDevice(context, ipAdress); //no id check just save the thing;
        if (rows > 0) {
            return new Response().setResponseCode(Response.ResponseCode.OKAY).setInfo(rows + "");
        }
        return new Response().setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to delete Device");
    }

    public static Response synchronize(Context context) {
        List<Device> devices = DeviceHelper.listDevices(context);
        String[] asJson = new String[devices.size()];
        int i = 0;
        for (Device tmp : devices) {
            asJson[i] = tmp.toJson();
            i++;
        }
        return new Response().setResponseCode(Response.ResponseCode.OKAY).setInfo(new Gson().toJson(asJson));
    }
}
