package network.api;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

import model.Request;
import model.Response;
import model.Transaction;
import network.MessageException;
import persistency.helper.TransactionHelper;

/**
 * Created by benedikt.
 */
public class TransactionApi {

    public static Response handleRequest(Context context, Request request) throws MessageException {

        Response response;
        Transaction ta = null;
        if (request == null) {
            throw new MessageException(Response.ResponseCode.BAD_REQUEST);
        }
        switch (request.getOperation()) {
            case create:
                ta = new Gson().fromJson(request.getPayload(), Transaction.class);
                response = create(context, ta);
                break;
            case update:
                ta = new Gson().fromJson(request.getPayload(), Transaction.class);
                response = update(context, ta);
                break;
            case delete:
                try {
                    ta = new Gson().fromJson(request.getPayload(), Transaction.class);
                    response = delete(context, ta);
                } catch (Exception e) {
                    try {
                        long id = Long.valueOf(request.getPayload().trim());
                        response = delete(context, id);
                    } catch (NumberFormatException nfe) {
                        throw new MessageException(Response.ResponseCode.BAD_REQUEST, "Wrong Number Format");
                    }
                }
                break;
            case synchronize:
                response = synchronize(context);
                break;
            default:
                throw new MessageException(Response.ResponseCode.UNKNOWN_METHOD, request.getOperation() + "");
        }

        return response;
    }

    public static Response create(Context context, Transaction tAction) {
        long id = TransactionHelper.saveTransactionFromServer(context, tAction); //no id check just save the thing;
        Response response = new Response();
        if (id > 0) {
            return response.setResponseCode(Response.ResponseCode.OKAY).setInfo(id + "");
        }
        return response.setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to insert Transaction");
    }

    public static Response update(Context context, Transaction tAction) {
        long rows = TransactionHelper.updateTransaction(context, tAction); //no id check just save the thing;
        Response response = new Response();
        if (rows > 0) {
            return response.setResponseCode(Response.ResponseCode.OKAY).setInfo(rows + "");
        }
        return response.setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to update Transaction");
    }

    public static Response delete(Context context, Transaction tAction) {
        if (tAction != null) {
            return delete(context, tAction.getId());
        }

        return new Response().setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to delete Transaction " + null);
    }

    public static Response delete(Context context, long id) {
        long rows = TransactionHelper.deleteTransaction(context, id); //no id check just save the thing;
        Response response = new Response();
        if (rows > 0) {
            return response.setResponseCode(Response.ResponseCode.OKAY).setInfo(rows + "");
        }
        return response.setResponseCode(Response.ResponseCode.SERVER_ERROR).setInfo("Failed to delete Transaction " + id);
    }

    public static Response synchronize(Context context) {
        List<Transaction> taList = TransactionHelper.getTransactionsAsList(context);
        String[] asJson = new String[taList.size()];
        int i = 0;
        for (Transaction ta : taList) {
            asJson[i] = ta.toJson();
            i++;
        }
        return new Response().setResponseCode(Response.ResponseCode.OKAY).setInfo(new Gson().toJson(asJson));
    }
}
