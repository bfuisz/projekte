package network;

import android.content.ContentValues;
import android.content.Context;

import com.google.gson.Gson;
import com.squareup.okhttp.internal.Util;

import java.util.List;
import java.util.TreeMap;

import model.Request;
import model.Response;
import model.Transaction;
import network.api.DeviceApi;
import network.api.HandshakeApi;
import network.api.TransactionApi;
import network.api.UserApi;
import persistency.contracts.TransactionContract;
import utils.Utils;

/**
 * Created by benedikt.
 */
public class MessageHandler {

    public static Response handleRequest(Context context, String input) throws MessageException {
        Gson gson = new Gson();
        Request request = gson.fromJson(input, Request.class);
        return handleRequest(context, request);
    }

    public static Response handleRequest(Context context, Request request) throws MessageException {
        Response response = null;
        if (request == null || request.getRequestType() == null || request.getOperation() == null) {
            throw new MessageException(Response.ResponseCode.BAD_REQUEST);
        }
        switch (request.getRequestType()) {
            case HANDSHAKE:
                response = HandshakeApi.handleRequest(context, request);
                break;
            case DEVICE:
                response = DeviceApi.handleRequest(context, request);
                break;
            case TRANSACTION:
                response = TransactionApi.handleRequest(context, request);
                break;
            case USER:
                response = UserApi.handleRequest(context, request);
                break;
            default:
                throw new MessageException(Response.ResponseCode.UNKNOWN_METHOD);
        }
        return response;
    }


    public static Response handleResponse(Context context, String input) throws IllegalArgumentException {
        Gson gson = new Gson();
        return handleResponse(context, gson.fromJson(input, Response.class));
    }

    public static Response handleResponse(Context context, Response response) throws IllegalArgumentException {
        if (response == null || response.getResponseCode() == null || response.getInfo() == null) {
            throw new IllegalArgumentException("Response Format exception");
        }
        switch (response.getResponseCode()) {
            case OKAY:

                break;
            /*
             * Handshake a newly added device send discovered devices sort of a hello message containing
             * the new device
             * the other device answeres with all the transactions in case the device missed some
             * horrible performance...
             */
            case HANDSHAKE:
                Transaction[] list = new Gson().fromJson(response.getInfo(), Transaction[].class);
                ContentValues[] values = new ContentValues[list.length];
                int i = 0;
                for (Transaction action : list) {
                    values[i] = list[i].values();
                    i++;
                }
                context.getContentResolver().bulkInsert(TransactionContract.TransactionEntry.CONTENT_URI, values);
                break;

            case BAD_REQUEST: //most likely due to server receiving null for some reason
                if (response.getRequest() == null) {
                    response = null;
                }
                //todo add request
                Utils.log(context, "BAD_REQUEST: ", Utils.LOG_TYPES[0], Utils.LOG_ORIGIN_CLIENT);
                break;
            case SERVER_ERROR:
                //panic
                Utils.log(context, "SERVER_ERROR: " + response.getRequest(), Utils.LOG_TYPES[0], Utils.LOG_ORIGIN_CLIENT);
                break;
            case UNKNOWN_METHOD:
                //panic
                Utils.log(context, "UNKNOWN_METHOD: " + response.getRequest(), Utils.LOG_TYPES[0], Utils.LOG_ORIGIN_CLIENT);

                break;
        }
        return response;
    }


}
