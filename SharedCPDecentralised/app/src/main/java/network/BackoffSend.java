package network;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import model.Device;
import model.Request;
import model.Response;
import persistency.contracts.DeviceContract;
import persistency.helper.DeviceHelper;
import utils.Utils;

public class BackoffSend extends Thread {
    private static String TAG = "BackoffSend";
    private static BackoffSend instance = new BackoffSend();//thread safe but eager
    private ExecutorService exeService;
    private ArrayBlockingQueue<BackoffSendRunnable> todoList;
    public boolean isRunning = true;
    private Context context;

    public static BackoffSend getInstance() {
        return instance;
    }

    private BackoffSend() {
        exeService = Executors.newFixedThreadPool(2);
        this.todoList = new ArrayBlockingQueue<BackoffSendRunnable>(10); //10 backofftasks oughta be enough for everyone
        start();
    }


    public void add(Context context, Device dev, Request request) {
        this.context = context;
        this.todoList.add(new BackoffSendRunnable(dev, request));
    }

    public void add(BackoffSendRunnable thread) {
        this.todoList.offer(thread);
    }

    public void shutDown() {
        isRunning = false;
        exeService.shutdown();
        try {
            exeService.awaitTermination(4000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (isRunning) {
            Log.d(TAG, "waiting for tasks");
            try {
                exeService.submit(todoList.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "got a task " + todoList.size());

        }
    }

    public class BackoffSendRunnable implements Runnable {
        private String TAG = "BackoffSendRunnable";

        private Device dev;
        private Request request;
        private int tryAmount;
        private int backoffAmount = 1000;
        private boolean backOffRunning = true;

        public BackoffSendRunnable(Device dev, Request request) {
            this.dev = dev;
            this.request = request;
        }

        @Override
        public void run() {
            while (backOffRunning) {
                try {
                    tryAmount++;
                    backoffAmount = backoffAmount * tryAmount;
                    Log.d(TAG, "working on task " + dev + " " + request + " tries  " + tryAmount);
                    Thread.sleep(backoffAmount);
                    Response response = NetUtils.pointToPoint(context, dev, request);
                    Log.d(TAG, response + "");
                    if (response != null) {
                        backOffRunning = false;
                        Log.d(TAG, "done " + response);
                        return;
                    }
                    if (tryAmount >= 3) {
                        backOffRunning = false;
                        Utils.log(context, "Wasn't able to retransmit " + request, Utils.LOG_TYPES[0], Utils.LOG_ORIGIN_CLIENT);
                        DeviceHelper.deleteDevice(context, dev);
                        return;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
