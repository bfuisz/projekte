package network;

import model.Request;
import model.Response;

/**
 * Created by benedikt.
 */
public class MessageException extends Exception {
    private Response response;


    public MessageException(Response.ResponseCode responseCode) {
        super(responseCode + "");
        this.response = new Response(responseCode, NetUtils.getIPAddress(false));
    }

    public MessageException(Response.ResponseCode responseCode, String info) {
        super(responseCode + "");
        this.response = new Response(responseCode, info);
    }


    public Response getResponse() {
        return this.response;
    }
}
