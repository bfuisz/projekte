package adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.TextView;

import at.fibuszene.sharedcpdecentralised.R;
import model.Device;

/**
 * Created by benedikt.
 */
public class DevicesAdapter extends ResourceCursorAdapter {

    public DevicesAdapter(Context context, Cursor c) {
        super(context, R.layout.device_list_item, c, false);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Device dev = Device.from(cursor);
        if (view == null) {
            view = View.inflate(context, R.layout.device_list_item, null);
        }

        TextView txtView = (TextView) view.findViewById(R.id.deviceName);
        txtView.setText(dev.getIpv4Adress() + " " + dev.getPortNumber());
        if (dev.isLocalDevice()) {
            txtView.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

}
