package adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import at.fibuszene.sharedcpdecentralised.R;
import model.Request;
import model.Response;
import model.Transaction;
import network.NetUtils;
import persistency.contracts.LogContract;
import persistency.helper.TransactionHelper;
import utils.CopyThat;

/**
 * Created by benedikt.
 */
public class LogAdapter extends ResourceCursorAdapter {

    public LogAdapter(Context context) {
        super(context, R.layout.log_item, context.getContentResolver().query(LogContract.LogEntry.CONTENT_URI, null, null, null, LogContract.LogEntry.COLUMN_DATE_TIME + " DESC"), false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        if (view == null) {
            view = View.inflate(context, R.layout.log_item, null);
        }
        TextView txtView = (TextView) view.findViewById(R.id.logItemTextView);
        txtView.setText(cursor.getString(2) + "\n" + cursor.getString(1));
    }

}
