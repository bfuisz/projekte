package adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import at.fibuszene.sharedcpdecentralised.R;
import model.Request;
import model.Response;
import model.Transaction;
import network.NetUtils;
import persistency.helper.TransactionHelper;
import utils.CopyThat;

/**
 * Created by benedikt.
 */
public class TransactionsAdapter extends ResourceCursorAdapter {

    public TransactionsAdapter(Context context, Cursor c) {
        super(context, R.layout.transaction_list_item, c, false);
    }


    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final Transaction tAction = Transaction.from(cursor);
        if (view == null) {
            view = View.inflate(context, R.layout.transaction_list_item, null);
        }

        ImageButton delete = (ImageButton) view.findViewById(R.id.trash);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, List<Response>>() {
                    @Override
                    protected List<Response> doInBackground(Void... params) {
                        Request request = new Request();
                        request.setRequestType(Request.RequestType.TRANSACTION).setOperation(Request.Operation.delete).setPayload(tAction.toJson());
                        return NetUtils.broadcast(context, request);
                    }

                    @Override
                    protected void onPostExecute(List<Response> response) {
                        if (response != null && response.size() > 0) {
                            TransactionHelper.deleteTransaction(context, tAction.getId());
                        }
                    }
                }.execute();
            }
        });

        ImageButton copy = (ImageButton) view.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //circlejerk
                CopyThat.getInstance().toggle();
                ClipboardManager manager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData cData = ClipData.newPlainText("message", tAction.getMessage());
                manager.setPrimaryClip(cData);
            }
        });

        TextView txtView = (TextView) view.findViewById(R.id.messageTextView);
        txtView.setText(tAction.getId() + " " + tAction.getMessage() + "\n" + tAction.getSource());
    }

}
