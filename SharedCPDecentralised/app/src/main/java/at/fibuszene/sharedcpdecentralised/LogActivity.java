package at.fibuszene.sharedcpdecentralised;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import adapter.LogAdapter;
import persistency.contracts.LogContract;
import utils.Utils;

//feels like logging is more "sophisticated" than the app itself :D
public class LogActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemSelectedListener {
    private LogAdapter adapter;
    private String selection;
    private String[] selectionArgs = new String[2];
    private String typeIs, originIs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        String[] logEntries = getResources().getStringArray(R.array.log_types);
        String[] originEntries = getResources().getStringArray(R.array.origin);

        Spinner typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        typeSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, android.R.id.text1, logEntries));
        typeSpinner.setOnItemSelectedListener(this);

        Spinner originSpinner = (Spinner) findViewById(R.id.originSpinner);
        originSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, android.R.id.text1, originEntries));
        originSpinner.setOnItemSelectedListener(this);

        ListView lView = (ListView) findViewById(R.id.logList);
        adapter = new LogAdapter(this);
        lView.setAdapter(adapter);
        typeIs = "is not";
        originIs = " != ";
        selection = build(typeIs, originIs);
        selectionArgs = new String[]{"null", "0"};

        getLoaderManager().restartLoader(890, null, this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.typeSpinner) {
            if (position == 0) {
                typeIs = "is not";
                selectionArgs[0] = "null";
            } else {
                typeIs = "is";
                position--;
                selectionArgs[0] = Utils.LOG_TYPES[position];
            }
        } else if (parent.getId() == R.id.originSpinner) {
            if (position == 0) { //all
                originIs = "!=";
            } else {
                originIs = "=";
            }
            selectionArgs[1] = "" + position;
        }
        selection = build(typeIs, originIs);
        getLoaderManager().restartLoader(890, null, this);
    }

    public String build(String typeIs, String originIs) {
        return String.format(LogContract.LogEntry.COLUMN_LOG_TYPE + " %s ? AND " + LogContract.LogEntry.COLUMN_ORIGIN + " %s ?", typeIs, originIs);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        typeIs = "is not";
        originIs = " != ";
        selection = build(typeIs, originIs);
        selectionArgs = new String[]{"null", "0"};
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, LogContract.LogEntry.CONTENT_URI, null, selection, selectionArgs, LogContract.LogEntry.COLUMN_DATE_TIME + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_clear) {
            getContentResolver().delete(LogContract.LogEntry.CONTENT_URI, null, null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
