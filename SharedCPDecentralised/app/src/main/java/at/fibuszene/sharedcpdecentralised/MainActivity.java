package at.fibuszene.sharedcpdecentralised;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import adapter.DevicesAdapter;
import fragments.APListFragment;
import model.Request;
import model.Response;
import model.Transaction;
import model.User;
import network.NetUtils;
import persistency.contracts.DeviceContract;
import persistency.helper.DeviceHelper;
import persistency.helper.TransactionHelper;
import persistency.helper.UserHelper;
import service.ClipboardSynchronizeService;
import service.NSDServerService;
import utils.PrefUtils;

//TODO:

/**
 * Folgender Ablauf:
 * <p/>
 * Ein Geraet installiert die App.
 * Beim starten der App wird der NSDServerService gestartet der
 * -> den NSDServer(Thread) startet
 * -> Service Discovery startet wobei 2 faelle auftreten koennen
 * a) Geraet ist das einzige im Netzwerk = U
 * b) bereits geraete vorhanden G1, G2, G3
 * a) Nicht zu tun
 * b) U broadcastet eigene Deviceinfo zu G1, G2, G3
 * <p/>
 * NDS macht an coolen eindruck nur crasht mei Handy (HTC Desire 500) dauernt wegen einem Bug https://code.google.com/p/android/issues/detail?id=35585
 * <p/>
 * deshalb verwend i jetzt jmdns
 * <p/>
 * REFACTORING !:D
 */

public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final int DEVICES_LOADER_ID = 100;
    private ListView devicesListView;
    private DevicesAdapter devAdapter;
    private User user;
    private LinearLayout buttonBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        long currentUserId = PrefUtils.getCurrentUserId(this);

        if (currentUserId > 0) {
            this.user = User.from(UserHelper.getUser(this, currentUserId));
        } else {
            loginActivity();
            return;
        }

        startService(new Intent(this, ClipboardSynchronizeService.class));
        Intent intent = new Intent(this, NSDServerService.class);
        intent.setAction(NSDServerService.START_SERVER_ACTION);
        startService(intent);

        buttonBar = (LinearLayout) findViewById(R.id.buttonBar);
        deactivateOthers(R.id.allTransactions);

        devicesListView = (ListView) findViewById(R.id.devicesListView);
        devAdapter = new DevicesAdapter(this, DeviceHelper.getUserDevices(this, user.getId()));
        devicesListView.setAdapter(devAdapter);
        getSupportLoaderManager().restartLoader(DEVICES_LOADER_ID, null, this);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_ALL, this.user.getId())).commit();

    }

    public void incomingTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_INCOMING, this.user.getId())).commit();
    }

    public void outgoingTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_OUTGOING, this.user.getId())).commit();
    }

    public void allTransactions(View view) {
        deactivateOthers(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.container, APListFragment.getInstance(APListFragment.TransactionType.T_ALL, this.user.getId())).commit();
    }


    public void deactivateOthers(int id) {
        int len = buttonBar.getChildCount();
        View child;
        for (int i = 0; i < len; i++) {
            child = buttonBar.getChildAt(i);
            if (child.getId() == id) {
                child.setActivated(true);
            } else {
                child.setActivated(false);
            }
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, DeviceContract.DeviceEntry.CONTENT_URI, null, null /*DeviceContract.DeviceEntry.COLUMN_USER_ID + " = " + this.user.getId()*/, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.devAdapter.swapCursor(data);
        this.devAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.devAdapter.swapCursor(null);
        this.devAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            Intent intent = new Intent(this, NSDServerService.class);
            intent.setAction(NSDServerService.START_REFRESH_DEVICES);
            startService(intent);
            return true;
        }
        if (id == R.id.action_login) {
            loginActivity();
            return true;
        }
        if (id == R.id.action_log) {
            logActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void logActivity() {
        startActivity(new Intent(this, LogActivity.class));
    }

    public void testSend(View view) {
        new AsyncTask<Void, Void, List<Response>>() {
            @Override
            protected List<Response> doInBackground(Void... params) {
                Transaction tAction = new Transaction(PrefUtils.getCurrentUserId(MainActivity.this), "Test String ", NetUtils.getIPAddress(true), false);
                long id = TransactionHelper.saveTransaction(MainActivity.this, tAction);
                tAction.setId(id);
                Request request = new Request().setRequestType(Request.RequestType.TRANSACTION).setOperation(Request.Operation.create).setPayload(tAction.toJson());
                return NetUtils.broadcast(MainActivity.this, request);
            }

            @Override
            protected void onPostExecute(List<Response> reponse) {
                super.onPostExecute(reponse);
                for (Response tmp : reponse) {
                    Log.d("Client received", tmp + "");
                }
            }
        }.execute();

    }


}
