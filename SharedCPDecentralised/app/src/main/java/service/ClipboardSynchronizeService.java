package service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import java.util.List;

import model.Request;
import model.Response;
import model.Transaction;
import network.NetUtils;
import persistency.helper.TransactionHelper;
import utils.CopyThat;
import utils.PrefUtils;

public class ClipboardSynchronizeService extends Service {
    String prev = "";

    public ClipboardSynchronizeService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        clipboard.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                long user = PrefUtils.getCurrentUserId(ClipboardSynchronizeService.this);

                String data;
                try {
                    data = clipboard.getPrimaryClip().getItemAt(0).getText().toString();
                } catch (NullPointerException npe) {
                    data = null;
                }
                if (data != null && CopyThat.getInstance().allowCopy() && !data.equals(prev)) {
                    prev = data;

                    //necessary data (user, source, message);
                    Transaction action = new Transaction();
                    action.setUser_id(user);
                    action.setMessage(data);
                    action.setSource(NetUtils.getIPAddress(true));

                    long id = TransactionHelper.saveTransaction(ClipboardSynchronizeService.this, action);
                    action.setId(id);
                    final Request request = new Request().setRequestType(Request.RequestType.TRANSACTION).setOperation(Request.Operation.create).setPayload(action.toJson());
                    request.setPayload(action.toJson());

                    new AsyncTask<Void, Void, List<Response>>() {
                        @Override
                        protected List<Response> doInBackground(Void... params) {
                            return NetUtils.broadcast(ClipboardSynchronizeService.this, request);
                        }

                        @Override
                        protected void onPostExecute(List<Response> response) {
                            super.onPostExecute(response);
                        }
                    }.execute();

                } else if (!CopyThat.getInstance().allowCopy()) {
                    //user copied content so toggle back clicklistener in adapter will toggle to false again if user copies something else
                    //since for some reason this gets called twice toggle is not an option to make sure it does something the next time just set it so allow copy
                    CopyThat.getInstance().setToggle(true);
                }
            }
        });
        return START_STICKY;
    }
}
