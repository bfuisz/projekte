package service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import at.fibuszene.sharedcpdecentralised.MainActivity;
import at.fibuszene.sharedcpdecentralised.R;
import model.Device;
import persistency.helper.DeviceHelper;
import utils.Utils;

public class NSDServerService extends Service {
    private static final String TAG = "NSDServerService";
    public static final String START_SERVER_ACTION = "at.fibuszene.sharedcpdecentralised.start_server";
    public static final String START_REFRESH_DEVICES = "at.fibuszene.sharedcpdecentralised.refresh_devices";
    public static final int NOTIFICATION_ID = 1;

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder builder;
    private NSDServer server;

    public NSDServerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();

            if (action.equals(START_REFRESH_DEVICES)) {
                discoverServices();
            } else if (action.equals(START_SERVER_ACTION)) {
                server = new NSDServer(this);
                server.start();
                sendNotification("Server Started " + server.getPort());
            }
        }
        return START_STICKY;
    }


    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Server Notification")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        startForeground(NOTIFICATION_ID, mBuilder.build());
    }

    public void discoverServices() {

    }


    @Override
    public void onDestroy() {
        server.tearDownServer();
        super.onDestroy();
    }


}
