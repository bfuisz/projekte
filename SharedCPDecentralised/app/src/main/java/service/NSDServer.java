package service;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.internal.Util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import model.Device;
import model.Request;
import model.Response;
import network.MessageException;
import network.MessageHandler;
import persistency.helper.DeviceHelper;
import network.NetUtils;
import utils.Utils;

public class NSDServer extends Thread implements ServiceListener {
    private Context context;
    private ServerSocket mServerSocket = null;
    private String TAG = "NSDServer";
    private final AtomicInteger port = new AtomicInteger(-1);

    private final String type = "_sharedcp._tcp.local.";
    private android.net.wifi.WifiManager.MulticastLock multicastLock;
    private ServiceInfo serviceInfo;
    private JmDNS jmDns;
    private static final String SERVICE_NAME = "AndroidTest";
    private Device thisDevice;
    private ExecutorService executorService;


    public NSDServer(Context context) {
        this.context = context;
        this.executorService = Executors.newFixedThreadPool(4);
    }

    public void setJmDns(int port) {
        try {
            thisDevice = new Device();
            thisDevice.setIpv4Adress(NetUtils.getIPAddress(true));
            thisDevice.setIpv6Adress(NetUtils.getIPAddress(false));
            thisDevice.setPortNumber(port);
            thisDevice.setLocalDevice(true);
            DeviceHelper.saveDevice(context, thisDevice);

            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            // get the device ip address
            String deviceIpAddress = NetUtils.getIPAddress(true);
            if (deviceIpAddress == null) {
                deviceIpAddress = NetUtils.getIPAddress(false);
            }
            InetAddress address = InetAddress.getByName(deviceIpAddress);

            multicastLock = wifi.createMulticastLock(getClass().getName());
            multicastLock.setReferenceCounted(true);
            multicastLock.acquire();

            jmDns = JmDNS.create(address, "localhost");
            jmDns.addServiceListener(type, this);
            serviceInfo = ServiceInfo.create(type, SERVICE_NAME, port, "mehr text");
            jmDns.registerService(serviceInfo);
        } catch (IOException ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }


    public void tearDownServer() {
        Request request = new Request();
        request.setRequestType(Request.RequestType.HANDSHAKE);
        request.setOperation(Request.Operation.stop);
        request.setPayload(mServerSocket.getInetAddress().getHostAddress());

        //doesn't really matter what the others say if the user want's to quit
        List<Response> response = NetUtils.broadcast(context, request);

        stopScan();
        currentThread().interrupt();
        try {
            mServerSocket.close();
        } catch (IOException ioe) {
            Log.e(TAG, "Error when closing server socket.");
        }
    }

    private void stopScan() {
        try {
            if (jmDns != null) {
                jmDns.unregisterAllServices();
                jmDns.close();
                jmDns = null;
            }
            if (multicastLock != null) {
                multicastLock.release();
                multicastLock = null;
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    public int getPort() {
        synchronized (port) {
            while (port.get() < 0) {
                try {
                    port.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return port.get();
    }

    public void setPort(int nPort) {
        synchronized (port) {
            port.set(nPort);
            port.notifyAll();
        }
    }

    @Override
    public void run() {

        // Since discovery will happen via Nsd, we don't need to care which port is
        // used.  Just grab an available one  and advertise it via Nsd.
        try {
            mServerSocket = new ServerSocket(0);
            int port = mServerSocket.getLocalPort();
            Utils.log(context, "Server started at " + mServerSocket.getInetAddress().getHostAddress() + " port: " + port, Utils.LOG_TYPES[1], Utils.LOG_ORIGIN_SERVER);
            setPort(port);
            setJmDns(port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (!Thread.currentThread().isInterrupted()) {
            try {
                executorService.submit(new ClientConnection(mServerSocket.accept()));
            } catch (IOException e) {
                Log.e(TAG, "Error creating ServerSocket: ", e);
            }
        }
        Utils.log(context, "Server stopped. ", Utils.LOG_TYPES[1], Utils.LOG_ORIGIN_SERVER);
    }


    public class ClientConnection implements Runnable {
        private Socket clientSocket;

        public ClientConnection(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                clientSocket.setSoTimeout(10000);
                ObjectOutputStream writer = new ObjectOutputStream(clientSocket.getOutputStream());
                ObjectInputStream reader = new ObjectInputStream(clientSocket.getInputStream());
                Response response = null;
                try {
                    Request request = (Request) reader.readObject();
                    Utils.log(context, "Got request: " + request, Utils.LOG_TYPES[1], Utils.LOG_ORIGIN_SERVER);
                    response = MessageHandler.handleRequest(context, request);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    response = new Response(Response.ResponseCode.BAD_REQUEST, NetUtils.getIPAddress(false));
                } catch (MessageException e) {
                    e.printStackTrace();
                    response = e.getResponse();
                }

                response.setDevice(thisDevice.getIpv4Adress());
                writer.writeObject(response);
                Utils.log(context, "Responded with: " + response, Utils.LOG_TYPES[1], Utils.LOG_ORIGIN_SERVER);

                writer.flush();
                writer.close();
                reader.close();
                clientSocket.close();
            } catch (IOException io) {
                Log.d("Server", "error " + io.getCause());
            }
        }
    }


    @Override
    public void serviceAdded(ServiceEvent event) {
        jmDns.requestServiceInfo(event.getType(), event.getName(), 1);
    }

    @Override
    public void serviceRemoved(ServiceEvent event) {
    }

    @Override
    public void serviceResolved(ServiceEvent event) {
        ServiceInfo info = event.getInfo();

        final Device dev = new Device();
        dev.setPortNumber(info.getPort());

        if (info.getInet4Addresses().length > 0) {
            dev.setIpv4Adress(info.getInet4Addresses()[0].getHostAddress() + "");
            dev.setHostName(info.getInet4Addresses()[0].getHostName() + "");
        }

        if (info.getInet6Addresses().length > 0) {
            dev.setIpv6Adress(info.getInet6Addresses()[0].getHostAddress() + "");
            dev.setHostName(info.getInet6Addresses()[0].getHostName() + "");
        }
        dev.setSynchronized(false);

        if (!dev.getIpv4Adress().equals(this.thisDevice.getIpv4Adress())) {

            long id = DeviceHelper.saveDevice(context, dev);
            new AsyncTask<Void, Void, Response>() {
                @Override
                protected Response doInBackground(Void... params) {
                    thisDevice.setLocalDevice(false);//otherwise it will be saved as local device
                    Request request = new Request().setRequestType(Request.RequestType.HANDSHAKE).setOperation(Request.Operation.join).setPayload(thisDevice.toJson());
                    return NetUtils.pointToPoint(context, dev, request);
                }

                @Override
                protected void onPostExecute(Response response) {
                    if (response != null) {
                        super.onPostExecute(response);
                        try {
                            MessageHandler.handleResponse(context, response);
                            thisDevice.setLocalDevice(true);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.execute();
        }
    }

}
