package utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import model.Device;
import model.JSONSerializable;
import network.NetUtils;
import persistency.contracts.LogContract;
import persistency.helper.DeviceHelper;

/**
 * Created by benedikt.
 */
public class Utils {
    public static final String[] LOG_TYPES = {"error", "info", "debug"};
    public static final int LOG_ORIGIN_SERVER = 1;
    public static final int LOG_ORIGIN_CLIENT = 2;


    public static String extraFromURI(Uri uri, int index) {
        return uri.getPathSegments().get(index);
    }

    public static Device getDeviceInfo(Context context) {
        long user = PrefUtils.getCurrentUserId(context);
        Device dev = new Device();
        if (user > 0) {
            dev.setUserId(user);
        }
        dev.setIpv4Adress(NetUtils.getIPAddress(true));
        dev.setIpv6Adress(NetUtils.getIPAddress(false));
        dev.setMacAdress(NetUtils.getMACAddress("wlan0"));
        return dev;
    }

    public static long thisDevice(Context context) {
        String ip = getIp(getDeviceInfo(context));
        Device dev = DeviceHelper.getDevice(context, ip);
        return dev.getId();
    }

    public boolean ipsEqual(Device one, Device two) {
        try {
            return getIp(one).equals(getIp(two));
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public static String getIp(Device dev) {
        String ip = dev.getIpv4Adress();
        if (ip == null || ip.equals("")) {
            return dev.getIpv6Adress();
        }
        return ip;
    }


    public static void log(Context context, String message, String type, int origin) {
        ContentValues values = new ContentValues();
        values.put(LogContract.LogEntry.COLUMN_MESSAGE, message);
        values.put(LogContract.LogEntry.COLUMN_LOG_TYPE, type);
        values.put(LogContract.LogEntry.COLUMN_ORIGIN, origin);
        context.getContentResolver().insert(LogContract.LogEntry.CONTENT_URI, values);
    }


    public static String toJson(List<JSONSerializable> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

}
