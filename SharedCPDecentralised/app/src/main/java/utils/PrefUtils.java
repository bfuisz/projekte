package utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by benedikt.
 */
public class PrefUtils {
    public static final String PREFS_FILENAME = "at.fibuszene.sharedcpdecentralised_prefs";
    public static final String USER_OBJECT = "at.fibuszene.prefs_user";


    public static void saveCurrentUserId(Context context, long userId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        prefs.edit().putLong(USER_OBJECT, userId).apply();
    }

    public static long getCurrentUserId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        return prefs.getLong(USER_OBJECT, 0);
    }

}
