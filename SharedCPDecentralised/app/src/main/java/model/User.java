package model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

import persistency.contracts.UserContract;

/**
 * Created by benedikt.
 */
public class User implements Serializable {
    private long id;
    private String email;
    private boolean isSynchronized;


    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (id > 0) {
            values.put(UserContract.UserEntry._ID, this.id);
        }
        values.put(UserContract.UserEntry.COLUMN_EMAIL, this.email);
        values.put(UserContract.UserEntry.COLUMN_SYNCHRONIZED, this.isSynchronized);
        return values;
    }

    public static User from(Cursor cursor) {
        User user = new User();
        user.id = cursor.getInt(UserContract.UserEntry.COLUMN_ID_INDEX);
        user.email = cursor.getString(UserContract.UserEntry.COLUMN_EMAIL_INDEX);
        user.isSynchronized = (cursor.getInt(UserContract.UserEntry.COLUMN_SYNCHRONIZED_INDEX) == 1);
        return user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsSynchronized() {
        return isSynchronized;
    }

    public void setIsSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }
}
