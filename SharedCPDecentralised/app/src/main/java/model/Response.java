package model;

import com.google.gson.Gson;

/**
 * Created by benedikt.
 */
public class Response implements JSONSerializable {

    private static final long serialVersionUID = 7644336039941005971L;

    public enum ResponseCode {
        OKAY, SERVER_ERROR, BAD_REQUEST, UNKNOWN_METHOD, HANDSHAKE;
    }

    private ResponseCode responseCode;
    private String device;
    private String info;
    private Request request;


    public Response() {
    }

    public Response(ResponseCode responseCode, String info) {
        this.responseCode = responseCode;
        this.info = info;
    }

    public Response(ResponseCode responseCode, String device, String info, Request request) {
        this.responseCode = responseCode;
        this.device = device;
        this.info = info;
        this.request = request;
    }


    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public Response setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public String getInfo() {
        return info;
    }

    public Response setInfo(String info) {
        this.info = info;
        return this;
    }

    public String getDevice() {
        return device;
    }

    public Response setDevice(String device) {
        this.device = device;
        return this;
    }

    public Request getRequest() {
        return request;
    }

    public Response setRequest(Request request) {
        this.request = request;
        return this;
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public JSONSerializable fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Response.class);
    }


    @Override
    public String toString() {
        return "Response{" +
                "responseCode=" + responseCode +
                ", device='" + device + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
