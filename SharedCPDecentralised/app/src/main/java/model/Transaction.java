package model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.Gson;

import persistency.contracts.TransactionContract;

/**
 * Created by benedikt.
 */
public class Transaction implements JSONSerializable {
    private long id;
    private long user_id;
    private String message;
    private String source;
    private boolean isSynchronized;

    public Transaction() {
    }


    /**
     * @param user_id
     * @param message
     * @param source
     * @param isSynchronized
     */
    public Transaction(long user_id, String message, String source, boolean isSynchronized) {
        this.user_id = user_id;
        this.message = message;
        this.source = source;
        this.isSynchronized = isSynchronized;
    }

    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (this.id > 0) {
            values.put(TransactionContract.TransactionEntry._ID, this.id);
        }
        values.put(TransactionContract.TransactionEntry.COLUMN_USER_ID, this.user_id);
        values.put(TransactionContract.TransactionEntry.COLUMN_MESSAGE, this.message);
        values.put(TransactionContract.TransactionEntry.COLUMN_SOURCE, this.source);
        values.put(TransactionContract.TransactionEntry.COLUMN_SYNCHRONIZED, this.isSynchronized);

        return values;
    }


    public static Transaction from(Cursor cursor) {
        Transaction tAction = new Transaction();
        tAction.id = cursor.getInt(TransactionContract.TransactionEntry.COLUMN_ID_INDEX);
        tAction.user_id = cursor.getInt(TransactionContract.TransactionEntry.COLUMN_USER_ID_INDEX);
        tAction.message = cursor.getString(TransactionContract.TransactionEntry.COLUMN_MESSAGE_INDEX);
        tAction.source = cursor.getString(TransactionContract.TransactionEntry.COLUMN_SOURCE_INDEX);
        tAction.isSynchronized = cursor.getInt(TransactionContract.TransactionEntry.COLUMN_SYNCHRONIZED_INDEX) == 1;

        return tAction;
    }


    @Override
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public Transaction fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Transaction.class);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    public void setSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }

    @Override
    public String toString() {
        return id + "\n" + message;
    }
}
