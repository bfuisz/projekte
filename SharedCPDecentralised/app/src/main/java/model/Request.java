package model;

import com.google.gson.Gson;

/**
 * Created by benedikt.
 */
public class Request implements JSONSerializable {
    public enum RequestType {
        DEVICE, TRANSACTION, USER, HANDSHAKE
    }

    public enum Operation {
        join, create, update, delete, synchronize, stop
    }

    private RequestType requestType;
    private Operation operation;
    private String payload;

    public Request() {
    }

    public Request(RequestType requestType, Operation operation, String payload) {
        this.requestType = requestType;
        this.operation = operation;
        this.payload = payload;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public Request setRequestType(RequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public String getPayload() {
        return payload;
    }

    public Request setPayload(String payload) {
        this.payload = payload;
        return this;
    }


    public Operation getOperation() {
        return operation;
    }

    public Request setOperation(Operation operation) {
        this.operation = operation;
        return this;
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public JSONSerializable fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Request.class);
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestType=" + requestType +
                ", operation=" + operation +
                ", payload='" + payload + '\'' +
                '}';
    }
}
