package model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.Gson;

import persistency.contracts.DeviceContract;

/**
 * Created by benedikt.
 */
public class Device implements JSONSerializable {
    private long id;
    private String hostName;
    private long userId;
    private String macAdress;
    private String ipv4Adress;
    private String ipv6Adress;
    private int portNumber;
    private boolean isSynchronized;
    private boolean isLocalDevice;


    public Device() {
    }

    public ContentValues values() {
        ContentValues values = new ContentValues();

        if (this.id > 0) {
            values.put(DeviceContract.DeviceEntry._ID, this.id);
        }
        values.put(DeviceContract.DeviceEntry.COLUMN_USER_ID, this.userId);
        values.put(DeviceContract.DeviceEntry.COLUMN_HOST_NAME, this.hostName);
        values.put(DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS, this.macAdress);
        values.put(DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS, this.ipv4Adress);
        values.put(DeviceContract.DeviceEntry.COLUMN_IPV6_ADRESS, this.ipv4Adress);
        values.put(DeviceContract.DeviceEntry.COLUMN_PORT_NUMBER, this.portNumber);
        values.put(DeviceContract.DeviceEntry.COLOUMN_LOCAL_DEVICE, this.isLocalDevice);
        values.put(DeviceContract.DeviceEntry.COLUMN_SYNCHRONIZED, this.isSynchronized);
        return values;
    }

    public static Device from(Cursor cursor) {
        Device dev = new Device();
        dev.id = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_ID_INDEX);
        dev.hostName = cursor.getString(DeviceContract.DeviceEntry.COLUMN_HOST_NAME_INDEX);
        dev.userId = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_USER_ID_INDEX);
        dev.macAdress = cursor.getString(DeviceContract.DeviceEntry.COLUMN_MAC_ADRESS_INDEX);
        dev.portNumber = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_PORT_NUMBER_INDEX);
        dev.ipv4Adress = cursor.getString(DeviceContract.DeviceEntry.COLUMN_IPV4_ADRESS_INDEX);
        dev.ipv6Adress = cursor.getString(DeviceContract.DeviceEntry.COLUMN_IPV6_ADRESS_INDEX);
        dev.isLocalDevice = cursor.getInt(DeviceContract.DeviceEntry.COLOUMN_LOCAL_DEVICE_INDEX) == 1;
        dev.isSynchronized = cursor.getInt(DeviceContract.DeviceEntry.COLUMN_SYNCHRONIZED_INDEX) == 1;
        return dev;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }

    public String getIpv4Adress() {
        return ipv4Adress;
    }

    public void setIpv4Adress(String ipv4Adress) {
        this.ipv4Adress = ipv4Adress;
    }

    public String getIpv6Adress() {
        return ipv6Adress;
    }

    public void setIpv6Adress(String ipv6Adress) {
        this.ipv6Adress = ipv6Adress;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    public void setSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }


    public boolean isLocalDevice() {
        return isLocalDevice;
    }

    public void setLocalDevice(boolean isLocalDevice) {
        this.isLocalDevice = isLocalDevice;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", hostName='" + hostName + '\'' +
                ", userId=" + userId +
                ", macAdress='" + macAdress + '\'' +
                ", ipv4Adress='" + ipv4Adress + '\'' +
                ", ipv6Adress='" + ipv6Adress + '\'' +
                ", portNumber=" + portNumber +
                ", isSynchronized=" + isSynchronized +
                ", isLocalDevice=" + isLocalDevice +
                '}';
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public JSONSerializable fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Device.class);
    }
}
