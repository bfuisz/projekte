package model;

import java.io.Serializable;

/**
 * Created by benedikt.
 */
public interface JSONSerializable extends Serializable {


    public String toJson();

    public JSONSerializable fromJson(String json);

}
