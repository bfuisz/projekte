# README #

Siehe SharedCP. 
Mit dem unterschied, dass diese Version keinen Server zur Kommunikation verwendet.  

# App #
Die App ist im gro�en und ganzen gleich geblieben. Hinzugef�gt wurde lediglich die LogActivity und die LogTabelle zum Loggen 
verschiedener Events.
Die gr��ten �nderungen befinden sich im service package.

## Aufbau Server ##

Der GCMIntentService wurde durch den NSDServerService ausgetauscht und dient zum starten des NSDServers. 
Der NSDServer registriert einen Service im Netzwerk der von anderen Instanzen der App gefunden werden kann. 
Nach der Registrierung sucht der NSDServer Services im lokalen Netzwerk. 
Wird ein Service gefunden, schickt der NSDServer die lokale IP und die Portnummer zum gefundenen Ger�t. 
Das empfangende Ger�t antwortet mit einer Liste bekannter Ger�te. Nach diesem Handshake beginnt der NSDServer
auf dem gennanten Port auf eingehende Verbindungen zu h�ren.  

## Aufbau Client ##

�hnlich wie SharedCP. Der ClipboardSynchronizeService schickt den Inhalt des Clipboards und andere Ger�te. 
Der Unterschied ist jedoch, dass das Ger�t die Nachricht selbst an bekannte Ger�te im Netzwerk broadcastet. 

## TODOs ##

+ IpChangeReceiver 
	broadcastet eine neue Ip im Netzwerk
	
## Vor und Nachteile ##

SharedCP 

+ Einfachere Einbindung des PCs �ber Webinterface. 
+ Schneller
+ global Erreichbar

SharedCPDecentralised

+ kein Server
+ Rechenintensiver

## Lerneffekt ##

Client Server, jmdns. 
