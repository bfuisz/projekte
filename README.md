# README #

Dieses Repository stellt eine Auswahl einiger Apps dar, die ich zu Übungszwecken
programmiert habe. 
Die Apps wurden mit dem Ziel entwickelt so viel wie möglich dabei zu lernen und nicht ein 
release fähiges Produkt zu erstellen. 

### Inhalt des Repositories ###

1. [avoid](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/avoid/?at=master)
	+ Unity Spiel.
	
1. [CountIt](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/CountIt/?at=master)
	+ Counter, Rechner, Countdown, Stoppuhr. 
	
1. [FeedNews](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/FeedNews/?at=master)
	+ Client App für Feedzilla. 
	
1. [QuizApp](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/QuizApp/?at=master)
	+ Quiz basierend auf [MediQuiz Playstore](https://play.google.com/store/apps/details?id=de.quizzb)  
	
1. [SharedCP](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/SharedCP/?at=master)
	+ Server Client Anwendung zum teile des Clipboard Inhaltes. 

1. [SharedCPDecentralised](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/SharedCPDecentralised/?at=master)
	+ Dezentralisierte Version von "SharedCP". 

1. [WhatsAppReader](https://bitbucket.org/bfuisz/projekte/src/773fe51d0564e8e0743d685f27282deabe76ceba/WhatsAppReader/?at=master)
	+ WhatsApp Message Reader. 

Genauere Infos über die jeweiligen Projekte befinden sich im README des Ordners.

### Geplante Projekte ###

**Reddit Client:** 
	Zur Zeit verwende ich zum Browser von Reddit die App Reddit Now. 
	Obwohl die App ausgezeichnet Funktioniert, gibt es einige Dinge die 
	ich gerne anders machen würde. 

[**Irgendwas mit Musik Streaming:**](https://bitbucket.org/bfuisz/listendirble/overview)
	IP Radio oder ähnliches am coolsten wäre es mit Chromecast Support 
	
### Wem gehört dieses Repository ? ###

Name: Benedikt Fuisz

Email: 

* benedikt.fuisz@gmail.com [Mail](mailto:benedikt.fuisz@gmail.com)

* benedikt.fuisz@student.uibk.ac.at [Mail](mailto:benedikt.fuisz@student.uibk.ac.at)