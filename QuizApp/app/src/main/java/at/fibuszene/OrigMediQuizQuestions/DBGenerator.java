package at.fibuszene.OrigMediQuizQuestions;

import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;

public class DBGenerator {

    public DBGenerator() {
    }

    private static final int Richtig1[] = {
            1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
            3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
            1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
            3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
            1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
            3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
            1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
            3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
            3, 3, 4, 1, 4, 2, 2, 1, 3, 2,
            2, 4, 1, 2, 3, 4, 1, 2, 1, 4,
            3, 3, 1, 2, 3, 4, 1, 1, 4
    };

    private static final String ant1[] = {
            "Mexikanische Grippe", "Durchfall zu bekommen", "Schilddrüsenentfernung", "20 m^2", "Hartes Hühnerauge", "Akrophobie", "Abendmahlstreit", "Angioskopie", "Emotionale Störung", "Pumpleistung linke Herzkammer betroffen",
            "Salmonellen", "Herzkranzgefäß", "Stimmheilkunde", "Blutgruppe 0", "Pilze", "Kartelle", "Ca. 30 min nach einer Mahlzeit", "Mangelkrankheiten", "A", "Syringomyelie",
            "Blutgruppe A, B, AB, oder 0", "Angst vor dem Wasser", "Magenbeschwerden", "Basilikum", "Mondscheinkrankheit oder Lichtschrumpfhaut", "Eine Milzerkrankung", "Laibesmedizin", "C/H1B2", "Inkubationszeit", "Vorhalle",
            "Mumps", "Pocken", "Höhepunkt einer Krankheit", "Schuppenflechte führen", "Windpocken", "Säure", "Photoallergie", "Im Gehirn", "dünn", "Grünes Erbrechen",
            "A", "Leber", "Wiederherstellung des Energieflusses im Gehirn", "Schilddrüsenverpflanzung", "Ca. 21000 Fälle", "Muskelschwund durch Untätigkeit", "Auge", "Hüpfniere", "Herzschlag außerhalb des normalen Herzrhythmus", "Krankheiten an den Blutgefäßen",
            "Einnahme pilzbefallener Nahrung", "Halsschmuck", "Endemie", "Golferellenbogen", "9-10 liter", "Myopie", "Tamiflu", "Alphateilchen", "bleibt laut WHO gleich mit zunehmendem Alter", "das Lungenvolumen",
            "Anis", "Masern", "Herzinfarkt", "Lotus", "Differentialblutbild", "Hormonersatztage", "Wachstum", "Arthritis", "die Aktivität des Gehirns", "Skorpione",
            "Lunge", "Ibuprofen", "Ca. 80 Mio.", "Herz", "Antikörper", "Ägypten", "Bikuspide Aortenklappe", "In den Dickdarm", "Esszwang", "Stoffwechselkrankheit",
            "Gold", "Sprite", "Vitamin A", "Platzangst", "Bunter Sittich", "Körpergewicht/Größe in Meter", "Vitamin E", "Augenerkrankung", "Blinzeln", "Angst vor Alluminium",
            "Angst vor Obst", "Angst vor Gemüse", "Angst zu kochen", "Angst vor Affen", "Angst vor Eis", "Angst vor Hunden", "Weiderinde", "Medalliengewinner+Entdecker", "Vitamin A", "ca 1g",
            "Es gibt keine", "Pinguinsäure", "Alkoholdehydrogenase 2", "Rückatmung von O2", "Alles", "Knochen", "Morgens", "N2", "40ml"
    };

    private static final String ant2[] = {
            "Tamigrippe", "Falten zu bekommen", "Schilddrüsenunterfunktion", "100 m^2", "Verhärtete Fußsohle", "Agoraphobie", "Blutarmut", "Bending", "Querschnitt des Gehirnes", "Pumpleistung beider Herzkammern beeinträchtigt",
            "Leukozyten", "Nebenschilddrüse", "Atemtechnik", "Blutgruppe AB", "Würmer", "Rondelle", "Gleich nach Einnahme der Mahlzeit", "degenerative Krankheiten", "B", "Sirenomelie",
            "Blutgruppe B oder 0", "Angst von einer Ente beobachtet zu werden", "Reizdarm", "Pfefferminze", "Zellulitis (Entzündung)", "Eine Bauchspeicheldrüsenerkrankung", "Humanmedizin", "A/H2N3", "Bakubationszeit", "Atrium",
            "Legionärskrankheit", "Malaria", "Gesichtsfeld-Entzündung", "Erbrechen und Durchfall führen", "Masern", "Gift", "Plasmaallergie", "Im Herzen", "groß", "Gelbes Erbrechen",
            "1", "Muskulatur", "Wiederherstellung des Energieflusses der Atemwege", "Schilddrüsenüberfunktion", "Ca. 43000 Fälle", "Vitamin-B1-Mangelkrankheit", "Magen", "Schwimmniere", "Herzschlag, der nur bei Angstzuständen entsteht", "Geschlechtskrankheiten",
            "Den Stich einer subtropischen Libellenart", "Venen", "Epidemie", "Tennisarm", "7-9 liter", "Dialyse", "Ibuprofen", "Hormon", "sinkt laut WHO mit zunehmendem Alter", "Herzkranzgefäße entzündet",
            "Ackerstiefmütterchen", "Mumps", "Vorhofflimmern", "Lorbeer", "Differentialblutbildung", "Hormonersatztherapie", "Stoffwechsel", "Hexenschuss", "die Herzfunktion", "Tsetsefliegen",
            "Leber", "Aspirin", "Ca. 40 Mio.", "Niere", "Fresszellen", "Indien", "Anatomische Aortenklappe", "In den Zwölffingerdarm", " Waschzwang", "Wachstumsstörung",
            "Ceramic", "Cola", "Calcium", "Angst vor Aligatoren", "Grüne Amsel", "Körpergewicht/(Größe in Meter)^2", "Vitamin B12", "Magenentzündngung", "Niesen", "Angst vor Knoblauch",
            "Angst vor Gemüse", "Angst vor Flugzeugen", "Angst vor Magiern", "Angst vor dem Himmel", "Angst vor Melonen", "Angst vor Büchern", "Gänseblümchen", "1972", "Vitamin B", "12-15g",
            "ca 3,0 Promille", "Elefantensäure", "Lactase", "Rückatmung von CO2", "Säuren", "Knorpel", "Mittags", "O2", "120ml"
    };

    private static final String ant3[] = {
            "Spanische Grippe", "ewig zu leben", "Schilddrüsenüberfunktion", "70 m^2", "Weiche Muskeln", "Arachnophobie", "Erinnerung", "Vasoligatur", "Angststörung", "Pumpleistung rechte Herzkammer betroffen",
            "Cholesterin", "Brustkorb", "Wahrheitsforschung", "Blutgruppe B", "Bakterien", "Bordelle", "Morgens", "funktionale Krankheiten", "0", "Phocomelie",
            "Blutgruppe A oder B", "Angst vor Männern in Lederjacken", "Kurzsichtigkeit", "Kümmel", "Buchweizenkrankheit oder Buchweizenausschlag", "Eine Magenschleimhauterkrankung", "Veterinärmedizin", "1/A1N2", "Pukubationszeit", "Diele",
            "Wundstarrkrampf", "Grippe", "Hautausschlag", "Erblinden und Demenz führen", "Scharlach", "Medikament", "Strahlenallergie", "Im All", "klein", "Rotes Erbrechen",
            "2D", "Herz", "Wiederherstellung des Energieflusses im Körper", "Schilddrüsenüberfunktion", "Ca.125000 Fälle", "Angeborene Sehschwäche", "Ohr", "Kletterniere", "chronischer Bluthochdruck", "Virusbedingte Krankheiten",
            "Stechmücken", "Warzen", "Panleukopenie", "Volleyballarm", "5-6 liter", "Diastole", "Eukalyptol", "Endorphin", "steigt laut WHO mit zunehmendem Alter", "eine Nierenunterversorgung",
            "Fette Henne", "Windpocken", "langsamer Herzschlag", "Estragon", "Differentialblutbad", "Hormoneinsatzherapie", "Bewegung", "Gicht", "die elektrische Aktivität des Gehirns", "Krokodile",
            "Niere", "Nervengift", "Ca. 20 Mio.", "Leber", "rote Blutkörperchen", "Japan", "Trykuspide Aortenklappe", "In den Magen", "Schlafzwang", "Depression",
            "Flusspferdzahn", "Fanta", "Eisen", "Angst vor Mäusen", "Gelbe Meise", "Körpergewicht * (Größe in Meter / 2)", "Vitamin B6", "Lungenerkrankung", "Schluckauf", "Angst vor Vampiren",
            "Angst vor Fleisch", "Angst vor Fahrrädern", "Angst vor Phobien", "Angst vor Musik", "Angst vor Musik", "Angst vor Teenagern", "Sonnenblumen", "München", "Vitamin C", "ca 5g",
            "ca 4,0 Promille", "Ameinsensäure", "Amylase", "Plazeboeffekt", "Basen", "Fingernägel", "Abends", "h2", "160ml"
    };
    private static final String ant4[] = {
            "Vogelgrippe", "Durst zu haben", "Schilddrüsenverpflanzung", "40 m^2", "Steifer Nacken", "Erythrophobie", "Gedächtnisverlust", "Varizenstripping", "Trennlinie einer Amputation", "Das Blut staut sich in die Körpervenen zurück",
            "Streptokokken", "Hauptschlagader", "Stammbaum", "Blutgruppe A", "Viren", "Fontanelle", "Morgens & Abends", "Infektionskrankheiten", "AB", "Syndaktylie",
            "Blutgruppe A, B oder AB", "Abneigung gegen Holzbänke", "Schulterschmerzen", "Melisse", "Photosensibilitätsreaktion", "Eine Gallenwegserkrankung", "Fuchsmedizin", "A/H1N1", "Rekubationszeit", "Vestibül",
            "Pfeiffer-Drüsenfieber", "Masern", "Gelbsucht", "einer Hirnhautentzündung führen", "Hepatitis", "Hormon", "Schattenallergie", "Im Taschenrechner", "dick", "Schwarzes Erbrechen",
            "C", "Knochen", "Wiederherstellung des Energieflusses der Seele", "Schilddrüsenunterfunktion", "Ca. 1000 Fälle", "Vorrübergehende Gedächtnisstörung", "Gehirn", "Wanderniere", "Herzstillstand", "Gibt es nicht",
            "Bakterien im Trinkwasser", "Krampfadern", "Pandemie", "Raucherbein", "3-4 liter", "Systole", "ACC Akut", "Immunschwäche", "ist laut WHO völlig uninteressant", "eine Schwellung des Gewebes",
            "Immergrün", "Keuchhusten", "Bluthochdruck", "Baldrian", "definiertes Blutbild", "Harnersatztherapie", "Vermehrung", "Arthrose", "den Blutzuckergehalt", "Kurzschwanzmäuse",
            "Milz", "Rattengift", "Ca. 10 Mio", "Magen", "weiße Blutkörperchen", "China", "Atypische Aortenklappe", "In die Gallenblase", "Bewegungszwang", "Essstörung",
            "Elfenbein", "Wasser", "Vitamin C", "Gibt es nicht", "Grauer Star", "Größe in Meter/Körpergewicht", "Vitamin F1", "Haunerkrankung", "Gähnen", "Angst vor Häusern",
            "Angst vor Chips", "Angst vor Autos", "Gibt es nicht", "Angst vor Phobien", "Angst vor Zucker", "Angst vor Eichhörnchen", "Birkenrinde", "Gar nichts", "Vitamin TV", "7-10g",
            "ca 6,0 Promille", "Katzensäure", "R-Entzym", "bringt gar nichts", "Säuren und Basen gleich", "Zahnschmelz", "Nachts", "gar keins", "240ml"
    };

    private static final String erk[] = {
            "Die Schweinegrippe wird auch Mexikanische Grippe genannt, weil sie in Mexiko zuerst aufgetreten ist.", "Dagegen hilft wohl nur eincremen und der Beauty Doc :-)", " Die Schilddrüsenüberfunktion äußert sich oft in Untergewicht und starker Unruhe des Patienten.", "Die Lungenoberfläche muss so groß sein, damit der Gasautausch in den Alveolen in ausreichendem Maße erfolgen kann.", "Beim Hühnerauge handelt es sich um eine lokale Verhornungsstörung der Haut. Meist ist wegen der mechanischen Belastung der Fuß betroffen.", "Agoraphobie ist genau genommen die Angst vor weiten Räumen, großen Menschenansammlungen etc. Bei Angst vor engen Räumen spricht man von Klaustrophobie.", "Anamnese ist wörtlich übersetzt die Erinnerung, im medizischen Kontext wird jedoch die systematische Befragung des Patienten so genannt.", "Beim Varizenstripping werden die betroffenen Venen (Varizen) aus der Unterhaut herausgezogen.", "Diese emotionale oder auch psychische Störung ist leider weit verbreitet und zeigt sich in großer Unzufriedenheit mit sich selbst.", "Wenn beide Herzkammern betroffen sind spricht man von global, wenn nur eine Seite betroffen ist wird es Partialinsuffizienz genannt.",
            "Das Cholesterin spielt bei der Entstehung der Arteriosklerose (Gefäßverengung durch Ablagerungen) eine große Rolle.", "Die Aorta ist die Hauptschlagader des Menschen und führt das Blut direkt vom Herzen an der hinteren Bauchwand entlang. Von dort zweigen sich schließlich weiter versorgende Gefäße ab.", "Bei der Logopädie geht es darum Störungen der Sprache zw. der Aussprache zu behandeln.", "Blutgruppe AB haben nur 5% der Menschen in Deutschland.", "Meningokokken sind kugelförmige Bakterien die Hinrhautentzündungen auslösen können.", "Genauer gesagt hat der Säugling mehrere Fontanellen, eine große vordere und eine hintere Fontanelle und jeweils zwei Seitenfontanellen. Sie sind aus Bindegewebe und ermöglichen das weitere Schädelwachstum.", "Man sollte immer etwas mit dem Zähne putzen warten, da z.B. Säuren bei sofortiger Reinigung den Schmelz aggressiver angreifen.", "Bei degenerativen Krankheiten spricht man vom Verlust der ursprünglichen Zellfunktion.", "Es kann nur die Blutgruppe 0 haben, weil dieses Merkmal rezessiv vererbt wird.", "Syndaktylie tritt vor allem bei Säuglingen auf, da in der Embyonalentwicklung die Finger zusammen angelegt werden.",
            "Da die Blutgruppenmerkmale A & B kodominant und 0 rezessiv vererbt wird ist alles möglich. Würde man nicht nur den Phänotyp sondern auch die genetischere Ausprägung kennen gäbe es nicht mehr so viele Möglichkeiten.", "Ja vor was man nicht alles Angst haben kann:-)", "Bei Kurzsichtigkeit ist der Augapfel zu kurz bzw. die Brechung der Linse zu stark", "Die Zitronenmelisse wurde bereits in der Antike als Heilmittel verwendet.", "Es handelt sich um eine genetisch bedingte Erkrankung, bei der die Haut extrem empfindlich auf Sonnenlicht reagiert. Die Patienten sterben häufig in frühem Alter an Hautkrebs.", "Diese Entzündung der Bauchspeicheldrüse (Pankreas) ist relativ gefährlich für den Patienten, weil dieses Organ ziemlich Mittig in den Bauchraum eingebunden ist und operativ schwer zu erreichen.", "Die Veterinärmedizin beschäftigt sich nicht nur mit den Krankheiten der Tiere, sondern auch mit Tierschutz, Forschung und der Kontrolle von tierischen Lebensmitteln ", "Er gehört zur Gruppe A der Influenzaviren und kann bekanntermaßen nicht nur Schweine sondern auch Menschen befallen", "Die Inkubationszeit kann nur wenige Tage aber auch Monate und Jahre betragen.", "Das Atrium (Vorhof) des Herzens füllt sich in der Diastole und wird duch jeweils eine Segelklappe von der jeweiligen Kammer getrennt ",
            "Tetanus ist eine bakterielle Infektionskrankheit welche die muskelsteuernden Nervenzellen befüllt.", "Bei den Masern handelt es sich eigentlich um eine Kinderkrankheit gegen die eine Impfung vorgenommen werden kann. Erfolgt diese in jungen Jahren nicht, so tritt sie wieder häufiger und schwerwiegend auf.", "Sowohl der Krankheitshöhepunkt als auch der höchste Stand der Fieberkurve  wird mit dem griechischen Wort Akme betitelt.", "Der Name der Rotaviren beruht auf ihrer radähnlichen Struktur unter dem Elektroenenmikroskop.", "Scharlach wird durch Streptokokken (Bakterien) verursacht und seine Spätfolgen sollen durch Antibiotka vermieden werden.", "Insulin ist ein Hormon der Bauchspeicheldrüse,genauer gesagt aus dem endokrinen Teil.", "Photoallergie der Haut kommt durch eine vorherige Sensibilisierung z.B. durch Medikamente zu Stande.", "Der Sinusknoten des Herzens ist der Schrittmacher des Herzreizleitungssystems", "In Deutschland gelten übrigends Menschen unter 1.50. als kleinwüchsig", "Gelbfieber ist eine virale Infektionserkrankung, bei der eine Gelbsucht aufgrund einer Leberentzündung entstehen kann.",
            "Zu den Influenzaviren der Gruppe A gehören u.a. die Spanische Grippe oder auch die Vogelgrippe", "In der Muskulatur wird bei einer Kontraktion der Energieträger ATP verbraucht, den der Körper nur mit Hilfe von Magnesium herstellen kann.", "Eine alternativmedizinische Lehre, bei der sich gesundheitliche Störungen jeder Art als Muskelblockade äußern.", "Bei einer Hypothyreose stellt die Schilddrüse aus unterschiedlichen Gründen zu wenig Schilddrüsenhormone her. Das Gegenteil heißt Hyperthyreose.", "Richtig eraten:-) Interessant dabei ist, dass 2009 unterdurchschnittlich wenige normale Influenzafälle aufgetreten sind.", "Durch Mangel an Thiamin (Vit B1) kommt es zu Störungen der Nerven, der Muskulatur und des Herz-Kreislauf Systems. Sie ist in Südostasien noch weit verbreitet.", "In der Paukenhöhle (Mittelohr), zwischen dem Amboss, einem weiteren Gehörknöchelchen, und dem ovalen Fenster des Innenohrs", "Die Niere entsteht in der Embryonalentwicklung im Becken und wandert dann nach kranial (oben). Ebenso wandert diese wieder nach unten, wenn ihr Nierenlager, aus Fett bestehend, abgebaut wird.", "Diese Herzschläge außerhalb des Rhythmus können jedoch auch normal sein, wenn sie nur selten auftreten.", "Sexuell übertragbaren Krankheiten können von Viren, Bakterien, Protozoen (Einzeller), Pilzen und Arthropoden (Gliederfüßler) verursacht werden.",
            "Diese Tropenkrankheit wird durch die weibliche Stechmücke Anopheles übertragen. Dabei verursachen die eingeschleusten Parasiten eine Blutarmut(Anämie)", "Die Krampfadern entstehen durch Abflussstörungen des venösen Systems, häufig durch Venenklappendefekte.", "Lokal aber nicht zeitlich begrenzte Krankheiten sind z.B. Malaria oder Cholera", "Die beschriebenen Störungen sind durch die jeweilige Sportart bedingte Reizungen der Sehnenursprünge der betroffenen Muskeln.", "Das genaue Volumen hängt von der Größe und Gewicht der Person ab. Hin und wieder einen halben Liter beim Blutspenden abzugeben schadet aber nicht :-)", "Bei der Systole kontrahiert sich das Herz von der Spitze ausgehend und es kommt zum Auswurf des Blutes in die Aorta.", "Tamiflu (Wirkstoff Oseltamivir) verhindert die Weitervermerung des Virus im Körper, wirkt also virostatisch.", "Das männliche Sexualhormon wird von den Leydigzellen in den beiden Hoden gebildet.", "Der Bodymassindex steigt zwar mit zunehmendem Alter aber nicht so stark wie das Gewicht des Durchschnittsdeutschen.", "Die Schwellung wird durch ein Flüssigkeitsansammlung hervorgerufen. Diese erfolgt durch Wasseraustritt aus den Gefäßen in das Gewebe.",
            "Anis gehört zur Familie der Doldenblütler und soll die Drüsen des Magen-Darm Traktes anregen.", "Bei Mumps handelt es sich um eine Virusinfektion welche die Speicheldüsen befüllt.Die Ohrspeicheldrüse heißt auf Latein Parotis", "Man spricht generell von Bradykardie bei einem Puls weniger 60 Schlägen pro Minute. Das Gegenteil davon wäre Tachykardie, also schneller Herzschlag", "Die Baldrianwurzel enthült u.a. den Wirkstoff Valerensäure, der beruhigend und schlaffördernd wirkt.", "Beim Differentialblutbild werden alle Unterformen derLeukozyten (weißen Blutkörperchen) differenziert abgezählt. Dies erfolgt heute maschinell nach dem Prinzip der Durchflusszytometrie.", "Die Hormonersatztherapie wird vor allem bei Frauen in der Menopause angewandt und soll durch Gabe von Ästrogen die Begleitsymptome wie Hitzewallungen etc. lindern.", "Die Kennzeichen eines Lebewesens sind: Stoffwechsel, Fortpflanzung, Reizbarkeit, Wachstum, Evolution", "Die Arthritis wäre eine Gelenkentzündung und bei Gicht handelt es sich um Harnsäurekristalle, welche in Gelenken ausfallen und dort Schmerzen bereiten.", "Das Elektronenencephalogramm zeigt die Aktivität der Hirnrinde an. Die Herzfunktion wird mittels EKG erfasst.", "Die Tsetsefliege überträgt den Erreger aus der Trypanosomenfamilie (Einzeller). Im Endstadium der Krankheit fallen die Patienten in den typischen Dämmerzustand",
            "Neprologen beschäftigen sich sowohl mit dem Organ (Niere), als auch mit Bluthochdruck und dessen Regulation.", "Das Cumarin als Gerinnungshemmer ist nicht nur in Marcumar sondern auch in Rattengift zu finden.", "Bei gut 80 Millionen Einwohnern wäre das zumindest angebracht :-)", "Eine US Forscherin soll aus Rinderstammzellen differenzierte Rindernierenzellen geklont haben.", "Die roten Blutkörperchen sind die Sauerstofftransporter des Blutes und für die rote Farbe verantwortlich.", "Die Ärzte aus dem Reich der Mitte entwickelten die Akupunktur bereits vor 3000 Jahren. Ihre tatsächliche Wirkung ist stark umstritten.", "Darunter versteht man eine Aortenklappe, die statt aus drei Taschen nur aus zwei Taschen besteht. Die Aortenklappe verbindet die linke Herzhauptkammer mit der Hauptschlagader (Aorta)", "Der Zwöffingerdarm (Duodenum) schließt direkt an den Magen an und ist Teil des Dünndarms", "Es handelt sich um eine ernst zunehmende Erkrankung wobei der Schlafdrang plötzlich auftritt und kaum oder gar nicht unterdrückt werden kann.", "Bei dieser Essstörung handelt es sich um die sogenannte Ess-Brech-Sucht.",
            "", "", "", "", "", "", "", "", "", "Alliumphobie - die Angst vor Knoblauch",
            "Lachanophobie - die Angst vor Gemüse", "Motorphobie - die Angst vor Automobilien", "Mageirocophobie - die Angst zu kochen", "Ouranphobie - die Angst vor dem Himmel", "Melophobie - die Angst vor Musik", "Ephebiphobie - die Angst vor Teenagern", "Weidenrinde - Kommt natürlich in Weiderinde vor. Klingt komisch ist aber so.", "1972 - 10,9,7,2 sind Vitamin K-abhängige Gerinnungsfaktoren. ;P ", "Vitamin A - Es ist essentiell für den Sehvorgang Netzhaut.", "ca 7-10g - Der Abbau erfolgt kontinuierlich.",
            "ca 4,0 Promille - Das entspircht 4g Alkohol pro Liter Blut.", "Ameisensäure - Es ist äußerst giftig und wird bei einer Methanolvergiftung gebildet. Es wird mti Alkohol behandelt.", "Alkoholdehydrogenase 2 - Dieses Entzym ist für den Alkoholabbau notwendig. Darus folg eine Alkoholintolleranz. Lactase fehlt bei 80%.", "Rückatmung von CO2 - Der CO2 Partialdruck steigt wieder und normalisiert sich. Ein gewisser CO2 Partialdruck ist nötig um CO2 ausatmen zu können.", "Basen - Sie können das Gewebe verflüssigen.", "Zahnschmelz - Er enthält Hydroxylapatit, ein sehr hartes Mineral", "Morgens - die Temperatur wird zentral vom hypothalamischen Kreislaufzentrum gesteuert und man somit schneller auf Turen kommt", "Stickstoff(N2)- Aus diesem Grund wird in der Taucherflasche der Stickstoff durch Helium ersetzt.", "240ml - (chemisch) Wegen Hämoglobinbindung, das ist 10x mehr als rein physikalisch bei 37\260C möglich ist"
    };
    private static final String fragen[] = {
            "Wie wird die Schweinegrippe auch genannt?", "Was versteht man unter Rhytidophobie? Die Angnst...", "Was versteht man unter Hyperthyreose?", "Wie groß ist die Oberfläche einer menschlichen Lunge?", "Was bedeutet Clavus durus?", "Wie bezeichnet man die Krankheit Platzangst?", "Was bedeutete Anamnese?", "Wie nennt man das Entfernen von Krampfadern?", "Borderline ist der Name für ein/eine ...?", "Was bedeutet kardiale Globalinsuffizienz?",
            "Welche Werte entscheiden häufig über Herzinfarkt- oder Schlaganfallrisiko?", "Was ist die Aorta?", "Was ist Logopädie?", "Welche Blutgruppe ist in Deutschland am seltensten?", "Was sind Meningokokken?", "Wie heißt die Öffnung im Schädelknochen eines Kleinkindes?", "Wann sollte man idealerweise die Zähne putzen?", "Leberzirrhose und Krebs sind im Allgemeinen?", "Beide Elternteile habe die Blutgruppe 0. Das Kind hat dann die Blutgruppe...?", "Wie heißt die Missbildung, bei der Finger oder Zehen miteinander verwachsen sind?",
            "Die Blutgruppen der Eltern sind A und B. Das Kind hat dann...?", "Was genau versteht man unter Anatidaephobie?", "Wer an Myopie leidet, leidet unter...?", "Welches Kraut sorgt für einen gesunden Schlaf?", "Welche Krankheit wird kurz XP genannt?", "Was ist eine Pankreatitis?", "Wie nennt man die medizinische Heilkunde des tierischen Körpers?", "Wie heißt der Erreger der Schweinegrippe?", "Die Zeitspanne zwischen Ansteckung und Ausbruch einer Krankheit ist die ...?", "Wie nennt man in der Anatomie die Vorkammer des Herzens?",
            "Wie heißt die schwerwiegende Erkrankung namens Tetanus auf Deutsch ?", "Welche Epidemie grassierte ab 2008/2009 in der Schweiz?", "Was bedeutet Akme in der Medizin?", "Was sind Rotaviren? Erreger, die zu..", "Wobei handelt es sich nicht um eine typische Kinderkrankheit?", "Insulin ist ein/eine...?", "Wie wird Lichtallergie noch genannt?", "Wo ist der Sinusknoten?", "Ein Mensch, der an Nanismus leidet, ist sehr...?", "Wie wird Gelbfieber in Lateinamerika noch genannt?",
            "Auslöser der Epidemien und Pandemien sind in den meisten Fällen Influenzaviren der Gruppe A\u2026?", "Wofür braucht unser Körper Magnesium?", "Was ist Kinesiologie?", "Was versteht man unter Hypothyreose?", "Wie viele deutsche Schweinegrippe-Patienten wurden dem Robert-Koche-Institut bis 09/2009 gemeldet?", "Beriberi ist eine...?", "Wo im menschlichen Körper befindet sich der Steigbügel?", "Das Absenken der Niere durch verminderte Muskelspannung oder starke Gewichtsabnahme nennt man... ?", "Extrasystole bedeutet...?", "Venerische Krankheiten sind...?",
            "Wodurch wird Malaria übertragen?", "Was sind Varizen?", "Eine Infektionskrankheit, die in einem Gebiet ständig herrscht oder immer wieder auftritt, nennt man?", "Welche Erkrankung gibt es nicht?", "Wie viel Blut befinden sich nromalerweise im menschlichen Körper?", "Wie nennt man die Kontraktionsphase des Herzmuskels?", "Wie heißt eines der existierenden Grippemedikamente gegen den Schweinegrippe-Virus?", "Was ist Testosteron?", "Der BMI Idealwert (kg/m\302^2)...?", "Was sind Ödeme?",
            "Welches Kraut hat eine verdauungsfördernde Wirkung?", "Die lateinische Bezeichnung Parotitis epidemica steht für...?", "Bradykardie bedeutet einfach ausgedrückt?", "Welches dieser Kräuter wirkt beruhigend und schlaffördernd?", "Die Abkürzung DiffBB steht für ...?", "HET steht in der Medizin für ...?", "Welches Merkmal ist kein notwendiges Kennzeichen eines Lebewesens?", "Wie nennt man eine nicht-entzündliche, chronische Erkrankung der Gelenkknorpel?", "Ein EEG (Elektronenzephalogramm) misst...?", "Welche Tiere sind überträger der Afrikanischen Schlafkrankheit?",
            "Nephrologen beschäftigen sich mit der...?", "Welchen Bestandteil enthält das Medikament Marcumar (=Blutverdünner)?", "Für wie viele Deutsche muss im Falle einer akuten Pandemie ein Impfstoff zur Verfügung stehen?", "Welches Organ wurde 2003 zum 1. Mal geklont?", "Erythrozyten sind...?", "Woher kommt das Heilverfahren durch Akupunktur ursprünglich?", "Was ist die häufigste angeborene Herzanomalie?", "Wohin gibt die Bauchspeicheldrüße die von ihr produierten Verdauungsenzyme ab?", "Was ist Narkolepsie?", "Was ist Bulimie?",
            "Woraus wurde der erste Zahnersatz gemacht?", "Karotten sind gut für die Augen. Was kann man alternativ auch essen/trinken", "Was können Hunde selbst herstellen, Menschen aber nicht?", "Was ist eine Agoraphobie?", "Was ist eine Augenkrankheit?", "Wie wird der BMI berechnet?", "Von welchem Vitaminmangel sind Veganer eher betroffen?", "Ein Glaukom ist eine..?", "Was ist die unwillkürliche Kontraktion des Diaphragma (Zwerchfell)?", "Was ist Alliumphobie?",
            "Was ist Lachanophobie?", "Was ist Motorphobie?", "Was ist Mageirocophobie", "Was ist Ouranphobie?", "Was ist Melophobie?", "Was ist Ephebiphobie?", "Woher stammt das Salicin des Aspirins ursprünglich?", "Was haben die Olympischen Spiele von München und die Blutgerinnung gemeinsam?", "Welches Vitamin brauchst du unbedingt zum Fernsehen?", "Wie viel Gramm Alkohol baut der Körper pro Stunde ab?",
            "Was ist die tödliche Alkoholkonzentration?", "Methanol bildet im Körper?", "Welches Entzym fehtl bei 50% der Asiaten?", "Was bewirkt das Atmen in eine Plastiktüte beim Hyperventilieren?", "Was dringt bei Verätzungen tiefer in dei Haut ein?", "Was ist die härteste Körpersubstanz?", "Wann ist die Körperkerntemperatur am höchsten?", "Welches Gas kann beim Auftauchen aus größerer Tiefe zu Bläschen im Blut führen?", "Wie viel O2 kann pro Liter Blut gebunden werden?"
    };

    String fragenCount[] = {
            "Frage 1 von 10", "Frage 2 von 10", "Frage 3 von 10", "Frage 4 von 10", "Frage 5 von 10", "Frage 6 von 10", "Frage 7 von 10", "Frage 8 von 10", "Frage 9 von 10", "Frage 10 von 10"
    };


    public static void createObjects() {
        int index = 0;

        for (String frage : fragen) {
            Question question = new Question();
            question.setQuestionCreator(Constants.SYSTEM_CREATOR);
            question.setQuestion(frage);

            question.save();

            Answer answer1 = new Answer();
            answer1.setAnswer(ant1[index]);

            Answer answer2 = new Answer();
            answer2.setAnswer(ant2[index]);

            Answer answer3 = new Answer();
            answer3.setAnswer(ant3[index]);

            Answer answer4 = new Answer();
            answer4.setAnswer(ant4[index]);

            switch (Richtig1[index]) {
                case 1:
                    answer1.setCorrect(true);
                    answer1.setExplanation(erk[index]);
                    break;

                case 2:
                    answer2.setCorrect(true);
                    answer2.setExplanation(erk[index]);
                    break;

                case 3:
                    answer3.setCorrect(true);
                    answer3.setExplanation(erk[index]);
                    break;
                case 4:
                    answer4.setCorrect(true);
                    answer4.setExplanation(erk[index]);
                    break;
            }


            answer1.setQuestion(question);
            answer2.setQuestion(question);
            answer3.setQuestion(question);
            answer4.setQuestion(question);

            answer1.save();
            answer2.save();
            answer3.save();
            answer4.save();

            question.addAnswer(answer1);
            question.addAnswer(answer2);
            question.addAnswer(answer3);
            question.addAnswer(answer4);


            index++;
        }
    }


//    public void next(View view)
//    {
//
//        textview.setText((new StringBuilder("Das nächste mal wirds besser!\r\nNimm dir lieber noch ein paar Stunden im Labor :) \r\nDein Score: ")).append(score).append(" von 10 Fragen richtig.").toString());
//
//        if (score == 4 || score == 5 || score == 6)
//        {
//            textview1.setText((new StringBuilder("Das war schon ganz gut :)\r\nDu hast das Zeug zum Assistenzarzt! \r\nDein Score: ")).append(score).append(" von 10 Fragen richtig.").toString());
//
//        if (score == 7 || score == 8 || score == 9)
//        {
//            textview2.setText((new StringBuilder("Du bist richtig gut! :)\r\nWenn du so weiter machst kannst du dich bald Facharzt nennen!\r\nDein Score: ")).append(score).append(" von 10 Fragen richtig.").toString());
//
//        textview3.setText((new StringBuilder("Einfach nur Perfekt! :)\r\nDu hast das Zeug zum Chefarzt!\r\nDein Score: ")).append(score).append(" von 10 Fragen richtig.").toString());
//
//    }


}
