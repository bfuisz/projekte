package at.fibuszene.models;


import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Question extends SugarRecord<Question> implements Serializable {
    private String question; //the actual question
    private List<Answer> answers;   //the possible answers
    private long clickCount;    //the amount the user answered this question
    private long correct;   //the amount of times the user was rigth
    private String questionCreator; //the creator of the question S-> System(Generator), U -> User
    private String category = "default";    //an optional category

    @Ignore
    private boolean locked;

    public Question() {
        this.answers = new ArrayList<Answer>();
    }

    public Question(String question, List<Answer> answers, long clickCount, long correct, String questionCreator, String category, boolean locked) {
        this.question = question;
        this.answers = answers;
        this.clickCount = clickCount;
        this.correct = correct;
        this.questionCreator = questionCreator;
        this.category = category;
        this.locked = locked;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public long getClickCount() {
        return clickCount;
    }

    public void setClickCount(long clickCount) {
        this.clickCount = clickCount;
    }

    public long getCorrect() {
        return correct;
    }

    public void setCorrect(long correct) {
        this.correct = correct;
    }

    public String getQuestionCreator() {
        return questionCreator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void incrementClickCount() {
        this.clickCount++;
    }

    public void incrementCorrectCount() {
        this.correct++;
    }

    public void setQuestionCreator(String questionCreator) {
        this.questionCreator = questionCreator;
    }

    public Answer getCorrectAnswer() {
        for (Answer tmp : this.answers) {
            if (tmp.isCorrect()) {
                return tmp;
            }
        }
        return null;
    }

    public void addAnswer(Answer answer) {
        if (this.answers != null) {
            this.answers.add(answer);
        }
    }


    @Override
    public String toString() {
        return "[Question Meta: " + this.id + ", Creator:" + this.questionCreator + ", ClickCount: " + this.clickCount + ", Correct:" + this.correct + "]\n" + this.question + "\n" + this.answers;
    }

}
