package at.fibuszene.models;


import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import at.fibuszene.utils.Constants;

/**
 * Important lesson learned:
 * println needs a message (= do not print null )in database was bc e.getMessage = null
 * no further info as to why the exception occured in the first place (InvocationTargetException).
 * After extensiv investigation (library code step by step...) it turns out this class was the culprit.
 * Calling Database Stuff in the Constructor leads to the getDatabase() Method beeing called recursivly
 * thus leading to a StackOverflowError(most likely just guessing over here) which was masked by the
 * InvocationTargetException and voila recipe for desperation.
 * off to test this theory:
 * - fix the recursive call
 * - changing from local library (where the nullpointerbug has been fixed) to maven
 * - run test and report back
 * => boom hours of debugging paid off
 */
public class Quiz extends SugarRecord<Quiz> implements Serializable {

    private List<QuizQuestions> questions; //the questions the user got
    private Date date; //the time the user took this quiz (=starttime, could be extended to the time the user took...)
    private int questionAmount;

    @Ignore
    private int currentQuestion;

    public Quiz() {
        this.questionAmount = Constants.DEFAULT_QUESTION_AMOUNT;
        this.currentQuestion = 0;
        this.questions = new ArrayList<QuizQuestions>(questionAmount);
    }

    public QuizQuestions next() {
        if (currentQuestion < 0) { //in case the user tries to go back from 0
            currentQuestion = 0;
        }
        if (this.currentQuestion >= this.questions.size()) {
            //game finished
            return null;
        } else {
            QuizQuestions tmp = this.questions.get(currentQuestion);
            currentQuestion++;
            return tmp;
        }
    }

    public QuizQuestions previouse() {
        currentQuestion = currentQuestion - 2;//next increments immediately
        return next();
    }


    public QuizQuestions current() {
        int current = currentQuestion - 1;//bc of next()
        //adjust for out of bounds
        if (current < 0) {
            current = 0;
        }
        if (current >= this.questions.size()) { //in case the user tries to go back from 0
            current = this.questions.size();
        }
        return this.questions.get(current);
    }

    public void answered(int index) {
        QuizQuestions quizQuestions = current();
        Question tmpQuest = quizQuestions.getQuestion();
        Answer answer = tmpQuest.getAnswers().get(index);
        tmpQuest.incrementClickCount();

        quizQuestions.setAnswer(answer);
        quizQuestions.setCorrect(answer.isCorrect());

        if (answer.isCorrect()) {
            tmpQuest.incrementCorrectCount();
        }
        tmpQuest.save();
        quizQuestions.save();
    }

    public void addQuestion(QuizQuestions quizQuestion) {
        if (this.questions == null) {
            this.questions = new ArrayList<QuizQuestions>();
        }
        this.questions.add(quizQuestion);
    }

    public int getCorrectAmount() {
        int correct = 0;

        for (QuizQuestions tmp : this.questions) {
            if (tmp.isCorrect()) {
                correct++;
            }
        }
        return correct;
    }

    /**
     * Calculates a ratio between correct and wrong
     * e.g. 10/10 = 1
     * 5/10 = 0.5
     * and so forth the UI will decide what to to with that
     */
    public float calculateScore() {
        int correct = getCorrectAmount();
        return (correct == 0 && this.questionAmount != 0) ? 0 : ((float) correct / (float) this.questionAmount);
    }


    public int getCurrent() {
        return this.currentQuestion;
    }

    public int getQuestionAmount() {
        return questionAmount;
    }

    public void setQuestionAmount(int questionAmount) {
        this.questionAmount = questionAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<QuizQuestions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuizQuestions> questions) {
        this.questions = questions;
    }


}
