package at.fibuszene.models;


import com.orm.SugarRecord;

import java.io.Serializable;


public class Answer extends SugarRecord<Answer> implements Serializable {
    private Question question;
    private boolean correct ; //whether this answer is the correct answer or not
    private String answer;  //the answer string
    private String explanation;//an optional explanation for the answer


    public Answer() {
    }

    public Answer(Question question, boolean correct, String answer, String explanation) {
        this.question = question;
        this.correct = correct;
        this.answer = answer;
        this.explanation = explanation;

    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Answer)) return false;

        Answer answer = (Answer) o;

        if (id != null ? !id.equals(answer.id) : answer.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.answer + " " + ((correct) ? "x" : "") + "\n";
    }
}
