package at.fibuszene.models;


import com.orm.SugarRecord;


public class User extends SugarRecord<User> {
    //save to shared prefs for the moment

    private String name;

    public User() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
