package at.fibuszene.models;


import com.orm.SugarRecord;

import java.io.Serializable;


public class QuizQuestions extends SugarRecord<QuizQuestions> implements Serializable {
    private Quiz quiz;//the quiz this mapping belongs to
    private Question question; //the question which was answered
    private Answer answer;  // the answer clicked by user
    private boolean correct; //shortcut to whether this was the correct answer


    public QuizQuestions() {
    }

    public QuizQuestions(Quiz quiz, Question question, Answer answer, boolean correct) {
        this.quiz = quiz;
        this.question = question;
        this.answer = answer;
        this.correct = correct;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }


}

