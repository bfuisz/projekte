package at.fibuszene.carousel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import at.fibuszene.quiz.EditQuestionsActivity;
import at.fibuszene.quiz.LearningModeActivity;
import at.fibuszene.quiz.QuizActivity;
import at.fibuszene.quiz.QuizHistory;
import at.fibuszene.quiz.R;
import at.fibuszene.utils.Constants;

//https://github.com/mrleolink/SimpleInfiniteCarousel
public class CarouselFragment extends Fragment implements View.OnClickListener {
    private int position;
    private float scale;
    private String title;
    private int resId;

    public static CarouselFragment newInstance(Context context, int position, float scale) {
        Bundle args = new Bundle();
        args.putFloat(Constants.SCALE_KEY, scale);
        args.putInt(Constants.ITEM_POSITION_KEY, position);
        return (CarouselFragment) Fragment.instantiate(context, CarouselFragment.class.getName(), args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.scale = this.getArguments().getFloat(Constants.SCALE_KEY);
        this.position = getArguments().getInt(Constants.ITEM_POSITION_KEY);
        this.title = getResources().obtainTypedArray(R.array.nav_titel).getString(position);
        this.resId = getResources().obtainTypedArray(R.array.nav_drawable).getResourceId(position, -1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.carousel_nav_item, null);

        TextView tv = (TextView) rootView.findViewById(R.id.navigationText);
        tv.setText(title);
        Button button = (Button) rootView.findViewById(R.id.navigationContent);
        if (resId > 0) {
            button.setBackgroundResource(resId);
        } else {
            button.setBackgroundResource(R.drawable.ic_launcher);
        }
        CustomLinearLayout root = (CustomLinearLayout) rootView.findViewById(R.id.root);
        button.setOnClickListener(this);
        root.setScaleBoth(scale);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent(getActivity(), QuizActivity.class);
                break;
            case 1:
                intent = new Intent(getActivity(), LearningModeActivity.class);
                break;
            case 2:
                intent = new Intent(getActivity(), EditQuestionsActivity.class);
                break;
            case 3:
                intent = new Intent(getActivity(), QuizHistory.class);
                break;
        }
        startActivity(intent);
    }
}
