package at.fibuszene.carousel;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import at.fibuszene.quiz.HomeActivity;
import at.fibuszene.quiz.R;
import at.fibuszene.utils.Constants;


public class CarouselAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    private CustomLinearLayout cur = null;
    private CustomLinearLayout next = null;
    private HomeActivity context;
    private FragmentManager fm;
    private float scale;

    public CarouselAdapter(HomeActivity context, FragmentManager fm) {
        super(fm);
        this.fm = fm;
        this.context = context;

    }

    @Override
    public CarouselFragment getItem(int position) {
        // make the first pager bigger than others
        if (position == Constants.FIRST_PAGE)
            scale = Constants.BIG_SCALE;
        else
            scale = Constants.SMALL_SCALE;

        position = position % Constants.PAGES;
        return CarouselFragment.newInstance(context, position, scale);
    }

    @Override
    public int getCount() {
        return Constants.PAGES * Constants.LOOPS;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset >= 0f && positionOffset <= 1f) {
            cur = getRootView(position);
            next = getRootView(position + 1);
            cur.setScaleBoth(Constants.BIG_SCALE - Constants.DIFF_SCALE * positionOffset);
            next.setScaleBoth(Constants.SMALL_SCALE + Constants.DIFF_SCALE * positionOffset);
        }
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private CustomLinearLayout getRootView(int position) {
        return (CustomLinearLayout)
                fm.findFragmentByTag(this.getFragmentTag(position)).getView().findViewById(R.id.root);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + context.pager.getId() + ":" + position;
    }
}