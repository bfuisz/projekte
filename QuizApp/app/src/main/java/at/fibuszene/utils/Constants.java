package at.fibuszene.utils;


public class Constants {
    public static final boolean DEBUG = true;
    public static final int DEFAULT_QUESTION_AMOUNT = 10;
    public static final int SCORE_BACKGROUND_ALPHA = 85;
    public static final String NAVIGATION_ITEM_TITEL = "NAVIGATION_ITEM_TITEL_KEY";
    public static final String ITEM_POSITION_KEY = "ITEM_POSITION_KEY";
    public static final String SCALE_KEY = "SCALE_KEY";
    public static final String HISTORY_MODE = "HISTORY_MODE_KEY";
    public static final String RECREATE = "RECREATE_AFTER_ORIENTATION";
    public static String ERROR_OCCURED_KEY = "ERROR_OCCURED_KEY";
    public static String ERROR_MSG_KEY = "ERROR_OCCURED_KEY";
    public static String QUIZ_ID = "QUIZ_ID_KEY";
    public static String QUESTION_ID = "QUESTION_ID_KEY";
    public static final String SYSTEM_CREATOR = "S";
    public static final String USER_CREATOR = "U";

    public final static int PAGES = 4;
    public final static int LOOPS = 1000;
    public final static int FIRST_PAGE = PAGES * LOOPS / 2;
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.7f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;


}
