package at.fibuszene.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeAdapter;

import java.util.List;

import at.fibuszene.quiz.AddEditActivity;
import at.fibuszene.quiz.R;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class EditQuizAdapter extends SwipeAdapter {
    private Context context;
    private List<Question> questions;

    public EditQuizAdapter(Context context, List<Question> questions) {
        this.context = context;
        this.questions = questions;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup viewGroup) {
        return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.edit_questions_item, null);
    }

    public void edit(int position) {

        Intent intent = new Intent(context, AddEditActivity.class);
        intent.putExtra(Constants.QUESTION_ID, getItemId(position));
        context.startActivity(intent);
    }

    public void delete(int position) {
        long id = getItemId(position);
        this.questions.remove(position);
        Question.deleteAll(Question.class, " id = ?", new String[]{"" + id});
        this.notifyDataSetChanged();
    }

    /*generateView gets recycled position whereas fillValues gets
     *the actual position
     * see https://github.com/daimajia/AndroidSwipeLayout/blob/master/library/src/main/java/com/daimajia/swipe/SwipeAdapter.java#L38-L64
     * fill values gets called after recycling or creating the listitem
     */
    @Override
    public void fillValues(int position, View view) {
        final int pos = position;
        TextView t = (TextView) view.findViewById(R.id.position);
        t.setText(getItem(position).getQuestion() + "");
        ImageButton button = (ImageButton) view.findViewById(R.id.trash);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(pos);
            }
        });
        button = (ImageButton) view.findViewById(R.id.edit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(pos);
            }
        });
    }

    @Override
    public int getCount() {
        return this.questions.size();
    }

    @Override
    public Question getItem(int position) {
        return this.questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.questions.get(position).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void addQuestions(List<Question> moreQuestions) {
        this.questions.addAll(moreQuestions);
        notifyDataSetChanged();
    }


}
