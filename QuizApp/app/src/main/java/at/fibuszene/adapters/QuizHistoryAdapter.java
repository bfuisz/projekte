package at.fibuszene.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeAdapter;

import java.util.List;

import at.fibuszene.quiz.QuizHistory;
import at.fibuszene.quiz.R;
import at.fibuszene.models.Quiz;


public class QuizHistoryAdapter extends SwipeAdapter {
    private QuizHistory quizHistory;
    private List<Quiz> quizList;

    public QuizHistoryAdapter(QuizHistory quizHistory, List<Quiz> quizList) {
        this.quizHistory = quizHistory;
        this.quizList = quizList;
    }


    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipeQuizHistory;
    }

    @Override
    public View generateView(int position, ViewGroup viewGroup) {
        return ((LayoutInflater) quizHistory.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.quiz_history_item, null);
    }

    @Override
    public void fillValues(int position, View view) {
        Quiz quiz = quizList.get(position);
        final int pos = position;
        TextView txtView = (TextView) view.findViewById(R.id.quizName);
        txtView.setText(quiz.getDate().toString());
        txtView = (TextView) view.findViewById(R.id.quizData);
        txtView.setText(quiz.getCorrectAmount() + " / " + quiz.getQuestionAmount());
        ImageButton trash = (ImageButton) view.findViewById(R.id.trash);

        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(pos);
            }
        });

    }

    public void delete(int pos) {
        Quiz quiz = getItem(pos);
        Quiz.deleteAll(Quiz.class, " id = ? ", new String[]{quiz.getId() + ""});
        this.quizList.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.quizList.size();
    }

    @Override
    public Quiz getItem(int position) {
        return this.quizList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.quizList.get(position).getId();
    }
}
