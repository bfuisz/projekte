package at.fibuszene.adapters;

import android.os.AsyncTask;

import java.io.Serializable;
import java.util.List;

import at.fibuszene.contentproviders.database.QuestionCPDB;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class LearnModeAdapter implements Serializable {

    private List<Question> questions;
    private int current = 0;
    private int page = 0;

    public LearnModeAdapter() {
        this.questions = QuestionCPDB.list(Constants.DEFAULT_QUESTION_AMOUNT, page * Constants.DEFAULT_QUESTION_AMOUNT);
    }

    public int checkBounds() {
        if (this.current <= 0) {
            this.current = 0;
            return this.current;
        }
        if (this.current >= size()) {
            return (this.current = size());
        }
        if ((this.current + 4) >= size()) {
            refreshList();
        }
        return this.current;
    }

    public Question next() {
        int pos = checkBounds();
        Question quest = this.questions.get(pos);
        current++;
        return quest;
    }

    public Question previouse() {
        current -= 2;
        return next();
    }

    public Question current() {
        try {
            return this.questions.get(current - 1);
        } catch (IndexOutOfBoundsException ioobe) {
            return this.questions.get(current);
        }
    }


    public Answer correctAnswer() {
        List<Answer> answers = current().getAnswers();
        for (Answer answer : answers) {
            if (answer.isCorrect()) {
                return answer;
            }
        }
        return null;
    }

    public int size() {
        return this.questions.size() - 1;
    }

    public void replaceQuestions(List<Question> list) {
        this.questions.addAll(list);
    }

    public void refreshList() {
        page++;
        new AsyncTask<Void, Void, List<Question>>() {
            @Override
            protected List<Question> doInBackground(Void... params) {
                return QuestionCPDB.list(Constants.DEFAULT_QUESTION_AMOUNT, page * Constants.DEFAULT_QUESTION_AMOUNT);
            }

            @Override
            protected void onPostExecute(List<Question> questions) {
                super.onPostExecute(questions);
                replaceQuestions(questions);

            }
        }.execute();
    }


}
