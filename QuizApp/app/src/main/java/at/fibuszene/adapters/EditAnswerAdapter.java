package at.fibuszene.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.cocosw.undobar.UndoBarController;
import com.daimajia.swipe.SwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.models.Answer;
import at.fibuszene.quiz.R;


public class EditAnswerAdapter extends SwipeAdapter {
    private Activity context;
    private Answer lastRemoved;
    private List<Answer> answers;

    public EditAnswerAdapter(Activity context, List<Answer> answers) {
        this.context = context;
        if (answers != null) {
            this.answers = answers;
        } else {
            this.answers = new ArrayList<Answer>();
        }
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Answer getItem(int position) {
        return answers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return answers.get(position).getId();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup viewGroup) {
        return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.edit_answer_item, null);
    }


    public void delete(final int position) {
        Answer answer = getItem(position);
        if (answer != null) {
            lastRemoved = answers.remove(position);
            new UndoBarController.UndoBar(context).message("Undo-bar title").listener(new UndoBarController.UndoListener() {
                @Override
                public void onUndo(Parcelable parcelable) {
                    if (lastRemoved != null) {
                        answers.add(position, lastRemoved);
                        lastRemoved = null;
                        notifyDataSetChanged();
                    }
                }
            }).show();
            notifyDataSetChanged();
        }
    }


    @Override
    public void fillValues(int position, final View view) {
        final int pos = position;
        final Answer answer = getItem(pos);

        ImageButton button = (ImageButton) view.findViewById(R.id.trash);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(pos);
            }
        });

        EditText t = (EditText) view.findViewById(R.id.editPosition);
        t.setText(answer.getAnswer());

        RadioButton chkBX = (RadioButton) view.findViewById(R.id.correctAnswerCheckbox);
        chkBX.setChecked(answer.isCorrect());


        chkBX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Answer answer = getItem(pos);
                RadioButton tmp = (RadioButton) v;
                answer.setCorrect(tmp.isChecked());
                deselectOthers(pos);
            }
        });
    }

    public void deselectOthers(int except) {
        int len = this.answers.size();
        for (int i = 0; i < len; i++) {
            if (i != except) {
                answers.get(i).setCorrect(false);
            }
        }
        notifyDataSetChanged();
    }


    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
        notifyDataSetChanged();
    }

    public void addAnswer(Answer answer) {
        this.answers.add(0, answer);
        notifyDataSetChanged();
    }
}
