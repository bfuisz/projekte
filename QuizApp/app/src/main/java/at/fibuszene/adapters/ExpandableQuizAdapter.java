package at.fibuszene.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import at.fibuszene.quiz.R;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.models.Quiz;
import at.fibuszene.models.QuizQuestions;
import at.fibuszene.utils.Constants;

/**
 * Created by benedikt on 01.10.2014.
 */
public class ExpandableQuizAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<QuizQuestions> quizQuestionses;
    private Quiz quiz;


    //Group == Question
    //Children == List<Answers> for said question
    //Child == Answer
    public ExpandableQuizAdapter(Context context, Quiz quiz) {
        this.context = context;
        this.quiz = quiz;
        this.quizQuestionses = quiz.getQuestions();
    }


    @Override
    public int getGroupCount() {
        return quizQuestionses.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.quizQuestionses.get(groupPosition).getQuestion().getAnswers().size();
    }

    @Override
    public Question getGroup(int groupPosition) {
        return this.quizQuestionses.get(groupPosition).getQuestion();
    }

    @Override
    public Answer getChild(int groupPosition, int childPosition) {
        return this.quizQuestionses.get(groupPosition).getQuestion().getAnswers().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return this.quizQuestionses.get(groupPosition).getQuestion().getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Question quest = getGroup(groupPosition);
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.question_header, null);
        }
        TextView txtView = (TextView) convertView.findViewById(R.id.textViewQ);
        txtView.setText(quest.getQuestion());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Answer answer = getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.question_item, null);
        }
        TextView txtView = (TextView) convertView.findViewById(R.id.textViewA);
        txtView.setText(answer.getAnswer());

        if (answer.isCorrect()) { //if the answer is correct color it green
            convertView.setBackgroundColor(context.getResources().getColor(R.color.mostly_rigth));
        } else { //otherwise check is the user clicked the quest
            convertView.setBackgroundColor(context.getResources().getColor(R.color.quiz_item_background));
        }

        if (answer.equals(this.quizQuestionses.get(groupPosition).getAnswer()) && !answer.isCorrect()) {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.mostly_wrong));
        }

        convertView.setAlpha(Constants.SCORE_BACKGROUND_ALPHA);

        return convertView;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
