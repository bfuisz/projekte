package at.fibuszene.listeners;

import android.os.AsyncTask;

import java.util.List;

import at.fibuszene.adapters.EditQuizAdapter;
import at.fibuszene.adapters.QuizHistoryAdapter;
import at.fibuszene.contentproviders.database.QuestionCPDB;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class QuestionScrollListener extends EndlessOnScrollListener {
    private EditQuizAdapter adapter;

    public QuestionScrollListener(EditQuizAdapter adapter) {
        this.adapter = adapter;
    }


    @Override
    public void onLoadMore(final int page, int totalItemsCount) {
        new AsyncTask<Void, Void, List<Question>>() {
            @Override
            protected List<Question> doInBackground(Void... params) {
                return QuestionCPDB.list(Constants.DEFAULT_QUESTION_AMOUNT, page * Constants.DEFAULT_QUESTION_AMOUNT);
            }

            @Override
            protected void onPostExecute(List<Question> questions) {
                super.onPostExecute(questions);
                adapter.addQuestions(questions);
            }
        }.execute();
    }
}
