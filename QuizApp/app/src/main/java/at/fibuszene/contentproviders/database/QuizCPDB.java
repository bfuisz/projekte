package at.fibuszene.contentproviders.database;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.models.Quiz;
import at.fibuszene.models.QuizQuestions;

public class QuizCPDB {


    public static Quiz newQuiz(int limit) {
        Quiz quiz = plainQuiz(limit);
        List<Question> questions = QuestionCPDB.listRandom(limit); //QuestionCPDB.list(limit);
        Collections.shuffle(questions);
        for (Question tmp : questions) {
            QuizQuestions quizQuestion = new QuizQuestions();
            Collections.shuffle(tmp.getAnswers());
            quizQuestion.setQuestion(tmp);
            quizQuestion.setQuiz(quiz);
            quizQuestion.save();
            quiz.addQuestion(quizQuestion);
        }
        return quiz;
    }

    public static Quiz plainQuiz(int limit) {
        Quiz quiz = new Quiz();
        quiz.setQuestionAmount(limit);
        quiz.setDate(new Date(System.currentTimeMillis()));
        quiz.save();
        return quiz;
    }


    public static void saveQuiz(Quiz quiz) {
        quiz.save();
        for (QuizQuestions tmp : quiz.getQuestions()) {
            tmp.setQuiz(quiz);
            tmp.save();
        }
    }

    public static Quiz getQuiz(long id) {
        Quiz quiz = Quiz.findById(Quiz.class, id);
        List<QuizQuestions> qqs = QuizQuestions.find(QuizQuestions.class, " quiz = ?", new String[]{"" + id});

        for (QuizQuestions tmpQQ : qqs) {
            List<Answer> list = Answer.find(Answer.class, " question = ?", new String[]{"" + tmpQQ.getQuestion().getId()});
            tmpQQ.getQuestion().setAnswers(list);
        }

        quiz.setQuestions(qqs);
        return quiz;
    }


    public static void attachQuizQuestions(Quiz quiz) {
    }

    public static List<Quiz> listQuiz() {
        List<Quiz> quizzes = Quiz.listAll(Quiz.class);
        for (Quiz tmp : quizzes) {
            tmp.setQuestions(QuizQuestions.find(QuizQuestions.class, " quiz = ?", new String[]{tmp.getId() + ""}));
        }
        return quizzes;
    }


    public static List<Quiz> list(int limit, int offset) {
        return Quiz.find(Quiz.class, null, null, null, offset + ", " + limit);
    }


}
