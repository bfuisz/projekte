package at.fibuszene.contentproviders.database;

import com.orm.query.Select;

import java.math.MathContext;
import java.util.List;
import java.util.Set;

import at.fibuszene.OrigMediQuizQuestions.DBGenerator;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.models.Quiz;


public class QuestionCPDB {

    public static void saveOrUpdateQuestion(Question quest) {
        quest.save();
        for (Answer answer : quest.getAnswers()) {
            answer.setQuestion(quest);
            answer.save();
        }
    }

    public static Question find(long id) {
        Question quest = Question.findById(Question.class, id);
        List<Answer> answers = Answer.find(Answer.class, " question = ?", new String[]{quest.getId() + ""});
        quest.setAnswers(answers);
        return quest;
    }

    public static List<Question> list() {
        List<Question> quests = Question.listAll(Question.class);
        attachAnswers(quests);
        return quests;
    }

    public static List<Question> list(int limit) {
        List<Question> quests = Question.find(Question.class, null, null, null, null, "" + limit);
        attachAnswers(quests);
        return quests;
    }

    public static List<Question> list(int limit, int offset) {
        List<Question> quests = Question.find(Question.class, null, null, null, null, offset + "," + limit);
        attachAnswers(quests);
        return quests;
    }

    public static void attachAnswers(List<Question> quests) {
        for (Question tmp : quests) {
            tmp.setAnswers(Answer.find(Answer.class, " question = ? ", new String[]{"" + tmp.getId()}));
        }
    }


    public static List<Question> listRandom(int limit) {
        long count = Question.count(Question.class, null, null);

        //doesn't work on htc desire 500

//        String inClause = "";
//        long random = 0;
//        for (int i = 0; i < (limit * 10); i++) {
//            random = (((int) (Math.random() * 1000)) % count);
//            inClause += random + ", ";
//        }
//        inClause = inClause.trim();
//        inClause = inClause.substring(0, inClause.lastIndexOf(","));
//        List<Question> quests = new Select<Question>(Question.class).where(" id IN (" + inClause + ")").list();
//        quests = quests.subList(0, limit);
//        attachAnswers(quests);
//        return quests;

        //TODO: choose random entries not random pages
        int randomPage = (int) (count / limit);
        int page = ((int) (Math.random() * 1000)) % randomPage;
        return list(limit, limit * page);
    }


    public static void regenerateQuestions() {
        Question.deleteAll(Question.class, " question_creator = ? ", new String[]{"S"});
        DBGenerator.createObjects();
    }
}
