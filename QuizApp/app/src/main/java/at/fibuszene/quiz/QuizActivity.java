package at.fibuszene.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import at.fibuszene.contentproviders.database.QuizCPDB;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.models.Quiz;
import at.fibuszene.models.QuizQuestions;
import at.fibuszene.utils.Constants;

public class QuizActivity extends ActionBarActivity implements View.OnClickListener {
    private TextView questionTextView;
    private LinearLayout questionAnswerLinLay;
    private List<View> answersView;
    private ProgressBar progressBar;

    private Quiz quiz;

    private boolean leave = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_fragment_layout);
        initViews();

        if (savedInstanceState != null && savedInstanceState.containsKey(Constants.RECREATE)) {
            this.quiz = (Quiz) savedInstanceState.getSerializable(Constants.RECREATE);
            setQuestion(quiz.current().getQuestion());
        } else {
            this.quiz = QuizCPDB.newQuiz(Constants.DEFAULT_QUESTION_AMOUNT);
            setQuestion(quiz.next().getQuestion());
        }
        this.progressBar.setMax(quiz.getQuestionAmount());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.RECREATE, this.quiz);
    }


    public void initViews() {
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.questionTextView = (TextView) findViewById(R.id.questionTextView);
        this.questionAnswerLinLay = (LinearLayout) findViewById(R.id.questionAnswerLayout);
        answersView = new ArrayList<View>();
    }

    /*
     * Set progress
     * process answer
     * new question
     */
    @Override
    public void onClick(View answerItem) {

        this.progressBar.setProgress(quiz.getCurrent());
        int index = this.answersView.indexOf(answerItem);
        this.quiz.answered(index);
        QuizQuestions quizQuestions = quiz.next();

        if (quizQuestions != null) {
            setQuestion(quizQuestions.getQuestion());
        } else {
            //quiz finished
            finishQuiz();
        }
    }

    public void setQuestion(Question question) {
        questionAnswerLinLay.removeAllViews();
        int index = 0;
        int viewIndex = 0;
        TextView txtView;
        View view;
        int mil = 50;
        for (Answer answer : question.getAnswers()) {
            try {
                view = answersView.get(index);
            } catch (IndexOutOfBoundsException ioobe) {
                view = View.inflate(this, R.layout.quiz_answer_layout, null);
                answersView.add(view);
            }
            txtView = (TextView) view.findViewById(R.id.answer1);
            txtView.setText(answer.getAnswer());
            view.setOnClickListener(this);
            this.questionAnswerLinLay.addView(view, viewIndex);
            index++;
            viewIndex++;
            YoYo.with(Techniques.SlideInRight).delay(0).duration(50 + mil).playOn(view);
            mil += 50;
        }
        this.questionTextView.setText(question.getQuestion());
    }

    public void finishQuiz() {
        Intent intent = new Intent(this, FinishedQuiz.class);
        intent.putExtra(Constants.QUIZ_ID, this.quiz.getId());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        setQuestion(this.quiz.previouse().getQuestion());
        this.progressBar.setProgress(quiz.getCurrent());

// alternativ
//        if (leave) {
//            Intent intent = new Intent(this, HomeActivity.class);
//            startActivity(intent);
//        }
//        Toast.makeText(this, "If you leave now forfeit press again to leave!", Toast.LENGTH_SHORT).show();
//
//        leave = true;
//        new Timer().schedule(new TimerTask() {//resets leaveFlag
//            @Override
//            public void run() {
//                leave = false;
//            }
//        }, 1500);
    }

}
