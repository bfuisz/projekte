package at.fibuszene.quiz;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import at.fibuszene.adapters.LearnModeAdapter;
import at.fibuszene.listeners.CustomSwipeListener;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;


public class LearningModeActivity extends ActionBarActivity {
    private FrameLayout rootView;
    private View questionView;
    private View answerView;
    private LearnModeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning_mode);
        this.rootView = (FrameLayout) findViewById(R.id.container);
        this.questionView = getLayoutInflater().inflate(R.layout.question_fragment_layout, null);
        this.answerView = getLayoutInflater().inflate(R.layout.answer_fragment_layout, null);
        if (savedInstanceState != null) {
            this.adapter = (LearnModeAdapter) savedInstanceState.getSerializable("Adapter");
        } else {
            this.adapter = new LearnModeAdapter();
        }
        rootView.setOnTouchListener(new CustomSwipeListener(this));
        showQuestion(this.adapter.current(), true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("Adapter", this.adapter);
    }


    public void showQuestionAgain(View view) {
        showQuestion(this.adapter.current(), false);
    }

    public void checkAnswer(View view) {
        showAnswer(this.adapter.correctAnswer());
    }

    public void previousQuestion(View view) {
        showQuestion(this.adapter.previouse(), false);
    }

    public void nextQuestion(View view) {
        showQuestion(this.adapter.next(), true);
    }

    private void showQuestion(Question question, boolean next) {
        if (next) {
            YoYo.with(Techniques.ZoomInRight).delay(0).duration(200).playOn(this.rootView);
        } else {
            YoYo.with(Techniques.ZoomInLeft).delay(0).duration(200).playOn(this.rootView);
        }
        ((TextView) this.questionView.findViewById(R.id.questionTextView)).setText(question.getQuestion());
        this.rootView.removeAllViews();
        this.rootView.addView(this.questionView);
    }

    private void showAnswer(Answer answer) {
        YoYo.with(Techniques.FlipInX).delay(0).duration(200).playOn(this.rootView);
        ((TextView) this.answerView.findViewById(R.id.answerTextView)).setText(answer.getAnswer());
        TextView txtView = (TextView) this.answerView.findViewById(R.id.explTextView);
        txtView.setText(answer.getExplanation());
        txtView.setMovementMethod(new ScrollingMovementMethod());

        this.rootView.removeAllViews();
        this.rootView.addView(this.answerView);
    }


}
