package at.fibuszene.quiz;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.apache.commons.lang3.ObjectUtils;

import java.util.List;

import at.fibuszene.adapters.EditAnswerAdapter;
import at.fibuszene.contentproviders.database.QuestionCPDB;
import at.fibuszene.models.Answer;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class AddEditActivity extends ActionBarActivity {
    private Question question;
    private EditText questionView, descriptionEditText;
    private ListView answerListView;
    private EditAnswerAdapter answerAdapter;
    private View header;
    private ViewSwitcher switcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        switcher = (ViewSwitcher) findViewById(R.id.switcher);

        long id = getIntent().getLongExtra(Constants.QUESTION_ID, 0);
        if (id > 0) {
            //edit question
            this.question = QuestionCPDB.find(id);
            try {
                getActionBar().setTitle(R.string.title_activity_edit_question);
            } catch (NullPointerException npe) {

            }
        } else {
            //new
            this.question = new Question();
            question.save();
            try {
                getActionBar().setTitle(R.string.title_activity_add_question);
            } catch (NullPointerException npe) {

            }

        }

        this.questionView = (EditText) findViewById(R.id.questionEditText);
        this.questionView.setText(question.getQuestion());

        this.descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        String description = "";
        for (Answer answer : question.getAnswers()) {
            if (answer.getExplanation() != null) {
                description = answer.getExplanation();
                break;
            }
        }
        descriptionEditText.setText(description);

        this.answerListView = (ListView) findViewById(R.id.answersListView);
        this.answerListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        this.answerListView.addHeaderView(getAddListItem());

        this.answerAdapter = new EditAnswerAdapter(this, question.getAnswers());
        this.answerListView.setAdapter(answerAdapter);
    }


    public void editAnswers(View view) {
        view.setActivated(true);
        switcher.showNext();
    }

    public void editDescription(View view) {
        view.setActivated(true);
        switcher.showNext();
    }

    public View getAddListItem() {
        this.header = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.edit_answer_item, null);
        ImageButton button = (ImageButton) header.findViewById(R.id.trash);
        button.setImageResource(android.R.drawable.ic_menu_add);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtView = (TextView) header.findViewById(R.id.position);
                String text = txtView.getText() + "";

                if (!TextUtils.isEmpty(text)) {
                    RadioButton checkBox = (RadioButton) header.findViewById(R.id.correctAnswerCheckbox);
                    Answer answer = new Answer();
                    answer.setQuestion(question);
                    answer.setCorrect(checkBox.isChecked());
                    answer.setAnswer(text.trim());
                    answerAdapter.addAnswer(answer);
                    txtView.setText("");
                } else {
                    Toast.makeText(AddEditActivity.this, "Text explaining what went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return header;
    }


    public void submitQuestion() {
        String question = this.questionView.getText().toString();
        if (!TextUtils.isEmpty(question)) {
            this.question.setQuestion(question);
            this.question.setQuestionCreator(Constants.USER_CREATOR);
            this.question.setAnswers(this.answerAdapter.getAnswers());
            for (Answer answer : this.question.getAnswers()) {
                if (answer.isCorrect()) {
                    answer.setExplanation(this.descriptionEditText.getText() + "");
                }
            }
            QuestionCPDB.saveOrUpdateQuestion(this.question);
            NavUtils.navigateUpFromSameTask(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit_cancel_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_cancel) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if (id == R.id.action_add) {
            submitQuestion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
