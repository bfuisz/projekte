package at.fibuszene.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import at.fibuszene.adapters.EditQuizAdapter;
import at.fibuszene.contentproviders.database.QuestionCPDB;
import at.fibuszene.listeners.QuestionScrollListener;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class EditQuestionsActivity extends ActionBarActivity {
    private EditQuizAdapter adapter;
    private ListView questionsListView;
    private List<Question> questions;
    private QuestionScrollListener questionScroller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_questions);
        this.questionsListView = (ListView) findViewById(R.id.questionsListView);

        this.questions = QuestionCPDB.list(Constants.DEFAULT_QUESTION_AMOUNT, 0);
        this.adapter = new EditQuizAdapter(this, this.questions);
        this.questionsListView.setAdapter(this.adapter);
        this.questionScroller = new QuestionScrollListener(adapter);
        this.questionsListView.setOnScrollListener(questionScroller);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_questions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            Intent intent = new Intent(this, AddEditActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
