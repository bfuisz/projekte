package at.fibuszene.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.widget.ExpandableListView;
import android.widget.TextView;

import at.fibuszene.adapters.ExpandableQuizAdapter;
import at.fibuszene.contentproviders.database.QuizCPDB;
import at.fibuszene.models.Quiz;
import at.fibuszene.utils.Constants;


public class FinishedQuiz extends ActionBarActivity {
    private long quizID;
    private Quiz quiz;
    private ExpandableListView expListView;
    private ExpandableQuizAdapter expAdapter;
    private TextView scoreView;
    private boolean history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finished_quiz);
        history = getIntent().getBooleanExtra(Constants.HISTORY_MODE, false);
        initQuiz();
        initView();
    }

    public void initView() {
        this.scoreView = (TextView) findViewById(R.id.scoreView);
        this.expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expAdapter = new ExpandableQuizAdapter(this, this.quiz);
        this.expListView.setAdapter(expAdapter);
        setScore();
    }

    public void setScore() {
        float correct = quiz.calculateScore();
        String tmp = this.quiz.getCorrectAmount() + " / " + this.quiz.getQuestionAmount();
        this.scoreView.setText(tmp);
        if (correct <= 0.33) {
            scoreView.setBackgroundColor(getResources().getColor(R.color.mostly_wrong));
        } else if (correct >= 0.33 && correct <= 0.66) {
            scoreView.setBackgroundColor(getResources().getColor(R.color.fiftyfifty));
        } else {
            scoreView.setBackgroundColor(getResources().getColor(R.color.mostly_rigth));
        }
        scoreView.getBackground().setAlpha(Constants.SCORE_BACKGROUND_ALPHA);
    }

    @Override
    public void onBackPressed() {
        if (history) {
            Intent intent = new Intent(this, QuizHistory.class);
            NavUtils.navigateUpTo(this, intent);
        } else {
            goHome(null);
        }
    }

    public void initQuiz() {
        this.quizID = getIntent().getLongExtra(Constants.QUIZ_ID, 0);
        if (quizID == 0) {
            goHome("An error occured while fetching the Quiz"); //TODO error msg R.string ....
        }
        this.quiz = QuizCPDB.getQuiz(this.quizID);
    }

    public void goHome(String msg) {
        Intent intent = new Intent(this, HomeActivity.class);
        if (msg != null) {
            intent.putExtra(Constants.ERROR_OCCURED_KEY, true);
            intent.putExtra(Constants.ERROR_MSG_KEY, msg);
        }
        startActivity(intent);
    }


}
