package at.fibuszene.quiz;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import at.fibuszene.carousel.CarouselAdapter;
import at.fibuszene.contentproviders.database.QuestionCPDB;
import at.fibuszene.models.Question;
import at.fibuszene.utils.Constants;


public class HomeActivity extends ActionBarActivity {
    public ViewPager pager;
    private CarouselAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        pager = (ViewPager) findViewById(R.id.navigationPager);
        adapter = new CarouselAdapter(this, this.getSupportFragmentManager());

        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(adapter);
        pager.setCurrentItem(Constants.FIRST_PAGE);
        pager.setOffscreenPageLimit(3);
        pager.setPageMargin(-200);


        boolean error = getIntent().getBooleanExtra(Constants.ERROR_OCCURED_KEY, false);
        if (error) {
            Toast.makeText(this, getIntent().getStringExtra(Constants.ERROR_MSG_KEY), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onStart() {

        long count = Question.count(Question.class, " question_creator = ? ", new String[]{Constants.SYSTEM_CREATOR});
        if (count == 0) {
            new Thread() {
                @Override
                public void run() {
                    regenerateQuestions();
                }
            }.start();
        }
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_regenerate_questions) {
            regenerateQuestions();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void regenerateQuestions() {
        hideButtonsShowProgress();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                QuestionCPDB.regenerateQuestions();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                hideProgressShowButtons();
            }
        }.execute();
    }


    public void hideButtonsShowProgress() {
//        upperNavButtons.setVisibility(View.INVISIBLE);
//        lowerNavButtons.setVisibility(View.INVISIBLE);
//        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressShowButtons() {
//        upperNavButtons.setVisibility(View.VISIBLE);
//        lowerNavButtons.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.INVISIBLE);
    }


}
