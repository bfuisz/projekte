package at.fibuszene.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import at.fibuszene.adapters.QuizHistoryAdapter;
import at.fibuszene.contentproviders.database.QuizCPDB;
import at.fibuszene.utils.Constants;


public class QuizHistory extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private ListView quizListView;
    private QuizHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_history);
        quizListView = (ListView) findViewById(R.id.quizHistoryList);
        adapter = new QuizHistoryAdapter(this, QuizCPDB.listQuiz());
        this.quizListView.setAdapter(adapter);
        this.quizListView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, FinishedQuiz.class);
        intent.putExtra(Constants.QUIZ_ID, this.adapter.getItemId(position));
        intent.putExtra(Constants.HISTORY_MODE, true);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quiz_history, menu);
        return true;
    }


}
