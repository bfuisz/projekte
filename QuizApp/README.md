# README #

Einer meiner ersten Android Versuche. Die QuizApp basiert auf den Fragen von [MediQuiz Playstore](https://play.google.com/store/apps/details?id=de.quizzb)  

# App #

## Funktionen ##

Die App erstellt einen Quiz aus den vom User zur Verf�gung gestellten Fragen. Fragen k�nnen erstellt, 
bearbeitet und gel�scht werden. Au�erdem hat der User die M�glichkeit die Fragen zu lernen und sich seine 
Quiz History anzusehen. 

## TODOs ##

+ Fragenauswahl 
	fragen sollten aufgrund von verschiedenen Faktoren ausgew�hlt werden zum Beispiel wie oft der User sie falsch 
	beantwortet hat oder wann sie zum letzten mal beantwortet wurde. 

Obwohl es einige Teile der App gibt die mir sehr gut gefallen, w�rde ich die App neu schreiben um zum Beispiel 
Fragekategorien zu erm�glichen oder um Fragen von einem Webservice zu laden. 
Au�erdem w�rde ich sugar-orm vermeiden. 

## Lerneffekt ##

Basics und sugar-orm. 
	