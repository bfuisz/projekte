package at.fibuszene.feednews.persistency.usercultures;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code usercultures} table.
 */
public class UserculturesSelection extends AbstractSelection<UserculturesSelection> {
    @Override
    public Uri uri() {
        return UserculturesColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code UserculturesCursor} object, which is positioned before the first entry, or null.
     */
    public UserculturesCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new UserculturesCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public UserculturesCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public UserculturesCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public UserculturesSelection id(long... value) {
        addEquals(UserculturesColumns._ID, toObjectArray(value));
        return this;
    }


    public UserculturesSelection countryCode(String... value) {
        addEquals(UserculturesColumns.COUNTRY_CODE, value);
        return this;
    }

    public UserculturesSelection countryCodeNot(String... value) {
        addNotEquals(UserculturesColumns.COUNTRY_CODE, value);
        return this;
    }

    public UserculturesSelection countryCodeLike(String... value) {
        addLike(UserculturesColumns.COUNTRY_CODE, value);
        return this;
    }

    public UserculturesSelection cultureCode(String... value) {
        addEquals(UserculturesColumns.CULTURE_CODE, value);
        return this;
    }

    public UserculturesSelection cultureCodeNot(String... value) {
        addNotEquals(UserculturesColumns.CULTURE_CODE, value);
        return this;
    }

    public UserculturesSelection cultureCodeLike(String... value) {
        addLike(UserculturesColumns.CULTURE_CODE, value);
        return this;
    }

    public UserculturesSelection displayCultureName(String... value) {
        addEquals(UserculturesColumns.DISPLAY_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection displayCultureNameNot(String... value) {
        addNotEquals(UserculturesColumns.DISPLAY_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection displayCultureNameLike(String... value) {
        addLike(UserculturesColumns.DISPLAY_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection englishCultureName(String... value) {
        addEquals(UserculturesColumns.ENGLISH_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection englishCultureNameNot(String... value) {
        addNotEquals(UserculturesColumns.ENGLISH_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection englishCultureNameLike(String... value) {
        addLike(UserculturesColumns.ENGLISH_CULTURE_NAME, value);
        return this;
    }

    public UserculturesSelection languageCode(String... value) {
        addEquals(UserculturesColumns.LANGUAGE_CODE, value);
        return this;
    }

    public UserculturesSelection languageCodeNot(String... value) {
        addNotEquals(UserculturesColumns.LANGUAGE_CODE, value);
        return this;
    }

    public UserculturesSelection languageCodeLike(String... value) {
        addLike(UserculturesColumns.LANGUAGE_CODE, value);
        return this;
    }
}
