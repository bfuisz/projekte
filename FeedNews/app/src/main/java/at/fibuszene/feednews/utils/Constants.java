package at.fibuszene.feednews.utils;

import android.os.Handler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import at.fibuszene.feednews.service.ExceptionHandler;

/**
 * Created by benedikt.
 */
public class Constants {
    public static final String ENDPOINT = "http://api.feedzilla.com";
    public static final DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
    public static final DateFormat toDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

    public static final int CATEGORY_ERROR_CODE = 100;
    public static final int ARTICLE_ERROR_CODE = 105;

}
