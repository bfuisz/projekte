package at.fibuszene.feednews.persistency.article;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code article} table.
 */
public class ArticleColumns implements BaseColumns {
    public static final String TABLE_NAME = "article";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String URL = "url";
    public static final String AUTHOR = "author";
    public static final String FK_CATEGORY_ID = "fk_category_id";
    public static final String TITLE = "title";
    public static final String SUMMARY = "summary";
    public static final String PUBLISH_DATE = "publish_date";
    public static final String SOURCE = "source";
    public static final String READ = "read";
    public static final String SAVED = "saved";
    public static final String ENCLOSURE_ID = "enclosure_id";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." +PUBLISH_DATE;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[] {
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + URL,
            TABLE_NAME + "." + AUTHOR,
            TABLE_NAME + "." + FK_CATEGORY_ID,
            TABLE_NAME + "." + TITLE,
            TABLE_NAME + "." + SUMMARY,
            TABLE_NAME + "." + PUBLISH_DATE,
            TABLE_NAME + "." + SOURCE,
            TABLE_NAME + "." + READ,
            TABLE_NAME + "." + SAVED,
            TABLE_NAME + "." + ENCLOSURE_ID
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();
    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(URL);
        ALL_COLUMNS.add(AUTHOR);
        ALL_COLUMNS.add(FK_CATEGORY_ID);
        ALL_COLUMNS.add(TITLE);
        ALL_COLUMNS.add(SUMMARY);
        ALL_COLUMNS.add(PUBLISH_DATE);
        ALL_COLUMNS.add(SOURCE);
        ALL_COLUMNS.add(READ);
        ALL_COLUMNS.add(SAVED);
        ALL_COLUMNS.add(ENCLOSURE_ID);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
