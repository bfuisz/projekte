package at.fibuszene.feednews.model;

/**
 * Created by benedikt.
 */

public class Subcategory {
    private long category_id;
    private String display_subcategory_name;
    private String english_subcategory_name;
    private long subcategory_id;
    private String url_subcategory_name;

    public Subcategory() {
    }

    public Subcategory(long category_id, String display_subcategory_name, String english_subcategory_name, long subcategory_id, String url_subcategory_name) {
        this.category_id = category_id;
        this.display_subcategory_name = display_subcategory_name;
        this.english_subcategory_name = english_subcategory_name;
        this.subcategory_id = subcategory_id;
        this.url_subcategory_name = url_subcategory_name;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public String getDisplay_subcategory_name() {
        return display_subcategory_name;
    }

    public void setDisplay_subcategory_name(String display_subcategory_name) {
        this.display_subcategory_name = display_subcategory_name;
    }

    public String getEnglish_subcategory_name() {
        return english_subcategory_name;
    }

    public void setEnglish_subcategory_name(String english_subcategory_name) {
        this.english_subcategory_name = english_subcategory_name;
    }

    public long getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(long subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getUrl_subcategory_name() {
        return url_subcategory_name;
    }

    public void setUrl_subcategory_name(String url_subcategory_name) {
        this.url_subcategory_name = url_subcategory_name;
    }
}
