package at.fibuszene.feednews.adapter;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.model.lists.Categories;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.category.CategoryCursor;
import at.fibuszene.feednews.utils.PrefUtils;

/**
 * Created by benedikt.
 */
public class DrawerAdapter extends BaseAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    private List<String> staticItems;
    private Categories shortCutCategories;
    private Context context;

    public DrawerAdapter(Context context) {
        this.context = context;
        this.staticItems = Arrays.asList(context.getResources().getStringArray(R.array.drawer_items));
        this.shortCutCategories = new Categories();
    }

    @Override
    public int getCount() {
        return (staticItems.size()) + shortCutCategories.size();
    }

    @Override
    public String getItem(int position) {
        try {
            return staticItems.get(position);
        } catch (IndexOutOfBoundsException iob) {
            position = position % (staticItems.size()); // normalize position -> staticitems + shortcutitems would be position 4 but is position 1 in shortcutcategories...
            return shortCutCategories.get(position).getEnglish_category_name();
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return staticItems.get(position).hashCode();
        } catch (IndexOutOfBoundsException iob) {
            position = position % (staticItems.size()); //see get item
            return shortCutCategories.get(position).getCategory_id();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String text = getItem(position);
        boolean header = (getItemViewType(position) == 1);
        if (convertView == null) {
            if (header) {
                convertView = View.inflate(context, R.layout.drawer_category_layout, null);
            } else {
                convertView = View.inflate(context, R.layout.drawer_item, null);
            }
        }
        ((TextView) convertView.findViewById(R.id.navItemTextView)).setText(text);
        if (position == PrefUtils.selectedNavItem(context)) {
            convertView.findViewById(R.id.locationIndicator).setSelected(true);
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == (staticItems.size() - 1) ? 1 : 0);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public void setItems(Categories items) {
        if (items == null) {
            items = new Categories();
        }
        this.shortCutCategories = items;
        notifyDataSetChanged();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(context, CategoryColumns.CONTENT_URI, null, CategoryColumns.SHORTCUT + " = 1 AND " + CategoryColumns.HIDDEN + " = 0", null, CategoryColumns.ORDER_IN_HOME);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        CategoryCursor cursor = new CategoryCursor(data);
        setItems(Categories.fromCursor(cursor));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        setItems(null);
    }

}
