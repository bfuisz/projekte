package at.fibuszene.feednews.persistency.article;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code article} table.
 */
public class ArticleContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return ArticleColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, ArticleSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public ArticleContentValues putUrl(String value) {
        mContentValues.put(ArticleColumns.URL, value);
        return this;
    }

    public ArticleContentValues putUrlNull() {
        mContentValues.putNull(ArticleColumns.URL);
        return this;
    }


    public ArticleContentValues putAuthor(String value) {
        mContentValues.put(ArticleColumns.AUTHOR, value);
        return this;
    }

    public ArticleContentValues putAuthorNull() {
        mContentValues.putNull(ArticleColumns.AUTHOR);
        return this;
    }


    public ArticleContentValues putFkCategoryId(Long value) {
        mContentValues.put(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleContentValues putFkCategoryIdNull() {
        mContentValues.putNull(ArticleColumns.FK_CATEGORY_ID);
        return this;
    }


    public ArticleContentValues putTitle(String value) {
        mContentValues.put(ArticleColumns.TITLE, value);
        return this;
    }

    public ArticleContentValues putTitleNull() {
        mContentValues.putNull(ArticleColumns.TITLE);
        return this;
    }


    public ArticleContentValues putSummary(String value) {
        mContentValues.put(ArticleColumns.SUMMARY, value);
        return this;
    }

    public ArticleContentValues putSummaryNull() {
        mContentValues.putNull(ArticleColumns.SUMMARY);
        return this;
    }


    public ArticleContentValues putPublishDate(Long value) {
        mContentValues.put(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleContentValues putPublishDateNull() {
        mContentValues.putNull(ArticleColumns.PUBLISH_DATE);
        return this;
    }


    public ArticleContentValues putSource(String value) {
        mContentValues.put(ArticleColumns.SOURCE, value);
        return this;
    }

    public ArticleContentValues putSourceNull() {
        mContentValues.putNull(ArticleColumns.SOURCE);
        return this;
    }


    public ArticleContentValues putRead(Boolean value) {
        mContentValues.put(ArticleColumns.READ, value);
        return this;
    }

    public ArticleContentValues putReadNull() {
        mContentValues.putNull(ArticleColumns.READ);
        return this;
    }


    public ArticleContentValues putSaved(Boolean value) {
        mContentValues.put(ArticleColumns.SAVED, value);
        return this;
    }

    public ArticleContentValues putSavedNull() {
        mContentValues.putNull(ArticleColumns.SAVED);
        return this;
    }


    public ArticleContentValues putEnclosureId(Long value) {
        mContentValues.put(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleContentValues putEnclosureIdNull() {
        mContentValues.putNull(ArticleColumns.ENCLOSURE_ID);
        return this;
    }

}
