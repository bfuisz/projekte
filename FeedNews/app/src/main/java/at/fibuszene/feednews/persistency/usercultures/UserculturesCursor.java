package at.fibuszene.feednews.persistency.usercultures;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code usercultures} table.
 */
public class UserculturesCursor extends AbstractCursor {
    public UserculturesCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code country_code} value.
     * Can be {@code null}.
     */
    public String getCountryCode() {
        Integer index = getCachedColumnIndexOrThrow(UserculturesColumns.COUNTRY_CODE);
        return getString(index);
    }

    /**
     * Get the {@code culture_code} value.
     * Can be {@code null}.
     */
    public String getCultureCode() {
        Integer index = getCachedColumnIndexOrThrow(UserculturesColumns.CULTURE_CODE);
        return getString(index);
    }

    /**
     * Get the {@code display_culture_name} value.
     * Can be {@code null}.
     */
    public String getDisplayCultureName() {
        Integer index = getCachedColumnIndexOrThrow(UserculturesColumns.DISPLAY_CULTURE_NAME);
        return getString(index);
    }

    /**
     * Get the {@code english_culture_name} value.
     * Can be {@code null}.
     */
    public String getEnglishCultureName() {
        Integer index = getCachedColumnIndexOrThrow(UserculturesColumns.ENGLISH_CULTURE_NAME);
        return getString(index);
    }

    /**
     * Get the {@code language_code} value.
     * Can be {@code null}.
     */
    public String getLanguageCode() {
        Integer index = getCachedColumnIndexOrThrow(UserculturesColumns.LANGUAGE_CODE);
        return getString(index);
    }
}
