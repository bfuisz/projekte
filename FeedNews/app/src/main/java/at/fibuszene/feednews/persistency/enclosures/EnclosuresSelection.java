package at.fibuszene.feednews.persistency.enclosures;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code enclosures} table.
 */
public class EnclosuresSelection extends AbstractSelection<EnclosuresSelection> {
    @Override
    public Uri uri() {
        return EnclosuresColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code EnclosuresCursor} object, which is positioned before the first entry, or null.
     */
    public EnclosuresCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new EnclosuresCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public EnclosuresCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public EnclosuresCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public EnclosuresSelection id(long... value) {
        addEquals(EnclosuresColumns._ID, toObjectArray(value));
        return this;
    }


    public EnclosuresSelection articleId(Long... value) {
        addEquals(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection articleIdNot(Long... value) {
        addNotEquals(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection articleIdGt(long value) {
        addGreaterThan(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection articleIdGtEq(long value) {
        addGreaterThanOrEquals(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection articleIdLt(long value) {
        addLessThan(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection articleIdLtEq(long value) {
        addLessThanOrEquals(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresSelection mediaType(String... value) {
        addEquals(EnclosuresColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosuresSelection mediaTypeNot(String... value) {
        addNotEquals(EnclosuresColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosuresSelection mediaTypeLike(String... value) {
        addLike(EnclosuresColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosuresSelection uri(String... value) {
        addEquals(EnclosuresColumns.URI, value);
        return this;
    }

    public EnclosuresSelection uriNot(String... value) {
        addNotEquals(EnclosuresColumns.URI, value);
        return this;
    }

    public EnclosuresSelection uriLike(String... value) {
        addLike(EnclosuresColumns.URI, value);
        return this;
    }
}
