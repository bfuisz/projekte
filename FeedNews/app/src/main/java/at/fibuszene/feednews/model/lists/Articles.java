package at.fibuszene.feednews.model.lists;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.feednews.BuildConfig;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.persistency.article.ArticleCursor;

/**
 * Created by benedikt.
 */

public class Articles {
    private static final boolean DEBUG = BuildConfig.DEBUG;

    @SerializedName("articles")
    private List<Article> articles;

    private String description;
    private String syndication_url;
    private String title;

    public Articles() {
        this.articles = new ArrayList<Article>();
    }

    public Articles(List<Article> articles, String description, String syndication_url, String title) {
        this.articles = articles;
        this.description = description;
        this.syndication_url = syndication_url;
        this.title = title;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSyndication_url() {
        return syndication_url;
    }

    public void setSyndication_url(String syndication_url) {
        this.syndication_url = syndication_url;
    }

    public void add(Article article) {
        this.articles.add(article);
    }

    public Article get(int position) {
        try {
            return this.articles.get(position);
        } catch (IndexOutOfBoundsException ioobe) {
            if (DEBUG) {
                ioobe.printStackTrace();
            }
            return null;
        }
    }

    public int size() {
        return this.getArticles().size();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static Articles fromCursor(ArticleCursor cursor) {
        Articles list = new Articles();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Article tmp = Article.from(cursor);
                list.add(tmp);
                cursor.moveToNext();
            }
        }
        return list;
    }


    @Override
    public String toString() {
        return "Articles{" +
                "articles=" + articles +
                ", description='" + description + '\'' +
                ", syndication_url='" + syndication_url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
