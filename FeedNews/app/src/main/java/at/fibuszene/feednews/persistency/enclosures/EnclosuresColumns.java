package at.fibuszene.feednews.persistency.enclosures;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code enclosures} table.
 */
public class EnclosuresColumns implements BaseColumns {
    public static final String TABLE_NAME = "enclosures";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String ARTICLE_ID = "article_id";
    public static final String MEDIA_TYPE = "media_type";
    public static final String URI = "uri";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[] {
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + ARTICLE_ID,
            TABLE_NAME + "." + MEDIA_TYPE,
            TABLE_NAME + "." + URI
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();
    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(ARTICLE_ID);
        ALL_COLUMNS.add(MEDIA_TYPE);
        ALL_COLUMNS.add(URI);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
