package at.fibuszene.feednews.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.utils.ReadingDialog;
import at.fibuszene.feednews.utils.Utils;

public class ArticleActivity extends AbstractActivity {
    public static final String ARTICLE_URL_EXTRA = "at.fibuszene.feednews.article_url";
    private WebView articleText;
    private String url;
    private ReadingDialog dialog;
    private ActionMode mActionMode = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_article);

        articleText = (WebView) findViewById(R.id.articleText);
        url = getIntent().getStringExtra(ARTICLE_URL_EXTRA);
        this.dialog = new ReadingDialog();

        if (url != null) {
//            clean up article

            articleText.loadUrl(url);
            debug(url);
        }
    }

    public void debug(final String url) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    URLConnection connection = new URL(url).openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String tmp = "";
                    while ((tmp = reader.readLine()) != null) {
                        Log.d("debug", tmp);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_article, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_browser) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.url));
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_share) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, url);
            intent.setType("text/plain");
            startActivity(Intent.createChooser(intent, getResources().getText(R.string.send_to)));
            return true;
        }
        if (id == R.id.action_speed) {
            ClipboardManager manager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData data = manager.getPrimaryClip();
            if (data != null) {
                Bundle args = new Bundle();
                try {
                    args.putString(ReadingDialog.TEXT_ARG, data.getItemAt(0).getText() + "");
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(), "readingDialog");
                } catch (Exception e) {
                    Utils.emptySelectionToast(this);
                }

            } else {
                Utils.emptySelectionToast(this);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
