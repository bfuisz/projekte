package at.fibuszene.feednews.persistency.category;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code category} table.
 */
public class CategoryCursor extends AbstractCursor {
    public CategoryCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code category_id} value.
     * Can be {@code null}.
     */
    public Long getCategoryId() {
        return getLongOrNull(CategoryColumns.CATEGORY_ID);
    }

    /**
     * Get the {@code display_category_name} value.
     * Can be {@code null}.
     */
    public String getDisplayCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.DISPLAY_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code english_category_name} value.
     * Can be {@code null}.
     */
    public String getEnglishCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.ENGLISH_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code url_category_name} value.
     * Can be {@code null}.
     */
    public String getUrlCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.URL_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code user_favorite} value.
     * Can be {@code null}.
     */
    public Boolean getUserFavorite() {
        return getBoolean(CategoryColumns.USER_FAVORITE);
    }

    /**
     * Get the {@code order_in_home} value.
     * Can be {@code null}.
     */
    public Integer getOrderInHome() {
        return getIntegerOrNull(CategoryColumns.ORDER_IN_HOME);
    }

    /**
     * Get the {@code color} value.
     * Can be {@code null}.
     */
    public Integer getColor() {
        return getIntegerOrNull(CategoryColumns.COLOR);
    }

    /**
     * Get the {@code frontpage} value.
     * Can be {@code null}.
     */
    public Boolean getFrontpage() {
        return getBoolean(CategoryColumns.FRONTPAGE);
    }

    /**
     * Get the {@code hidden} value.
     * Can be {@code null}.
     */
    public Boolean getHidden() {
        return getBoolean(CategoryColumns.HIDDEN);
    }

    public Boolean getShortcut() {
        return getBoolean(CategoryColumns.SHORTCUT);
    }
}
