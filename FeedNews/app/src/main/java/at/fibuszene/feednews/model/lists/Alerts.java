package at.fibuszene.feednews.model.lists;

import android.database.Cursor;

import java.util.ArrayList;

import at.fibuszene.feednews.model.Alert;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.NewAlert;
import at.fibuszene.feednews.persistency.alert.AlertCursor;
import at.fibuszene.feednews.utils.Utils;

/**
 * Created by benedikt.
 */
public class Alerts extends ArrayList<Alert> {

    //View Spielraum zum optimieren :D
    public static Alerts fromCursor(Cursor cursor) {
        Alerts alerts = new Alerts();
        if (cursor.moveToFirst()) {
            Alert alert;
            NewAlert newAlert;
            Article art;
            long lastId;
            while (!cursor.isAfterLast()) {
                alert = new Alert();
                alert.set_id(cursor.getLong(0));
                alert.setKeyword(cursor.getString(1));
                alert.setCount(cursor.getLong(2));
                alert.setSeen(cursor.getLong(3));
                lastId = alert.get_id();

                while (alert.get_id() == lastId && !cursor.isAfterLast()) {

                    newAlert = new NewAlert();
                    newAlert.set_id(cursor.getLong(4));

                    art = new Article();
                    art.set_id(cursor.getLong(5));
                    art.setTitle(cursor.getString(6));
                    art.setUrl(cursor.getString(7));
                    art.setPublish_date(Utils.fromDB(cursor.getLong(8)));

                    newAlert.setArticle(art);
                    alert.getNewAlerts().add(newAlert);
                    cursor.moveToNext();
                    if (!cursor.isAfterLast()) {
                        lastId = cursor.getLong(0);
                    }
                }
                alerts.add(alert);
            }
        }
        return alerts;

    }


    public static Alerts fromCursor(AlertCursor cursor) {
        Alerts alerts = new Alerts();
        if (cursor.moveToFirst()) {
            Alert tmp;
            long id;
            while (!cursor.isAfterLast()) {
                tmp = Alert.fromCursor(cursor);
                alerts.add(tmp);

                cursor.moveToNext();
            }
        }
        return alerts;
    }

    /**
     * use this to get the alerts rows only without
     * everything else
     */
    public static Alerts fromCursorForService(AlertCursor cursor) {
        Alerts alerts = new Alerts();
        if (cursor.moveToFirst()) {
            Alert tmp;
            long id;
            while (!cursor.isAfterLast()) {
                tmp = Alert.alertOnlyfromCursor(cursor);
                alerts.add(tmp);

                cursor.moveToNext();
            }
        }
        return alerts;
    }
}
