package at.fibuszene.feednews.model.lists;

import java.util.ArrayList;

import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.persistency.category.CategoryCursor;

/**
 * Created by benedikt.
 */
public class Categories extends ArrayList<Category> {

    public static Categories fromCursor(CategoryCursor cursor) {
        Categories categories = new Categories();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                categories.add(Category.fromCatCursor(cursor));
                cursor.moveToNext();
            }
        }
        return categories;
    }


}
