package at.fibuszene.feednews.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Pair;

import java.util.HashMap;

import at.fibuszene.feednews.model.Metadata;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.category.CategorySelection;
import at.fibuszene.feednews.persistency.metadata.MetadataCursor;
import at.fibuszene.feednews.persistency.metadata.MetadataSelection;

/**
 * Created by benedikt.
 */
public class DBUtils {
    private static HashMap<Long, Pair<String, Integer>> catCache;
    private static CategorySelection selection;

    static {
        catCache = new HashMap<Long, Pair<String, Integer>>();
    }

    public static Pair<String, Integer> getCategoryColor(Context context, long category_id) {
        if (catCache.containsKey(category_id)) {
            return catCache.get(category_id);
        }
        selection = new CategorySelection();
        selection.categoryId(category_id);
        Cursor cursor = selection.query(context.getContentResolver(), new String[]{CategoryColumns.DISPLAY_CATEGORY_NAME, CategoryColumns.COLOR});
        int color = -1;
        String name = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(0);
                color = cursor.getInt(1);
            }
        }
        selection = null;
        catCache.put(category_id, new Pair<String, Integer>(name, color));
        return catCache.get(category_id);
    }

    public static Metadata getMetadata(Context context, String tableName, long categoryId) {
        MetadataSelection selection = new MetadataSelection();

        selection.colTableName(tableName).and().colCategoryId(categoryId);

        MetadataCursor cursor = selection.query(context.getContentResolver());
        if (cursor.moveToFirst()) {
            return Metadata.fromCursor(cursor);
        }
        return null;
    }


    public static void updateMetadata(Context context, String tableName, long lastUpdated, long lastCleared, int count) {
        updateMetadata(context, tableName, 0, lastUpdated, lastCleared, count);
    }

    public static void updateMetadata(Context context, String tableName, long categoryId, long lastUpdated, long lastCleared, int count) {
        Metadata data = getMetadata(context, tableName, categoryId);
        if (data == null) {
            data = new Metadata();
        }
        data.setCategoryId(categoryId);
        if (lastCleared > 0) {
            data.setLast_cleared(lastCleared);
        }
        if (lastUpdated > 0) {
            data.setLast_updated(lastUpdated);
        }

        if (tableName != null) {
            data.setTableName(tableName);
        }
        if (count > 0) {
            data.setCount(count);
        }
        insertMetadate(context, data);
    }

    public static void insertMetadate(Context context, Metadata data) {
        if (data != null) {
            if (data.getId() > 0) {
                MetadataSelection selection1 = new MetadataSelection();
                selection1.id(data.getId());
                data.toMetaContentValues().update(context.getContentResolver(), selection1);
            } else {
                data.toMetaContentValues().insert(context.getContentResolver());
            }
        }
    }
}
