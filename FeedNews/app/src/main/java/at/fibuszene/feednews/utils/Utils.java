package at.fibuszene.feednews.utils;

import android.content.Context;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import at.fibuszene.feednews.model.Metadata;

/**
 * Created by benedikt.
 */
public class Utils {

    /**
     * returns the timestamp of now - days ago
     * e.g. now (1417082937854) - 172800000
     *
     * @param daysAgo
     * @return
     */
    public static long timestamp(int daysAgo) {
        return new Date(System.currentTimeMillis() - (1000 * 60 * 60 * (24 * daysAgo))).getTime();
    }

    //eg (1418462515674L, TimeUnit.DAYS) -> Three days ago
    public static long daysAgo(long timeStamp, TimeUnit dst) {
        Date date1 = new Date(timeStamp);
        Date date2 = new Date(System.currentTimeMillis());
        return dst.convert(date2.getTime() - date1.getTime(), TimeUnit.MILLISECONDS);
    }

    public static boolean checkMetadata(Context context, TimeUnit dst, int duration, String tablename) {
        return checkMetadata(context, dst, duration, tablename, 0);
    }

    public static boolean checkMetadata(Context context, TimeUnit dst, int duration, String tablename, long categoryId) {
        Metadata data = DBUtils.getMetadata(context, tablename, categoryId);
        if (data == null) {//no db entry  => update
            return true;
        }
        if (Utils.daysAgo(data.getLast_updated(), dst) > duration) { //
            return true;
        } else {
            return false;
        }
    }


    /*
     * commentetd stuff in method produces weird stuff:
     * Unstable for some reason:
     * 12-15 15:24:54.110  16662-16763/at.fibuszene.feednews D/date mystery ar﹕ 15 12 2014 14:06:00 3514 1418648760000 15 0012 2014 13:01:00
     * 12-15 15:24:54.240  16662-16765/at.fibuszene.feednews D/date mystery ar﹕ 15 12 2014 14:06:00 3514 1418648760000 15 12 2014 14:06:00
     * for
     * Log.d("date mystery ar", article.getPublish_date() + " " + article.get_id() + " " + date + " " + Utils.fromDB(date));
     *
     */
    public static String fromDB(Long date) {
        return DateFormat.getInstance().format(date);

//        try {
//            Constants.toDateFormat.setLenient(false);
//            return Constants.toDateFormat.format(new Date(date));
//        } catch (NullPointerException npe) {
//            return Constants.toDateFormat.format(new Date(System.currentTimeMillis()));
//        }
    }


    public static void emptySelectionToast(Context context) {
        Toast.makeText(context, "Empty selection", Toast.LENGTH_SHORT).show();
    }

    /**
     * Calculates the words per minute for the spritz reading dialog
     * where speed is the pause between words
     * e.g. speed = 1000 (= 1000 milisecond pause between words) equals 1 word per second times 60 = 60 wpm
     * so wpm = wps * 60;
     * and wps = (1000 / speed) + 1
     *
     * @param speed
     * @return
     */
    public static int wpmCount(long speed) {
        return ((int) (1000 / speed) + 1) * 60;
    }

    /**
     * Alteration to the previouse method now the user sets a desired wpm count and the app calculates the necessary pause to achieve the wpm count.
     * imho more userfriendly
     * <p/>
     * user wants 200 wpm
     * int pause = 1000 / (200 / 60)
     * (200 / 60) = wps = 3.33 ~ 3 wps
     * 1 second = 1000ms so
     * <p/>
     * pause ~ 300 ms
     *
     * @param desiredWPM
     * @return
     */
    public static int speedForWPM(int desiredWPM) {
        float wpm = (float) desiredWPM;
        double wps = (wpm / 60);
        wps = (wps > 0) ? wps : 1;//default to 1
        return (int) (1000 / wps);
    }

}
