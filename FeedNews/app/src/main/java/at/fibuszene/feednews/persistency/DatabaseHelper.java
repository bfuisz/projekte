package at.fibuszene.feednews.persistency;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import at.fibuszene.feednews.BuildConfig;
import at.fibuszene.feednews.persistency.alert.AlertColumns;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.enclosure.EnclosureColumns;
import at.fibuszene.feednews.persistency.metadata.MetadataColumns;
import at.fibuszene.feednews.persistency.newalert.NewalertColumns;
import at.fibuszene.feednews.persistency.usercultures.UserculturesColumns;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "feedzillaFnews.db";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper sInstance;
    private final Context mContext;
    private final DatabaseHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    private static final String SQL_CREATE_TABLE_ALERT = "CREATE TABLE IF NOT EXISTS "
            + AlertColumns.TABLE_NAME + " ( "
            + AlertColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + AlertColumns.CATEGORY_ID + " INTEGER DEFAULT 0, "
            + AlertColumns.KEYWORD + " TEXT, "
            + AlertColumns.COUNT + " INTEGER DEFAULT 0, "
            + AlertColumns.SEEN + " INTEGER DEFAULT 0, "
            + AlertColumns.NEW_ALERT_ID + " INTEGER DEFAULT 0"
            + ", CONSTRAINT unique_id unique (keyword) on conflict ignore"
            + " );";

    //TODO s: when generating the ContentProvider
    //change default order article to publish_date, category to order in home
    //change foreign key references from newalert(_id) to newalert(alert_id)
    //and article from category (_id) to category (category_id)
    // newAlert index+ ", CONSTRAINT unique_id unique (article_id) on conflict ignore"
    //change NO_ACTION to NO ACTION

    private static final String SQL_CREATE_TABLE_ARTICLE = "CREATE TABLE IF NOT EXISTS "
            + ArticleColumns.TABLE_NAME + " ( "
            + ArticleColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ArticleColumns.URL + " TEXT, "
            + ArticleColumns.AUTHOR + " TEXT, "
            + ArticleColumns.FK_CATEGORY_ID + " INTEGER, "
            + ArticleColumns.TITLE + " TEXT, "
            + ArticleColumns.SUMMARY + " TEXT, "
            + ArticleColumns.PUBLISH_DATE + " INTEGER, "
            + ArticleColumns.SOURCE + " TEXT, "
            + ArticleColumns.READ + " INTEGER, "
            + ArticleColumns.SAVED + " INTEGER, "
            + ArticleColumns.ENCLOSURE_ID + " INTEGER "
            + ", CONSTRAINT fk_fk_category_id FOREIGN KEY (fk_category_id) REFERENCES category (category_id) ON DELETE NO ACTION"
            + ", CONSTRAINT fk_enclosure_id FOREIGN KEY (enclosure_id) REFERENCES enclosure (_id) ON DELETE CASCADE"
            + ", CONSTRAINT unique_id unique (title) on conflict ignore"
            + " );";

    private static final String SQL_CREATE_TABLE_CATEGORY = "CREATE TABLE IF NOT EXISTS "
            + CategoryColumns.TABLE_NAME + " ( "
            + CategoryColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CategoryColumns.CATEGORY_ID + " INTEGER, "
            + CategoryColumns.DISPLAY_CATEGORY_NAME + " TEXT, "
            + CategoryColumns.ENGLISH_CATEGORY_NAME + " TEXT, "
            + CategoryColumns.URL_CATEGORY_NAME + " TEXT, "
            + CategoryColumns.USER_FAVORITE + " INTEGER, "
            + CategoryColumns.ORDER_IN_HOME + " INTEGER, "
            + CategoryColumns.COLOR + " INTEGER, "
            + CategoryColumns.FRONTPAGE + " INTEGER, "
            + CategoryColumns.HIDDEN + " INTEGER, "
            + CategoryColumns.SHORTCUT + " INTEGER DEFAULT 0, "
            + " CONSTRAINT unique_id unique (category_id) on conflict ignore"
            + " );";

    private static final String SQL_CREATE_TABLE_ENCLOSURE = "CREATE TABLE IF NOT EXISTS "
            + EnclosureColumns.TABLE_NAME + " ( "
            + EnclosureColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EnclosureColumns.MEDIA_TYPE + " TEXT, "
            + EnclosureColumns.URI + " TEXT "
            + " );";

    private static final String SQL_CREATE_TABLE_METADATA = "CREATE TABLE IF NOT EXISTS "
            + MetadataColumns.TABLE_NAME + " ( "
            + MetadataColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + MetadataColumns.COL_TABLE_NAME + " TEXT, "
            + MetadataColumns.COL_CATEGORY_ID + " INTEGER, "
            + MetadataColumns.LAST_UPDATED + " INTEGER, "
            + MetadataColumns.LAST_CLEARED + " INTEGER, "
            + MetadataColumns.CONTENT_COUNT + " INTEGER "
            + ", CONSTRAINT unique_cat_id unique (col_category_id) on conflict ignore"
            + " );";

    private static final String SQL_CREATE_TABLE_NEWALERT = "CREATE TABLE IF NOT EXISTS "
            + NewalertColumns.TABLE_NAME + " ( "
            + NewalertColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NewalertColumns.ALERT_ID + " INTEGER, "
            + NewalertColumns.ARTICLE_ID + " INTEGER DEFAULT 0, "
            + NewalertColumns.SEEN + " INTEGER "
            + ", CONSTRAINT fk_article_id FOREIGN KEY (article_id) REFERENCES article (_id) ON DELETE CASCADE"
            + ", CONSTRAINT fk_alert_id FOREIGN KEY (alert_id) REFERENCES alert (_id) ON DELETE CASCADE"
            + ", CONSTRAINT unique_id unique (article_id) on conflict ignore"
            + " );";

    private static final String SQL_CREATE_TABLE_USERCULTURES = "CREATE TABLE IF NOT EXISTS "
            + UserculturesColumns.TABLE_NAME + " ( "
            + UserculturesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UserculturesColumns.COUNTRY_CODE + " TEXT, "
            + UserculturesColumns.CULTURE_CODE + " TEXT, "
            + UserculturesColumns.DISPLAY_CULTURE_NAME + " TEXT, "
            + UserculturesColumns.ENGLISH_CULTURE_NAME + " TEXT, "
            + UserculturesColumns.LANGUAGE_CODE + " TEXT "
            + " );";

    // @formatter:on
    public static DatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static DatabaseHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */

    private static DatabaseHelper newInstancePreHoneycomb(Context context) {
        return new DatabaseHelper(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
    }

    private DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        mContext = context;
        mOpenHelperCallbacks = new DatabaseHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static DatabaseHelper newInstancePostHoneycomb(Context context) {
        return new DatabaseHelper(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private DatabaseHelper(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new DatabaseHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_ALERT);
        db.execSQL(SQL_CREATE_TABLE_ARTICLE);
        db.execSQL(SQL_CREATE_TABLE_CATEGORY);
        db.execSQL(SQL_CREATE_TABLE_ENCLOSURE);
        db.execSQL(SQL_CREATE_TABLE_METADATA);
        db.execSQL(SQL_CREATE_TABLE_NEWALERT);
        db.execSQL(SQL_CREATE_TABLE_USERCULTURES);
        db.execSQL("PRAGMA foreign_keys=ON;");
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
