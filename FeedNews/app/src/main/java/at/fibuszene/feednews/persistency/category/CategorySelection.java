package at.fibuszene.feednews.persistency.category;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code category} table.
 */
public class CategorySelection extends AbstractSelection<CategorySelection> {
    @Override
    public Uri uri() {
        return CategoryColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection      A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder       How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *                        order, which may be unordered.
     * @return A {@code CategoryCursor} object, which is positioned before the first entry, or null.
     */
    public CategoryCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new CategoryCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public CategoryCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public CategoryCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public CategorySelection id(long... value) {
        addEquals(CategoryColumns._ID, toObjectArray(value));
        return this;
    }


    public CategorySelection categoryId(Long... value) {
        addEquals(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection categoryIdNot(Long... value) {
        addNotEquals(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection categoryIdGt(long value) {
        addGreaterThan(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection categoryIdGtEq(long value) {
        addGreaterThanOrEquals(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection categoryIdLt(long value) {
        addLessThan(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection categoryIdLtEq(long value) {
        addLessThanOrEquals(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategorySelection displayCategoryName(String... value) {
        addEquals(CategoryColumns.DISPLAY_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection displayCategoryNameNot(String... value) {
        addNotEquals(CategoryColumns.DISPLAY_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection displayCategoryNameLike(String... value) {
        addLike(CategoryColumns.DISPLAY_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection englishCategoryName(String... value) {
        addEquals(CategoryColumns.ENGLISH_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection englishCategoryNameNot(String... value) {
        addNotEquals(CategoryColumns.ENGLISH_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection englishCategoryNameLike(String... value) {
        addLike(CategoryColumns.ENGLISH_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection urlCategoryName(String... value) {
        addEquals(CategoryColumns.URL_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection urlCategoryNameNot(String... value) {
        addNotEquals(CategoryColumns.URL_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection urlCategoryNameLike(String... value) {
        addLike(CategoryColumns.URL_CATEGORY_NAME, value);
        return this;
    }

    public CategorySelection userFavorite(Boolean value) {
        addEquals(CategoryColumns.USER_FAVORITE, toObjectArray(value));
        return this;
    }

    public CategorySelection orderInHome(Integer... value) {
        addEquals(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection orderInHomeNot(Integer... value) {
        addNotEquals(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection orderInHomeGt(int value) {
        addGreaterThan(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection orderInHomeGtEq(int value) {
        addGreaterThanOrEquals(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection orderInHomeLt(int value) {
        addLessThan(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection orderInHomeLtEq(int value) {
        addLessThanOrEquals(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategorySelection color(Integer... value) {
        addEquals(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection colorNot(Integer... value) {
        addNotEquals(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection colorGt(int value) {
        addGreaterThan(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection colorGtEq(int value) {
        addGreaterThanOrEquals(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection colorLt(int value) {
        addLessThan(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection colorLtEq(int value) {
        addLessThanOrEquals(CategoryColumns.COLOR, value);
        return this;
    }

    public CategorySelection frontpage(Boolean value) {
        addEquals(CategoryColumns.FRONTPAGE, toObjectArray(value));
        return this;
    }

    public CategorySelection shortcut(Boolean value) {
        addEquals(CategoryColumns.SHORTCUT, toObjectArray(value));
        return this;
    }

    public CategorySelection hidden(Boolean value) {
        addEquals(CategoryColumns.HIDDEN, toObjectArray(value));
        return this;
    }
}
