package at.fibuszene.feednews.persistency.newalert;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code newalert} table.
 */
public class NewalertContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return NewalertColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, NewalertSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public NewalertContentValues putAlertId(Long value) {
        mContentValues.put(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertContentValues putAlertIdNull() {
        mContentValues.putNull(NewalertColumns.ALERT_ID);
        return this;
    }


    public NewalertContentValues putArticleId(Long value) {
        mContentValues.put(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertContentValues putArticleIdNull() {
        mContentValues.putNull(NewalertColumns.ARTICLE_ID);
        return this;
    }


    public NewalertContentValues putSeen(Boolean value) {
        mContentValues.put(NewalertColumns.SEEN, value);
        return this;
    }

    public NewalertContentValues putSeenNull() {
        mContentValues.putNull(NewalertColumns.SEEN);
        return this;
    }

}
