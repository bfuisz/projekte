package at.fibuszene.feednews.persistency.alert;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code alert} table.
 */
public class AlertContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return AlertColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, AlertSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public AlertContentValues putCategoryId(Long value) {
        mContentValues.put(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertContentValues putCategoryIdNull() {
        mContentValues.putNull(AlertColumns.CATEGORY_ID);
        return this;
    }


    public AlertContentValues putKeyword(String value) {
        mContentValues.put(AlertColumns.KEYWORD, value);
        return this;
    }

    public AlertContentValues putKeywordNull() {
        mContentValues.putNull(AlertColumns.KEYWORD);
        return this;
    }


    public AlertContentValues putCount(Long value) {
        mContentValues.put(AlertColumns.COUNT, value);
        return this;
    }

    public AlertContentValues putCountNull() {
        mContentValues.putNull(AlertColumns.COUNT);
        return this;
    }


    public AlertContentValues putSeen(Long value) {
        mContentValues.put(AlertColumns.SEEN, value);
        return this;
    }

    public AlertContentValues putSeenNull() {
        mContentValues.putNull(AlertColumns.SEEN);
        return this;
    }


    public AlertContentValues putNewAlertId(Long value) {
        mContentValues.put(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertContentValues putNewAlertIdNull() {
        mContentValues.putNull(AlertColumns.NEW_ALERT_ID);
        return this;
    }

}
