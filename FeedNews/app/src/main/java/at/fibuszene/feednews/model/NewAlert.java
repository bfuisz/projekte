package at.fibuszene.feednews.model;

import at.fibuszene.feednews.persistency.alert.AlertCursor;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.newalert.NewalertContentValues;
import at.fibuszene.feednews.persistency.newalert.NewalertCursor;

/**
 * Created by benedikt.
 */
public class NewAlert {
    private long _id;
    private long alert_id;
    private long article_id;
    private Article article;
    private boolean seen;

    public static NewAlert fromCursor(NewalertCursor cursor) {
        NewAlert newAlert = new NewAlert();
        newAlert._id = cursor.getId();
        newAlert.alert_id = cursor.getAlertId();
        newAlert.article_id = cursor.getArticleId();
        newAlert.article = Article.from(new ArticleCursor(cursor));
        newAlert.seen = cursor.getSeen();
        return newAlert;
    }

    public static NewAlert fromCursor(AlertCursor cursor) {
        return fromCursor(new NewalertCursor(cursor));
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long getAlert_id() {
        return alert_id;
    }

    public void setAlert_id(long alert_id) {
        this.alert_id = alert_id;
    }

    public long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(long article_id) {
        this.article_id = article_id;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public NewalertContentValues toContentValues() {
        NewalertContentValues values = new NewalertContentValues();
        values.putAlertId(this.alert_id);
        values.putArticleId(this.article_id);
        values.putSeen(this.seen);

        return values;
    }
}
