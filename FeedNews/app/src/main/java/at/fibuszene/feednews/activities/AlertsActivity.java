package at.fibuszene.feednews.activities;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.adapter.AlertsExpListAdapter;
import at.fibuszene.feednews.loader.AlertLoader;
import at.fibuszene.feednews.model.Alert;
import at.fibuszene.feednews.model.lists.Alerts;
import at.fibuszene.feednews.persistency.alert.AlertColumns;
import at.fibuszene.feednews.persistency.alert.AlertSelection;
import at.fibuszene.feednews.service.AlertService;

public class AlertsActivity extends AbstractActivity implements ExpandableListView.OnGroupClickListener, View.OnClickListener, LoaderManager.LoaderCallbacks<Alerts> {
    private static final int ALERT_LOADER_ID = 123;
    private EditText keyWordText;
    private ExpandableListView explListView;
    private AlertsExpListAdapter explListAdapter;
    private AlertSelection selection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_alerts);
        this.explListView = (ExpandableListView) findViewById(R.id.alertsListView);
        this.explListView.addHeaderView(createHeader());
        selection = new AlertSelection();

        this.explListAdapter = new AlertsExpListAdapter(this, null);
        this.explListView.setAdapter(explListAdapter);
        this.explListView.setOnGroupClickListener(this);
        getLoaderManager().restartLoader(ALERT_LOADER_ID, null, this);

        //TEST should be executed in user defined intervals or better yet in a syncadapter
        //Update: syncadapter see package alertsync
        //still do it somewhere around here to instantly update alerts
        Intent intent = new Intent(this, AlertService.class);
        startService(intent);
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        long alertID = this.explListAdapter.getGroupId(groupPosition);
        ContentValues values = new ContentValues();
        //just completely reset
        //the alertservice will count new entries and
        values.put(AlertColumns.COUNT, 0);
        int updated = getContentResolver().update(AlertColumns.CONTENT_URI, values, AlertColumns._ID + " = " + alertID, null);

        if (updated > 0) {
            this.explListAdapter.notifyDataSetChanged();
        }
        return false;//return false to trigger listview expand
    }

    public View createHeader() {
        View view = View.inflate(this, R.layout.alerts_list_header, null);
        ImageButton submit = (ImageButton) view.findViewById(R.id.submitAlertButton);
        submit.setOnClickListener(this);
        this.keyWordText = (EditText) view.findViewById(R.id.keyWordEditText);
        return view;
    }


    @Override
    public void onClick(View v) {
        Alert alert = new Alert();
        alert.setKeyword(this.keyWordText.getText() + "");
        alert.toValues().insert(getContentResolver());
        this.keyWordText.setText("");
        this.explListAdapter.notifyDataSetChanged();
    }

    @Override
    public Loader<Alerts> onCreateLoader(int id, Bundle args) {
        return new AlertLoader(this, selection);
    }

    @Override
    public void onLoadFinished(Loader<Alerts> loader, Alerts data) {
        this.explListAdapter.swapList(data);
        this.explListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Alerts> loader) {
        this.explListAdapter.swapList(null);
        this.explListAdapter.notifyDataSetChanged();
    }
}
