package at.fibuszene.feednews.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.activities.AlertsActivity;
import at.fibuszene.feednews.interfaces.ArticlesInterface;
import at.fibuszene.feednews.model.Alert;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.NewAlert;
import at.fibuszene.feednews.model.lists.Alerts;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.alert.AlertColumns;
import at.fibuszene.feednews.persistency.alert.AlertSelection;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.newalert.NewalertColumns;
import at.fibuszene.feednews.utils.Constants;
import at.fibuszene.feednews.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AlertService extends Service {
    private RestAdapter adapter;

    public AlertService() {
        this.adapter = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).build();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (PrefUtils.allowUpdate(this)) {
            new Thread() {
                @Override
                public void run() {
                    ArticlesInterface api = adapter.create(ArticlesInterface.class);
                    Alerts alerts = Alerts.fromCursorForService(new AlertSelection().query(getContentResolver(), AlertColumns.FULL_PROJECTION));
                    for (Alert alert : alerts) {
                        api.searchArticles(alert.getKeyword(), "date", 1, new AlertStorage(alert.get_id()));
                    }
                }
            }.start();
        }
        return START_STICKY;
    }

    public class AlertStorage implements Callback<Articles> {
        private long alertId;

        public AlertStorage(long alertId) {
            this.alertId = alertId;
        }

        @Override
        public void success(Articles articles, Response response) {
            Cursor cursor;
            ContentValues[] bulkValues = new ContentValues[articles.size()];
            int i = 0;
            for (Article tmp : articles.getArticles()) {
                tmp.setEnclosures(null);
                tmp.setCategoryId(0);

                Uri uri = tmp.toContentValue().insert(getContentResolver());
                long artId = 0;
                if (uri != null) { //article already in db
                    artId = Long.valueOf(uri.getLastPathSegment());
                } else {
                    cursor = getContentResolver().query(ArticleColumns.CONTENT_URI, new String[]{ArticleColumns.TABLE_NAME + "." + ArticleColumns._ID},
                            ArticleColumns.TITLE + " = ?", new String[]{tmp.getTitle()}, null);
                    if (cursor.moveToFirst()) {
                        artId = cursor.getLong(0);
                    }
                }

                if (artId > 0) {
                    NewAlert alert = new NewAlert();
                    alert.setArticle_id(artId);
                    alert.setAlert_id(this.alertId);
                    bulkValues[i] = alert.toContentValues().values();
                    i++;
                }
            }
            int inserted = getContentResolver().bulkInsert(NewalertColumns.CONTENT_URI, bulkValues);

            if (inserted > 0) { //added articles
                String[] alertIdSelArgs = new String[]{alertId + ""};
                //NewAlert Count
                //database gets AppContext anyway
                ContentValues values = new ContentValues();
                values.put(AlertColumns.COUNT, inserted);
                //completely removed seen just use count as "newCount" meaning onclick it will be reset and on new articels it will be != 0
                //display info icon instead of actual amount of new articels a lot less annoying for both, the user and me
                //but the database fields are there if i change my mind
                ////inserted: inserted geht nicht weil nicht alle artikel eingefuegt werden
                ////title article unique => articel contains keyword reference instead of new article
                //values.put(AlertColumns.SEEN, inserted);
                getContentResolver().update(AlertColumns.CONTENT_URI, values, AlertColumns._ID + " = ?", alertIdSelArgs);
                //eigentlich NewAlert koennt ma prazisieren allerdings braeuchts dann auch in der view einen eigenen loader fuer newAlerts
                getContentResolver().notifyChange(AlertColumns.CONTENT_URI, null);

                //update current alert
                PrefUtils.currentAlerts(getApplicationContext(), PrefUtils.currentAlerts(getApplicationContext()) + inserted);

                if (PrefUtils.showNotifications(getApplicationContext())) {
                    boolean bundle = true;

                    if (PrefUtils.bundleNotifications(getApplicationContext())) {
                        bundle = (PrefUtils.currentAlerts(getApplicationContext()) >= PrefUtils.bundleAmount(getApplicationContext()));
                        if (bundle) {
                            inserted = PrefUtils.currentAlerts(getApplicationContext()); //set inserted to actual number of inserted alerts
                        }
                    }

                    if (bundle) {

                        newAlert(inserted);
                        //set to inserted - bundleamount inserted is all inserted alerts
                        //24 - 20 = 4 alert again in 16 articles
                        PrefUtils.currentAlerts(getApplicationContext(), inserted - PrefUtils.bundleAmount(getApplicationContext()));
                    }
                }
            }
        }

        @Override
        public void failure(RetrofitError error) {
            Log.d("AlertService", error + "");
        }
    }

    public void newAlert(int count) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notificationBar = new Notification(R.drawable.ic_launcher, "New Articles", System.currentTimeMillis());

        Intent notificationIntent = new Intent(this, AlertsActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        notificationBar.setLatestEventInfo(this, "New Message", "You have " + count + " new articles.", intent);
        notificationBar.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notificationBar);
    }


}
