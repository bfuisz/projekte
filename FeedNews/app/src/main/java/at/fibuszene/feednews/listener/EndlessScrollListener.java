package at.fibuszene.feednews.listener;

import android.content.Context;
import android.widget.AbsListView;

/**
 * Created by benedikt.
 * Pretty much useless since the api does not support pagination
 */
public class EndlessScrollListener implements AbsListView.OnScrollListener {
    private Context context;
    private int visibleThreshold = 25;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;

    public EndlessScrollListener(Context context) {
        this.context = context;
    }

    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {

            load();
            loading = true;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    public void load() {
//        Intent intent = new Intent(context, LoadService.class);
//        intent.setAction(LoadService.LOAD_ARTICLES);
//        intent.putExtra(LoadService.LOAD_ARTICLES_ARG1, this.categoryId);
//        context.startService(intent);
    }
}