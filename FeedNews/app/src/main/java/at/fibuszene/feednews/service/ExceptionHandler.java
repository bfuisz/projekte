package at.fibuszene.feednews.service;


import android.content.Context;
import android.os.Handler;
import android.os.Message;

import at.fibuszene.feednews.activities.HomeActivity;

/**
 * Created by benedikt.
 */
public class ExceptionHandler extends Handler {
    private HomeActivity homeActivity;

    public ExceptionHandler(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        homeActivity.displayError(msg.arg1, msg.arg2);
    }
}
