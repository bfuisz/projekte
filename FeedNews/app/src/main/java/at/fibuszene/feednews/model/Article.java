package at.fibuszene.feednews.model;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Date;

import at.fibuszene.feednews.model.lists.Enclosures;
import at.fibuszene.feednews.persistency.article.ArticleContentValues;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.enclosure.EnclosureColumns;
import at.fibuszene.feednews.utils.Constants;
import at.fibuszene.feednews.utils.Utils;

/**
 * Created by benedikt.
 */
public class Article {
    private long _id;
    private String url;
    private String title;
    private String summary;
    private String publish_date;
    private String author;
    private String source;
    private long categoryId;
    private boolean read;
    private boolean saved = false;

    @SerializedName("enclosures")
    private Enclosures enclosures;

    public Article() {
    }

    public Article(long _id, String url, String title, String summary, String publish_date, String author, String source) {
        this._id = _id;
        this.url = url;
        this.title = title;
        this.summary = summary;
        this.publish_date = publish_date;
        this.author = author;
        this.source = source;
    }


    public void toggleSaved() {
        this.saved = !this.saved;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }


    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public Enclosures getEnclosures() {
        return enclosures;
    }

    public void setEnclosures(Enclosures enclosures) {
        this.enclosures = enclosures;
    }

    public static Article from(ArticleCursor cursor) {

        Article article = new Article();
        try {
            article.set_id(cursor.getId());

        } catch (NullPointerException npe) {
            //why ?
            article._id = cursor.getLong(0);
        }

        article.setUrl(cursor.getUrl());
        article.setTitle(cursor.getTitle());
        article.setSummary(cursor.getSummary());
        long date = cursor.getPublishDate();
        article.setPublish_date(Utils.fromDB(date));
        article.setAuthor(cursor.getAuthor());
        article.setSource(cursor.getSource());

        Long catId = cursor.getFkCategoryId();//fk is the one i want from article !
        if (catId != null) {//e.g. from AlertService (=> No Catgegory id)
            article.setCategoryId(catId);
        }

        article.setRead(cursor.getRead());
        article.setSaved(cursor.getSaved());

        try {
            Enclosures encs = new Enclosures();
            Enclosure enc = new Enclosure();
            enc.setMedia_type(cursor.getString(cursor.getColumnIndex(EnclosureColumns.MEDIA_TYPE)));
            enc.setUri(cursor.getString(cursor.getColumnIndex(EnclosureColumns.URI)));
            encs.add(enc);
            article.setEnclosures(encs);
        } catch (Exception e) {
            //no enclosures
            article.setEnclosures(null);
        }
        return article;
    }


    public ArticleContentValues toContentValue() {
        ArticleContentValues values = new ArticleContentValues();
        String date = publish_date;

        try {
            Date dateObj = Constants.dateFormat.parse(date);
            values.putPublishDate(dateObj.getTime());
        } catch (ParseException pe) {
            date = publish_date;
        }

        values.putRead(this.read);
        values.putSource(this.source);
        values.putSummary(this.summary);
        values.putTitle(this.title);
        values.putUrl(this.url);
        values.putAuthor(this.author);

        if (categoryId > 0) {
            values.putFkCategoryId(this.categoryId);
        } else {
            values.putFkCategoryId(null);
        }
        values.putSaved(this.saved);

        if (enclosures != null && enclosures.get(0) != null) {
            long id = enclosures.get(0).getId();
            if (id > 0) {
                values.putEnclosureId(id);
            } else {
                values.putEnclosureId(null);
            }
        } else {
            values.putEnclosureId(null);
        }
        return values;
    }


    @Override
    public String toString() {
        return "Article{" +
                "_id=" + _id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                ", publish_date='" + publish_date + '\'' +
                ", author='" + author + '\'' +
                ", source='" + source + '\'' +
                ", categoryId=" + categoryId +
                ", read=" + read +
                ", saved=" + saved +
                ", enclosures=" + enclosures +
                '}';
    }
}
