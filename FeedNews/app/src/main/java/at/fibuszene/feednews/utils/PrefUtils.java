package at.fibuszene.feednews.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.util.IllegalFormatException;

/**
 * Created by benedikt.
 */
public class PrefUtils {

    public static boolean firstStart(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("first_start", true);
    }

    public static void firstStart(Context context, boolean start) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("first_start", start).commit();
    }

    private static boolean startFrom(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("start_from_last", true);
    }

    public static void startFrom(Context context, boolean start) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("start_from_last", start).commit();
        if (!start) {
            lastPage(context, -1);
        }
    }

    public static void lastPage(Context context, int lastPage) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("last_page", lastPage).commit();
    }

    private static int lastPage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("last_page", 0);
    }

    public static int getLastPage(Context context) {
        if (startFrom(context)) {
            return lastPage(context);
        } else {
            return 0;
        }
    }

    public static boolean showNotifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_articles", true);
    }

    public static int alertSyncFrequency(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString("article_sync_alert_frequency", "60");
        try {
            return Integer.valueOf(value);
        } catch (IllegalFormatException ilf) {
            return 60;
        }
    }

    public static boolean bundleNotifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("bundle_notifications", false);
    }

    public static boolean synchronizeViaWifiOnly(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("wifi_only", false);
    }

    public static int bundleAmount(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString("bundle_amount", "0");
        try {
            return Integer.valueOf(value);
        } catch (IllegalFormatException ilf) {
            return 0;
        }
    }

    public static int currentAlerts(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("current_alerts", 0);
    }

    public static void currentAlerts(Context context, int alerts) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("current_alerts", alerts).commit();
    }

    public static void selectedNavItem(Context context, int lastPage) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("selected_nav_item", lastPage).commit();
    }

    public static int selectedNavItem(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("selected_nav_item", 0);
    }


    public static boolean allowUpdate(Context context) {
        if (PrefUtils.synchronizeViaWifiOnly(context)) {
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return mWifi.isConnected();
        }
        return true;
    }
}
