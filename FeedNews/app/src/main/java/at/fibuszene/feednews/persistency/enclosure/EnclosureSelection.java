package at.fibuszene.feednews.persistency.enclosure;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code enclosure} table.
 */
public class EnclosureSelection extends AbstractSelection<EnclosureSelection> {
    @Override
    public Uri uri() {
        return EnclosureColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code EnclosureCursor} object, which is positioned before the first entry, or null.
     */
    public EnclosureCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new EnclosureCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public EnclosureCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public EnclosureCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public EnclosureSelection id(long... value) {
        addEquals(EnclosureColumns._ID, toObjectArray(value));
        return this;
    }


    public EnclosureSelection mediaType(String... value) {
        addEquals(EnclosureColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosureSelection mediaTypeNot(String... value) {
        addNotEquals(EnclosureColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosureSelection mediaTypeLike(String... value) {
        addLike(EnclosureColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosureSelection uri(String... value) {
        addEquals(EnclosureColumns.URI, value);
        return this;
    }

    public EnclosureSelection uriNot(String... value) {
        addNotEquals(EnclosureColumns.URI, value);
        return this;
    }

    public EnclosureSelection uriLike(String... value) {
        addLike(EnclosureColumns.URI, value);
        return this;
    }
}
