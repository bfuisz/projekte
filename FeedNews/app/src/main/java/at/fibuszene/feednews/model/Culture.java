package at.fibuszene.feednews.model;

/**
 * Created by benedikt.
 */
public class Culture {
    private String country_code;
    private String culture_code;
    private String display_culture_name;
    private String english_culture_name;
    private String language_code;


    public Culture() {
    }

    public Culture(String country_code, String culture_code, String display_culture_name, String english_culture_name, String language_code) {
        this.country_code = country_code;
        this.culture_code = culture_code;
        this.display_culture_name = display_culture_name;
        this.english_culture_name = english_culture_name;
        this.language_code = language_code;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCulture_code() {
        return culture_code;
    }

    public void setCulture_code(String culture_code) {
        this.culture_code = culture_code;
    }

    public String getDisplay_culture_name() {
        return display_culture_name;
    }

    public void setDisplay_culture_name(String display_culture_name) {
        this.display_culture_name = display_culture_name;
    }

    public String getEnglish_culture_name() {
        return english_culture_name;
    }

    public void setEnglish_culture_name(String english_culture_name) {
        this.english_culture_name = english_culture_name;
    }

    public String getLanguage_code() {
        return language_code;
    }

    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }
}
