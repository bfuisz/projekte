package at.fibuszene.feednews.persistency.usercultures;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code usercultures} table.
 */
public class UserculturesColumns implements BaseColumns {
    public static final String TABLE_NAME = "usercultures";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String COUNTRY_CODE = "country_code";
    public static final String CULTURE_CODE = "culture_code";
    public static final String DISPLAY_CULTURE_NAME = "display_culture_name";
    public static final String ENGLISH_CULTURE_NAME = "english_culture_name";
    public static final String LANGUAGE_CODE = "language_code";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[] {
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + COUNTRY_CODE,
            TABLE_NAME + "." + CULTURE_CODE,
            TABLE_NAME + "." + DISPLAY_CULTURE_NAME,
            TABLE_NAME + "." + ENGLISH_CULTURE_NAME,
            TABLE_NAME + "." + LANGUAGE_CODE
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();
    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(COUNTRY_CODE);
        ALL_COLUMNS.add(CULTURE_CODE);
        ALL_COLUMNS.add(DISPLAY_CULTURE_NAME);
        ALL_COLUMNS.add(ENGLISH_CULTURE_NAME);
        ALL_COLUMNS.add(LANGUAGE_CODE);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
