package at.fibuszene.feednews.persistency.usercultures;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code usercultures} table.
 */
public class UserculturesContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return UserculturesColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, UserculturesSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public UserculturesContentValues putCountryCode(String value) {
        mContentValues.put(UserculturesColumns.COUNTRY_CODE, value);
        return this;
    }

    public UserculturesContentValues putCountryCodeNull() {
        mContentValues.putNull(UserculturesColumns.COUNTRY_CODE);
        return this;
    }


    public UserculturesContentValues putCultureCode(String value) {
        mContentValues.put(UserculturesColumns.CULTURE_CODE, value);
        return this;
    }

    public UserculturesContentValues putCultureCodeNull() {
        mContentValues.putNull(UserculturesColumns.CULTURE_CODE);
        return this;
    }


    public UserculturesContentValues putDisplayCultureName(String value) {
        mContentValues.put(UserculturesColumns.DISPLAY_CULTURE_NAME, value);
        return this;
    }

    public UserculturesContentValues putDisplayCultureNameNull() {
        mContentValues.putNull(UserculturesColumns.DISPLAY_CULTURE_NAME);
        return this;
    }


    public UserculturesContentValues putEnglishCultureName(String value) {
        mContentValues.put(UserculturesColumns.ENGLISH_CULTURE_NAME, value);
        return this;
    }

    public UserculturesContentValues putEnglishCultureNameNull() {
        mContentValues.putNull(UserculturesColumns.ENGLISH_CULTURE_NAME);
        return this;
    }


    public UserculturesContentValues putLanguageCode(String value) {
        mContentValues.put(UserculturesColumns.LANGUAGE_CODE, value);
        return this;
    }

    public UserculturesContentValues putLanguageCodeNull() {
        mContentValues.putNull(UserculturesColumns.LANGUAGE_CODE);
        return this;
    }

}
