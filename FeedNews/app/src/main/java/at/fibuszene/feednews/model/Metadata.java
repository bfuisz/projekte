package at.fibuszene.feednews.model;

import at.fibuszene.feednews.persistency.metadata.MetadataContentValues;
import at.fibuszene.feednews.persistency.metadata.MetadataCursor;

/**
 * Created by benedikt.
 */
public class Metadata {

    private long id;
    private String tableName;
    private long categoryId;
    private long last_updated;
    private long last_cleared;
    private int count;


    public Metadata() {
    }

    public Metadata(long id, String tableName, long categoryId, long last_updated, long last_cleared, int count) {
        this.id = id;
        this.tableName = tableName;
        this.categoryId = categoryId;
        this.last_updated = last_updated;
        this.last_cleared = last_cleared;
        this.count = count;
    }

    public MetadataContentValues toMetaContentValues() {
        MetadataContentValues values = new MetadataContentValues();
        values.putColTableName(this.tableName);
        values.putLastUpdated(this.last_updated);
        values.putLastCleared(this.last_cleared);
        values.putContentCount(this.count);
        values.putColCategoryId(this.categoryId);
        return values;
    }

    public static Metadata fromCursor(MetadataCursor cursor) {
        Metadata data = new Metadata();
        data.id = cursor.getId();
        data.categoryId = cursor.getColCategoryId();
        data.tableName = cursor.getColTableName();
        data.last_updated = cursor.getLastUpdated();
        data.last_cleared = cursor.getLastCleared();
        data.count = cursor.getContentCount();
        return data;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public long getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(long last_updated) {
        this.last_updated = last_updated;
    }

    public long getLast_cleared() {
        return last_cleared;
    }

    public void setLast_cleared(long last_cleared) {
        this.last_cleared = last_cleared;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}
