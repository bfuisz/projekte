package at.fibuszene.feednews.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.activities.ArticleActivity;
import at.fibuszene.feednews.model.Alert;
import at.fibuszene.feednews.model.NewAlert;
import at.fibuszene.feednews.model.lists.Alerts;
import at.fibuszene.feednews.persistency.alert.AlertColumns;
import at.fibuszene.feednews.persistency.newalert.NewalertColumns;

/**
 * Created by benedikt.
 */
public class AlertsExpListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private Alerts alerts;

    public AlertsExpListAdapter(Context context, Alerts alerts) {
        this.context = context;
        this.alerts = alerts;
        if (alerts == null) {
            this.alerts = new Alerts();
        }
    }

    @Override
    public int getGroupCount() {
        return alerts.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return alerts.get(groupPosition).getNewAlerts().size();
    }

    @Override
    public Alert getGroup(int groupPosition) {
        return alerts.get(groupPosition);
    }

    @Override
    public NewAlert getChild(int groupPosition, int childPosition) {
        return alerts.get(groupPosition).getNewAlerts().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return alerts.get(groupPosition).get_id();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return alerts.get(groupPosition).getNewAlerts().get(childPosition).get_id();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final Alert group = getGroup(groupPosition);
        final long id = group.get_id();
        final String keyWord = group.getKeyword();

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.alert_list_group_item, null);
        }

        TextView txtView = (TextView) convertView.findViewById(R.id.textView);
        txtView.setText(group.getKeyword());

        final TextView finalTxtView = (TextView) convertView.findViewById(R.id.clearAlerts);
        finalTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int deleted = context.getContentResolver().delete(NewalertColumns.CONTENT_URI, NewalertColumns.ALERT_ID + " = " + id, null);
                clearGroup(groupPosition);
                notifyDataSetChanged();
                finalTxtView.setTextColor(context.getResources().getColor(android.R.color.black));
            }
        });

        ImageView button = (ImageView) convertView.findViewById(R.id.deleteAlertButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int deleted = context.getContentResolver().delete(AlertColumns.CONTENT_URI, AlertColumns._ID + " = " + id, null);
                if (deleted > 0) {
                    Toast.makeText(context, keyWord + " deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });

        long count = group.getCount();
        if (count == 0) {
            convertView.findViewById(R.id.unseenAlerts).setVisibility(View.INVISIBLE);
        } else {
            convertView.findViewById(R.id.unseenAlerts).setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    public void clearGroup(int group) {
        try {
            getGroup(group).getNewAlerts().clear();
        } catch (NullPointerException npe) {
            Log.d("For the sake of thoroughness", "should not happen ");
        }
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final NewAlert newAlert = getChild(groupPosition, childPosition);
        if (newAlert != null && newAlert.get_id() > 0) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.alert_list_child_item, null);
            }

            TextView txtView = (TextView) convertView.findViewById(R.id.textView);
            try {
                txtView.setText(newAlert.getArticle().getTitle());
            } catch (NullPointerException npe) {
                txtView.setText(newAlert.get_id() + "");
            }

            txtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ArticleActivity.class);
                    intent.putExtra(ArticleActivity.ARTICLE_URL_EXTRA, newAlert.getArticle().getUrl());
                    context.startActivity(intent);
                }
            });
            txtView = (TextView) convertView.findViewById(R.id.dateView);
            try {
                txtView.setText(newAlert.getArticle().getPublish_date());
            } catch (NullPointerException npe) {
                Log.wtf("should not happen", "at all alert not publish date");
            }
        }else{
            convertView = View.inflate(context, R.layout.all_purpose_empty, null);
            ((TextView)convertView.findViewById(R.id.emptyList)).setText("No Items");
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void swapList(Alerts alerts) {
        if (alerts == null) {
            this.alerts.clear();
        } else {
            this.alerts = alerts;
        }
    }
}
