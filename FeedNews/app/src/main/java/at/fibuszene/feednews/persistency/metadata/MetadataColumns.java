package at.fibuszene.feednews.persistency.metadata;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code metadata} table.
 */
public class MetadataColumns implements BaseColumns {
    public static final String TABLE_NAME = "metadata";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String COL_TABLE_NAME = "col_table_name";
    public static final String COL_CATEGORY_ID = "col_category_id";
    public static final String LAST_UPDATED = "last_updated";
    public static final String LAST_CLEARED = "last_cleared";
    public static final String CONTENT_COUNT = "content_count";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[] {
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + COL_TABLE_NAME,
            TABLE_NAME + "." + COL_CATEGORY_ID,
            TABLE_NAME + "." + LAST_UPDATED,
            TABLE_NAME + "." + LAST_CLEARED,
            TABLE_NAME + "." + CONTENT_COUNT
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();
    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(COL_TABLE_NAME);
        ALL_COLUMNS.add(COL_CATEGORY_ID);
        ALL_COLUMNS.add(LAST_UPDATED);
        ALL_COLUMNS.add(LAST_CLEARED);
        ALL_COLUMNS.add(CONTENT_COUNT);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
