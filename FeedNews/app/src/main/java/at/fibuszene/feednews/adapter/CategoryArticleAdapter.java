package at.fibuszene.feednews.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.text.TextPaint;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.Enclosure;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.utils.DBUtils;

/**
 * Created by benedikt.
 */
public class CategoryArticleAdapter extends BaseAdapter {
    private Typeface headlineFont, summaryFont;

    private Context context;
    private Articles articles;

    public CategoryArticleAdapter(Context context, Articles articles) {
        this.context = context;
        this.articles = articles;
        headlineFont = Typeface.createFromAsset(context.getAssets(), "Montserrat-Regular.ttf");
        summaryFont = Typeface.createFromAsset(context.getAssets(), "LiberationSans-Regular.ttf");
        //e.g. no frontpage articles
        if (articles == null) {
            this.articles = new Articles();
        }
    }

    @Override
    public int getCount() {
        try {
            return articles.size();
        } catch (NullPointerException npe) {//weirdly enough this actually happens on occasion
            return 0;
        }
    }

    @Override
    public Article getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return articles.get(position).get_id();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Article article = getItem(position);

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.article_item, null);
        }
        if (article.getEnclosures() != null) {
            final ImageView background = (ImageView) convertView.findViewById(R.id.enclosedImage);
            final Enclosure enc = article.getEnclosures().get(0);

            if (enc != null) {//&& enc.getMedia_type().equals("image/jpeg")) {
                try {
                    ImageLoader.getInstance().displayImage(enc.getUri(), background);
                } catch (Exception nf) {
//                    FileNotFoundException  404
                }

            }
        }

        TextView txtView = (TextView) convertView.findViewById(R.id.titleTextView);
        txtView.setTypeface(this.headlineFont);
        String text = article.getTitle();
        if (text != null) {
            txtView.setText(Html.fromHtml(text));
        }

        if (article.isRead()) {
            txtView.setPaintFlags(txtView.getPaintFlags() | TextPaint.STRIKE_THRU_TEXT_FLAG);
        } else {
            txtView.setPaintFlags(txtView.getPaintFlags() & ~TextPaint.STRIKE_THRU_TEXT_FLAG);
        }

        txtView = (TextView) convertView.findViewById(R.id.summaryTextView);
        txtView.setTypeface(this.summaryFont);
        text = article.getSummary();
        if (text != null) {
            txtView.setText(Html.fromHtml(text));
        }

        txtView = (TextView) convertView.findViewById(R.id.authorTextView);
        text = article.getAuthor();
        if (text != null) {
            txtView.setText(Html.fromHtml(text));
        }
        txtView = (TextView) convertView.findViewById(R.id.dateTextView);
        txtView.setText(article.getPublish_date());
        txtView = (TextView) convertView.findViewById(R.id.categoryName);

        if (article.getCategoryId() > 1) {
            final LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.articleDetailsLayout);
            Pair<String, Integer> catPair = DBUtils.getCategoryColor(context, article.getCategoryId());
            layout.setBackgroundColor(catPair.second);
            txtView.setText(catPair.first);
        }

        ImageView button = (ImageView) convertView.findViewById(R.id.saveArticleButton);

        if (article.isSaved()) {
            activateButton(button, true);
        } else {
            button.setEnabled(true);
            button.setActivated(false);
        }

        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          article.toggleSaved();
                                          ImageView button = (ImageView) v;
                                          activateButton(button, article.isSaved());

                                          ContentValues values = new ContentValues();
                                          values.put(ArticleColumns.SAVED, article.isSaved());
                                          context.getContentResolver().update(ArticleColumns.CONTENT_URI, values, ArticleColumns._ID + " = " + article.get_id(), null);

//                      article.toContentValue().update(context.getContentResolver(), selection);
                                      }
                                  }
        );

        button = (ImageView) convertView.findViewById(R.id.openInBrowser);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl()));
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    public void activateButton(ImageView button, boolean saved) {
        button.setEnabled(saved);
        button.setActivated(saved);
    }

    public void setArticles(Articles articles) {
        this.articles = articles;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public Articles getArticles() {
        return articles;
    }
}
