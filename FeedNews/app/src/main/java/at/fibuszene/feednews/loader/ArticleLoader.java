package at.fibuszene.feednews.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.article.ArticleSelection;

/**
 * Created by benedikt.
 */
public class ArticleLoader extends AsyncTaskLoader<Articles> {
    private Articles articles;
    private ArticleCursor cursor;
    private ArticleSelection selection;
    private ForceLoadContentObserver contentObserver = new ForceLoadContentObserver();


    public ArticleLoader(Context context, ArticleSelection selection) {
        super(context);
        this.selection = selection;
    }

    @Override
    public Articles loadInBackground() {

        cursor = selection.query(getContext().getContentResolver(), null, ArticleColumns.PUBLISH_DATE + " DESC");
        cursor.setNotificationUri(getContext().getContentResolver(), ArticleColumns.CONTENT_URI);
        getContext().getContentResolver().registerContentObserver(ArticleColumns.CONTENT_URI, true, contentObserver);
        this.articles = Articles.fromCursor(cursor);
        return articles;
    }

    public ArticleSelection getSelection() {
        return selection;
    }

    public void setSelection(ArticleSelection selection) {
        this.selection = selection;
        if (selection != null) {
            loadInBackground();
        }
    }


    @Override
    public void deliverResult(Articles articles) {
        if (isReset()) {
            if (articles != null) {
                onReleaseResources(articles);
            }
        }

        Articles oldArticles = articles;
        this.articles = articles;

        if (isStarted()) {
            super.deliverResult(articles);
        }
        if (oldArticles != null) {
            onReleaseResources(oldArticles);
        }
    }

    @Override
    protected void onStartLoading() {
        if (articles != null) {
            deliverResult(articles);
        }
        if (takeContentChanged() || articles == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(Articles articles) {
        super.onCanceled(articles);
        onReleaseResources(articles);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (articles != null) {
            onReleaseResources(articles);
            articles = null;
        }
        getContext().getContentResolver().unregisterContentObserver(contentObserver);

    }

    protected void onReleaseResources(Articles articles) {
//        articles = null;
    }


}
