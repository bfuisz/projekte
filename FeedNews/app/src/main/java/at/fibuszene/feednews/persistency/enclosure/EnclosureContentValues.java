package at.fibuszene.feednews.persistency.enclosure;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code enclosure} table.
 */
public class EnclosureContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return EnclosureColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, EnclosureSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public EnclosureContentValues putMediaType(String value) {
        mContentValues.put(EnclosureColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosureContentValues putMediaTypeNull() {
        mContentValues.putNull(EnclosureColumns.MEDIA_TYPE);
        return this;
    }


    public EnclosureContentValues putUri(String value) {
        mContentValues.put(EnclosureColumns.URI, value);
        return this;
    }

    public EnclosureContentValues putUriNull() {
        mContentValues.putNull(EnclosureColumns.URI);
        return this;
    }

}
