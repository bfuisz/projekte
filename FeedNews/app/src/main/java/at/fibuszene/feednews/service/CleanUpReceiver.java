package at.fibuszene.feednews.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class CleanUpReceiver extends BroadcastReceiver {
    public static final String START_CLEANUP_BROADCAST = "at.fibuszene.receiver.start_cleanup";
    public static final int REQUEST_CODE = 100;

    public CleanUpReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //start at boot or at start_cleanup
        Log.d("BroadcastClean", "got an cast");
//        if (intent != null && (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") || intent.getAction().equals(START_CLEANUP_BROADCAST) || intent.getAction().equals("android.intent.action.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE"))) {
            Log.d("BroadcastClean", "got an cast and doing stuff");
            Intent intent1 = new Intent(context, CleanUpService.class);
            intent1.setAction(CleanUpService.CLEAN_UP);
            context.startService(intent1);
//        }
    }


}
