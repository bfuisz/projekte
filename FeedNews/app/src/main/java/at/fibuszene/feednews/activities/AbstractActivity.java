package at.fibuszene.feednews.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.manuelpeinado.fadingactionbar.FadingActionBarHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.adapter.DrawerAdapter;
import at.fibuszene.feednews.service.ExceptionHandler;
import at.fibuszene.feednews.utils.PrefUtils;

/**
 * Created by benedikt.
 * Fragement waer schoener gwesen...
 */
public abstract class AbstractActivity extends FragmentActivity implements AdapterView.OnItemClickListener {

    private final static int ANOTHER_CATEGORY_LOADER = 500;
    private final static String CURRENT_NAV_ITEM = "at.fibuszene.current_nav_item";
    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    protected DrawerAdapter drawerAdapter;
    protected ActionBarDrawerToggle mDrawerToggle;
    private Handler mHandler = new Handler();

    protected void onCreate(Bundle savedInstanceState, int layout) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.abstract_activity);
        View view = View.inflate(this, layout, null);
        FrameLayout layout1 = (FrameLayout) findViewById(R.id.container);
        layout1.removeAllViews();
        layout1.addView(view);
        setUpDrawer();
        restartLoader();
    }

    protected void onCreate(Bundle savedInstanceState, View layout) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.abstract_activity);
        FrameLayout layout1 = (FrameLayout) findViewById(R.id.container);
        layout1.removeAllViews();
        layout1.addView(layout);
        setUpDrawer();
    }

    public void createFadingActionBar(Bundle savedInstanceState, int mainlayout, int headerlayout) {
        super.onCreate(savedInstanceState);
        View v = View.inflate(this, mainlayout, null);
        View fadingV = (View) v.findViewById(headerlayout);
        FadingActionBarHelper helper = new FadingActionBarHelper().contentLayout(mainlayout).headerView(fadingV);

        setContentView(helper.createView(this));
        helper.initActionBar(this);
        setUpDrawer();
        restartLoader();
    }

    public void restartLoader() {
        getLoaderManager().restartLoader(ANOTHER_CATEGORY_LOADER, null, drawerAdapter);
    }

    /* Drawer and Menu Stuff Start {*/
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = null;
        try {
            view.findViewById(R.id.locationIndicator).setSelected(true);
            view.findViewById(R.id.locationIndicator).setActivated(true);
            view.setPressed(true);
            PrefUtils.selectedNavItem(this, position);
        } catch (NullPointerException npe) {
            //e.g. the user clicks on "Shortcut" heading
        }
        switch (position) {
            case 0:
                intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
            case 1:
                intent = new Intent(this, SavedArticles.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, CatergoriesSettings.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, AlertsActivity.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                if (this instanceof HomeActivity) {
                    ((HomeActivity) this).scrollTo(id);
                } else {
                    intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(HomeActivity.SCROLL_TO_CATEGORY, id);
                    startActivity(intent);
                }
                break;
        }
        closeDrawer();
    }

    public void closeDrawer() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        }, 150);
    }

    public void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.app_name) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getActionBar() != null) {
                    getActionBar().setTitle(getResources().getString(R.string.app_name));
                }
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getActionBar() != null) {
                    getActionBar().setTitle(getResources().getString(R.string.drawer_title));
                }
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerList = (ListView) findViewById(R.id.drawerList);

        this.drawerAdapter = new DrawerAdapter(this);
        mDrawerList.setAdapter(drawerAdapter);

        mDrawerList.setOnItemClickListener(this);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        if (id == R.id.action_dumpdb) {
//            exportDatabse("feedzillaFnews.db");
//            return true;
//        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void animateUpdate() {
        ImageView iv = (ImageView) findViewById(R.id.refreshView);
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.setVisibility(View.VISIBLE);
        iv.startAnimation(rotation);
    }

    public void cancelUpdate(boolean error) {
        ImageView iv = (ImageView) findViewById(R.id.refreshView);
        iv.clearAnimation();
        iv.setVisibility(View.GONE);
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            String backupDBPath = "backupname.db";

            if (sd.canWrite()) {
                File currentDB = getDatabasePath(databaseName);
                File backupDB = new File(sd, backupDBPath);
                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
            Toast.makeText(this, "DB Dumped to " + backupDBPath, Toast.LENGTH_SHORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /* Drawer and Menu Stuff End } */


}
