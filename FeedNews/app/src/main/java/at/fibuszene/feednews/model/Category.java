package at.fibuszene.feednews.model;

import android.database.CursorIndexOutOfBoundsException;
import android.util.Log;

import at.fibuszene.feednews.persistency.category.CategoryContentValues;
import at.fibuszene.feednews.persistency.category.CategoryCursor;

/**
 * Created by benedikt.
 */
public class Category {
    private long id;
    private long category_id;
    private String display_category_name;
    private String english_category_name;
    private String url_category_name;
    private boolean hidden;
    private int orderInHome;
    private int color;
    private boolean frontpage;
    private boolean shortcut;

    public Category() {
    }


    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public String getDisplay_category_name() {
        return display_category_name;
    }

    public void setDisplay_category_name(String display_category_name) {
        this.display_category_name = display_category_name;
    }

    public String getEnglish_category_name() {
        return english_category_name;
    }

    public void setEnglish_category_name(String english_category_name) {
        this.english_category_name = english_category_name;
    }

    public String getUrl_category_name() {
        return url_category_name;
    }

    public void setUrl_category_name(String url_category_name) {
        this.url_category_name = url_category_name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getOrderInHome() {
        return orderInHome;
    }

    public void setOrderInHome(int orderInHome) {
        this.orderInHome = orderInHome;
    }

    public boolean isFrontpage() {
        return frontpage;
    }

    public void setFrontpage(boolean frontpage) {
        this.frontpage = frontpage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void toggleFrontpage() {
        this.frontpage = !this.frontpage;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }


    public boolean isShortcut() {
        return shortcut;
    }

    public void setShortcut(boolean shortcut) {
        this.shortcut = shortcut;
    }

    public static Category fromCatCursor(CategoryCursor cursor) {
        try {
            Category category = new Category();
            category.setId(cursor.getId());
            category.setCategory_id(cursor.getCategoryId());
            category.setDisplay_category_name(cursor.getDisplayCategoryName());
            category.setEnglish_category_name(cursor.getEnglishCategoryName());
            category.setUrl_category_name(cursor.getUrlCategoryName());
            category.setOrderInHome(cursor.getOrderInHome());
            category.setColor(cursor.getColor());
            category.setHidden(cursor.getHidden());
            category.setFrontpage(cursor.getFrontpage());

            try {
                category.setShortcut(cursor.getShortcut());
            } catch (NullPointerException npe) {
                Log.d("CategoryShortCut", "Expcetion");
                category.setShortcut(false);
            }
            return category;
        } catch (CursorIndexOutOfBoundsException cie) {
            return null; //at least one of the categories is a dummy category
        }
    }

    public CategoryContentValues toContentValues() {
        CategoryContentValues values = new CategoryContentValues();
        values.putCategoryId(this.category_id);
        values.putDisplayCategoryName(this.display_category_name);
        values.putEnglishCategoryName(this.english_category_name);
        values.putUrlCategoryName(this.url_category_name);
        values.putOrderInHome(this.orderInHome);
        values.putColor(this.color);
        values.putFrontpage(this.frontpage);
        values.putHidden(this.hidden);
        values.putShortcut(this.shortcut);
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;

        Category category = (Category) o;

        if (category_id != category.category_id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (category_id ^ (category_id >>> 32));
    }
}
