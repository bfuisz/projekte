package at.fibuszene.feednews.interfaces;

import at.fibuszene.feednews.model.lists.Articles;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by benedikt.
 */
public interface ArticlesInterface {

    @GET("/v1/articles/search.json")
    public void searchArticles(@Query("q") String query, @Query("order") String order, @Query("title_only") int titleOnly, Callback<Articles> response);

    @GET("/v1/articles.json")
    public void listArticles(@Query("count") int count, @Query("order") String order, Callback<Articles> articles);

    @GET("/v1/categories/{category_id}/articles.json")
    public void listArticles(@Path("category_id") long categoryId, @Query("order") String order, @Query("count") long count, Callback<Articles> response);

    @GET("/v1/categories/{category_id}/articles.json")
    public void listArticles(@Path("category_id") long categoryId, @Query("order") String order, @Path("since") String since, @Query("count") long count, Callback<Articles> response);

    /*
     * since - Optional. Returns articles that was published since the given date. Date should be formatted as YYYY-MM-DD
     * order = relevance or date
     * title only = 0 or 1
     */
    @GET("/v1/categories/{category_id}/articles.json?count={count}&since={sin}&order={ord}&title_only={t_only}")
    public void listArticles(@Path("category_id") long categoryId, @Path("count") long count, @Path("sin") long since, @Path("t_only") int title_only, Callback<Articles> response);

    @GET("/v1/categories/{category_id}/articles/search.json?q={query}&count={count}&since={sin}&order={ord}&title_only={t_only}")
    public void searchArticles(@Path("query") String query, @Path("category_id") long categoryId, @Path("count") long count, @Path("sin") long since, @Path("t_only") int title_only, Callback<Articles> response);

    @GET("/v1/categories/{category_id}/subcategories/{subcategory_id}/articles.json?count={count}&since={sin}&order={ord}&title_only={t_only}")
    public void listArticlesSub(@Path("subcategory_id") long subcategoryId, @Path("category_id") long categoryId, @Path("count") long count, @Path("sin") long since, @Path("t_only") int title_only, Callback<Articles> response);

    @GET("/v1/categories/{category_id}/subcategories/{subcategory_id}/articles.json?q={query}&count={count}&since={sin}&order={ord}&title_only={t_only}")
    public void searchArticlesSub(@Path("subcategory_id") long subcategoryId, @Path("query") String query, @Path("category_id") long categoryId, @Path("count") long count, @Path("sin") long since, @Path("t_only") int title_only, Callback<Articles> response);

    @GET("/v1/articles/search.json")
    public void searchAll(@Query("q") String query, @Query("count") int count, Callback<Articles> response);


}
