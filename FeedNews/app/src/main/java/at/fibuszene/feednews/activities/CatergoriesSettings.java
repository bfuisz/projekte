package at.fibuszene.feednews.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.TouchViewDraggableManager;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.adapter.ConfigureCategoriesAdapter;
import at.fibuszene.feednews.listener.ColorChangeListener;
import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.model.lists.Categories;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.category.CategorySelection;

public class CatergoriesSettings extends AbstractActivity implements ColorChangeListener {
    private ConfigureCategoriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_catergories_settings);
        CategorySelection selection = new CategorySelection();
        DynamicListView listView = (DynamicListView) findViewById(R.id.configCategoryListView);
        this.adapter = new ConfigureCategoriesAdapter(this, Categories.fromCursor(selection.query(getContentResolver())));
//        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        listView.enableDragAndDrop();
        listView.setDraggableManager(new TouchViewDraggableManager(R.id.itemrow_gripview));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_catergories_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == R.id.action_sortalpha) {
            this.adapter.sortAlpha();
            this.adapter.notifyDataSetChanged();
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    int i = 0;
                    Long[] args = new Long[1];
                    for (Category cat : adapter.getCategories()) {
                        cat.setOrderInHome(i);
                        args[0] = cat.getId();
                        getContentResolver().update(CategoryColumns.CONTENT_URI, cat.toContentValues().values(), CategoryColumns._ID + " = ? ", new String[]{args[0] + ""});
                        i++;
                    }
                    return null;
                }
            }.execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        if (super.mDrawerLayout.isDrawerOpen(mDrawerList)) {
//            super.onItemClick(parent, view, position, id);
//        } else {
//            Category cat = this.adapter.getItem(position);
//            cat.toggleFrontpage();
//            updateCategory(cat);
//            this.adapter.notifyDataSetChanged();
//        }
//    }

    public void updateCategory(Category cat) {
        CategorySelection selection = new CategorySelection();
        selection.categoryId(cat.getCategory_id());
        cat.toContentValues().update(getContentResolver(), selection);
    }

    @Override
    public void changeColor(int position, int color) {
        Category category = adapter.getItem(position);
        category.setColor(color);
        updateCategory(category);
        this.adapter.notifyDataSetChanged();
//        Utils.updateCache(this, category.getCategory_id(), color);
    }


    public void reload() {
        super.restartLoader();
    }
}
