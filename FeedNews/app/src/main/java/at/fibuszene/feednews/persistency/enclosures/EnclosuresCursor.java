package at.fibuszene.feednews.persistency.enclosures;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code enclosures} table.
 */
public class EnclosuresCursor extends AbstractCursor {
    public EnclosuresCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code article_id} value.
     * Can be {@code null}.
     */
    public Long getArticleId() {
        return getLongOrNull(EnclosuresColumns.ARTICLE_ID);
    }

    /**
     * Get the {@code media_type} value.
     * Can be {@code null}.
     */
    public String getMediaType() {
        Integer index = getCachedColumnIndexOrThrow(EnclosuresColumns.MEDIA_TYPE);
        return getString(index);
    }

    /**
     * Get the {@code uri} value.
     * Can be {@code null}.
     */
    public String getUri() {
        Integer index = getCachedColumnIndexOrThrow(EnclosuresColumns.URI);
        return getString(index);
    }

    /**
     * Get the {@code url} value.
     * Can be {@code null}.
     */
    public String getUrl() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.URL);
        return getString(index);
    }

    /**
     * Get the {@code author} value.
     * Can be {@code null}.
     */
    public String getAuthor() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.AUTHOR);
        return getString(index);
    }

    /**
     * Get the {@code categoryid} value.
     * Can be {@code null}.
     * <p/>
     * WHAAAAAT ?
     */
    public Long getCategoryid() {
        return getLongOrNull(ArticleColumns.FK_CATEGORY_ID);
    }

    /**
     * Get the {@code title} value.
     * Can be {@code null}.
     */
    public String getTitle() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.TITLE);
        return getString(index);
    }

    /**
     * Get the {@code summary} value.
     * Can be {@code null}.
     */
    public String getSummary() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.SUMMARY);
        return getString(index);
    }

    /**
     * Get the {@code publish_date} value.
     * Can be {@code null}.
     */
    public Long getPublishDate() {
        return getLongOrNull(ArticleColumns.PUBLISH_DATE);
    }

    /**
     * Get the {@code source} value.
     * Can be {@code null}.
     */
    public String getSource() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.SOURCE);
        return getString(index);
    }

    /**
     * Get the {@code read} value.
     * Can be {@code null}.
     */
    public Boolean getRead() {
        return getBoolean(ArticleColumns.READ);
    }

    /**
     * Get the {@code saved} value.
     * Can be {@code null}.
     */
    public Boolean getSaved() {
        return getBoolean(ArticleColumns.SAVED);
    }
}
