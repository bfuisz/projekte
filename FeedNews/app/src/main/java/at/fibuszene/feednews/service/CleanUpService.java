package at.fibuszene.feednews.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.utils.Utils;

public class CleanUpService extends Service {
    public static final String CLEAN_UP = "at.fibuszene.feednews.clean_up";

    public CleanUpService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(cleanUpDB()).start();
        return START_STICKY;
    }

    public Runnable cleanUpDB() {
        return new Runnable() {
            @Override
            public void run() {
                //clean up articles older than ...
                //but keep saved articles even if they are older
                // a seperate field download date would be nicer instead of using the publish date !
                long twoDaysAgo = Utils.timestamp(2);
                //fk constraints fail
                //DELETE FROM article WHERE publish_date <= 1418902303192 AND saved = 0
                int debugDelete = getContentResolver().delete(ArticleColumns.CONTENT_URI, ArticleColumns.PUBLISH_DATE + " <= " + twoDaysAgo + " AND " + ArticleColumns.SAVED + " = 0", null);
                stopSelf();
            }
        };
    }

}
