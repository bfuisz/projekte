package at.fibuszene.feednews.persistency.metadata;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code metadata} table.
 */
public class MetadataSelection extends AbstractSelection<MetadataSelection> {
    @Override
    public Uri uri() {
        return MetadataColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code MetadataCursor} object, which is positioned before the first entry, or null.
     */
    public MetadataCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new MetadataCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public MetadataCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public MetadataCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public MetadataSelection id(long... value) {
        addEquals(MetadataColumns._ID, toObjectArray(value));
        return this;
    }


    public MetadataSelection colTableName(String... value) {
        addEquals(MetadataColumns.COL_TABLE_NAME, value);
        return this;
    }

    public MetadataSelection colTableNameNot(String... value) {
        addNotEquals(MetadataColumns.COL_TABLE_NAME, value);
        return this;
    }

    public MetadataSelection colTableNameLike(String... value) {
        addLike(MetadataColumns.COL_TABLE_NAME, value);
        return this;
    }

    public MetadataSelection colCategoryId(Long... value) {
        addEquals(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection colCategoryIdNot(Long... value) {
        addNotEquals(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection colCategoryIdGt(long value) {
        addGreaterThan(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection colCategoryIdGtEq(long value) {
        addGreaterThanOrEquals(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection colCategoryIdLt(long value) {
        addLessThan(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection colCategoryIdLtEq(long value) {
        addLessThanOrEquals(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataSelection lastUpdated(Long... value) {
        addEquals(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastUpdatedNot(Long... value) {
        addNotEquals(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastUpdatedGt(long value) {
        addGreaterThan(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastUpdatedGtEq(long value) {
        addGreaterThanOrEquals(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastUpdatedLt(long value) {
        addLessThan(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastUpdatedLtEq(long value) {
        addLessThanOrEquals(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataSelection lastCleared(Long... value) {
        addEquals(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection lastClearedNot(Long... value) {
        addNotEquals(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection lastClearedGt(long value) {
        addGreaterThan(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection lastClearedGtEq(long value) {
        addGreaterThanOrEquals(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection lastClearedLt(long value) {
        addLessThan(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection lastClearedLtEq(long value) {
        addLessThanOrEquals(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataSelection contentCount(Integer... value) {
        addEquals(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataSelection contentCountNot(Integer... value) {
        addNotEquals(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataSelection contentCountGt(int value) {
        addGreaterThan(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataSelection contentCountGtEq(int value) {
        addGreaterThanOrEquals(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataSelection contentCountLt(int value) {
        addLessThan(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataSelection contentCountLtEq(int value) {
        addLessThanOrEquals(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }
}
