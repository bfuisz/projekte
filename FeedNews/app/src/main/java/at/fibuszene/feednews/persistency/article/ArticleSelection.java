package at.fibuszene.feednews.persistency.article;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code article} table.
 */
public class ArticleSelection extends AbstractSelection<ArticleSelection> {
    @Override
    public Uri uri() {
        return ArticleColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection      A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder       How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *                        order, which may be unordered.
     * @return A {@code ArticleCursor} object, which is positioned before the first entry, or null.
     */
    public ArticleCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new ArticleCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public ArticleCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public ArticleCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }

    public ArticleSelection id(long... value) {
        addEquals(ArticleColumns.TABLE_NAME + "." + ArticleColumns._ID, toObjectArray(value));
        return this;
    }


    public ArticleSelection url(String... value) {
        addEquals(ArticleColumns.URL, value);
        return this;
    }

    public ArticleSelection urlNot(String... value) {
        addNotEquals(ArticleColumns.URL, value);
        return this;
    }

    public ArticleSelection urlLike(String... value) {
        addLike(ArticleColumns.URL, value);
        return this;
    }

    public ArticleSelection author(String... value) {
        addEquals(ArticleColumns.AUTHOR, value);
        return this;
    }

    public ArticleSelection authorNot(String... value) {
        addNotEquals(ArticleColumns.AUTHOR, value);
        return this;
    }

    public ArticleSelection authorLike(String... value) {
        addLike(ArticleColumns.AUTHOR, value);
        return this;
    }

    public ArticleSelection fkCategoryId(Long... value) {
        addEquals(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection fkCategoryIdNot(Long... value) {
        addNotEquals(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection fkCategoryIdGt(long value) {
        addGreaterThan(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection fkCategoryIdGtEq(long value) {
        addGreaterThanOrEquals(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection fkCategoryIdLt(long value) {
        addLessThan(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection fkCategoryIdLtEq(long value) {
        addLessThanOrEquals(ArticleColumns.FK_CATEGORY_ID, value);
        return this;
    }

    public ArticleSelection title(String... value) {
        addEquals(ArticleColumns.TITLE, value);
        return this;
    }

    public ArticleSelection titleNot(String... value) {
        addNotEquals(ArticleColumns.TITLE, value);
        return this;
    }

    public ArticleSelection titleLike(String... value) {
        addLike(ArticleColumns.TITLE, value);
        return this;
    }

    public ArticleSelection summary(String... value) {
        addEquals(ArticleColumns.SUMMARY, value);
        return this;
    }

    public ArticleSelection summaryNot(String... value) {
        addNotEquals(ArticleColumns.SUMMARY, value);
        return this;
    }

    public ArticleSelection summaryLike(String... value) {
        addLike(ArticleColumns.SUMMARY, value);
        return this;
    }

    public ArticleSelection publishDate(Long... value) {
        addEquals(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection publishDateNot(Long... value) {
        addNotEquals(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection publishDateGt(long value) {
        addGreaterThan(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection publishDateGtEq(long value) {
        addGreaterThanOrEquals(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection publishDateLt(long value) {
        addLessThan(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection publishDateLtEq(long value) {
        addLessThanOrEquals(ArticleColumns.PUBLISH_DATE, value);
        return this;
    }

    public ArticleSelection source(String... value) {
        addEquals(ArticleColumns.SOURCE, value);
        return this;
    }

    public ArticleSelection sourceNot(String... value) {
        addNotEquals(ArticleColumns.SOURCE, value);
        return this;
    }

    public ArticleSelection sourceLike(String... value) {
        addLike(ArticleColumns.SOURCE, value);
        return this;
    }

    public ArticleSelection read(Boolean value) {
        addEquals(ArticleColumns.READ, toObjectArray(value));
        return this;
    }

    public ArticleSelection saved(Boolean value) {
        addEquals(ArticleColumns.SAVED, toObjectArray(value));
        return this;
    }

    public ArticleSelection enclosureId(Long... value) {
        addEquals(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleSelection enclosureIdNot(Long... value) {
        addNotEquals(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleSelection enclosureIdGt(long value) {
        addGreaterThan(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleSelection enclosureIdGtEq(long value) {
        addGreaterThanOrEquals(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleSelection enclosureIdLt(long value) {
        addLessThan(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }

    public ArticleSelection enclosureIdLtEq(long value) {
        addLessThanOrEquals(ArticleColumns.ENCLOSURE_ID, value);
        return this;
    }
}
