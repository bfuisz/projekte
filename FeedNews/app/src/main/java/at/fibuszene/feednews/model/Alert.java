package at.fibuszene.feednews.model;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.feednews.persistency.alert.AlertContentValues;
import at.fibuszene.feednews.persistency.alert.AlertCursor;

/**
 * Created by benedikt.
 */
public class Alert {
    private long _id;
    private String keyword;
    private long category_id;
    private long seen;
    private long count;
    private List<NewAlert> newAlerts;

    public Alert() {
        newAlerts = new ArrayList<NewAlert>();
    }

    public static Alert alertOnlyfromCursor(AlertCursor cursor) {
        Alert alert = new Alert();
        try {
            alert._id = cursor.getId();
        } catch (NullPointerException npe) {
            //why indeed
        }

        try {
            alert.category_id = cursor.getCategoryId();
        } catch (NullPointerException npe) {
            //why indeed
        }

        alert.count = cursor.getCount();
        alert.keyword = cursor.getKeyword();

        try {
            alert.seen = cursor.getSeen();
        } catch (NullPointerException npe) {
            //why indeed
        }

        return alert;
    }

    public static Alert fromCursor(AlertCursor cursor) {
        Alert alert = new Alert();
        try {
            alert._id = cursor.getId();
        } catch (NullPointerException npe) {
            //why indeed
        }

        try {
            alert.category_id = cursor.getCategoryId();
        } catch (NullPointerException npe) {
            //why indeed
        }

        alert.count = cursor.getCount();
        alert.keyword = cursor.getKeyword();

        try {
            alert.seen = cursor.getSeen();
        } catch (NullPointerException npe) {
            //why indeed
        }

//        alert.newAlerts.add(NewAlert.fromCursor(new NewalertCursor(cursor)));

        return alert;
    }

    public AlertContentValues toValues() {
        AlertContentValues values = new AlertContentValues();
        values.putKeyword(this.keyword);
        values.putSeen(this.seen);
        values.putCount(this.count);
        //saved seperately by service
        values.putNewAlertId(null);
        return values;
    }


    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public long getSeen() {
        return seen;
    }

    public void setSeen(long seen) {
        this.seen = seen;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<NewAlert> getNewAlerts() {
        return newAlerts;
    }

    public void setNewAlerts(List<NewAlert> newAlerts) {
        this.newAlerts = newAlerts;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "_id=" + _id +
                ", keyword='" + keyword + '\'' +
                ", category_id=" + category_id +
                ", seen=" + seen +
                ", count=" + count +
                ", newAlerts=" + newAlerts +
                '}';
    }
}