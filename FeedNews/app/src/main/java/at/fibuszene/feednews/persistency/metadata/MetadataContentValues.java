package at.fibuszene.feednews.persistency.metadata;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code metadata} table.
 */
public class MetadataContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return MetadataColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, MetadataSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public MetadataContentValues putColTableName(String value) {
        mContentValues.put(MetadataColumns.COL_TABLE_NAME, value);
        return this;
    }

    public MetadataContentValues putColTableNameNull() {
        mContentValues.putNull(MetadataColumns.COL_TABLE_NAME);
        return this;
    }


    public MetadataContentValues putColCategoryId(Long value) {
        mContentValues.put(MetadataColumns.COL_CATEGORY_ID, value);
        return this;
    }

    public MetadataContentValues putColCategoryIdNull() {
        mContentValues.putNull(MetadataColumns.COL_CATEGORY_ID);
        return this;
    }


    public MetadataContentValues putLastUpdated(Long value) {
        mContentValues.put(MetadataColumns.LAST_UPDATED, value);
        return this;
    }

    public MetadataContentValues putLastUpdatedNull() {
        mContentValues.putNull(MetadataColumns.LAST_UPDATED);
        return this;
    }


    public MetadataContentValues putLastCleared(Long value) {
        mContentValues.put(MetadataColumns.LAST_CLEARED, value);
        return this;
    }

    public MetadataContentValues putLastClearedNull() {
        mContentValues.putNull(MetadataColumns.LAST_CLEARED);
        return this;
    }


    public MetadataContentValues putContentCount(Integer value) {
        mContentValues.put(MetadataColumns.CONTENT_COUNT, value);
        return this;
    }

    public MetadataContentValues putContentCountNull() {
        mContentValues.putNull(MetadataColumns.CONTENT_COUNT);
        return this;
    }

}
