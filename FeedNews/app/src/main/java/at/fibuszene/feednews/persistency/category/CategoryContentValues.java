package at.fibuszene.feednews.persistency.category;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code category} table.
 */
public class CategoryContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return CategoryColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where           The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, CategorySelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public CategoryContentValues putCategoryId(Long value) {
        mContentValues.put(CategoryColumns.CATEGORY_ID, value);
        return this;
    }

    public CategoryContentValues putCategoryIdNull() {
        mContentValues.putNull(CategoryColumns.CATEGORY_ID);
        return this;

    }

    public CategoryContentValues putShortcut(Boolean value) {
        mContentValues.put(CategoryColumns.SHORTCUT, value);
        return this;
    }

    public CategoryContentValues putShortcutNull() {
        mContentValues.putNull(CategoryColumns.SHORTCUT);
        return this;
    }


    public CategoryContentValues putDisplayCategoryName(String value) {
        mContentValues.put(CategoryColumns.DISPLAY_CATEGORY_NAME, value);
        return this;
    }

    public CategoryContentValues putDisplayCategoryNameNull() {
        mContentValues.putNull(CategoryColumns.DISPLAY_CATEGORY_NAME);
        return this;
    }


    public CategoryContentValues putEnglishCategoryName(String value) {
        mContentValues.put(CategoryColumns.ENGLISH_CATEGORY_NAME, value);
        return this;
    }

    public CategoryContentValues putEnglishCategoryNameNull() {
        mContentValues.putNull(CategoryColumns.ENGLISH_CATEGORY_NAME);
        return this;
    }


    public CategoryContentValues putUrlCategoryName(String value) {
        mContentValues.put(CategoryColumns.URL_CATEGORY_NAME, value);
        return this;
    }

    public CategoryContentValues putUrlCategoryNameNull() {
        mContentValues.putNull(CategoryColumns.URL_CATEGORY_NAME);
        return this;
    }


    public CategoryContentValues putUserFavorite(Boolean value) {
        mContentValues.put(CategoryColumns.USER_FAVORITE, value);
        return this;
    }

    public CategoryContentValues putUserFavoriteNull() {
        mContentValues.putNull(CategoryColumns.USER_FAVORITE);
        return this;
    }


    public CategoryContentValues putOrderInHome(Integer value) {
        mContentValues.put(CategoryColumns.ORDER_IN_HOME, value);
        return this;
    }

    public CategoryContentValues putOrderInHomeNull() {
        mContentValues.putNull(CategoryColumns.ORDER_IN_HOME);
        return this;
    }


    public CategoryContentValues putColor(Integer value) {
        mContentValues.put(CategoryColumns.COLOR, value);
        return this;
    }

    public CategoryContentValues putColorNull() {
        mContentValues.putNull(CategoryColumns.COLOR);
        return this;
    }


    public CategoryContentValues putFrontpage(Boolean value) {
        mContentValues.put(CategoryColumns.FRONTPAGE, value);
        return this;
    }

    public CategoryContentValues putFrontpageNull() {
        mContentValues.putNull(CategoryColumns.FRONTPAGE);
        return this;
    }


    public CategoryContentValues putHidden(Boolean value) {
        mContentValues.put(CategoryColumns.HIDDEN, value);
        return this;
    }

    public CategoryContentValues putHiddenNull() {
        mContentValues.putNull(CategoryColumns.HIDDEN);
        return this;
    }

}
