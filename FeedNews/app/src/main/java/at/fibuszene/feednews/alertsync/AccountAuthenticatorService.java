package at.fibuszene.feednews.alertsync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AccountAuthenticatorService extends Service {
    private Authenticator auth;

    public AccountAuthenticatorService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.auth = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return auth.getIBinder();
    }
}
