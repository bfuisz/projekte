package at.fibuszene.feednews.fragments;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.activities.ArticleActivity;
import at.fibuszene.feednews.activities.HomeActivity;
import at.fibuszene.feednews.adapter.CategoryArticleAdapter;
import at.fibuszene.feednews.listener.EndlessScrollListener;
import at.fibuszene.feednews.loader.ArticleLoader;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.article.ArticleSelection;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.service.LoadService;
import at.fibuszene.feednews.utils.PrefUtils;

/**
 * Created by benedikt.
 */
public class SearchFragment extends Fragment implements LoaderManager.LoaderCallbacks<Articles>, AdapterView.OnItemClickListener {
    public static final String CATEGORY_ID_EXTRA = "at.fibuszene.feednews.category_id";
    public int ARTICLES_LOADER_ID = 101;
    private long categoryId;
    private ListView articleList;
    private CategoryArticleAdapter adapter;
    private ArticleSelection articleSelection;

    public static SearchFragment getInstance(long category_id) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putLong(CATEGORY_ID_EXTRA, category_id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.articleSelection = new ArticleSelection();

        if (args != null) {
            this.categoryId = args.getLong(CATEGORY_ID_EXTRA, 0);
            ARTICLES_LOADER_ID += categoryId;

            if (categoryId == 0) { //frontpage

                Cursor cursor = getActivity().getContentResolver().query(CategoryColumns.CONTENT_URI,
                        new String[]{CategoryColumns.CATEGORY_ID}, CategoryColumns.FRONTPAGE + " = 1", null, null);

                if (cursor.moveToFirst()) {
                    Long[] catIds = new Long[cursor.getCount()];
                    int i = 0;
                    while (!cursor.isAfterLast()) {
                        catIds[i] = cursor.getLong(0);
                        i++;
                        cursor.moveToNext();
                    }
                    articleSelection.fkCategoryId(catIds);
                } else {
                    articleSelection.fkCategoryId(-1L);//officially gone insane
                }


            } else if (categoryId > 1) {//1 reserved for "all" fkcategoryid == null => all
                articleSelection.fkCategoryId(categoryId);
            }
            loadArticles(false);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_view, null);
        setUpDrawerHeader(view);
        return view;
    }

    public void setUpDrawerHeader(View view) {
//        View header = View.inflate(this, R.layout.drawer_header, null);
        final EditText searchText = (EditText) view.findViewById(R.id.drawerSearchTextView);
        ImageButton submitSearch = (ImageButton) view.findViewById(R.id.submitSearchButton);

        submitSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoadService.class);
                intent.setAction(LoadService.SEARCH_ARTICLES);
                intent.putExtra(LoadService.SEARCH_ARTICLES_ARG1, searchText.getText().toString());
                getActivity().startService(intent);
                searchText.setHint(searchText.getText().toString());
                searchText.setText("");

                if (getActivity() instanceof HomeActivity) {
                    final HomeActivity tmp = (HomeActivity) getActivity();
                    PrefUtils.lastPage(getActivity(), 2);
                    tmp.getLoaderManager().restartLoader(ARTICLES_LOADER_ID, null, SearchFragment.this);
                    tmp.scrollTo(2);
                    tmp.loading();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tmp.doneLoading(false);
                        }
                    }, 4500);
                }
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.articleList = (ListView) view.findViewById(R.id.articleListView);
        ArticleCursor cursor = articleSelection.query(getActivity().getContentResolver());
        this.adapter = new CategoryArticleAdapter(getActivity(), Articles.fromCursor(cursor));
        this.articleList.setAdapter(adapter);
        this.articleList.setOnItemClickListener(this);
        articleList.setOnScrollListener(new EndlessScrollListener(getActivity()));
        getActivity().getLoaderManager().restartLoader(ARTICLES_LOADER_ID, null, this);
//        setUpSwipeRefresh(view);
    }

    public void loadArticles(boolean userInit) {
        Intent intent = new Intent(getActivity(), LoadService.class);
        intent.setAction(LoadService.LOAD_ARTICLES);
        intent.putExtra(LoadService.LOAD_ARTICLES_ARG1, this.categoryId);
        intent.putExtra(LoadService.LOAD_ARTICLES_ARG2, userInit);
        getActivity().startService(intent);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Article item = this.adapter.getItem(position);
        item.setRead(true);

        ContentValues values = new ContentValues();
        values.put(ArticleColumns.READ, true);
        getActivity().getContentResolver().update(ArticleColumns.CONTENT_URI, values, ArticleColumns._ID + " = " + item.get_id(), null);

        Intent intent = new Intent(getActivity(), ArticleActivity.class);
        intent.putExtra(ArticleActivity.ARTICLE_URL_EXTRA, item.getUrl());
        startActivity(intent);
    }

    @Override
    public Loader<Articles> onCreateLoader(int id, Bundle args) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).loading();
        }
        return new ArticleLoader(getActivity(), articleSelection);
    }

    @Override
    public void onLoadFinished(Loader<Articles> loader, Articles data) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).doneLoading(false);
        }
        this.adapter.setArticles(data);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Articles> loader) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).doneLoading(false);
        }
        this.adapter.setArticles(null);
    }

}
