package at.fibuszene.feednews.interfaces;

import java.util.List;

import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.model.Subcategory;
import at.fibuszene.feednews.model.lists.Categories;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by benedikt.
 */
public interface CategoryInterface {

    @GET("/v1/categories.json")
    public void listCategories(Callback<List<Category>> response);

    @GET("/v1/categories.json?culture_code={culture}&order={ord}")
    public void listCategories(@Path("culture") String cultureCode, @Path("order") String order, Callback<Categories> response);

    @GET("/v1/subcategories.json")
    public void listSubcategories(Callback<List<Subcategory>> response);

    @GET("/v1/subcategories.json?culture_code={culture}&order={ord}")
    public void listSubcategories(@Path("culture") String cultureCode, @Path("order") String order, Callback<List<Subcategory>> response);


    @GET("/v1/categories/{category_id}/subcategories.json?culture_code={culture}&order={ord}")
    public void listSubcategoriesFor(@Path("category_id") long categoryId, @Path("culture") String cultureCode, @Path("order") String order, Callback<List<Subcategory>> response);

}
