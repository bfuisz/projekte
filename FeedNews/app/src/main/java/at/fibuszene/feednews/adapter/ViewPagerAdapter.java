package at.fibuszene.feednews.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;

import at.fibuszene.feednews.fragments.CategoryFragment;
import at.fibuszene.feednews.fragments.SearchFragment;
import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.model.lists.Categories;

/**
 * Created by benedikt.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private Categories categories;

    public ViewPagerAdapter(FragmentActivity context, Categories categories) {
        super(context.getSupportFragmentManager());
        this.context = context;
        if (categories != null && categories.size() > 0) {
            this.categories = categories;
        } else {
            initDummy();
        }
    }

    public int getPosOf(long categoryId) {
        int index = 0;
        for (Category cat : categories) {
            if (cat.getCategory_id() == categoryId) {
                return index;
            }
            index++;
        }
        return -1;
    }


    //add dummys because PagerSlidingTabStrip freaks out if size == 0
    public void initDummy() {
        this.categories = new Categories();
        Category dummy = new Category();
        dummy.setDisplay_category_name("Frontpage");
        dummy.setEnglish_category_name("Frontpage");

        this.categories.add(dummy);
        dummy.setDisplay_category_name("All");
        dummy.setEnglish_category_name("All");
        this.categories.add(dummy);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int i) {
        long catId = this.categories.get(i).getCategory_id();
        if (i == 2) {
            return SearchFragment.getInstance(catId);
        }
        return CategoryFragment.getInstance(catId);
    }

    @Override
    public int getCount() {
        return categories.size();
    }


    public Category getCategory(int position) {
        if (position >= 0 && position < categories.size()) {
            return this.categories.get(position);
        } else {
            return this.categories.get(0);
        }
    }

    public int getCategoryColor(int position) {
        try {
            return getCategory(position).getColor();
        } catch (NullPointerException npe) {
            return android.R.color.darker_gray;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (categories.size() > 0) {
            return categories.get(position).getEnglish_category_name();
        } else {
            return "Loading";
        }
    }


    public void setCategories(Categories list) {
        if (list != null && list.size() > 0) {
            this.categories = list;
        } else {
            initDummy();
        }
        notifyDataSetChanged();
    }
}
