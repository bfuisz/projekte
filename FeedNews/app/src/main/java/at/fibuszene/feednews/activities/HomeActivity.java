package at.fibuszene.feednews.activities;

import android.app.AlarmManager;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.adapter.ViewPagerAdapter;
import at.fibuszene.feednews.alertsync.AlertSyncAdapter;
import at.fibuszene.feednews.alertsync.Authenticator;
import at.fibuszene.feednews.loader.CategoryLoader;
import at.fibuszene.feednews.model.lists.Categories;
import at.fibuszene.feednews.persistency.category.CategoryCursor;
import at.fibuszene.feednews.persistency.category.CategorySelection;
import at.fibuszene.feednews.service.CleanUpReceiver;
import at.fibuszene.feednews.service.ExceptionHandler;
import at.fibuszene.feednews.service.LoadService;
import at.fibuszene.feednews.utils.Constants;
import at.fibuszene.feednews.utils.PrefUtils;


public class HomeActivity extends AbstractActivity implements LoaderManager.LoaderCallbacks<Categories>, ViewPager.OnPageChangeListener {
    public static Handler messageHandler;

    public static final String SCROLL_TO_CATEGORY = "at.fibuszene.scroll_to";
    public static final int CATEGORY_LOADER_ID = 100;
    private ViewPager pager;
    private ViewPagerAdapter pagerAdapter;
    private PagerSlidingTabStrip tabs;
    private CategorySelection selection = new CategorySelection().hidden(false);
    private int neutralIndicator;
    private Animation fade_out, fade_in;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_home);
        fade_out = AnimationUtils.makeOutAnimation(this, false);
        fade_in = AnimationUtils.makeInAnimation(this, false);
        messageHandler = new ExceptionHandler(this);

        PrefUtils.firstStart(this, true);
        loadCategories();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        getLoaderManager().restartLoader(CATEGORY_LOADER_ID, null, this);
        setUpViewPager();
        this.pager.setCurrentItem(PrefUtils.getLastPage(this));
        startPeriodicCleanUp();

        if (savedInstanceState != null && savedInstanceState.containsKey(SCROLL_TO_CATEGORY)) {
            scrollTo(savedInstanceState.getInt(SCROLL_TO_CATEGORY));
        }
    }

    public void displayError(int categoryError, int error) {
        String message = getResources().getString(R.string.network_error);
        switch (categoryError) {
            case Constants.CATEGORY_ERROR_CODE:
                message = String.format(message, "categories");
                break;
            case Constants.ARTICLE_ERROR_CODE:
                message = String.format(message, "articles");
                break;
        }
        Log.d("Error Handler", message);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageSelected(int i) {
        int color = this.pagerAdapter.getCategoryColor(i);
        if (color != 0) {
            this.tabs.setIndicatorColor(color);
        } else {
            this.tabs.setIndicatorColor(neutralIndicator);
        }
    }

    public void scrollTo(long categoryId) {
        int indexOf = this.pagerAdapter.getPosOf(categoryId);
        if (indexOf >= 0) {
            this.pager.setCurrentItem(indexOf);
        }
        closeDrawer();
    }

    public void scrollTo(int page) {
        this.pager.setCurrentItem(page);
        closeDrawer();
    }

    public void loadCategories() {
        Intent intent = new Intent(this, LoadService.class);
        intent.setAction(LoadService.LOAD_CATEGORIES);
        intent.putExtra(LoadService.EXCEPTION_HANDLER, new Messenger(messageHandler));
        startService(intent);
    }

    @Override
    public Loader<Categories> onCreateLoader(int id, Bundle args) {
        return new CategoryLoader(this, selection);
    }

    @Override
    public void onLoadFinished(Loader<Categories> loader, Categories data) {
        this.pagerAdapter.setCategories(data);
        this.tabs.notifyDataSetChanged();
    }


    public void loading() {
        animateUpdate();
    }

    public void doneLoading(boolean error) {
        cancelUpdate(error);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        PrefUtils.lastPage(this, this.pager.getCurrentItem());
    }

    @Override
    public void onLoaderReset(Loader<Categories> loader) {
        this.pagerAdapter.setCategories(null);
    }


    public void setUpViewPager() {
        pager = (ViewPager) findViewById(R.id.homeViewPager);
        pager.setOffscreenPageLimit(4);
        CategoryCursor cursor = selection.query(getContentResolver());
        pagerAdapter = new ViewPagerAdapter(this, Categories.fromCursor(cursor));
        pager.setAdapter(pagerAdapter);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        this.neutralIndicator = this.tabs.getIndicatorColor();
        tabs.setViewPager(pager);
        this.tabs.setOnPageChangeListener(this);
    }


    public void startPeriodicCleanUp() {
        Intent intent = new Intent(getApplicationContext(), CleanUpReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, CleanUpReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_DAY * 2, pIntent);

        //uncomment to see CleanUpService at work remove all articels older than 2 days
        // sendBroadcast(intent);
        //automatic sync
        ContentResolver.setIsSyncable(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, 1);
        ContentResolver.setSyncAutomatically(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, true);

        //would have been nice to know that a polltime of 0 breaks your phone :D
        if (PrefUtils.alertSyncFrequency(this) > 0) {
            ContentResolver.addPeriodicSync(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, Bundle.EMPTY, PrefUtils.alertSyncFrequency(this) * 60); //the fuck... why use seconds ????
            ContentResolver.requestSync(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, Bundle.EMPTY);
        } else {
            ContentResolver.removePeriodicSync(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, Bundle.EMPTY);
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }

    public void animateTabStrip(boolean fade_out) {
//        if (fade_out) {
//            tabs.startAnimation(this.fade_out);
//            tabs.setVisibility(View.INVISIBLE);
//        } else {
//            tabs.startAnimation(fade_in);
//            tabs.setVisibility(View.VISIBLE);
//        }
    }
}
