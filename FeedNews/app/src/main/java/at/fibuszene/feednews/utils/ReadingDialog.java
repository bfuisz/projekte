package at.fibuszene.feednews.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import at.fibuszene.feednews.R;

/**
 * Created by benedikt.
 */
public class ReadingDialog extends android.support.v4.app.DialogFragment {
    public static final String TEXT_ARG = "at.fibuszene.feednews.clipboard_data";
    private ReadingTask task;
    private String[] text;
    private TextView wpmCount;
    private TextView txtView;
    private ImageButton playButton;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        try {
            this.text = getArguments().getString(TEXT_ARG).split(" ");
        } catch (NullPointerException npe) {
            Toast.makeText(getActivity(), R.string.no_text, Toast.LENGTH_SHORT).show();
            this.dismiss();
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.reading_dialog, null);
        txtView = (TextView) view.findViewById(R.id.textView);
        wpmCount = (TextView) view.findViewById(R.id.wpmCount);
        task = new ReadingTask(txtView, text);

        setWPMCount(task.getWpm());

        ImageButton button = (ImageButton) view.findViewById(R.id.fasterButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task.faster();
                setWPMCount(task.getWpm());

            }
        });

        playButton = (ImageButton) view.findViewById(R.id.playPauseButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageButton imgB = (ImageButton) v;
                if (task.pause()) {
                    imgB.setImageResource(android.R.drawable.ic_media_play);
                } else {
                    imgB.setImageResource(android.R.drawable.ic_media_pause);
                }
            }
        });

        button = (ImageButton) view.findViewById(R.id.slowerButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task.slower();
                setWPMCount(task.getWpm());
            }
        });

        button = (ImageButton) view.findViewById(R.id.resetButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int prevWPM = task.getWpm();
                resetTask(prevWPM);
                resetView(task);
            }
        });

        builder.setView(view)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        task.execute();
        return builder.create();
    }

    public void resetTask(int prevWPM) {
        if (task != null) {
            task.cancel(true);
            task = null;
        }
        task = new ReadingTask(txtView, text);
        prevWPM = (prevWPM >= 50) ? prevWPM : 0;
        task.setWpm(prevWPM);
        task.execute();
    }

    public void resetView(ReadingTask task) {
        wpmCount.setText(task.getWpm() + " wpm");
        txtView.setText(text[0]);
        playButton.setImageResource(android.R.drawable.ic_media_play);
    }

    public void setWPMCount(int wpm) {
        wpmCount.setText(wpm + " wpm");
    }

    public static class ReadingTask extends AsyncTask<Void, String, String> {
        private int wpm = 200;
        private int speed = 1000;
        private TextView txtView;
        private final Object lock = new Object();
        private boolean pause, running = true;
        private String[] text;

        public ReadingTask(TextView txtView, String[] text) {
            this.txtView = txtView;
            this.text = text;
            this.speed = Utils.speedForWPM(wpm);
            this.pause = true;
        }

        public void faster() {
            this.wpm += 50;
            updateSpeed();
        }

        public int getWpm() {
            return this.wpm;
        }

        public void setWpm(int wpm) {
            this.wpm = wpm;
            updateSpeed();

        }

        public void slower() {
            this.wpm -= 50;
            if (this.wpm < 50) {
                this.wpm = 50;
            }
            updateSpeed();
        }

        public void updateSpeed() {
            this.speed = Utils.speedForWPM(wpm);
        }

        public boolean isPaused() {
            return this.pause;
        }

        public boolean pause() {
            synchronized (lock) {
                this.pause = !this.pause;
                lock.notifyAll();
            }
            return pause;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            txtView.setText(values[0]);
        }

        @Override
        protected String doInBackground(Void... params) {
            for (int i = 0; i < text.length && !isCancelled(); i++) {
                synchronized (lock) {
                    while (pause) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                publishProgress(text[i]);
                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
