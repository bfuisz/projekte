package at.fibuszene.feednews.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.nhaarman.listviewanimations.util.Swappable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.activities.CatergoriesSettings;
import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.model.lists.Categories;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.category.CategorySelection;
import at.fibuszene.feednews.utils.Comparables;

/**
 * Created by benedikt.
 */
public class ConfigureCategoriesAdapter extends BaseAdapter implements Swappable {
    private CatergoriesSettings activity;
    private Categories categories;
    private ColorChooserDialog dialog;
    private FragmentManager fm;
    private static final List<String> immutableCategories;

    static {
        immutableCategories = new ArrayList<String>();
        immutableCategories.add("Frontpage");
        immutableCategories.add("All");
        immutableCategories.add("Search");
    }

    public ConfigureCategoriesAdapter(CatergoriesSettings activity, Categories categories) {
        this.activity = activity;
        this.categories = categories;
        this.dialog = new ColorChooserDialog();
        this.fm = activity.getSupportFragmentManager();
    }

    @Override
    public int getCount() {
        return this.categories.size();
    }

    @Override
    public Category getItem(int position) {
        return this.categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getCategory_id();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void showDialog(int position, int color) {
        Bundle bundle = new Bundle();
        bundle.putInt(ColorChooserDialog.POSITION_ARG, position);
        bundle.putInt(ColorChooserDialog.INITIAL_COLOR, color);
        dialog.setArguments(bundle);
        dialog.show(fm, "colorChooser");
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Category cat = getItem(position);
        boolean immutable = getItemViewType(position) == 0;
//        boolean immutable = !immutableCategories.contains(cat.getEnglish_category_name());

        if (convertView == null) {
            if (immutable) {
                convertView = View.inflate(activity, R.layout.im_config_category_item, null);
            } else {
                convertView = View.inflate(activity, R.layout.config_category_item, null);
            }
        }


        setUpCategoryView(convertView, cat);
        setUpPicker(convertView, cat, position);

        if (!immutable) {
            setUpCheckboxes(convertView, cat);
        }
        return convertView;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                return 0;
            default:
                return 1;
        }
    }

    public void setUpCheckboxes(final View convertView, final Category cat) {
        final CheckedTextView frontpageToggle = (CheckedTextView) convertView.findViewById(R.id.frontpageCheckbox);

        frontpageToggle.setChecked(cat.isFrontpage());

        frontpageToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frontpageToggle.toggle();
                ContentValues values = new ContentValues();
                values.put(CategoryColumns.FRONTPAGE, frontpageToggle.isChecked());
                cat.setFrontpage(frontpageToggle.isChecked());
                activity.getContentResolver().update(CategoryColumns.CONTENT_URI, values, CategoryColumns.CATEGORY_ID + " = " + cat.getCategory_id(), null);
                notifyDataSetChanged();
            }
        });

        final CheckedTextView hideCategoryToggle = (CheckedTextView) convertView.findViewById(R.id.hideCategoryCheckbox);
        hideCategoryToggle.setChecked(cat.isHidden());

        hideCategoryToggle.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideCategoryToggle.toggle();
                        cat.setHidden(hideCategoryToggle.isChecked());
                        int res = activity.getContentResolver().update(CategoryColumns.CONTENT_URI, cat.toContentValues().values(), CategoryColumns.CATEGORY_ID + " = " + cat.getCategory_id(), null);
                        notifyDataSetChanged();
                    }
                }
        );


        final CheckedTextView shortCutToggle = (CheckedTextView) convertView.findViewById(R.id.shortcutCheckbox);
        shortCutToggle.setChecked(cat.isShortcut());

        shortCutToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shortCutToggle.toggle();
                ContentValues values = new ContentValues();
                values.put(CategoryColumns.SHORTCUT, shortCutToggle.isChecked());
                cat.setShortcut(shortCutToggle.isChecked());
                activity.getContentResolver().update(CategoryColumns.CONTENT_URI, values, CategoryColumns.CATEGORY_ID + " = " + cat.getCategory_id(), null);
                notifyDataSetChanged();
                activity.reload();
            }
        });

        if (cat.isHidden()) {
            shortCutToggle.setChecked(false);
            frontpageToggle.setChecked(false);

            frontpageToggle.setEnabled(false);
            shortCutToggle.setEnabled(false);
        } else {
            frontpageToggle.setEnabled(true);
            shortCutToggle.setEnabled(true);
        }

        disabledView(convertView.findViewById(R.id.optionsTableLayout), cat.isHidden());
    }


    public void setUpCategoryView(View convertView, Category cat) {
        TextView txtView = (TextView) convertView.findViewById(R.id.categoryItemTextView);
        String text = cat.getEnglish_category_name();
        if (text.length() > 12) {
            text = text.replace(" ", "\r\n");
        }
        txtView.setText(text);
    }

    public ImageView setUpPicker(View convertView, final Category cat, final int position) {
        ImageView pickerView = (ImageView) convertView.findViewById(R.id.pickerDialog);
        pickerView.setBackgroundColor(cat.getColor());

        pickerView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(position, cat.getColor());
                    }
                }
        );
        return pickerView;
    }

    public void disabledView(View layout, boolean disabled) {
        GridLayout linLayout = (GridLayout) layout;
        float alpha = 1;
        if (disabled) {
            alpha = 0.5f;
        }
        View tmp;
        for (int i = 0; i < linLayout.getChildCount(); i++) {
            tmp = linLayout.getChildAt(i);

            if (tmp.getId() != R.id.hideCategoryCheckbox) {
                //toggle always enabled
                tmp.setAlpha(alpha);
                tmp.setEnabled(!disabled);
            }
        }
    }

    public Categories getCategories() {
        return this.categories;
    }

    public void sortAlpha() {
        Collections.sort(this.categories, new Comparables.AlphaComp());
    }

    @Override
    public void swapItems(int i, int i2) {
        if (i > 2 && i2 > 2) {
            if (i2 > i) {
                int tmp = i;
                i = i2;
                i2 = tmp;
            }

            Category one = this.categories.remove(i);
            Category two = this.categories.remove(i2);

            this.categories.add(i2, one);
            this.categories.add(i, two);

            if (one != null && two != null) {
                one.setOrderInHome(i2);
                two.setOrderInHome(i);

                CategorySelection selection = new CategorySelection();

                selection.id(one.getId());
                one.toContentValues().update(this.activity.getContentResolver(), selection);
                //no clear method or like that
                //if not for a new object the selection would look something like this where _id = ?_id=? :D
                //TODO: implement clear method when cp no longer changes
                selection = new CategorySelection();
                selection.id(two.getId());
                two.toContentValues().update(this.activity.getContentResolver(), selection);
                notifyDataSetChanged();
            }
        }
    }

    public static class ColorChooserDialog extends DialogFragment {
        public static final String POSITION_ARG = "at.fibuszene.feednews.positionarg";
        public static final String INITIAL_COLOR = "at.fibuszene.feednews.initial_color";
        private int position;
        private int color;

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            this.position = getArguments().getInt(POSITION_ARG);
            int initColor = getArguments().getInt(INITIAL_COLOR);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.color_chooser_dialog, null);
            ColorPicker picker = buildColorPicker(view);
            picker.setColor(initColor);
            //(ColorPicker) view.findViewById(R.id.picker);

            picker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
                @Override
                public void onColorChanged(int i) {
                    color = i;
                }
            });

            builder.setView(view)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                ((CatergoriesSettings) getActivity()).changeColor(position, color);
                            } catch (ClassCastException cce) {
                                cce.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });

            return builder.create();
        }

        public ColorPicker buildColorPicker(View view) {
            ColorPicker picker = (ColorPicker) view.findViewById(R.id.picker);
            SVBar svBar = (SVBar) view.findViewById(R.id.svbar);
            OpacityBar opacityBar = (OpacityBar) view.findViewById(R.id.opacitybar);
            SaturationBar saturationBar = (SaturationBar) view.findViewById(R.id.saturationbar);
            ValueBar valueBar = (ValueBar) view.findViewById(R.id.valuebar);

            picker.addSVBar(svBar);
            picker.addOpacityBar(opacityBar);
            picker.addSaturationBar(saturationBar);
            picker.addValueBar(valueBar);

            picker.getColor();

            picker.setOldCenterColor(picker.getColor());
            picker.setShowOldCenterColor(false);

            //adding onChangeListeners to bars
            opacityBar.setOnOpacityChangedListener(new OpacityBar.OnOpacityChangedListener() {
                @Override
                public void onOpacityChanged(int i) {

                }
            });

            valueBar.setOnValueChangedListener(new ValueBar.OnValueChangedListener() {
                @Override
                public void onValueChanged(int i) {

                }
            });

            saturationBar.setOnSaturationChangedListener(new SaturationBar.OnSaturationChangedListener() {
                @Override
                public void onSaturationChanged(int i) {

                }
            });

            return picker;
        }
    }


}
