package at.fibuszene.feednews.persistency.category;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code category} table.
 */
public class CategoryColumns implements BaseColumns {
    public static final String TABLE_NAME = "category";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String CATEGORY_ID = "category_id";
    public static final String DISPLAY_CATEGORY_NAME = "display_category_name";
    public static final String ENGLISH_CATEGORY_NAME = "english_category_name";
    public static final String URL_CATEGORY_NAME = "url_category_name";
    public static final String USER_FAVORITE = "user_favorite";
    public static final String ORDER_IN_HOME = "order_in_home";
    public static final String COLOR = "color";
    public static final String FRONTPAGE = "frontpage";
    public static final String HIDDEN = "hidden";
    public static final String SHORTCUT = "shortcut";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." + ORDER_IN_HOME;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[]{
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + CATEGORY_ID,
            TABLE_NAME + "." + DISPLAY_CATEGORY_NAME,
            TABLE_NAME + "." + ENGLISH_CATEGORY_NAME,
            TABLE_NAME + "." + URL_CATEGORY_NAME,
            TABLE_NAME + "." + USER_FAVORITE,
            TABLE_NAME + "." + ORDER_IN_HOME,
            TABLE_NAME + "." + COLOR,
            TABLE_NAME + "." + FRONTPAGE,
            TABLE_NAME + "." + HIDDEN,
            TABLE_NAME + "." + SHORTCUT
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();

    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(CATEGORY_ID);
        ALL_COLUMNS.add(DISPLAY_CATEGORY_NAME);
        ALL_COLUMNS.add(ENGLISH_CATEGORY_NAME);
        ALL_COLUMNS.add(URL_CATEGORY_NAME);
        ALL_COLUMNS.add(USER_FAVORITE);
        ALL_COLUMNS.add(ORDER_IN_HOME);
        ALL_COLUMNS.add(COLOR);
        ALL_COLUMNS.add(FRONTPAGE);
        ALL_COLUMNS.add(HIDDEN);
        ALL_COLUMNS.add(SHORTCUT);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
