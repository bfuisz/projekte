package at.fibuszene.feednews.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.webkit.WebView;

/**
 * Created by benedikt.
 */
public class CustomWebView extends WebView {
    private ContextMenu contextMenu;

    public CustomWebView(Context context) {
        super(context);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }


}
