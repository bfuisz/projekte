package at.fibuszene.feednews.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import at.fibuszene.feednews.model.lists.Alerts;
import at.fibuszene.feednews.persistency.DatabaseHelper;
import at.fibuszene.feednews.persistency.alert.AlertColumns;
import at.fibuszene.feednews.persistency.alert.AlertCursor;
import at.fibuszene.feednews.persistency.alert.AlertSelection;

/**
 * Created by benedikt.
 */
public class AlertLoader extends AsyncTaskLoader<Alerts> {
    private Alerts alerts;
    private AlertCursor cursor;
    private AlertSelection selection;
    private ForceLoadContentObserver contentObserver = new ForceLoadContentObserver();


    public AlertLoader(Context context, AlertSelection selection) {
        super(context);
        this.selection = selection;
    }

    @Override
    public Alerts loadInBackground() {
        String rawQuery = "SELECT alert._id, keyword, alert.count, alert.seen, newalert._id, article_id ,title, url, publish_date FROM alert LEFT OUTER JOIN newalert ON alert._id=newalert.alert_id LEFT OUTER JOIN article ON newalert.article_id=article._id order by alert._id";
        Cursor cursor = DatabaseHelper.getInstance(getContext()).getReadableDatabase().rawQuery(rawQuery, null);
        DatabaseUtils.dumpCursor(cursor);
        this.alerts = Alerts.fromCursor(cursor);
        cursor.setNotificationUri(getContext().getContentResolver(), AlertColumns.CONTENT_URI);
        getContext().getContentResolver().registerContentObserver(AlertColumns.CONTENT_URI, true, contentObserver);
        return alerts;
    }

    public AlertSelection getSelection() {
        return selection;
    }

    public void setSelection(AlertSelection selection) {
        this.selection = selection;
        if (selection != null) {
            loadInBackground();
        }
    }


    @Override
    public void deliverResult(Alerts alerts) {
        if (isReset()) {
            if (alerts != null) {
                onReleaseResources(alerts);
            }
        }

        Alerts oldAlerts = alerts;
        this.alerts = alerts;

        if (isStarted()) {
            super.deliverResult(alerts);
        }
        if (oldAlerts != null) {
            onReleaseResources(oldAlerts);
        }
    }

    @Override
    protected void onStartLoading() {
        if (alerts != null) {
            deliverResult(alerts);
        }
        if (takeContentChanged() || alerts == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(Alerts articles) {
        super.onCanceled(articles);
        onReleaseResources(articles);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (alerts != null) {
            onReleaseResources(alerts);
            alerts = null;
        }
        getContext().getContentResolver().unregisterContentObserver(contentObserver);
    }

    protected void onReleaseResources(Alerts articles) {
//        alerts = null;
    }


}
