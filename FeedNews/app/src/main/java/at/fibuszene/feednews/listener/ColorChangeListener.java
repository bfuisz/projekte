package at.fibuszene.feednews.listener;

/**
 * Created by benedikt.
 */
public interface ColorChangeListener {
    public void changeColor(int position, int color);
}
