package at.fibuszene.feednews.persistency.newalert;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code newalert} table.
 */
public class NewalertSelection extends AbstractSelection<NewalertSelection> {
    @Override
    public Uri uri() {
        return NewalertColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code NewalertCursor} object, which is positioned before the first entry, or null.
     */
    public NewalertCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new NewalertCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public NewalertCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public NewalertCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public NewalertSelection id(long... value) {
        addEquals(NewalertColumns._ID, toObjectArray(value));
        return this;
    }


    public NewalertSelection alertId(Long... value) {
        addEquals(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection alertIdNot(Long... value) {
        addNotEquals(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection alertIdGt(long value) {
        addGreaterThan(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection alertIdGtEq(long value) {
        addGreaterThanOrEquals(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection alertIdLt(long value) {
        addLessThan(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection alertIdLtEq(long value) {
        addLessThanOrEquals(NewalertColumns.ALERT_ID, value);
        return this;
    }

    public NewalertSelection articleId(Long... value) {
        addEquals(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection articleIdNot(Long... value) {
        addNotEquals(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection articleIdGt(long value) {
        addGreaterThan(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection articleIdGtEq(long value) {
        addGreaterThanOrEquals(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection articleIdLt(long value) {
        addLessThan(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection articleIdLtEq(long value) {
        addLessThanOrEquals(NewalertColumns.ARTICLE_ID, value);
        return this;
    }

    public NewalertSelection seen(Boolean value) {
        addEquals(NewalertColumns.SEEN, toObjectArray(value));
        return this;
    }
}
