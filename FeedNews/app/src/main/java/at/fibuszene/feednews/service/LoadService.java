package at.fibuszene.feednews.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import at.fibuszene.feednews.interfaces.ArticlesInterface;
import at.fibuszene.feednews.interfaces.CategoryInterface;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.Category;
import at.fibuszene.feednews.model.Enclosure;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.utils.Constants;
import at.fibuszene.feednews.utils.DBUtils;
import at.fibuszene.feednews.utils.PrefUtils;
import at.fibuszene.feednews.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoadService extends Service {
    private RestAdapter adapter;
    private ExecutorService executor;

    public static final String EXCEPTION_HANDLER = "at.fibuszene.feednews.exception_handler";
    private Messenger messageHandler;

    public static final String LOAD_CATEGORIES = "at.fibuszene.feednews.load_categories";
    public static final String LOAD_ARTICLES = "at.fibuszene.feednews.load_articles";
    public static final String SEARCH_ARTICLES = "at.fibuszene.feednews.search_articles";

    public static final String LOAD_ARTICLES_ARG1 = "at.fibuszene.feednews.load_articles";
    public static final String LOAD_ARTICLES_ARG2 = "at.fibuszene.feednews.ignore_metadata";
    public static final String SEARCH_ARTICLES_ARG1 = "at.fibuszene.feednews.search_articles";


    public LoadService() {

        this.adapter = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).build();
        executor = Executors.newCachedThreadPool();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            try {
                this.messageHandler = (Messenger) intent.getExtras().get(EXCEPTION_HANDLER);
            } catch (NullPointerException npe) {
                //no message handler
                //for syncadapter no need to display anything
            }
            if (PrefUtils.allowUpdate(this)) { //wifi only setting
                if (action.equals(LOAD_CATEGORIES)) {
                    executor.execute(loadCategories());
                } else if (action.equals(LOAD_ARTICLES)) {
                    long categoryId = intent.getLongExtra(LOAD_ARTICLES_ARG1, 0);
                    boolean ignoreMetaData = intent.getBooleanExtra(LOAD_ARTICLES_ARG2, false);
                    executor.execute(loadArticles(categoryId, ignoreMetaData));
                } else if (action.equals(SEARCH_ARTICLES)) {
                    String query = intent.getStringExtra(SEARCH_ARTICLES_ARG1);
                    executor.execute(searchArticles(query));
                }
            }
        }
        return START_STICKY;
    }

    public Runnable loadCategories() {
        return new Runnable() {

            @Override
            public void run() {
                //update daily
                if (Utils.checkMetadata(LoadService.this, TimeUnit.DAYS, 1, CategoryColumns.TABLE_NAME)) {
                    firstStarSetup();

                    final CategoryInterface catApi = adapter.create(CategoryInterface.class);

                    catApi.listCategories(new Callback<List<Category>>() {
                        @Override
                        public void success(List<Category> categories, Response response) {
                            ContentValues[] values = new ContentValues[categories.size()];
                            Random rnd = new Random();
                            int i = 0, j = 2;

                            for (Category cat : categories) {
                                cat.setOrderInHome(j);
                                cat.setHidden(false);
                                cat.setColor(Color.argb(rnd.nextInt(150), rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
                                values[i] = cat.toContentValues().values();
                                i++;
                                j++;
                            }
                            getContentResolver().bulkInsert(CategoryColumns.CONTENT_URI, values);
                            getContentResolver().notifyChange(CategoryColumns.CONTENT_URI, null);
                            DBUtils.updateMetadata(LoadService.this, CategoryColumns.TABLE_NAME, System.currentTimeMillis(), 0, categories.size());
                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            sendError(Constants.CATEGORY_ERROR_CODE, error);
                        }
                    });
                }
            }
        };
    }

    public void sendError(int errorCategory, RetrofitError error) {
        if (messageHandler != null) {
            Message message = Message.obtain();
            message.arg1 = errorCategory;
            message.arg2 = 500;
            try {
                messageHandler.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public Runnable loadArticles(final long categoryID, final boolean ignoreMetaData) {
        return new Runnable() {
            @Override
            public void run() {
                //update hourly
                //ignoreMetaData => true update without looking at the last time articles hav been updated
                if (ignoreMetaData || Utils.checkMetadata(LoadService.this, TimeUnit.HOURS, 1, ArticleColumns.TABLE_NAME, categoryID)) {
                    adapter.setLogLevel(RestAdapter.LogLevel.BASIC);
                    final ArticlesInterface articlesAPI = adapter.create(ArticlesInterface.class);
                    StoreArticles articleStorage = new StoreArticles(categoryID);

                    if (categoryID > 0) {
                        articlesAPI.listArticles(categoryID, "date", 25, articleStorage);
                    } else {
                        articlesAPI.listArticles(25, "date", articleStorage);
                    }
                    DBUtils.updateMetadata(LoadService.this, ArticleColumns.TABLE_NAME, categoryID, System.currentTimeMillis(), 0, 0);
                }
            }
        };
    }


    public Runnable searchArticles(final String query) {
        return new Runnable() {
            @Override
            public void run() {
                final ArticlesInterface articlesAPI = adapter.create(ArticlesInterface.class);

                Cursor cur = getContentResolver().query(CategoryColumns.CONTENT_URI, new String[]{CategoryColumns.CATEGORY_ID}, CategoryColumns.URL_CATEGORY_NAME + " = 'my-search'", null, null);
                if (cur.moveToFirst()) {
                    long id = cur.getLong(0);
                    getContentResolver().delete(ArticleColumns.CONTENT_URI, ArticleColumns.FK_CATEGORY_ID + " = " + id, null);//clean up old search results
                    articlesAPI.searchAll(query, 25, new StoreArticles(id));
                }
            }
        };
    }

    public class StoreArticles implements Callback<Articles> {
        private long categoryID = 0;

        public StoreArticles(long categoryID) {
            this.categoryID = categoryID;
        }

        @Override
        public void success(Articles articles, Response response) {
            ContentValues[] values = new ContentValues[articles.getArticles().size()];

            int i = 0;
            for (Article article : articles.getArticles()) {
                article.setCategoryId(categoryID);

                //if an article has an enclosure insert it right away for the id
                //since one enclosure ought a be enough get the first
                if (article.getEnclosures() != null) {
                    Enclosure enc = article.getEnclosures().get(0);
                    Uri uri = enc.toContentValues().insert(getContentResolver());
                    if (uri != null) {
                        long enclosureId = Long.valueOf(uri.getLastPathSegment());
                        enc.setId(enclosureId);
                        article.getEnclosures().remove(0);
                        article.getEnclosures().add(0, enc);
                    }
                }

                values[i] = article.toContentValue().values();
                i++;
            }
            int res = getContentResolver().bulkInsert(ArticleColumns.CONTENT_URI, values);
            getContentResolver().notifyChange(ArticleColumns.CONTENT_URI, null);
        }

        @Override
        public void failure(final RetrofitError error) {
            sendError(Constants.ARTICLE_ERROR_CODE, error);
            if (error.getCause() instanceof SocketTimeoutException) {
                Log.d("Timeout Exception Error", "SocketTimeoutException");
            }
        }
    }

    public void firstStarSetup() {
        if (PrefUtils.firstStart(this)) {
            Random rnd = new Random();
            Category category = new Category();
            category.setCategory_id(0);
            category.setEnglish_category_name("Frontpage");
            category.setDisplay_category_name("Frontpage");
            category.setColor(Color.argb(rnd.nextInt(150), rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

            category.setOrderInHome(0);
            category.toContentValues().insert(getContentResolver());

            category = new Category();
            category.setCategory_id(1);
            category.setOrderInHome(1);
            category.setEnglish_category_name("All");
            category.setColor(Color.argb(rnd.nextInt(150), rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
            category.setDisplay_category_name("All");
            category.toContentValues().insert(getContentResolver());

            category = new Category();
            category.setEnglish_category_name("Search");
            category.setDisplay_category_name("Search");
            category.setCategory_id(5000);
            category.setColor(Color.argb(rnd.nextInt(150), rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
            category.setOrderInHome(2);
            category.setUrl_category_name("my-search");
            category.toContentValues().insert(getContentResolver());
            PrefUtils.firstStart(this, false);
        }
    }

}
