package at.fibuszene.feednews.persistency.metadata;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code metadata} table.
 */
public class MetadataCursor extends AbstractCursor {
    public MetadataCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code col_table_name} value.
     * Can be {@code null}.
     */
    public String getColTableName() {
        Integer index = getCachedColumnIndexOrThrow(MetadataColumns.COL_TABLE_NAME);
        return getString(index);
    }

    /**
     * Get the {@code col_category_id} value.
     * Can be {@code null}.
     */
    public Long getColCategoryId() {
        return getLongOrNull(MetadataColumns.COL_CATEGORY_ID);
    }

    /**
     * Get the {@code last_updated} value.
     * Can be {@code null}.
     */
    public Long getLastUpdated() {
        return getLongOrNull(MetadataColumns.LAST_UPDATED);
    }

    /**
     * Get the {@code last_cleared} value.
     * Can be {@code null}.
     */
    public Long getLastCleared() {
        return getLongOrNull(MetadataColumns.LAST_CLEARED);
    }

    /**
     * Get the {@code content_count} value.
     * Can be {@code null}.
     */
    public Integer getContentCount() {
        return getIntegerOrNull(MetadataColumns.CONTENT_COUNT);
    }
}
