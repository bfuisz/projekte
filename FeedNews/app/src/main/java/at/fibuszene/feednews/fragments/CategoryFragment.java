package at.fibuszene.feednews.fragments;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.activities.ArticleActivity;
import at.fibuszene.feednews.activities.HomeActivity;
import at.fibuszene.feednews.adapter.CategoryArticleAdapter;
import at.fibuszene.feednews.listener.EndlessScrollListener;
import at.fibuszene.feednews.loader.ArticleLoader;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.article.ArticleSelection;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.service.ExceptionHandler;
import at.fibuszene.feednews.service.LoadService;

/**
 * Created by benedikt.
 */
public class CategoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Articles>, AdapterView.OnItemClickListener {
    public static final String CATEGORY_ID_EXTRA = "at.fibuszene.feednews.category_id";
    public int ARTICLES_LOADER_ID = 989;
    private long categoryId;
    private ListView articleList;
    private CategoryArticleAdapter adapter;
    private ArticleSelection articleSelection;
    private SwipeRefreshLayout swipeRefresh;
    private ImageButton toTopButton;
    private float dragStartX;

    public static CategoryFragment getInstance(long category_id) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putLong(CATEGORY_ID_EXTRA, category_id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.articleSelection = new ArticleSelection();

        if (args != null) {
            this.categoryId = args.getLong(CATEGORY_ID_EXTRA, 0);
            ARTICLES_LOADER_ID += categoryId;

            if (categoryId == 0) { //frontpage

                Cursor cursor = getActivity().getContentResolver().query(CategoryColumns.CONTENT_URI,
                        new String[]{CategoryColumns.CATEGORY_ID}, CategoryColumns.FRONTPAGE + " = 1", null, null);

                if (cursor.moveToFirst()) {
                    Long[] catIds = new Long[cursor.getCount()];
                    int i = 0;
                    while (!cursor.isAfterLast()) {
                        catIds[i] = cursor.getLong(0);
                        i++;
                        cursor.moveToNext();
                    }
                    articleSelection.fkCategoryId(catIds);
                } else {
                    articleSelection.fkCategoryId(-1L);//officially gone insane
                }


            } else if (categoryId > 1) {//1 reserved for "all" fkcategoryid == null => all
                articleSelection.fkCategoryId(categoryId);
            }
            loadArticles(false);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.category_fragment_view, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.articleList = (ListView) view.findViewById(R.id.articleListView);
        ArticleCursor cursor = articleSelection.query(getActivity().getContentResolver());
        this.adapter = new CategoryArticleAdapter(getActivity(), Articles.fromCursor(cursor));
        this.articleList.setAdapter(adapter);
        this.articleList.setOnItemClickListener(this);
        articleList.setOnScrollListener(new EndlessScrollListener(getActivity()));

        toTopButton = (ImageButton) view.findViewById(R.id.toTopButton);
        toTopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                articleList.setSelectionAfterHeaderView();
            }
        });

        getActivity().getLoaderManager().restartLoader(ARTICLES_LOADER_ID, null, this);
        setUpSwipeRefresh(view);
        final Animation fade_out = AnimationUtils.makeOutAnimation(getActivity(), false);
        final Animation fade_in = AnimationUtils.makeInAnimation(getActivity(), false);

        articleList.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;
            boolean out = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mLastFirstVisibleItem < firstVisibleItem) {
                    if (!out) {
                        toTopButton.startAnimation(fade_out);
                        toTopButton.setVisibility(View.GONE);
                        out = true;
                        animateTabPanel(out);
                    }
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    if (out) {
                        toTopButton.startAnimation(fade_in);
                        toTopButton.setVisibility(View.VISIBLE);
                        out = false;
                        animateTabPanel(out);
                    }
                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        });
    }

    public void animateTabPanel(boolean fade_out) {
        if (getActivity() instanceof HomeActivity) {
            HomeActivity activity = (HomeActivity) getActivity();
            activity.animateTabStrip(fade_out);
        }
    }


    public void loadArticles(boolean userInit) {
        Intent intent = new Intent(getActivity(), LoadService.class);
        intent.setAction(LoadService.LOAD_ARTICLES);
        intent.putExtra(LoadService.LOAD_ARTICLES_ARG1, this.categoryId);
        intent.putExtra(LoadService.LOAD_ARTICLES_ARG2, userInit);
        intent.putExtra(LoadService.EXCEPTION_HANDLER, new Messenger(new ExceptionHandler(((HomeActivity) getActivity()))));
        getActivity().startService(intent);
    }

    public void setUpSwipeRefresh(View view) {
        this.swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeColors(R.color.blue, R.color.purple, R.color.green, R.color.orange);

        this.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                loadArticles(true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }, 3500);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Article item = this.adapter.getItem(position);
        item.setRead(true);
        ContentValues values = new ContentValues();
        values.put(ArticleColumns.READ, true);
        getActivity().getContentResolver().update(ArticleColumns.CONTENT_URI, values, ArticleColumns._ID + " = " + item.get_id(), null);

        Intent intent = new Intent(getActivity(), ArticleActivity.class);
        intent.putExtra(ArticleActivity.ARTICLE_URL_EXTRA, item.getUrl());
        startActivity(intent);
    }

    @Override
    public Loader<Articles> onCreateLoader(int id, Bundle args) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).loading();
        }
        return new ArticleLoader(getActivity(), articleSelection);
    }

    @Override
    public void onLoadFinished(Loader<Articles> loader, Articles data) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).doneLoading(false);
        }
        this.swipeRefresh.setRefreshing(false);
        this.adapter.setArticles(data);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Articles> loader) {
        if (isAdded()) {
            ((HomeActivity) getActivity()).doneLoading(false);
        }
        this.swipeRefresh.setRefreshing(false);
        this.adapter.setArticles(null);
        this.adapter.notifyDataSetChanged();
    }


}
