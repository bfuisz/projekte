package at.fibuszene.feednews.persistency.alert;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractSelection;

/**
 * Selection for the {@code alert} table.
 */
public class AlertSelection extends AbstractSelection<AlertSelection> {
    @Override
    public Uri uri() {
        return AlertColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code AlertCursor} object, which is positioned before the first entry, or null.
     */
    public AlertCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new AlertCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null}.
     */
    public AlertCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null}.
     */
    public AlertCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public AlertSelection id(long... value) {
        addEquals(AlertColumns._ID, toObjectArray(value));
        return this;
    }


    public AlertSelection categoryId(Long... value) {
        addEquals(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection categoryIdNot(Long... value) {
        addNotEquals(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection categoryIdGt(long value) {
        addGreaterThan(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection categoryIdGtEq(long value) {
        addGreaterThanOrEquals(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection categoryIdLt(long value) {
        addLessThan(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection categoryIdLtEq(long value) {
        addLessThanOrEquals(AlertColumns.CATEGORY_ID, value);
        return this;
    }

    public AlertSelection keyword(String... value) {
        addEquals(AlertColumns.KEYWORD, value);
        return this;
    }

    public AlertSelection keywordNot(String... value) {
        addNotEquals(AlertColumns.KEYWORD, value);
        return this;
    }

    public AlertSelection keywordLike(String... value) {
        addLike(AlertColumns.KEYWORD, value);
        return this;
    }

    public AlertSelection count(Long... value) {
        addEquals(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection countNot(Long... value) {
        addNotEquals(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection countGt(long value) {
        addGreaterThan(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection countGtEq(long value) {
        addGreaterThanOrEquals(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection countLt(long value) {
        addLessThan(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection countLtEq(long value) {
        addLessThanOrEquals(AlertColumns.COUNT, value);
        return this;
    }

    public AlertSelection seen(Long... value) {
        addEquals(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection seenNot(Long... value) {
        addNotEquals(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection seenGt(long value) {
        addGreaterThan(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection seenGtEq(long value) {
        addGreaterThanOrEquals(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection seenLt(long value) {
        addLessThan(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection seenLtEq(long value) {
        addLessThanOrEquals(AlertColumns.SEEN, value);
        return this;
    }

    public AlertSelection newAlertId(Long... value) {
        addEquals(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertSelection newAlertIdNot(Long... value) {
        addNotEquals(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertSelection newAlertIdGt(long value) {
        addGreaterThan(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertSelection newAlertIdGtEq(long value) {
        addGreaterThanOrEquals(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertSelection newAlertIdLt(long value) {
        addLessThan(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }

    public AlertSelection newAlertIdLtEq(long value) {
        addLessThanOrEquals(AlertColumns.NEW_ALERT_ID, value);
        return this;
    }
}
