package at.fibuszene.feednews.persistency.enclosures;

import android.content.ContentResolver;
import android.net.Uri;

import at.fibuszene.feednews.persistency.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code enclosures} table.
 */
public class EnclosuresContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return EnclosuresColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, EnclosuresSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public EnclosuresContentValues putArticleId(Long value) {
        mContentValues.put(EnclosuresColumns.ARTICLE_ID, value);
        return this;
    }

    public EnclosuresContentValues putArticleIdNull() {
        mContentValues.putNull(EnclosuresColumns.ARTICLE_ID);
        return this;
    }


    public EnclosuresContentValues putMediaType(String value) {
        mContentValues.put(EnclosuresColumns.MEDIA_TYPE, value);
        return this;
    }

    public EnclosuresContentValues putMediaTypeNull() {
        mContentValues.putNull(EnclosuresColumns.MEDIA_TYPE);
        return this;
    }


    public EnclosuresContentValues putUri(String value) {
        mContentValues.put(EnclosuresColumns.URI, value);
        return this;
    }

    public EnclosuresContentValues putUriNull() {
        mContentValues.putNull(EnclosuresColumns.URI);
        return this;
    }

}
