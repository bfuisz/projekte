package at.fibuszene.feednews.model;


import at.fibuszene.feednews.persistency.enclosure.EnclosureContentValues;
import at.fibuszene.feednews.persistency.enclosure.EnclosureCursor;

/**
 * Created by benedikt.
 */
public class Enclosure {
    private long id;
    private String media_type;
    private String uri;

    public Enclosure() {
    }

    public Enclosure(String media_type, String uri) {
        this.media_type = media_type;
        this.uri = uri;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public static Enclosure fromCursor(EnclosureCursor cursor) {
        Enclosure enc = new Enclosure();
        enc.id = cursor.getId();
        enc.media_type = cursor.getMediaType();
        enc.uri = cursor.getUri();
        return enc;
    }

    public EnclosureContentValues toContentValues() {
        EnclosureContentValues values = new EnclosureContentValues();

        values.putMediaType(this.media_type);
        values.putUri(this.uri);
        return values;
    }

    @Override
    public String toString() {
        return "Enclosure{" +
                "id=" + id +
                ", media_type='" + media_type + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}
