package at.fibuszene.feednews.persistency.newalert;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.base.AbstractCursor;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.enclosure.EnclosureColumns;

/**
 * Cursor wrapper for the {@code newalert} table.
 */
public class NewalertCursor extends AbstractCursor {
    public NewalertCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code alert_id} value.
     * Can be {@code null}.
     */
    public Long getAlertId() {
        return getLongOrNull(NewalertColumns.ALERT_ID);
    }

    /**
     * Get the {@code article_id} value.
     * Can be {@code null}.
     */
    public Long getArticleId() {
        return getLongOrNull(NewalertColumns.ARTICLE_ID);
    }

    /**
     * Get the {@code seen} value.
     * Can be {@code null}.
     */
    public Boolean getSeen() {
        return getBoolean(NewalertColumns.SEEN);
    }

    /**
     * Get the {@code url} value.
     * Can be {@code null}.
     */
    public String getUrl() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.URL);
        return getString(index);
    }

    /**
     * Get the {@code author} value.
     * Can be {@code null}.
     */
    public String getAuthor() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.AUTHOR);
        return getString(index);
    }

    /**
     * Get the {@code fk_category_id} value.
     * Can be {@code null}.
     */
    public Long getFkCategoryId() {
        return getLongOrNull(ArticleColumns.FK_CATEGORY_ID);
    }

    /**
     * Get the {@code title} value.
     * Can be {@code null}.
     */
    public String getTitle() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.TITLE);
        return getString(index);
    }

    /**
     * Get the {@code summary} value.
     * Can be {@code null}.
     */
    public String getSummary() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.SUMMARY);
        return getString(index);
    }

    /**
     * Get the {@code publish_date} value.
     * Can be {@code null}.
     */
    public Long getPublishDate() {
        return getLongOrNull(ArticleColumns.PUBLISH_DATE);
    }

    /**
     * Get the {@code source} value.
     * Can be {@code null}.
     */
    public String getSource() {
        Integer index = getCachedColumnIndexOrThrow(ArticleColumns.SOURCE);
        return getString(index);
    }

    /**
     * Get the {@code read} value.
     * Can be {@code null}.
     */
    public Boolean getRead() {
        return getBoolean(ArticleColumns.READ);
    }

    /**
     * Get the {@code saved} value.
     * Can be {@code null}.
     */
    public Boolean getSaved() {
        return getBoolean(ArticleColumns.SAVED);
    }

    /**
     * Get the {@code enclosure_id} value.
     * Can be {@code null}.
     */
    public Long getEnclosureId() {
        return getLongOrNull(ArticleColumns.ENCLOSURE_ID);
    }

    /**
     * Get the {@code category_id} value.
     * Can be {@code null}.
     */
    public Long getCategoryId() {
        return getLongOrNull(CategoryColumns.CATEGORY_ID);
    }

    /**
     * Get the {@code display_category_name} value.
     * Can be {@code null}.
     */
    public String getDisplayCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.DISPLAY_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code english_category_name} value.
     * Can be {@code null}.
     */
    public String getEnglishCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.ENGLISH_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code url_category_name} value.
     * Can be {@code null}.
     */
    public String getUrlCategoryName() {
        Integer index = getCachedColumnIndexOrThrow(CategoryColumns.URL_CATEGORY_NAME);
        return getString(index);
    }

    /**
     * Get the {@code user_favorite} value.
     * Can be {@code null}.
     */
    public Boolean getUserFavorite() {
        return getBoolean(CategoryColumns.USER_FAVORITE);
    }

    /**
     * Get the {@code order_in_home} value.
     * Can be {@code null}.
     */
    public Integer getOrderInHome() {
        return getIntegerOrNull(CategoryColumns.ORDER_IN_HOME);
    }

    /**
     * Get the {@code color} value.
     * Can be {@code null}.
     */
    public Integer getColor() {
        return getIntegerOrNull(CategoryColumns.COLOR);
    }

    /**
     * Get the {@code frontpage} value.
     * Can be {@code null}.
     */
    public Boolean getFrontpage() {
        return getBoolean(CategoryColumns.FRONTPAGE);
    }

    /**
     * Get the {@code hidden} value.
     * Can be {@code null}.
     */
    public Boolean getHidden() {
        return getBoolean(CategoryColumns.HIDDEN);
    }

    /**
     * Get the {@code media_type} value.
     * Can be {@code null}.
     */
    public String getMediaType() {
        Integer index = getCachedColumnIndexOrThrow(EnclosureColumns.MEDIA_TYPE);
        return getString(index);
    }

    /**
     * Get the {@code uri} value.
     * Can be {@code null}.
     */
    public String getUri() {
        Integer index = getCachedColumnIndexOrThrow(EnclosureColumns.URI);
        return getString(index);
    }
}
