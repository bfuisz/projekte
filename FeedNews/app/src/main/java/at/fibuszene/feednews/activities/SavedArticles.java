package at.fibuszene.feednews.activities;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import at.fibuszene.feednews.R;
import at.fibuszene.feednews.adapter.CategoryArticleAdapter;
import at.fibuszene.feednews.loader.ArticleLoader;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.article.ArticleSelection;

public class SavedArticles extends AbstractActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Articles> {
    private ListView listView;
    private CategoryArticleAdapter adapter;
    public static int SAVED_ARTICLES_LOADER_ID = 102;
    private ArticleSelection selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_saved_articles);
        listView = (ListView) findViewById(R.id.savedArticlesList);
        selection = new ArticleSelection();
        selection.saved(true);
        adapter = new CategoryArticleAdapter(this, Articles.fromCursor(this.selection.query(this.getContentResolver())));
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        getLoaderManager().restartLoader(SAVED_ARTICLES_LOADER_ID, null, this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (super.mDrawerLayout.isDrawerOpen(mDrawerList)) {
            super.onItemClick(parent, view, position, id);

        } else {
            Intent intent = new Intent(this, ArticleActivity.class);
            intent.putExtra(ArticleActivity.ARTICLE_URL_EXTRA, this.adapter.getItem(position).getUrl());
            startActivity(intent);
        }
    }

    @Override
    public Loader<Articles> onCreateLoader(int id, Bundle args) {
        return new ArticleLoader(this, this.selection);
    }

    @Override
    public void onLoadFinished(Loader<Articles> loader, Articles data) {
        this.adapter.setArticles(data);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Articles> loader) {
        this.adapter.setArticles(null);
        this.adapter.notifyDataSetChanged();
    }


}
