package at.fibuszene.feednews.utils;

import java.util.Comparator;

import at.fibuszene.feednews.model.Category;

/**
 * Created by benedikt.
 */
public class Comparables {

    public static class AlphaComp implements Comparator<Category> {
        @Override
        public int compare(Category lhs, Category rhs) {
            return lhs.getEnglish_category_name().compareTo(rhs.getEnglish_category_name());
        }

    }


}
