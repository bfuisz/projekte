package at.fibuszene.feednews.alertsync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by google copied by benedikt :D.
 * The Authenticator Service for the AlertSyncAdapter
 */
public class AuthenticatorService extends Service {
    private Authenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new Authenticator(this);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}