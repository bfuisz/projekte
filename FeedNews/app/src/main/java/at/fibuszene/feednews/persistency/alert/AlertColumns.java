package at.fibuszene.feednews.persistency.alert;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.Set;

import at.fibuszene.feednews.persistency.FeedZillaProvider;

/**
 * Columns for the {@code alert} table.
 */
public class AlertColumns implements BaseColumns {
    public static final String TABLE_NAME = "alert";
    public static final Uri CONTENT_URI = Uri.parse(FeedZillaProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    public static final String _ID = BaseColumns._ID;
    public static final String CATEGORY_ID = "category_id";
    public static final String KEYWORD = "keyword";
    public static final String COUNT = "count";
    public static final String SEEN = "seen";
    public static final String NEW_ALERT_ID = "new_alert_id";

    public static final String DEFAULT_ORDER = TABLE_NAME + "." + _ID;

    // @formatter:off
    public static final String[] FULL_PROJECTION = new String[]{
            TABLE_NAME + "." + _ID + " AS " + BaseColumns._ID,
            TABLE_NAME + "." + CATEGORY_ID,
            TABLE_NAME + "." + KEYWORD,
            TABLE_NAME + "." + COUNT,
            TABLE_NAME + "." + SEEN,
            TABLE_NAME + "." + NEW_ALERT_ID,
            "*"
    };
    // @formatter:on

    private static final Set<String> ALL_COLUMNS = new HashSet<String>();

    static {
        ALL_COLUMNS.add(_ID);
        ALL_COLUMNS.add(CATEGORY_ID);
        ALL_COLUMNS.add(KEYWORD);
        ALL_COLUMNS.add(COUNT);
        ALL_COLUMNS.add(SEEN);
        ALL_COLUMNS.add(NEW_ALERT_ID);
    }

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (ALL_COLUMNS.contains(c)) return true;
        }
        return false;
    }
}
