package at.fibuszene.feednews.persistency.enclosure;

import android.database.Cursor;

import at.fibuszene.feednews.persistency.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code enclosure} table.
 */
public class EnclosureCursor extends AbstractCursor {
    public EnclosureCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code media_type} value.
     * Can be {@code null}.
     */
    public String getMediaType() {
        Integer index = getCachedColumnIndexOrThrow(EnclosureColumns.MEDIA_TYPE);
        return getString(index);
    }

    /**
     * Get the {@code uri} value.
     * Can be {@code null}.
     */
    public String getUri() {
        Integer index = getCachedColumnIndexOrThrow(EnclosureColumns.URI);
        return getString(index);
    }
}
