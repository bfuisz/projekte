package at.fibuszene.feednews.alertsync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import at.fibuszene.feednews.service.AlertService;
import at.fibuszene.feednews.service.LoadService;

/**
 * Created by benedikt.
 */
public class AlertSyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String AUTHORITY = "at.fibuszene.feednews.provider";

    private ContentResolver provider;


    public AlertSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.provider = context.getContentResolver();
        Log.d("LoadService", "INSTATIATED");
    }

    public AlertSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.provider = context.getContentResolver();
        Log.d("LoadService", "INSTATIATED");

    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Intent loadIntent = new Intent(getContext(), LoadService.class);
        loadIntent.setAction(LoadService.LOAD_CATEGORIES);
        Log.d("LoadService", "LOAD_CATEGORIES");
        getContext().startService(loadIntent);

        loadIntent.setAction(LoadService.LOAD_ARTICLES);//load all articles
        loadIntent.putExtra(LoadService.LOAD_ARTICLES_ARG2, false);//but with respect to the interval(hourly hardcoded :D)
        Log.d("LoadService", "LOAD_ARTICLES");
        getContext().startService(loadIntent);

        //everything before is just a sideshow
        //this is the point of this syncadapter
        //check the alerts hourly or so

        loadIntent = new Intent(getContext(), AlertService.class);
        Log.d("LoadService", "UPDATING ALERTS");
        getContext().startService(loadIntent);
    }

}
