package at.fibuszene.feednews.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import at.fibuszene.feednews.model.lists.Categories;
import at.fibuszene.feednews.persistency.category.CategoryColumns;
import at.fibuszene.feednews.persistency.category.CategoryCursor;
import at.fibuszene.feednews.persistency.category.CategorySelection;

/**
 * Created by benedikt.
 */
public class CategoryLoader extends AsyncTaskLoader<Categories> {
    private Categories categories;
    private CategoryCursor cursor;
    private ForceLoadContentObserver contentObserver = new ForceLoadContentObserver();
    private CategorySelection selection;

    public CategoryLoader(Context context, CategorySelection selection) {
        super(context);
        this.selection = selection;
    }

    @Override
    public Categories loadInBackground() {
        cursor = selection.query(getContext().getContentResolver());
        cursor.setNotificationUri(getContext().getContentResolver(), CategoryColumns.CONTENT_URI);
        getContext().getContentResolver().registerContentObserver(CategoryColumns.CONTENT_URI, true, contentObserver);
        this.categories = Categories.fromCursor(cursor);
        return categories;
    }

    @Override
    public void deliverResult(Categories categories) {
        if (isReset()) {
            if (categories != null) {
                onReleaseResources(categories);
            }
        }
        Categories oldCategories = categories;
        this.categories = categories;

        if (isStarted()) {
            super.deliverResult(categories);
        }
        if (oldCategories != null) {
            onReleaseResources(oldCategories);
        }
    }

    @Override
    protected void onStartLoading() {
        if (categories != null) {
            deliverResult(categories);
        }
        if (takeContentChanged() || categories == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(Categories categories) {
        super.onCanceled(categories);
        onReleaseResources(categories);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (categories != null) {
            onReleaseResources(categories);
            categories = null;
        }
        getContext().getContentResolver().unregisterContentObserver(contentObserver);
    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(Categories categories) {
        categories = null;
    }


}
