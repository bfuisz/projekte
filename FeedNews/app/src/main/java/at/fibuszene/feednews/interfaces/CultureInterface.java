package at.fibuszene.feednews.interfaces;

import at.fibuszene.feednews.model.lists.Cultures;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by benedikt.
 */
public interface CultureInterface {

    @GET("/v1/cultures.json")
    public void listCultures(Callback<Cultures> response);
}
