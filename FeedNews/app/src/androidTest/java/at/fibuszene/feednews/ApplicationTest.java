package at.fibuszene.feednews;

import android.app.Application;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.test.ApplicationTestCase;
import android.util.Log;

import at.fibuszene.feednews.interfaces.ArticlesInterface;
import at.fibuszene.feednews.model.Article;
import at.fibuszene.feednews.model.lists.Articles;
import at.fibuszene.feednews.persistency.DatabaseHelper;
import at.fibuszene.feednews.persistency.article.ArticleColumns;
import at.fibuszene.feednews.persistency.article.ArticleCursor;
import at.fibuszene.feednews.persistency.article.ArticleSelection;
import at.fibuszene.feednews.utils.Constants;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    private String rawTestQuery = "INSERT INTO article(summary,author,title,source,publish_date,enclosure_id,fk_category_id,saved,read,url) VALUES ('What happens when your big idea, your plan for a grand adventure, is met with a, “Meh,” attitude by your children. Or worse, what do you do when they absolutely declare that they won’t go, that you’re ruining their lives and  that they’d rather live with Grandma instead? Jenn Miller unpacks her toolkit for helping kids, from littles to teens, get on board and contribute to the family dream!','admin','Getting Kids On Board With Your Big Trip (Boots N All)','Boots N All',1416398760000,NULL,0,0,0,'http://news.feedzilla.com/en_us/stories/408800100?count=25&client_source=api&order=date&format=json')";

    public void testQuery() {
        Article art = new Article();
        art.setTitle("Getting Kids On Board With Your Big Trip (Boots N All)");
        art.setSummary("What happens when your big idea, your plan for a grand adventure, is met with a, “Meh,”");
        art.setPublish_date("1416398760000");
        art.setEnclosures(null);
        art.setAuthor("admin");
        art.setSource("Boots N All");
        art.setCategoryId(0);
        art.setUrl("http://news.feedzilla.com/en_us/stories/408800100?count=25&client_source=api&order=date&format=json");

        DatabaseHelper.getInstance(getContext()).getWritableDatabase().rawQuery(rawTestQuery, null); //ArticleColumns.TABLE_NAME, null, art.toContentValue().values());
//        art.toContentValue().insert(getContext().getContentResolver());

    }
}