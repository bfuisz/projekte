# README #

FeedNews ist eine Client App f�r die [FeedZilla API](https://code.google.com/p/feedzilla-api/wiki/RestApi) .
Prim�res Lernziel bei der Entwicklung der App war es eine Client Server Anwendung mit 
Retrofit zu entwickeln. Au�erdem wollte ich das [android-contentprovider-generator](https://github.com/BoD/android-contentprovider-generator)
tool ausprobieren. 

# App #

## Funktionen ##

Mit der App k�nnen Neuigkeiten des Newsfeedproviders [Feedzilla](http://www.feedzilla.com/about) 
angezeigt werden.
Die Newsfeeds sind in Kategorien aufgeteilt und der User kann entwscheiden, welche Feeds angezeigt werden. 
Au�erdem, kann ein "Newsalert" eingerichtet werden, der eine Notification anzeigt, falls Artikel mit einem 
Schl�sselwort gefunden werden. 
Neben dem Newsalert k�nnen Artikel auch f�r sp�ter gespeichert werden und eine personalisierte Frontpage 
erstellt werden. 

## Aufbau ##

### activities ###

Die Erkl�rung einiger Ausgew�hlter Activites. 
  
+ HomeActivity
	landing page der App. Baut einen ViewPager aus den, vom User gew�hlten Kategorien auf und st��t einen Service
	an, der beginnt Artikel zu laden. 
		
+ ArticleActivity
	stellt einen Artikel dar. FeedZilla bietet neben einem Abstrakt und anderen Infos zum Artikel,
	nur den Link zur Seite die den Artikel ver�ffentlich hat. Die ArticelAktivity stellt eine
	WebView dar, welche die Seite des Artikel l�dt. Der User hat dann die M�glichkeit den Artikel 
	entweder in der App zu lesen oder im StandartBrowser des Handys. Au�erdem kann der Link des Artikels
	geteilt werden. Der Artikel kann auch mit der "Spritz Reading" Technik gelesen werden indem der User
	Text aus dem Artikel in die Zwischenablage kopiert. 

+ CatergoriesSettings
	dienen dazu dem User Newsfeeds �bersichtlicher zu pr�sentieren. Der User kann Kategorien verstecken, 
	umsortieren, einen Shortcut ins Drawermenu erstellen und Artikel dieser Kategorie in der Frontpage 
	anzeigen lassen. Die App stellt au�erdem eine Farbcodierung f�r Kategorien zur Verf�gung die in 
	CatergoriesSettings ge�ndert werden kann. 
		
### service ###

Die App verwendet mehrere Services: 
	
+ LoadService
	l�dt Kategorien und Artikel von FeedZilla API
		
+ SyncService
	verwendet AlertSyncAdapter um in vom User bestimmten Intervallen neue Artikel zu laden. 
		
+ CleanUpService
	l�uft periodisch und l�scht alte Artikel. 
		
+ AlertService
	l�uft periodisch und sucht Artikel mit vom User bestimmten Schl�sselw�rtern. 
		
	
### persistency ###
	
Wie eingangs erw�hnt, wurden der ContentProvider mit dem [android-contentprovider-generator](https://github.com/BoD/android-contentprovider-generator)
tool erstellt. 

Die wichtigsten Tabellen sind:  

+ article
	speichert einen Artikel plus ein "read" flag und ein "saved" flag. 
	Gelesene "read" Artikel werden mit durchgestrichenem Titel angezeigt und "saved" Artikel werden 
	in der SavedArticles Activity angezeigt. 
	
+ category
	speichert neben der Kategorie selbst, flags f�r Frontpage, hidden und Shortcut au�erdem die Farbe der Kategorie. 
		
+ metadata
	speichert die Kategorie ID und zum Beispiel wann die Kategorie zum letzten Mal upgedatet wurde. 
		
+ newalert
	speichert eine Artikel ID zu einem Artikel, welcher ein vom User bestimmtes Schl�sselwort enth�lt. 
		
		
## TODOs ##

 + Testen
 + Refactoring 
 + Paging
	leider nicht wirklich m�glich von Seiten der Api 
	
## Lerneffekt ##

Nie 0 als polltime f�r PeriodicSync setzen !   
( ContentResolver.addPeriodicSync(Authenticator.getAccount(), AlertSyncAdapter.AUTHORITY, Bundle.EMPTY, PrefUtils.alertSyncFrequency(this) * 60); )
Beim einem meiner Testger�te (HTC Desire 500 mit Android 4.1.2) hat das anstatt zu einer 
IllegalArgumentsException zu einer "Division durch 0" gef�hrt und konnte nur durch Factory Reset des Ger�tes
gel�st werden. 

Umgang mit Contentprovider Generator, Syncadaptern und Loadern.  